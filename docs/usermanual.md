# Principes

Les données exposées sont une compilation des données de navigocorpus limitées
- au registre de la santé de Marseille pour les années 1749, 1759, 1769, 1779, 1787, 1789, 1799 
- aux congés fiscaux enregistrés par l'Amirauté (série G5), années 1787 et 1789
- aux cahiers du petit cabotage sur Marseille années 1786-1787
- aux expéditions coloniales depuis Marseille en 1789. 
Elles sont récupérées depuis la [base navigocorpus](http://navigocorpus.org/) (logiciel Filemaker) en ligne et téléchargées dans postgres avec le code d'un [ETL disponible dans le gitlab d'humanum](https://gitlab.huma-num.fr/portic/navigocorpus/-/tree/master/ETL). 

URL de l'API : **http://data.portic.fr/api/**

Une requête particulière : *fieldnames* récupère des métadonnées sur l'API, avec la liste des attributs, avec leurs noms court et long, leurs types et leurs significations. On passe en paramètre le nom de l'api (optionnel) dont on veut connaître les attributs :  

- sans préciser l'api : on récupère les attributs des api 'pointcalls' et 'travels';
- api existantes : **ports**, **pointcalls**, **rawflows**, **travels**, **sources**

Trois grands types de requêtes :

- **ports** : les lieux d'escale des navires, que ce soit des ports, ou des zones en mer (échouage, naufrage, capture), repérés par leurs coordonnées géographique et un identifiant UHGS_id unique qui fait jointure avec les autres données.
- **pointcalls** : les données d'observation à chaque escale des navires
- **rawflows** : les données de trajectoire brutes produites à partir de l'ordonnancement des pointcalls d'un même document (source_doc_id). 
- **travels** : les données de trajectoire calculées, reconstruites via l'identification d'un même navire (ship_id) dans plusieurs documents sources (source_doc_id)
- **sources** : liste par port des sources disponibles et saisies années par années, et de leur taux de renseignement (brut ou déduit) pour différentes variables (port d'attache, tonnage, etc.)

Liste des paramètres communs à chaque requête et valeurs par défaut en gras:

- params : **all** | tableau des noms longs des attributs de l'API à renvoyer
- format : csv | **json** | geojson
- shortenfields : true | **false**
- both_to : true | **false**
- date : YYYY | **1787**
- zipped : true | **false**


Si le parametre n'est pas précisé, le serveur opte pour la valeur de paramètre par défaut (en gras).

**params** permet de filtrer la liste des attributs récupérés.  

Par exemple :

<http://data.portic.fr/api/travels/?params=id,departure,destination>

Renvoie :

```json
[
    {
        "id":"0000001N- 01",
        "departure":"Boulogne sur Mer",
        "destination":"Angleterre",
        "destination_uhgs_id":"A0390929"
    },
    {
        "id":"0000001N- 02",
        "departure":"Angleterre",
        "destination":"Boulogne sur Mer",
        "destination_uhgs_id":"A0152606"
    }, 
    ..., 
    {
        "id":"0021518N- 01",
        "departure":"Honfleur",
        "destination":"Havre du Four en Terreneuve",
        "destination_uhgs_id":"B2119892"
    }
] 
```

**format** permet de télécharger soit au format CSV soit au format JSON les données. 

Par exemple :  

- <http://data.portic.fr/api/fieldnames/?format=csv>
- <http://data.portic.fr/api/travels/?format=csv>
- <http://data.portic.fr/api/pointcalls/?format=csv>

Pour avoir les mêmes données en JSON : 

- <http://data.portic.fr/api/fieldnames/?format=json>
- <http://data.portic.fr/api/travels/?format=json>
- <http://data.portic.fr/api/pointcalls/?format=json>

Le format GEOJSON est pour l'instant réservé à l'API ports et il renvoie des coordonnées géographiques (WGS84, EPSG 4326) quelque soit le SRID spécifié
- http://data.portic.fr/api/ports?format=geojson

**shortenfields** permet de raccourcir les noms des attributs et donc d'alléger la taille du JSON téléchargé. L'API permet de récupérer le mapping entre ces noms courts et longs avec :

<http://data.portic.fr/api/fieldnames/?format=json>  

Exemple :  
<http://data.portic.fr/api/pointcalls/?format=json&shortenfields=true>

**both_to** est particulier : la reconstruction des données de trajectoire s'appuie sur la lecture des observations de départ (G5) et/ou d'arrivée (Marseille). Il se peut qu'un bateau observé dans le G5 au départ de bateau soit vu à l'arrivée de Marseille. Dans ce cas, il peut porter des informations ressemblantes mais légèrement différentes quant à son tonnage, son capitaine, l'orthographe de son nom, etc.
Dans la requête sur travels, par défaut, nous proposons les informations de description du bateau telles que données au départ pour éviter les duplicats d'information. Mais si souhaité, avec l'attribut *both_to*, l'utilisateur peut récupérer les données descriptives aussi au point d'arrivée, et il peut filtrer l'information sur l'attribut **source_entry** qui vaut : *from, to, both-from, both-to*

Voir la documentation de l'API pour plus de détails.

Exemple :  
<http://data.portic.fr/api/travels/?format=json&params=id,departure,destination&both_to=true>

**date** : pour filtrer les données sur une année donnée. L'année de la date d'arrivée ou de la date de départ doit commencer par ces 4 digits : 1787 ou  1789 par exemple.  

Exemple :  
<http://data.portic.fr/api/pointcalls/?format=json&date=1789>

**zipped** : pas encore implémenté


# Requêtes possibles

## /fieldnames?

Métadonnées sur l'API souhaitée : liste des attributs, avec leurs noms court et long, leurs types et leurs significations.

Paramètres :

- API = travels | rawflows | pointcalls | ports | **any**


## /pointcalls?

Returns all the pointcalls as specified in API : "les données d'observation à chaque escale des navires".

Will be extracted from postgres, schema navigoviz, table pointcall (see navigocorpus/ETL)

Parameters :

- params : **all** | tableau des noms longs des attributs de l'API à renvoyer
- date : 4 digits representing a year, to extract data for this given year only. **Defaults to no filtering on date.**

Examples :

    http://data.portic.fr/api/pointcalls/?params=pointcall,pointcall_uhgs_id&shortenfields=true
    http://data.portic.fr/api/pointcalls/?format=csv
    http://data.portic.fr/api/pointcalls/?format=json
    http://data.portic.fr/api/pointcalls/?params=pointcall,pointcall_uhgs_id&date=1787&format=csv
    http://data.portic.fr/api/pointcalls/?params=pointcall,pointcall_uhgs_id&shortenfields=false
    http://data.portic.fr/api/pointcalls/?format=json&params=id,pointcall,ship_name,destination,destination_uhgs_id&shortenfields=true


## /travels?

Return the travels as specified in API : "les données de trajectoire calculées".

Important : *travels*  repose  sur l'identification des navires ou des capitaines. Cette requête récupére tous les tronçons de trajectoires pour un même navire à travers plusieurs documents, et donc retourne une vision moins complète des données que *rawflows*, car les documents n'ont pas encore tous été traités pour ce qui concerne l'identification du navire (ship_id) ou du capitaine (captain_id). En revanche, elle ne comporte pas de doubles comptes (des navires identiques notés dans deux sources différentes).


Will be extracted from postgres, schema navigoviz, table built_travels (see navigocorpus/ETL), 
but with a filter by default, to avoid duplicates :

`source_entry = from and both-from`

- params : **all** | tableau des noms longs des attributs de l'API à renvoyer  

    Exemple :

    `params=pointcall,pointcall_uhgs_id`

- both_to : true | **false** 
 
    Exemples :  

        http://127.0.0.1:5004/api/travels/?format=csv&both_to=false
        http://127.0.0.1:80/api/travels/?format=csv&both_to=true&shortenfields=true&date=1789
        http://127.0.0.1:80/api/travels/?format=json&params=id,departure,destination,destination_uhgs_id
        http://127.0.0.1:80/api/travels/?format=json&params=id,departure,destination,destination_uhgs_id&shortenfields=true

- date : par défaut, pas de filtre sur les dates

- ship_id : par défaut, pas de filtre sur les identifiants de navire. 
    
    Exemple :

    `ship_id=0004171N`

- captain_id : par défaut, pas de filtre sur les identifiants de capitaines. 
    
    Exemple :

    `captain_id=00011085`

## /rawflows?

Returns the raw flows as specified in API (same as travel for the attributes).

Raw_flows is built by making an auto-join on pointcall for source_doc_id, 
and departure are "Out" action, destination are "In" action, ordered chronologically.  
Thus we do not take care of net_route_marker (A or Z), neither of ship_id to built travels.  
Data about the cargo, the captain, the homeport are those filled at the departure.

Important : *rawflows* ne repose pas sur l'identification des navires ou des capitaines. Cette requête récupére tous les tronçons de trajectoires annoncés dans un même document, et donc retourne une vision plus complète des données que travels. En revanche, elle peut comporter des doubles comptes (des navires identiques notés dans deux sources différentes).

Will be extracted from postgres, schema navigoviz, table raw_flows (see navigocorpus/ETL).

Parameters :

- params : **all** | tableau des noms longs des attributs de l'API à renvoyer  

    Exemple : 

    `params=pointcall,pointcall_uhgs_id`

Exemples :

    http://127.0.0.1:5004/api/rawflows/?format=csv
    http://127.0.0.1:80/api/rawflows/?format=csv&shortenfields=true&date=1789        
    http://127.0.0.1:80/api/rawflows/?format=json&params=id,departure,destination,destination_uhgs_id
    http://127.0.0.1:80/api/rawflows/?format=json&params=id,departure,destination,destination_uhgs_id&shortenfields=true


## /details/departures?

parameters :

- lon
- lat
- radius

Returns the travels, at the departure of the points located in a 100 km radius neighbourhood from the lat/lon given in parameter.

Will be extracted from postgres, schema navigoviz, table built_travels (see navigocorpus/ETL), but with a filter by default, only to avoid duplicates :

`source_entry = from and both-from`

Examples :

    http://localhost:80/api/details/departures?lat=46&lon=-1&radius=100
    http://localhost/api/details/departures/?lat=45.2333&lon=-1.5&radius=100
    http://localhost/api/details/departures/?lat=45.2333&lon=toto&radius=100

Par défaut, pas de filtre sur les dates

## /agg/departures?

parameters :

- lon
- lat
- radius

Return the count of departures, for the points located in a radius km neighbourhood from the lat/lon given in parameter.

Will be extracted from postgres, schema navigoviz, table built_travels (see navigocorpus/ETL), but with a filter by default : only source_entry = from and both-from, to avoid duplicates.

Les travels seulement au départ dans un rayon de radium km autour du point long/lat. 
Les calculs spatiaux sont déportés dans le SGBD Postgres pour bénéficier des index spatiaux.

Exemple :

`http://data.portic.fr/api/agg/departures/?lat=46&lon=-1&radius=20`

Renvoie :

```JS
[
    {
        "departure":"Fouras",
        "count":2
    },
    {
        "departure":"Martroux",
        "count":6
    },
    {
        "departure":"Port des Barques",
        "count":1
    },
    {
        "departure":"Rochefort",
        "count":562
    },
    {
        "departure":"Soubise",
        "count":44
    }
]
```

Par défaut, pas de filtre sur les dates


## /agg/destinations?

parameters :

- lon
- lat
- radius

Returns the count of destination for each different admiralties, for the points located in a radius km  neighbourhood from the lat/lon given in parameter.

Will be extracted from postgres, schema navigoviz, table built_travels (see navigocorpus/ETL), but with a filter by default : only source_entry = from and both-from, to avoid duplicates

    http://data.portic.fr/api/agg/destinations/?lat=45.2333&lon=-1&radius=50

Les calculs spatiaux sont déportés dans le SGBD Postgres pour bénéficier des index spatiaux.
```JS
[
    {"label":"Bayonne","value":1,"id":"Bayonne"},
    {"label":"Bordeaux","value":27,"id":"Bordeaux"},
    {"label":"Brest","value":43,"id":"Brest"},
    {"label":"Cherbourg","value":10,"id":"Cherbourg"},
    {"label":"Dunkerque","value":1,"id":"Dunkerque"},
    {"label":"Granville","value":1,"id":"Granville"},
    {"label":"Isigny","value":1,"id":"Isigny"},
    {"label":"La Hougue","value":1,"id":"La Hougue"},
    {"label":"La Rochelle","value":3,"id":"La Rochelle"},
    {"label":"Marennes","value":4,"id":"Marennes"},
    {"label":"Morlaix","value":29,"id":"Morlaix"},
    {"label":"Quimper","value":8,"id":"Quimper"},
    {"label":"Rouen","value":2,"id":"Rouen"},
    {"label":"Saint-Brieuc","value":1,"id":"Saint-Brieuc"},
    {"label":"Saint-Malo","value":12,"id":"Saint-Malo"},
    {"label":"Vannes","value":11,"id":"Vannes"},
    {"label":null,"value":1,"id":null}
]
```
Par défaut, pas de filtre sur les dates


## /flows?

Les données de trajectoire filtrées suivant les ports touchés, la direction (entrante, sortante, around), et le degré d'éloignement des escales dans l'ordre.
Non maintenu depuis 2020.

Paramètres  :

- ports : identifiants des ports (UHGS_id) dans la base de données, sur lesquels on filtre les données, séparés par des virgules (,) 
- direction : In | Out | In-out | **No**
- degree : **0**, 1 ou plus  
    (si 0 : tous les flux passant par le port précisé, avec la liste des escales précédentes et suivantes)
- date : par défaut, filtrage sur la date à 1787.
    
Tous les flux passant par ces ports :

- au degré spécifié :  
  - 0 : toutes les escales sur les trajets passant par ces ports,
  - 1 seulement les ports précédents et/ou suivants sur les trajets,
  - 2 : jusqu'à 2 escales avant ou après les ports, etc. 
- et dans la direction spécifiée :
  - In : les arrivées et les escales précédentes,
  - Out : les départs et les escales suivantes,
  - In-out : uniquement les trajectoires ayant navigué en aller-retour autour des mêmes ports,
  - No : toutes directions confondues


Return the flows as specified in API : a list of travels linked to the specified ports, either by :

- entering in (direction In),
- exiting from (direction Out), 
- having sailing around (direction In-out),
- or for any direction (default, or specify direction No).

Will be extracted from postgres, schema navigoviz, table built_travels (see navigocorpus/ETL), but with a filter by default : only source_entry = from and both-from, to avoid duplicates.

Parameters:

- ports : ports identifiers (UHGS_id) for which travels are filtered, comma separated(,) 
- direction : In | Out | In-out | **No**
- degree : **0**, 1 or more  
    (when 0 : all flows going through out the list of cited ports, with also the list of previous and next pointcalls)
- date : **1787** | yyyy (4 digits) (by default, only data of 1787 year are extracted)
- params : **all** | tableau des noms longs des attributs de l'API à renvoyer  
  Exemple :  
  `params=pointcall,pointcall_uhgs_id`
- both_to : true | **false**
- date : **1787** | YYYY  

Examples :

    http://data.portic.fr:80/api/flows/?format=csv&both_to=false : VERY LONG, AVOID. Use travels instead
    http://data.portic.fr:80/api/flows/?format=csv&both_to=true&shortenfields=true&ports=A0180923

- Incomings into Boulogne sur Mer (A0152606) and Bordeaux (A0180923) 
   
```
http://data.portic.fr:80/api/flows/?format=json&ports=A0180923,A0152606&direction=In&params=travel_rank,ship_id,departure,destination,departure_action,destination_action,distance_dep_dest,travel_uncertainity
```

- Exits Boulogne sur Mer (A0152606) and Bordeaux (A0180923)
  
```
http://data.portic.fr:80/api/flows/?format=json&ports=A0180923,A0152606&direction=Out&params=travel_rank,ship_id,departure,destination,departure_action,destination_action,distance_dep_dest,travel_uncertainity
```

- Sailing around Bordeaux (A0180923)  

```
http://data.portic.fr/api/flows/?format=json&ports=A0180923&direction=In-out&params=id,departure,destination&shortenfields=true  
```

```JSON
[
    {"t01":"0008663N- 05","t04":"Bordeaux","t19":"Bordeaux"},
    {"t01":"0009557N- 01","t04":"Bordeaux","t19":"Bordeaux"},
    {"t01":"0010656N- 01","t04":"Bordeaux","t19":"Bordeaux"}
]
```

- All passing by Bordeaux (A0180923) and Boulogne sur Mer (A0152606)  

```
http://data.portic.fr:80/api/flows/?format=json&ports=A0180923,A0152606&direction=No&params=travel_rank,ship_id,departure,destination,departure_action,destination_action,distance_dep_dest,travel_uncertainity
```

- All incomings into Boulogne sur Mer (A0152606) without previous steps  

```
http://data.portic.fr:80/api/flows/?format=json&ports=A0152606&degree=0&direction=In&params=travel_rank,ship_id,departure,destination,departure_action,destination_action,distance_dep_dest,travel_uncertainity
```

- All outgoings from Bordeaux (A0180923) and Boulogne sur Mer (A0152606) with the next step  

```
http://data.portic.fr:80/api/flows/?format=json&ports=A0180923,A0152606&degree=1&direction=Out&params=travel_rank,ship_id,departure,destination,departure_action,destination_action,distance_dep_dest,travel_uncertainity
```

- All ships having sailing around Bordeaux (A0180923) with the two next steps  

```
http://data.portic.fr:80/api/flows/?format=json&ports=A0180923&degree=2&direction=In-out&params=travel_rank,ship_id,departure,destination,departure_action,destination_action,distance_dep_dest,travel_uncertainity
```


## /ports?

Export list of ports_points (in 900013 projection or what is specified by user) in json format, with all required attributes for visualisations (selection of parameters is not possible for the moment).

List of attributes :

- ogc_fid,
- uhgs_id,
- total,
- toponym,
- toponyme_standard_fr, 
- toponyme_standard_en , 
- substate_1789_fr , 
- state_1789_fr, 
- substate_1789_en , 
- state_1789_en,
- belonging_states,
- belonging_substates,
- status,
- geonameid,
- admiralty,
- province,
- shiparea,
- point,
- ferme_direction, 
- ferme_bureau, 
- partner_balance_1789, 
- partner_balance_supp_1789 

User can get a description of the attributes by using :

`/api/fieldnames?api=ports`

Default srid is 900913  
You get another by specifying a srid param. Except if you request format=geojson (4326 will be sent back then)

Will be extracted from postgres, schema ports, table port_points (see navigocorpus/ETL).

Tested alone in a browser:  

    http://data.portic.fr/api/ports?srid=4326 
    http://data.portic.fr/api/ports?
    http://data.portic.fr/api/ports?format=geojson

and by using [explorex.portic.fr](http://explorex.portic.fr) application (code alphaportic for visualisation) as client : it works


## /sources?

Get statistics about the amount of pointcall data found in sources (couverture des sources de navigo), related to ports of France only. Only observed ("O") pointcalls are taken in account.

Port data is grouped by : source, port, and year. 

This means that for a given source (source_suite = G5 | ...), the API lists the ports (identified by a : toponym, ogc_fid or uhgs_id), and for each port, there is a data line for each year (pointcall_year = 1787 | 1789 | ...). So there may be more than one record for each port ! You can filter or sum yourself port data received, by source, and / or by year, to build your own map for instance.

Data lines returned are :

- if **pointcall data is present** in the database for a port : **nb_conges_inputdone** is given, and all fields available (listed below) are relevant
- if **pointcall data is known** (from archives), but not present in the database for a port : nb_conges_inputdone is null, but **nb_conges_cr** is given. source_suite is 'G5', point_call_year is relevant but all 'computed fields' are null (because there's no data to compute)
- if **all pointcall data is missing** for a port, **only port data** (like toponym and geographical coordinates) is given. source_suite and pointcall_year are set to null

Fields available :

- source_suite : the source where the data comes from (G5 | Santé Marseille | Registre du petit cabotage (1786-1787) | Expéditions "coloniales" Marseille (1789) )
- pointcall_year : the year ( 1787 | 1789 ) for which data is computed. A data line is returned for each year (if data is available)

Fields counting *congés* data from sources :

  - nb_conges_inputdone : total number of observed ("O") pointcalls (congés) actually recorded in database for this port in this source, outgoing ships, computed for a given year
  - nb_conges_cr : number of congés that are known to exist, according to national records (archives), and that are expected. For a given year  
  - nb_conges_sante : number of congés recorded in database, from register of the office of health of Marseille ("Santé Marseille"), incoming ships only. For a given year
  - nb_petitcabotage : number of congés recorded in database, from register "Registre du petit cabotage (1786-1787)" of Marseille (mainly ships coming from Mediterranean areas), incoming ships only. For a given year

Other computed fields :

- nb_tonnage_filled : number of pointcalls where tonnage data (volume de marchandise en tonneaux ou quintaux) is given
- nb_homeport_filled : number of pointcalls where homeport (port d'attache du navire) is known
- nb_product_filled : number of pointcalls where commodity_purpose (nature de la marchandise transportée, ou objet du voyage) is given
- nb_birthplace_filled : number of pointcalls where birthplace (lieu d'origine du capitaine du bateau) is known
- nb_citizenship_filled : number of pointcalls where citizenship (nationalité du capitaine du bateau) is known
- nb_flag_filled : number of pointcalls where flag (pavillon du bateau) is known
- good_sum_tonnage : sum of the tonnage that goes by this port (converted to barrel unit if given in quintals)

A join is made between pointcall and port data, to get nearly the same information about a port as with the ports api (see /api/fieldnames?api=ports) :

- ogc_fid : id
- uhgs_id : geo_general text id of the port
- toponym : standardised name of the port, in the 'lang' language
- substate : belonging substate of the port in 1787 / 1789. In the 'lang' language
- status : null | "oblique" | "siège amirauté", type of port
- has_a_clerk : true / false. Présence d'un greffier de l'amirauté locale dans le port
- geonameid : nearest geoname identifier for the port
- admiralty : name of the home admiralty (amirauté) for the port, in 1787
- province : name of the home province for the port, in 1787
- shiparea : name of the maritime area for the port in 2020
- point : coordinates for representation on a map
    
Will be extracted from postgres, schema navigoviz, table pointcall (see navigocorpus/ETL), completed with schema ports, table port_points.

Parameters :

- srid : **900913** | 4326 | 3857, for geometry transformation of the coordinates of the point representing the port on a map
- date : **none** | 4 digits representing a year (1787 | 1789), to extract pointcall data from the source, for this given pointcall_year only. Otherwise, when no date is given, the API returns one line per year of data (sums are done per year). Make sure to specify an 'order' if you wish to compute sums per port over the years.
- lang : **fr** | en, language for toponym(_standard), substate_1789
- params : **all** | coma-separated list of fields to be returned by the API
- order : **none** | coma-separated list of fields in desired order. Data lines will be sent in ascending order of first field values, and if equals then in order of second field listed, ...  
Example : 
`order=toponym,pointcall_year`

Examples :
```
http://localhost/api/sources/?srid=4326
http://localhost/api/sources/?srid=4326&date=1789
http://localhost/api/sources/?srid=4326&date=1789&lang=en
http://localhost/api/sources/?srid=4326&params=uhgs_id,toponym,pointcall_year,nb_conges_inputdone,nb_conges_cr,point
http://localhost/api/sources/?srid=4326&params=uhgs_id,toponym,pointcall_year,nb_conges_inputdone,nb_conges_cr,point&order=toponym,pointcall_year,source_suite
http://localhost/api/sources/?srid=4326&params=uhgs_id,toponym,pointcall_year,source_suite,nb_conges_inputdone,nb_conges_cr,point&order=toponym,pointcall_year,source_suite&format=csv
```

## /ships?

Return the ships'list that we can follow in travels 
    Will be extracted from postgres, schema navigoviz, table ship (see sql/portic_detailviz.sql), 
    but with a filter on the lang : only fr or en attributes

Parameters :

- lang : **fr** | en, language for flag(_standardized), homeport

Test : 

    - http://127.0.0.1:80/api/ships/?format=csv&lang=fr
    - http://127.0.0.1:80/api/ships/?format=json&lang=en

Content:

Each ship attribute (name, homeport, flag, tonnageclass, class) is associated with the frequency of occurence in source documents in a JSON array format : 
[{"variant1 of the attribute" : frequency}, {"variant2 of the attribute" : frequency}].

For instance :

    0010348N	[{"Negociant" : 1},{"Négociant de Bruges" : 1}]	[{"Bruges" : 1},{"Hambourg" : 1}]	[{"Bruges" : 1},{"Hamburg" : 1}]	[{"hambourgeois" : 1},{"Pays-Bas autrichien" : 1}]	[{"Austrian Netherlands" : 1},{"Hamburguese" : 1}]		[{"[101-200]" : 2}]
    [{"00007942" : 2}]	1787-06-08	1787-12-06	2

    This ship having 0010348N as identifier appears in 2 documents, named "Negociant" (once) or "Négociant de Bruges" (once). The ship's homeport is said "Bruges" in one case, "Hambourg" in the other case, and the ship's tonnage is between "101-200" tons in boths cases. 
    The captain was identified as "00007942" in both documents, and the ship has been moving on between le 8th of June to the 6th of December 1787

    0011092N	[{"Vierge du Rosaire" : 2},{"Notre Dame du Rosaire" : 1}]	[{"Gênes" : 1}]	[{"Genoa" : 1}]	[{"savoyard" : 2},{"génois" : 1}]	[{"Savoyard" : 2},{"Genoese" : 1}]	[{"bateau" : 2},{"tartane" : 1}]	[{"00011519" : 3}]	1787-03-30	1787-10-23	3

    This ship having 0011092N as identifier appears in 3 documents, named "Vierge du Rosaire" (twice) or "Notre Dame du Rosaire" (once). The ship's homeport is said "Gênes" in one case, "savoyard" in the other case, and the ship's tonnage is unknown but the ship's class is said to be "bateau" in two documents, "tartane" in the third document.
    The captain was identified as "00011519" in three documents, and the ship has been moving on between le 30th of March to the 23th of October 1787

## /captains?

Return the captains'list that we can follow in travels 
    Will be extracted from postgres, schema navigoviz, table captain (see sql/portic_detailviz.sql), 
    but with a filter on the lang : only fr or en attributes

Parameters :

- lang : **fr** | en, language for citizenship and birthplace (translations are not yet available on June 2022)

Test : 

    - http://127.0.0.1:80/api/ships/?format=csv&lang=fr
    - http://127.0.0.1:80/api/ships/?format=json&lang=en

Content:

Each captain attribute (name, citizenship, birthplace) is associated with the frequency of occurence in source documents in a JSON array format : 
[{"variant1 of the attribute" : frequency}, {"variant2 of the attribute" : frequency}].

For instance :

    00014569	[{"Accamo, Jean Baptiste" : 18},{"Accamo,Jean Baptiste" : 1},{"Accame, Jean Baptiste" : 1},{"Accamé, Jean Baptiste" : 1},{"Accamo, Jean BAptiste" : 1}]		[{"Génois" : 18},{"Genois" : 3}]   [{"0004424N" : 21}]	1787-01-07	1789-12-30	21

    This captain idendified by the number 00014569 has been found named "Accamo, Jean Baptiste" 18 times, but only once named "Accame, Jean Baptiste". In 18 documents, he was a Génois, in 3 he was a Genois. The citizenship attribute has not been yet standardized. 
    In the 21 documents, he was sailing on a ship identified as "0004424N", between the 7th January 1787 to the 30th December 1789.

