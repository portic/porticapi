# Portic data API : get dynamic data from database

*Web API for data above the postgres database developped with Flask*

API URL : **http://data.portic.fr/api/**  

See [user manual](usermanual.md) first for details

You can also [use the demonstrator tool on line](http://apidemo.portic.fr/)

Note that all fields are fully documented in [a tabular file - CSV -  on the GITLAB](https://gitlab.huma-num.fr/portic/porticapi/-/raw/master/static/data/api_portic.csv) : name of the field, definition, exemple. 

<!--  ![Pointcalls API - Results, and download time](img/temps_telechargement.png) -->

Licence : [![AGPLv3.png](http://www.gnu.org/graphics/agplv3-with-text-162x68.png)](agpl-3.0.md)

This is a **documentation** site for *porticapi*, a part of the *ANR Portic Project* : 
[![https://anr.portic.fr/](img/logo_portic_petit_100.jpg)](https://anr.portic.fr/)