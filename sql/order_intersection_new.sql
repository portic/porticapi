-- DROP FUNCTION ports.order_intersection(geometry,geometry)
create or replace function ports.order_intersection(p_depart geometry, linethatcut geometry) 
returns geometry[] as
$BODY$
/*
Calcul les deux points de l'intersection entre linethatcut et shoreline qui sont les plus proches de p_depart 
et renvoie un tableau de ces points.
Présuppose l'existence d'une table shoreline qui correspond à de GSHHS_i_L1.shp avec une linestring pour modéliser la cote
et la column shoreline = ST_LineMerge(st_boundary(st_setsrid(st_transform(st_setsrid(wkb_geometry, 4326), 3857), 3857)))
https://trac.osgeo.org/postgis/wiki/UsersWikiExamplesInterpolateWithOffset
-- suppression du buffer de 10
st_intersects(st_buffer(linethatcut, 10), shoreline)
*/
	declare pointslist geometry[];
	declare id_shoreline text;
begin
	
	id_shoreline:= s.id
	from navigoviz.shoreline s 
	where  shoreline is not null and s.area > 1 and st_intersects(linethatcut, shoreline)
	order by st_distance(p_depart,ST_ClosestPoint (shoreline, p_depart)) asc limit 1;

	select array_agg(geom) into pointslist from (
		select   (st_dump(intersectionPoints)).geom
		from 
		(select   st_intersection(linethatcut, shoreline) as intersectionPoints
			from navigoviz.shoreline s 
			where  s.id = id_shoreline
		) as k
		order by st_distance(p_depart, (st_dump(intersectionPoints)).geom) asc 
	) as q;

	return pointslist;

END;
$BODY$
language plpgsql;