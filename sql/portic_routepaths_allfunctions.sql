-- Christine Plumejeaud-Perreau, UMR 7301 MIGRINTER
-- date de création : 21 fev 2022 
-- Scripts pour calculer les chemins maritimes reliant les ports
-- Copie et simplification de C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\archives_sql\maritime_paths-21fev2022.sql
-- GIT : C:\Travail\Dev\portic_humanum\porticapi\sql\portic_routepaths_allfunctions.sql
-- Date de dernière mise à jour : 18 janvier 2023
-- Licence : GPL v3
-------------------------------------------------------------------------------------------------

-- Liste des types et fonctions créées dans le schéma ports

-- portic_computedPath
-- portic_points_on_path
-- portic_order_intersection
-- portic_move_pointp1_intosea
-- portic_simplifypath
-- portic_simplifypathbetter
-- portic_redo_points_on_path 
-- portic_subpoints_on_path
-- portic_array_reverse
-- portic_test_pathlength

/*
create  TYPE ports.portic_computedPath AS
(
    pointslist geometry[]      ,
    nbPoints  int,
    duration float,
    tampon float,
    distance float
);
*/

CREATE OR REPLACE FUNCTION ports.portic_points_on_path(p1_uhgsid text, p2_uhgsid text, m_offset double precision, p_distance_ratio integer)
 RETURNS ports.portic_computedpath
 LANGUAGE plpgsql
AS $function$
/*
Calcule les points intermédiaires permettant de tracer des segments qui relient deux ports sans croiser de terre
Donner les identifiant UHGS des deux ports , un offset (5 km) pour l'éloignement à la côte, 
et une distance (10 km) pour l'espacement entre les points sur l'offset
https://trac.osgeo.org/postgis/wiki/UsersWikiExamplesInterpolateWithOffset
requires : 
CREATE TYPE ports.portic_computedPath AS
(
    pointslist geometry[]      ,
    nbPoints  int,
    duration float,
    tampon float,
    distance float
);
*/
	declare m_seg geometry;
	declare p1 geometry;
	declare p2 geometry;
	declare finished boolean;
	declare f1 double precision;
	declare f2 double precision;
	declare longueur_segment double precision;
	declare ratio double precision;
	declare pointslist geometry[];
	declare pointslistnew geometry[];
	declare pointslistnew2 geometry[];
	declare pointslistpart geometry[];
	declare pointslistsimplified geometry[];
	declare test geometry[];
	declare iter int;
	declare iter2 int;
	declare nbPoints int;
	declare rec record;
	declare shoreid text;
	declare starttime timestamptz;
	declare endtime timestamptz;
	declare previousPoint geometry;
	declare pointp1prime geometry;
	declare sameOffset boolean;
	declare distance_ratio double precision;
	declare tampon double precision;
	DECLARE nbPointsPart int;
	DECLARE maxiter int;
	DECLARE k int;
	declare resultat ports.portic_computedPath;
	declare duration float;
	declare f2prime double precision;
	declare recurse boolean;
	
begin
	RAISE notice '###################################### % p1 % p2 %', now(), p1_uhgsid, p2_uhgsid ;
	starttime:= clock_timestamp();
	iter:= 0;
	iter2:= 0;
	nbPoints:=0;

	
	--p1.point3857
	select ports.portic_order_intersection(coalesce(p1.port_on_line_1mile, p1.point3857), st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile)) into test
	from ports.port_points p1 , ports.port_points p2
	where   p1.uhgs_id = p1_uhgsid and p2.uhgs_id = p2_uhgsid ;

	
	if test[1] is null 
	then 
		--RAISE notice '% STOP here, direct',NOW() ;
		finished:= true;
		-- Mettre le premier point : le port de départ, mais à 1 mile de la cote (IMPORTANT)
		pointslist[0]:=  coalesce(port_on_line_1mile, point3857) from  ports.port_points p where   p.uhgs_id = p1_uhgsid ;
		-- Mettre le dernier point : le port d'arrivée, mais à 1 mile de la cote (IMPORTANT)
		pointslist[1]:=  coalesce(port_on_line_1mile, point3857) from  ports.port_points p where   p.uhgs_id = p2_uhgsid ;
		nbPoints:=2;
	
		-- bug Marseille-Sète
		endtime:= clock_timestamp();
		duration:=1000 * ( extract(epoch from endtime) - extract(epoch from starttime) );
		resultat.pointslist := pointslist;
		resultat.nbPoints := array_length(pointslist, 1);
		resultat.duration := duration;
		resultat.tampon := m_offset;
		resultat.distance := p_distance_ratio;
	
		-- enregister le résultat
		/*
		update  navigoviz.routepaths r set m_offset= resultat.tampon, p_distance_ratio = resultat.distance,
		pointpath =resultat.pointslist , elapseTime=resultat.duration ,nbPoints = resultat.nbPoints
		where  r.from_uhgs_id = p1_uhgsid and r.to_uhgs_id = p2_uhgsid
		or (r.to_uhgs_id = p1_uhgsid and r.from_uhgs_id = p2_uhgsid);
		
		
		insert into  navigoviz.routepaths 
		(from_uhgs_id, to_uhgs_id, m_offset, p_distance_ratio, pointpath, elapseTime, nbPoints)
		values (p1_uhgsid, p2_uhgsid, resultat.tampon, resultat.distance, resultat.pointslist,resultat.duration, resultat.nbPoints);
		*/
		--commit;
	
		return resultat;
	else 
		RAISE notice ' Test p1 % p2 %', st_asewkt(test[1]), st_asewkt(test[2]);
		finished:= false;
		-- Mettre le premier point : le port de départ
		pointslist[0]:=  coalesce(port_on_line_1mile, point3857)  from  ports.port_points p where   p.uhgs_id = p1_uhgsid ;
		p1:=pointslist[0]; 
		-- ajout le 14/03/2022 pour test Marennes-Honfleur avec 100 km
		previousPoint:= pointslist[0];
		nbPoints:= nbPoints+1;
	end if;

	select  coalesce(port_on_line_1mile, point3857) into p2 from  ports.port_points  where   uhgs_id = p2_uhgsid ;

	RAISE notice  'DECOUPE de p1 % p2 %',  st_asewkt(p1), st_asewkt(p2) ;

	distance_ratio:= p_distance_ratio;
	tampon:=m_offset;
	resultat.tampon := tampon;
	resultat.distance := distance_ratio;
	-- avant iter2 = 10, mais maintenant on itère une seule fois. Test
	while finished is false and iter2 < 1 loop
	
		--k:= 2;
		k:= array_length(test, 1);
		m_seg:= NULL;
		longueur_segment:= st_length(st_setsrid(st_makeline(test[1], test[k]), 3857));
		--- ADAPTATION automatique de la densité de points et du buffer à la longueur approximée de la route
		if (longueur_segment > 1000000 and tampon!=50000) then
			distance_ratio := ((longueur_segment/1000)/(round((longueur_segment/1000)^(1/2.5))+1))*1000;--en mètres
			tampon:=50000;--50 km pour d>1700 km (75 % des observatons sont sous ce seuil)
		elseif (longueur_segment > 500000 and tampon!=36000) then
			tampon:=36000;
			distance_ratio := 40000;--40 km pour d>500 km 
		elseif (longueur_segment > 120000 and tampon!=20000) then
			tampon:=20000;
			distance_ratio := 20000;--20 km pour d>120 km 
		elseif (longueur_segment > 60000 and tampon!=8000) then
			tampon:=16000; --recalculer avec ce tampon 8 km pour d>60 km
			distance_ratio := 16000;
		elseif (longueur_segment <= 60000 and tampon!=5000) then
			tampon:=5000; --recalculer avec ce tampon 5 km pour d<=60 km
			distance_ratio := 10000;
		end if;
		raise notice 'LONGUEUR DIRECTE % TAMPON % DISTANCE %',round(longueur_segment/1000),tampon, distance_ratio;
		resultat.tampon := tampon;
		resultat.distance := distance_ratio;
		
		while m_seg IS NULL AND k > 1 loop
			
			RAISE notice 'Grande boucle iter2 % test[1] % test[2] %', iter2, st_asewkt(test[1]) , st_asewkt(test[k]);
			RAISE notice 'Distance k % entre test[1] et test[2] %', k, ST_Distance(test[1], test[k]);
			shoreid:=  s.id from navigoviz.shoreline s  where s.shoreline is not null and s.area > 1 
				and st_intersects(s.shoreline, st_buffer(st_setsrid(st_makeline(test[1], test[k]), 3857), 10)) 
				order by st_distance(test[1],ST_ClosestPoint(shoreline, test[1])) asc 
				limit 1;
			RAISE notice 'Identifiant du shoreline à contourner %', shoreid ;
			-- assertion : les deux points test partagent la même shoreline car deux points successifs appartiennent à la même shoreline qu'ils coupent par l'intérieur
			f1 :=  ST_LineLocatePoint(s.shoreline, test[1]) 
				from navigoviz.shoreline s 
				where s.id = shoreid;
			f2 :=  ST_LineLocatePoint(s.shoreline , test[k]) 
				from navigoviz.shoreline s 
				where s.id = shoreid;
			RAISE notice 'cas comme AVANT , f1 % f2 % f1-f2 = %', f1, f2, case when f1> f2 then f1-f2 else f2-f1 end ;
			f2prime :=  ST_LineLocatePoint(st_addpoint(s.shoreline, test[1], 0) , test[k]) 
				from navigoviz.shoreline s 
				where s.id = shoreid;
			--RAISE notice 'f1 % f2 %', f1, f2 ;
			RAISE notice 'CAS où test1 devient le début, f1 % f2 % f1-f2 = %', 0, f2prime, f2prime ;
			if (f2prime < Abs(f2-f1)) then 			
				-- parfois ST_OffsetCurve renvoie des polylines, ce qui ne marche pas. Donc on prend la plus petite.
				select g into m_seg from (
					select ST_NumGeometries(geom),  (st_dump(geom)).geom as g
					from (
					select ST_OffsetCurve(ST_LineSubstring(st_addpoint(s.shoreline, test[1], 0), 0, f2prime), tampon) as geom
								from navigoviz.shoreline s 
								where s.id = shoreid
					) as k
					) as q
				--where st_intersects(g, test[1]) and st_intersects(g, test[2]);
				order by  st_length(g) asc limit 1; 	
				f1:=0;	
				f2:=f2prime;
			else
				select g into m_seg from (
					select ST_NumGeometries(geom),  (st_dump(geom)).geom as g
					from (
					select ST_OffsetCurve(ST_LineSubstring(s.shoreline,case when f1<f2 then f1 else f2 end, case when f1<f2 then f2 else f1 end), tampon) as geom
								from navigoviz.shoreline s 
								where s.id = shoreid
					) as k
					) as q
				--where st_intersects(g, test[1]) and st_intersects(g, test[2]);
				order by  st_length(g) desc limit 1; --comme avant
			end if;
			longueur_segment:= st_length(m_seg); 
			RAISE notice 'Longueur du segment en offset %', longueur_segment ;
			if m_seg is null then
				k:=k-1;
			/*else 
				--- ADAPTATION automatique de la densité de points et du buffer à la longueur approximée de la route
				if (longueur_segment > 1700000 and tampon!=50000) then
					distance_ratio := ((longueur_segment/1000)/(round((longueur_segment/1000)^(1/2.5))+1))*1000;--en mètres
					tampon:=50000;--50 km pour d>1700 km (75 % des observatons sont sous ce seuil)
				elseif (longueur_segment > 500000 and tampon!=36000) then
					tampon:=36000;
					distance_ratio := 40000;
				elseif (longueur_segment > 120000 and tampon!=20000) then
					tampon:=20000;
					distance_ratio := 20000;
				elseif (longueur_segment <= 120000 and tampon!=16000) then
					tampon:=16000; --recalculer avec ce tampon 16 km pour d<=120 km
					distance_ratio := 16000;
				end if;
				raise notice 'TAMPON % DISTANCE %',tampon, distance_ratio;
				resultat.tampon := tampon;
				resultat.distance := distance_ratio;*/
			end if;
		END loop;
	
		--return resultat;
	
		iter:= 0;
		maxiter:=300;--300
		
		sameOffset:=true;
	
		
		--distance_ratio:= longueur_segment/20;
		--distance_ratio:=p_distance_ratio/(2*iter2+1) ; -- on réduit l'allonge vers l'arrivée
		
		if maxiter * distance_ratio < longueur_segment then 
			maxiter := round(longueur_segment/distance_ratio) +2;
			raise notice 'INCREASE le nombre max iterations % ',maxiter;
		end if;
	
		while sameOffset is true and finished is false and m_seg is not null and iter < maxiter loop 
		
			-- ratio progresse de 0 à 1 jusqu'à l'arrivée
			ratio:= case when f1> f2 then 1 - (distance_ratio* (iter+1) / st_Length(m_seg)) else (distance_ratio* (iter+1) / st_Length(m_seg)) end;
			-- raise notice 'valeur du ratio % - doit etre dans [0, 1]',ratio;
			--ratio:= (distance_ratio* (iter+1) / st_Length(m_seg)) ;
			
		
			if ratio > 1 or ratio < 0 then 
				raise notice 'CHANGE OFFSET valeur du ratio % - doit etre dans [0, 1]',ratio;
				-- distance_ratio:=distance_ratio/2; -- a voir si c'est ca qui merde : oui, ca fait merder
				-- précaution 
				--ratio:= case when ratio > 1 then ratio-1 else -ratio end;
				--ratio:= case when ratio < 0 then 0 else ratio end;
				sameOffset:= false; -- a voir si c'est ca qui merde
				--ratio:=1.0/maxiter*iter;
				--raise notice 'CHANGE ratio : new ratio % ',ratio;
			else
			
				-- Place a point on segment that is parallel  to shoreline
				select ST_LineInterpolatePoint(m_seg, ratio) into p1; 
				--raise notice '### points_on_path 1 ST_LineInterpolatePoint moved to %', st_asewkt(p1);
			
				-- move this point if required (test if it intersects the previous point and do the job)
				-- attention, présuppose que pointslist contient toujours le premier point, 
				-- premier point à 1 mile des cotes pour éviter une intersection.
			
				-- OPTION 
				-- Cette action bouge le point p1 de manière à ne pas couper la côte sur le troncon qui précède.
				-- C'est fin mais ca boucle infiniment. Il vaut mieux rappeler redo_point_on_path 
				-- select  ports.move_pointp1(previousPoint, p1, m_offset, p_distance_ratio ) into p1;
			
				-- Cette action agit grosso modo : on place des points à intervalles réguliers sur le grand segmnet
				-- si ils tombent dans la terre, on les déplace
				select ports.portic_move_pointp1_intosea(p1) into pointp1prime;
				--raise notice '### points_on_path 1 move_pointp1_intosea moved to %', st_asewkt(p1);
				--select ports.move_pointp1(previousPoint, p1, m_offset, round(distance_ratio)::integer  ) into p1;
			
				/*
				-- logguer les essais				
				insert into navigoviz.temp_points (iter2, maxiter, iter, previousPoint, pointp1, pointp1prime, shoreid, segment, longueur_segment, f1, f2, newoffset, distance_ratio, ratio) 
				values (iter2, maxiter, iter, previousPoint, p1, pointp1prime, shoreid, m_seg, longueur_segment, f1, f2, m_offset, distance_ratio, ratio);
				*/
			
				p1:= pointp1prime;

				-- test if finished for this shoreline
				select ports.portic_order_intersection(p1, st_makeline(p1, p2)) into test;
				-- and s.id = shoreid and st_intersects(st_makeline(p1, p2.port_on_line_1mile), shoreline);
			

				if test[1] is null 
					then finished:= true;
				end if;
				-- comment
				--raise notice 'Segment direct ? % Iteration % ', finished, iter;
				
				
				pointslist[nbPoints]:=p1;
				nbPoints:= nbPoints+1;
				previousPoint:=p1;
				iter:= iter+1;
			end if;--change offset sur ratio
			
		end loop;
	
		if sameOffset is false then
			raise notice 'Liste des points à tester pour prendre intersection Offset :  p1 %, p2 %', st_asewkt(p1), st_asewkt(p2);
		end if;
		-- fini, vraiment ? Il y a une autre shoreline à contourner
		select ports.portic_order_intersection(p1, st_makeline(p1, p2)) into test;
		
	
		if test[1] is null 
		then 
			RAISE notice 'STOP here,  iteration2  % ', iter2 ;
			finished:= true;
		else 
			RAISE notice 'INACHEVE Test p1 % p2 % iter2 %', st_asewkt(test[1]), st_asewkt(test[2]), iter2;
			finished:= false;
		end if;
	
		iter2:= iter2+1;
	end loop;

	-- Mettre le dernier point : le port d'arrivée
	pointslist[nbPoints]:=  p2;
	nbPoints:= nbPoints+1;
	
	/*
	-- simplifier
	pointslist := ports.portic_simplifypath(pointslist);
	-- rappeler la correction sur tous les segments et insérer la correction
	
	pointslistnew[0] := pointslist[0];
	FOR iter IN 0 .. array_length(pointslist, 1)-1 LOOP 
   		pointslistpart := ports.portic_subpoints_on_path(pointslist[iter], pointslist[iter+1],  m_offset, p_distance_ratio);
   		
   		nbPointsPart := array_length(pointslistpart, 1);
   		--pointslistnew := pointslist array_cat(anyarray, anyarray)
   		RAISE notice 'BOUCLE iter % subpoints_on_path p1 % p2 %  nbPointsPart %',iter, st_asewkt(pointslist[iter]), st_asewkt(pointslist[iter+1]), nbPointsPart ;
   		IF nbPointsPart = 2 THEN
   			pointslistnew := array_append(pointslistnew, pointslist[iter+1]);
   		ELSE 
   			pointslistnew := array_cat(pointslistnew, pointslistpart[1:nbPointsPart-1]);
   			pointslistnew := array_append(pointslistnew, pointslist[iter+1]);
   		END IF;
	END LOOP;
	
	-- simplifier au max (enlever les boucles). algo en n log n
	pointslistsimplified := ports.portic_simplifypathbetter(pointslistnew);
	*/
	

	endtime:= clock_timestamp();
	duration:=1000 * ( extract(epoch from endtime) - extract(epoch from starttime) );
	resultat.pointslist := pointslist;
	resultat.nbPoints := array_length(pointslist, 1);
	resultat.duration := duration;


	---------------------------------
	-- Introduire ici la recursivité
	---------------------------------
	
	recurse:=false;
	iter:=0;
	
	select ST_Intersects (st_makeline(pointslist),s.polygone3857 ) into recurse
	from navigoviz.shoreline s  
	where ST_Intersects(st_makeline(pointslist),s.polygone3857 ) ;

	raise notice '_______________________________ recursivité iter % need to recurse ? % ', iter, recurse;
	while recurse is true and iter < 4 loop 
		-- distance_ratio := round(p_distance_ratio/power(2,iter+1));-- select power(iter,2) iter+2
		-- tampon := m_offset/(power(2, iter+1));
		distance_ratio := round(distance_ratio/2);
		tampon := tampon/2;
		raise notice '_______________________________ recursivité distance_ratio % tampon ? % ', distance_ratio, tampon;
		if (mod(iter, 2) = 0) then
			raise notice '_______________________________ recursivité start SIMPLIFY %', clock_timestamp();
			pointslist := ports.portic_simplifypath(pointslist);
			raise notice '_______________________________ recursivité end SIMPLIFY %', clock_timestamp();
		end if;
	
		select ports.portic_redo_points_on_path(pointslist, tampon, distance_ratio::integer) into pointslist;
	
		select ST_Intersects (st_makeline(pointslist),s.polygone3857 ) into recurse
		from navigoviz.shoreline s  
		where ST_Intersects(st_makeline(pointslist),s.polygone3857 ) ;
		
		raise notice '_______________________________ recursivité iter % need to recurse ? % ', iter, recurse;
		iter:=iter+1;
	end loop;
	

	-- Tester les boucles 
	if ST_IsSimple(st_makeline(pointslist)) IS FALSE THEN
		raise notice '_______________________________ SIMPLIFY BETTER on nbpoints %', array_length(pointslist, 1);
		pointslist := ports.portic_simplifypathbetter(pointslist);
	END IF;
	

	endtime:= clock_timestamp();
	--return pointslistsimplified;
	duration:=1000 * ( extract(epoch from endtime) - extract(epoch from starttime) );
	resultat.duration := duration;
	resultat.pointslist := pointslist;
	resultat.nbPoints := array_length(pointslist, 1);
	
	
	raise notice '% DURATION % NB POINTS %', NOW(), 1000 * ( extract(epoch from endtime) - extract(epoch from starttime) ), array_length(pointslist, 1);

	-- enregistrer le résultat
	/*insert into  navigoviz.routepaths 
	(from_uhgs_id, to_uhgs_id, m_offset, p_distance_ratio, pointpath, elapseTime, nbPoints)
	values (p1_uhgsid, p2_uhgsid, resultat.tampon, resultat.distance, resultat.pointslist,resultat.duration, resultat.nbPoints);
	
	--commit;
	update  navigoviz.routepaths r set m_offset= resultat.tampon, p_distance_ratio = resultat.distance,
		pointpath =resultat.pointslist , elapseTime=resultat.duration ,nbPoints = resultat.nbPoints
		where  r.from_uhgs_id = p1_uhgsid and r.to_uhgs_id = p2_uhgsid
		or (r.to_uhgs_id = p1_uhgsid and r.from_uhgs_id = p2_uhgsid);
	*/
	return resultat;

END;
$function$
;

-- Permissions

ALTER FUNCTION ports.portic_points_on_path(text, text, float8, int4) OWNER TO postgres;
GRANT ALL ON FUNCTION ports.portic_points_on_path(text, text, float8, int4) TO postgres;

CREATE OR REPLACE FUNCTION ports.portic_order_intersection(p_depart geometry, linethatcut geometry)
 RETURNS geometry[]
 LANGUAGE plpgsql
AS $function$
/*
Calcul les deux points de l'intersection entre linethatcut et shoreline qui sont les plus proches de p_depart 
et renvoie un tableau de ces points.
Présuppose l'existence d'une table shoreline qui correspond à de GSHHS_i_L1.shp avec une linestring pour modéliser la cote
et la column shoreline = ST_LineMerge(st_boundary(st_setsrid(st_transform(st_setsrid(wkb_geometry, 4326), 3857), 3857)))
https://trac.osgeo.org/postgis/wiki/UsersWikiExamplesInterpolateWithOffset
-- suppression du buffer de 10
st_intersects(st_buffer(linethatcut, 10), shoreline)
*/
	declare pointslist geometry[];
	declare id_shoreline text;
begin
	
	id_shoreline:= s.id
	from navigoviz.shoreline s 
	where  shoreline is not null and s.area > 1 and st_intersects(linethatcut, shoreline)
	order by st_distance(p_depart,ST_ClosestPoint (shoreline, p_depart)) asc limit 1;

	select array_agg(geom) into pointslist from (
		select   (st_dump(intersectionPoints)).geom
		from 
		(select   st_intersection(linethatcut, shoreline) as intersectionPoints
			from navigoviz.shoreline s 
			where  s.id = id_shoreline
		) as k
		order by st_distance(p_depart, (st_dump(intersectionPoints)).geom) asc 
	) as q;

	return pointslist;

END;
$function$
;

-- Permissions

ALTER FUNCTION ports.portic_order_intersection(geometry, geometry) OWNER TO postgres;
GRANT ALL ON FUNCTION ports.portic_order_intersection(geometry, geometry) TO postgres;

CREATE OR REPLACE FUNCTION ports.portic_move_pointp1_intosea(pointp1 geometry)
 RETURNS geometry
 LANGUAGE plpgsql
AS $function$
/*
Bouge le point p1 qui appartient au polygone de la shoreline sur Mer
Présuppose l'existance d'une table shoreline qui correspond à de GSHHS_i_L1.shp avec une linestring pour modéliser la cote
et la column shoreline = ST_LineMerge(st_boundary(st_setsrid(st_transform(st_setsrid(wkb_geometry, 4326), 3857), 3857)))
https://trac.osgeo.org/postgis/wiki/UsersWikiExamplesInterpolateWithOffset
*/
	declare pointp1prime geometry;
	declare iter int;
	declare truc record;
	declare azimuth  double precision;
	declare aterre text; -- identifiant de la shoreline
	declare marge1mile int;
	declare geomline geometry;
begin
	marge1mile:=1854;
	pointp1prime:= pointp1;
	aterre:= s.id from navigoviz.shoreline s where st_contains(s.polygone3857, pointp1prime);


	if aterre is not null then
	-- Attention, toutes les shoreline n'avaient pas été calculées
		geomline := shoreline from navigoviz.shoreline where id = aterre;
		if geomline is null then 
			RAISE notice '\t ------------ MOVE P1 INTO SEA move_pointp1_intosea Compute a missing shoreline with id % ', aterre;
			update navigoviz.shoreline set shoreline = ST_LineMerge(st_boundary(polygone3857)) 
			where id = aterre;
		end if;
	end if;

	iter:= 0;
	while aterre is not null and iter < 10 loop 
		--RAISE notice '\t ------------ MOVE P1 INTO SEA  Bouger  pointp1 qui est à Terre % dans %', st_asewkt(pointp1prime), aterre;

		-- le replacer sur la cote la plus proche
		select st_length(st_shortestline(pointp1prime, shoreline)) as d, st_closestpoint(shoreline, pointp1prime) as proche into truc
		from navigoviz.shoreline where id = aterre  limit 1; --order by d asc
		--RAISE notice '\t ------------ Bouger de la distance % avec la projection sur la ligne % ', truc.d, st_asewkt(truc.proche);
	
		azimuth:= st_azimuth(pointp1prime, truc.proche);
		--RAISE notice '\t ------------ Bouger de azimuth % ', azimuth;

		--greatest(truc.d*2, truc.d+marge1mile)
		-- truc.d+marge1mile
		select st_transform(st_GeomFromEWKT(ST_AsEWKT (k.x)),3857) into pointp1prime from
		(
			select  st_setsrid(st_project(st_transform(pointp1prime, 4326)::geography, 2*truc.d, azimuth), 4326) as x
		) as k;
		-- pointp1prime est modifié , placé en mer, symétrique à la cote
		--RAISE notice '\t ------------ MOVE P1 INTO SEA -  resultat pointp1prime % ', st_asewkt(pointp1prime);
	
		aterre:= s.id from navigoviz.shoreline s where st_contains(s.polygone3857, pointp1prime);
		if aterre is null  then 
			return pointp1prime; 
		else
			-- Attention, toutes les shoreline n'avaient pas été calculées
			geomline := shoreline from navigoviz.shoreline where id = aterre;
			if geomline is null then 
				RAISE notice '\t ------------ MOVE P1 INTO SEA move_pointp1_intosea Compute a missing shoreline with id % ', aterre;
				update navigoviz.shoreline set shoreline = ST_LineMerge(st_boundary(polygone3857)) 
				where id = aterre;
			end if;
		end if;
		-- La je n'imagine pas que pointp1prime est à nouveau dans la terre.
		iter:=iter+1;
	end loop;
	--if iter=10 then 
		raise notice '\t ------------ MOVE P1 INTO SEA - IMPOSSIBLE DE BOUGER p1 % pointp1prime %', st_asewkt(pointp1), st_asewkt(pointp1prime);
		return pointp1;
	--end if;
	--return pointp1prime;

END;
$function$
;

-- Permissions

ALTER FUNCTION ports.portic_move_pointp1_intosea(geometry) OWNER TO postgres;
GRANT ALL ON FUNCTION ports.portic_move_pointp1_intosea(geometry) TO postgres;

CREATE OR REPLACE FUNCTION ports.portic_simplifypath(itineraire geometry[])
 RETURNS geometry[]
 LANGUAGE plpgsql
AS $function$
/*
Simplifie le chemin calculé en supprimant les ronds dans l'eau. 
Présuppose l'existance d'une table shoreline qui correspond à de GSHHS_i_L1.shp avec une linestring pour modéliser la cote
et la column shoreline = ST_LineMerge(st_boundary(st_setsrid(st_transform(st_setsrid(wkb_geometry, 4326), 3857), 3857)))
https://trac.osgeo.org/postgis/wiki/UsersWikiExamplesInterpolateWithOffset
*/
	declare previousPoint geometry;
	declare currentPoint geometry;
	declare pointp1 geometry;	
	declare pointslist geometry[];
	declare test geometry[];
	declare iter int;
	declare jiter int;
	declare previousindex int;
	declare nbPoints int;
	declare taille int;
begin
	nbPoints:=0;
	--nbPoints:= nbPoints+1;
	--pointslist[0]:=  itineraire[0];
	previousindex:=0;
	iter:=0;
	previousPoint:=itineraire[0];
	currentPoint:=previousPoint;
	taille:=array_length(itineraire, 1);--2
	raise notice 'array_length(itineraire, 1) %', 	taille;
	while iter < taille-1 loop
		--raise notice 'iter % currentPoint %', iter, st_asewkt(currentPoint) ;
		jiter := iter+1;
		pointp1:=itineraire[jiter];
		select ports.portic_order_intersection(currentPoint, st_makeline(currentPoint, pointp1)) into test;
		previousPoint:=pointp1;--oo
		previousindex:=jiter;
		while test[1] is null  and jiter < taille-1 loop
			previousPoint:=pointp1;
			previousindex:=jiter;
			jiter:=jiter+1;
			pointp1:=itineraire[jiter];
			--raise notice 'boucle interne jiter % pointp1 %', jiter, st_asewkt(previousPoint);
			select ports.portic_order_intersection(currentPoint, st_makeline(currentPoint, pointp1)) into test;
		end loop;
		pointslist[nbPoints]:=currentPoint;
		nbPoints:= nbPoints+1;
		currentPoint:=previousPoint;
		iter:=previousindex;
	end loop;
	pointslist[nbPoints]:=currentPoint;
	nbPoints:= nbPoints+1;
	--pointslist[nbPoints]:=  itineraire[taille-1];
	return pointslist;
END;
$function$
;

-- Permissions

ALTER FUNCTION ports.portic_simplifypath(_geometry) OWNER TO postgres;
GRANT ALL ON FUNCTION ports.portic_simplifypath(_geometry) TO postgres;

CREATE OR REPLACE FUNCTION ports.portic_simplifypathbetter(itineraire geometry[])
 RETURNS geometry[]
 LANGUAGE plpgsql
AS $function$
/*
Simplifie le chemin calculé en supprimant les ronds dans l'eau. 
Présuppose l'existance d'une table shoreline qui correspond à de GSHHS_i_L1.shp avec une linestring pour modéliser la cote
et la column shoreline = ST_LineMerge(st_boundary(st_setsrid(st_transform(st_setsrid(wkb_geometry, 4326), 3857), 3857)))
https://trac.osgeo.org/postgis/wiki/UsersWikiExamplesInterpolateWithOffset
*/
	declare previousPoint geometry;
	declare currentPoint geometry;
	declare pointp1 geometry;	
	declare pointslist geometry[];
	declare test geometry[];
	declare iter int;
	declare jiter int;
	declare previousindex int;
	declare nbPoints int;
	declare taille int;
begin
	nbPoints:=0;
	--nbPoints:= nbPoints+1;
	--pointslist[0]:=  itineraire[0];
	previousindex:=0;
	iter:=0;
	previousPoint:=itineraire[0];
	currentPoint:=previousPoint;
	taille:=array_length(itineraire, 1);--2
	raise notice 'array_length(itineraire, 1) %', 	taille;
	while iter < taille-1 loop
		--raise notice 'iter % currentPoint %', iter, st_asewkt(currentPoint) ;
		jiter := iter+1;
		pointp1:=itineraire[jiter];
		select ports.portic_order_intersection(currentPoint, st_makeline(currentPoint, pointp1)) into test;
		previousPoint:=pointp1;--oo
		previousindex:=jiter;
		while jiter < taille-1 loop
			if test[1] is null then 
				previousPoint:=pointp1;
				previousindex:=jiter;
			end if;
			jiter:=jiter+1;
			pointp1:=itineraire[jiter];
			--raise notice 'boucle interne jiter % pointp1 %', jiter, st_asewkt(previousPoint);
			select ports.portic_order_intersection(currentPoint, st_makeline(currentPoint, pointp1)) into test;
		end loop;
		pointslist[nbPoints]:=currentPoint;
		nbPoints:= nbPoints+1;
		currentPoint:=previousPoint;
		iter:=previousindex;
	end loop;
	pointslist[nbPoints]:=currentPoint;
	nbPoints:= nbPoints+1;
	--pointslist[nbPoints]:=  itineraire[taille-1];
	return pointslist;
END;
$function$
;

-- Permissions

ALTER FUNCTION ports.portic_simplifypathbetter(_geometry) OWNER TO postgres;
GRANT ALL ON FUNCTION ports.portic_simplifypathbetter(_geometry) TO postgres;

CREATE OR REPLACE FUNCTION ports.portic_redo_points_on_path(pointslist geometry[], m_offset double precision, p_distance_ratio integer)
 RETURNS geometry[]
 LANGUAGE plpgsql
AS $function$
/*
Calcule les points intermédiaires permettant de tracer des segments qui relient deux ports sans croiser de terre
Donner les identifiant UHGS des deux ports , un offset (5 km) pour l'éloignement à la côte, 
et une distance (10 km) pour l'espacement entre les points sur l'offset
https://trac.osgeo.org/postgis/wiki/UsersWikiExamplesInterpolateWithOffset
*/
	declare pointslistclean geometry[];
	declare pointslistnew geometry[];
	declare pointslistpart geometry[];
	declare pointslistsimplified geometry[];
	declare starttime timestamptz;
	declare endtime timestamptz;	
	DECLARE nbPointsPart int;
	
	--declare resultat ports.portic_computedPath;
	declare res geometry[];
	declare duration float;
begin
	--starttime:= clock_timestamp();
	
	RAISE notice 'portic_redo_points_on_path Longueur, %', array_length(pointslist, 1);
	pointslistclean[0] := pointslist[0];
	FOR iter IN 0 .. array_length(pointslist, 1)-1 LOOP 
		if pointslist[iter+1] is null then
   			--RAISE notice 'p2 IS NULL ? %', st_asewkt(pointslist[iter+1]);
   			--RAISE notice 'Pour la longueur, %, A iter+1, %, p2 IS NULL ? %', array_length(pointslist, 1), iter+1, st_asewkt(pointslist[iter+1]);
   			--iter:=iter+2;
   		else
   			pointslistclean := array_append(pointslistclean, pointslist[iter+1]);
   		end if;
   	
   	END LOOP;
   	RAISE notice 'portic_redo_points_on_path Longueur clean, %', array_length(pointslistclean, 1);
	
   
	pointslistnew[0] := pointslistclean[0];
	FOR iter IN 0 .. array_length(pointslistclean, 1)-2 LOOP 
		if pointslistclean[iter+1] is null then
   			--RAISE notice 'p2 IS NULL ? %', st_asewkt(pointslist[iter+1]);
   			RAISE notice 'portic_redo_points_on_path Pour la longueur, %, A iter+1, %, p2 IS NULL ? %', array_length(pointslistclean, 1), iter+1, st_asewkt(pointslistclean[iter+1]);
   			--iter:=iter+2;
   		else
	   		pointslistpart := ports.portic_subpoints_on_path(pointslistclean[iter], pointslistclean[iter+1],  m_offset, p_distance_ratio);
	
	   		nbPointsPart := array_length(pointslistpart, 1);
	   		--RAISE notice 'BOUCLE iter % subpoints_on_path p1 % p2 %  nbPointsPart %',iter, st_asewkt(pointslistclean[iter]), st_asewkt(pointslistclean[iter+1]), nbPointsPart ;
	
	   		IF nbPointsPart = 2 then
	   			-- RAISE notice 'NOTHING TO DO ';
	   			pointslistnew := array_append(pointslistnew, pointslistclean[iter+1]);
	   		else
	   			RAISE notice 'SEGMENT iter % de portic_redo_points_on_path  [p1 % p2 % ] : nbPointsPart %',iter, st_asewkt(pointslistclean[iter]), st_asewkt(pointslistclean[iter+1]), nbPointsPart ;

	   			pointslistnew := array_cat(pointslistnew, pointslistpart[1:nbPointsPart-1]);
	   			pointslistnew := array_append(pointslistnew, pointslistclean[iter+1]);
	   		END IF;
   	
   		end if;
	END LOOP;
	
	--endtime:= clock_timestamp();
	--duration:=1000 * ( extract(epoch from endtime) - extract(epoch from starttime) );

	--raise notice '% DURATION % NB POINTS %', NOW(), duration, array_length(pointslistnew, 1);

	

	--resultat.pointslist := pointslistnew;
	--resultat.nbPoints := array_length(pointslistnew, 1);
	--resultat.duration := duration;
	--return resultat;

	res:=pointslistnew;
	return res;
END;
$function$
;

-- Permissions

ALTER FUNCTION ports.portic_redo_points_on_path(_geometry, float8, int4) OWNER TO postgres;
GRANT ALL ON FUNCTION ports.portic_redo_points_on_path(_geometry, float8, int4) TO postgres;

CREATE OR REPLACE FUNCTION ports.portic_subpoints_on_path(p1_start geometry, p2_end geometry, m_offset double precision, p_distance_ratio integer)
 RETURNS geometry[]
 LANGUAGE plpgsql
AS $function$
/*
Calcule les points intermédiaires permettant de tracer des segments qui relient deux ports sans croiser de terre
Donner les identifiant UHGS des deux ports , un offset (5 km) pour l'éloignement à la côte, 
et une distance (10 km) pour l'espacement entre les points sur l'offset
https://trac.osgeo.org/postgis/wiki/UsersWikiExamplesInterpolateWithOffset
*/
	declare m_seg geometry;
	declare p1 geometry;
	declare p2 geometry;
	declare pointp1prime geometry;
	declare finished boolean;
	declare f1 double precision;
	declare f2 double precision;
	declare longueur_segment double precision;
	declare ratio double precision;
	declare pointslist geometry[];
	declare test geometry[];
	declare iter int;
	declare iter2 int;
	declare nbPoints int;
	declare rec record;
	declare shoreid text;
	declare starttime timestamptz;
	declare endtime timestamptz;
	declare previousPoint geometry;
	declare sameOffset boolean;
	declare distance_ratio double precision;
	declare aterre text; -- identifiant de la shoreline dans laquelle pourrait tomber le point.

begin
	--RAISE notice '###################################### subpoints_on_path %', now();
	starttime:= clock_timestamp();
	iter:= 0;
	iter2:= 0;
	nbPoints:=0;

	-- précaution
	IF p1_start IS NULL OR p2_end IS NULL THEN
		RAISE notice '###### BUG subpoints_on_path : un des deux points null p[1] % p[2] %', st_asewkt(p1_start) , st_asewkt(p2_end);
		nbPoints:=1;
		IF p1_start IS NULL THEN
			pointslist[1]:=  p2_end ;
		ELSE
			pointslist[1]:=  p1_start ;
		END IF;
		return pointslist;
	END IF;

	select ports.portic_order_intersection(p1_start , st_makeline(p1_start, p2_end)) into test;


	if test[1] is null 
	then 
		--RAISE notice '% STOP here, direct',NOW() ;
		finished:= true;
		-- Mettre le premier point : le port de départ, mais à 1 mile de la cote (IMPORTANT)
		pointslist[0]:=  p1_start;
		-- Mettre le dernier point : le port d'arrivée, mais à 1 mile de la cote (IMPORTANT)
		pointslist[1]:=  p2_end ;
		nbPoints:=2;
		return pointslist;
	else 
		--RAISE notice ' Test p1 % p2 %', test[1], test[2];
		finished:= false;
		-- Mettre le premier point : le port de départ
		pointslist[0]:=  p1_start ;
		-- p1:=pointslist[0]; 
		-- ajout le 14/03/2022 pour test Marennes-Honfleur avec 100 km
		previousPoint:= pointslist[0];
		nbPoints:= nbPoints+1;
	end if;

	--select  coalesce(port_on_line_1mile, point3857) into p2 from  ports.port_points  where   uhgs_id = p2_uhgsid ;

	distance_ratio:= p_distance_ratio;
	-- iter2 = 1 : faisait bien marcher
	while finished is false and iter2 < 10 loop
	
		RAISE notice '###### subpoints_on_path : Grand boucle iter2 % test[1] % test[2] %', iter2, st_asewkt(test[1]) , st_asewkt(test[2]);
		shoreid:=  s.id from navigoviz.shoreline s  where s.shoreline is not null and s.area > 1 
			and st_intersects(s.shoreline, st_buffer(st_setsrid(st_makeline(test[1], test[2]), 3857), 10)) 
			order by st_distance(test[1],ST_ClosestPoint(shoreline, test[1])) asc 
			limit 1;
		RAISE notice '###### subpoints_on_path : Identifiant du shoreline à contourner %', shoreid ;
		-- assertion : les deux points test partagent la même shoreline car deux points successifs appartiennent à la même shoreline qu'ils coupent par l'intérieur
		f1 :=  ST_LineLocatePoint(s.shoreline, test[1]) 
			from navigoviz.shoreline s 
			where s.id = shoreid;
		-- st_addpoint(s.shoreline, test[1], 0)
		f2 :=  ST_LineLocatePoint(s.shoreline, test[2]) 
			from navigoviz.shoreline s 
			where s.id = shoreid;
		
		-- On reduit l'offset au fur et à mesure des itérations iter2
		select g into m_seg from (
			-- parfois ST_OffsetCurve renvoie des polylines, ce qui ne marche pas. Donc on prend la plus GRANDE.
			select ST_NumGeometries(geom),  (st_dump(geom)).geom as g
			from (
			select ST_OffsetCurve(ST_LineSubstring(s.shoreline, case when f1<f2 then f1 else f2 end, case when f1<f2 then f2 else f1 end), m_offset/(iter2+1)) as geom
						from navigoviz.shoreline s 
						where s.id = shoreid
			) as k
			) as q
		order by  st_length(g) desc limit 1;--desc avant
		longueur_segment:= st_length(m_seg);
		RAISE notice '###### subpoints_on_path : Longueur du segment en offset %', longueur_segment ;
		
		-- On itère 10 fois max sur un dixième du segment.
		iter:= 0;
		distance_ratio:= longueur_segment/10;
		--distance_ratio:=p_distance_ratio/(2*iter2+1) ; -- on réduit l'allonge vers l'arrivée
		while finished is false and m_seg is not null and iter < 9 loop 
		
			ratio:= case when f1> f2 then 1 - (distance_ratio* (iter+1) / st_Length(m_seg)) else (distance_ratio* (iter+1) / st_Length(m_seg)) end;
			--ratio:= (distance_ratio* (iter+1) / st_Length(m_seg)) ;
			-- raise notice 'valeur du ratio % - doit etre dans [0, 1]',ratio;
			
			if ratio >= 0 and ratio <= 1   then 
						
				-- Place a point on segment that is parallel  to shoreline
				select ST_LineInterpolatePoint(m_seg, ratio) into p1; 
				IF p1 IS NULL THEN
					RAISE notice '###### BUG BUG 1 subpoints_on_path : ST_LineInterpolatePoint donne null ratio % m_seg %', ratio, st_asewkt(m_seg);
				END IF;
			
				-- test si le point p1 est bien en mer
				aterre:= s.id from navigoviz.shoreline s where st_contains(s.polygone3857, p1);
		
				-- empêche que le nouveau point soit sur terre
				/*
				select ports.portic_move_pointp1_intosea(p1) into pointp1prime;
				IF pointp1prime IS NULL THEN
					RAISE notice '###### BUG BUG 2 subpoints_on_path : move_pointp1_intosea donne null en bougeant p1 % ',  st_asewkt(p1);
				END IF;
				*/
			
				-- move this point if required (test if it intersects the previous point and do the job)
				-- attention, présuppose que pointslist contient toujours le premier point, 
				-- premier point à 1 mile des cotes pour éviter une intersection.
				-- select  ports.move_pointp1(previousPoint, p1, m_offset, p_distance_ratio ) into p1;
				/*
				select ports.move_pointp1(previousPoint, p1, m_offset, round(distance_ratio)::integer  ) into p1;
				IF p1 IS NULL THEN
					RAISE notice '###### BUG BUG 3 subpoints_on_path : move_pointp1 donne null en bougeant p1 % avec m_offset % et distance % et previousPoint %', st_asewkt(p1),  m_offset, round(distance_ratio)::integer, st_asewkt(previousPoint);
				END IF;
				*/
				
				/*
				-- logguer les essais
				insert into navigoviz.temp_points (iter2, maxiter, iter, previousPoint, pointp1, pointp1prime, shoreid, segment, longueur_segment, f1, f2, newoffset, distance_ratio, ratio) 
				values (iter2, 9, iter, previousPoint, p1, pointp1prime, shoreid, m_seg, longueur_segment, f1, f2, m_offset/(iter2+1), distance_ratio, ratio);
				*/
				
				
				-- on ne tient pas compte de move_pointp1_intosea (test)
				--p1 := pointp1prime;
				
				-- test if finished for this shoreline
				select ports.portic_order_intersection(p1, st_makeline(p1, p2_end)) into test;
			
				if test[1] is null  THEN
					finished:= true;
				end if;
				-- comment
				-- raise notice '###### subpoints_on_path - Segment direct ? % Iteration % ', finished, iter;
				
				-- On ajoute le point à la liste que si il n'est pas sur une terre. 
				if p1 is NOT NULL AND aterre IS NULL THEN
					pointslist[nbPoints]:=p1;
					nbPoints:= nbPoints+1;
					previousPoint:=p1;
				end if;
				iter:= iter+1;
			end if;
			
		end loop;
	
		-- fini, vraiment ? Il y a une autre shoreline à contourner 
		-- fix 18 juillet previousPoint au lieu de p1
		select ports.portic_order_intersection(previousPoint, st_makeline(previousPoint, p2_end)) into test;
		
	
		if test[1] is null 
		then 
			RAISE notice '###### subpoints_on_path : STOP here,  iteration2  % ', iter2 ;
			finished:= true;
		else 
			RAISE notice '###### subpoints_on_path : INCREMENT SUR iter2 Test p1 % p2 % iter2 %', st_asewkt(test[1]), st_asewkt(test[2]), iter2;
			finished:= false;
		end if;
	
		iter2:= iter2+1;--offset va diminuer pour la prochaine itération.
	end loop;

	-- Mettre le dernier point : le port d'arrivée
	pointslist[nbPoints]:=  p2_end;
	nbPoints:= nbPoints+1;

	pointslist := ports.portic_simplifypath(pointslist);

	return pointslist;
END;
$function$
;

-- Permissions

ALTER FUNCTION ports.portic_subpoints_on_path(geometry, geometry, float8, int4) OWNER TO postgres;
GRANT ALL ON FUNCTION ports.portic_subpoints_on_path(geometry, geometry, float8, int4) TO postgres;

-- https://wiki.postgresql.org/wiki/Array_reverse
-- drop FUNCTION ports.portic_array_reverse(anyarray);
CREATE OR REPLACE FUNCTION ports.portic_array_reverse(anyarray)
 RETURNS geometry[]
 LANGUAGE plpgsql
AS $function$
declare pointslist geometry[];
declare indexi int;
declare indexj int;
begin
    raise notice 'taille du tableau de points en entrée : %', array_length($1, 1);
	indexi:=array_length($1, 1)-1;
	indexj:=0;
	while indexi >= 0 loop
		pointslist[indexj] := $1[indexi];
		indexi:= indexi-1;
		indexj:=indexj+1;
	end loop;
	--pointslist[indexj] := anyarray[0];
	/*
    SELECT $1[i] into pointslist[i]
    FROM generate_subscripts($1,1) AS s(i)
    ORDER BY i desc;
    */
   	raise notice 'taille du tableau de points en sortie: %', array_length(pointslist, 1);
    return pointslist;
end
$function$
;

-- Permissions

ALTER FUNCTION ports.portic_array_reverse(anyarray) OWNER TO postgres;
GRANT ALL ON FUNCTION ports.portic_array_reverse(anyarray) TO postgres;

CREATE OR REPLACE FUNCTION ports.portic_test_pathlength(p1_uhgsid text, p2_uhgsid text, m_offset double precision, p_distance_ratio integer)
 RETURNS double precision
 LANGUAGE plpgsql
AS $function$
/*
Calcule les points intermédiaires permettant de tracer des segments qui relient deux ports sans croiser de terre
Donner les identifiant UHGS des deux ports , un offset (5 km) pour l'éloignement à la côte, 
et une distance (10 km) pour l'espacement entre les points sur l'offset
https://trac.osgeo.org/postgis/wiki/UsersWikiExamplesInterpolateWithOffset
requires : 
CREATE TYPE ports.portic_computedPath AS
(
    pointslist geometry[]      ,
    nbPoints  int,
    duration float
);
*/
	declare m_seg geometry;
	declare p1 geometry;
	declare p2 geometry;
	declare f1 double precision;
	declare f2 double precision;
	declare longueur_segment double precision;
	declare ratio double precision;
	declare pointslist geometry[];
	declare test geometry[];
	declare nbPoints int;
	declare shoreid text;
	declare previousPoint geometry;
	declare distance_ratio double precision;
	DECLARE k int;
	declare f2prime double precision;
	
	
begin
	RAISE notice '###################################### % p1 % p2 %', now(), p1_uhgsid, p2_uhgsid ;
	nbPoints:=0;

	
	--p1.point3857
	select ports.portic_order_intersection(coalesce(p1.port_on_line_1mile, p1.point3857), st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile)) into test
	from ports.port_points p1 , ports.port_points p2
	where   p1.uhgs_id = p1_uhgsid and p2.uhgs_id = p2_uhgsid ;

	
	if test[1] is null 
	then 
		--RAISE notice '% STOP here, direct',NOW() ;
		-- Mettre le premier point : le port de départ, mais à 1 mile de la cote (IMPORTANT)
		pointslist[0]:=  coalesce(port_on_line_1mile, point3857) from  ports.port_points p where   p.uhgs_id = p1_uhgsid ;
		-- Mettre le dernier point : le port d'arrivée, mais à 1 mile de la cote (IMPORTANT)
		pointslist[1]:=  coalesce(port_on_line_1mile, point3857) from  ports.port_points p where   p.uhgs_id = p2_uhgsid ;
		nbPoints:=2;
	
		longueur_segment:=st_length(st_makeline(pointslist[0],pointslist[1])) ;

		update navigoviz.routepaths set direct_length = longueur_segment, broader_length = null 
		where  from_uhgs_id = p1_uhgsid and to_uhgs_id = p2_uhgsid;
	
		return longueur_segment/1000;
	else 
		RAISE notice ' Test p1 % p2 %', st_asewkt(test[1]), st_asewkt(test[2]);
		-- Mettre le premier point : le port de départ
		pointslist[0]:=  coalesce(port_on_line_1mile, point3857)  from  ports.port_points p where   p.uhgs_id = p1_uhgsid ;
		p1:=pointslist[0]; 
		-- ajout le 14/03/2022 pour test Marennes-Honfleur avec 100 km
		previousPoint:= pointslist[0];
		nbPoints:= nbPoints+1;
	end if;

	select  coalesce(port_on_line_1mile, point3857) into p2 from  ports.port_points  where   uhgs_id = p2_uhgsid ;

	RAISE notice  'DECOUPE de p1 % p2 %',  st_asewkt(p1), st_asewkt(p2) ;

	distance_ratio:= p_distance_ratio;
	
	--k:= 2;
	k:= array_length(test, 1);
	m_seg:= NULL;
	while m_seg IS NULL AND k > 1 loop
		
		RAISE notice 'Grande boucle  test[1] % test[2] %',  st_asewkt(test[1]) , st_asewkt(test[k]);
		RAISE notice 'Distance k % entre test[1] et test[2] %', k, ST_Distance(test[1], test[k]);
		shoreid:=  s.id from navigoviz.shoreline s  where s.shoreline is not null and s.area > 1 
			and st_intersects(s.shoreline, st_buffer(st_setsrid(st_makeline(test[1], test[k]), 3857), 10)) 
			order by st_distance(test[1],ST_ClosestPoint(shoreline, test[1])) asc 
			limit 1;
		RAISE notice 'Identifiant du shoreline à contourner %', shoreid ;
		-- assertion : les deux points test partagent la même shoreline car deux points successifs appartiennent à la même shoreline qu'ils coupent par l'intérieur
		f1 :=  ST_LineLocatePoint(s.shoreline, test[1]) 
			from navigoviz.shoreline s 
			where s.id = shoreid;
		f2 :=  ST_LineLocatePoint(s.shoreline , test[k]) 
			from navigoviz.shoreline s 
			where s.id = shoreid;
		RAISE notice 'cas comme AVANT , f1 % f2 % f1-f2 = %', f1, f2, case when f1> f2 then f1-f2 else f2-f1 end ;
		f2prime :=  ST_LineLocatePoint(st_addpoint(s.shoreline, test[1], 0) , test[k]) 
			from navigoviz.shoreline s 
			where s.id = shoreid;
		--RAISE notice 'f1 % f2 %', f1, f2 ;
		RAISE notice 'CAS où test1 devient le début, f1 % f2 % f1-f2 = %', 0, f2prime, f2prime ;
		if (f2prime < Abs(f2-f1)) then 			
			-- parfois ST_OffsetCurve renvoie des polylines, ce qui ne marche pas. Donc on prend la plus petite.
			select g into m_seg from (
				select ST_NumGeometries(geom),  (st_dump(geom)).geom as g
				from (
				select ST_OffsetCurve(ST_LineSubstring(st_addpoint(s.shoreline, test[1], 0), 0, f2prime), m_offset) as geom
							from navigoviz.shoreline s 
							where s.id = shoreid
				) as k
				) as q
			--where st_intersects(g, test[1]) and st_intersects(g, test[2]);
			order by  st_length(g) asc limit 1; 	
			f1:=0;	
			f2:=f2prime;
		else
			select g into m_seg from (
				select ST_NumGeometries(geom),  (st_dump(geom)).geom as g
				from (
				select ST_OffsetCurve(ST_LineSubstring(s.shoreline,case when f1<f2 then f1 else f2 end, case when f1<f2 then f2 else f1 end), m_offset) as geom
							from navigoviz.shoreline s 
							where s.id = shoreid
				) as k
				) as q
			--where st_intersects(g, test[1]) and st_intersects(g, test[2]);
			order by  st_length(g) desc limit 1; --comme avant
		end if;
		longueur_segment:= st_length(m_seg);
		

	
		RAISE notice 'Longueur du segment en offset %', longueur_segment ;
		k:=k-1;
	END loop;
	
	update navigoviz.routepaths set direct_length=st_length(st_makeline(p1,p2)),  broader_length = longueur_segment 
	where  from_uhgs_id = p1_uhgsid and to_uhgs_id = p2_uhgsid;

	return longueur_segment/1000;
END;
$function$
;

-- Permissions

ALTER FUNCTION ports.portic_test_pathlength(text, text, float8, int4) OWNER TO postgres;
GRANT ALL ON FUNCTION ports.portic_test_pathlength(text, text, float8, int4) TO postgres;

---
-- prerequis : installer les tables shoreline et shoreline_valid_20mile
---



-- V8 
-- select uhgs_id, toponyme , toponyme_standard_fr, country2019_name , state_1789_fr , province , distance_shoreline, port_on_shoreline, id_shoreline
-- from ports.port_points where type_cote is null and point3857 is not null; 

--A1964713	Mediterranée	Mer Mediterranée		zone maritime
--A0037077	Canal de Malte	Canal de Malte	Italy	Malte
--A1969359	A 12 lieues du Cap Saint Sébastien			
--A1964261	Au sud de Mayorque			
--B0000978	Colonies anglaises	colonies anglaises [en Amérique]		Grande-Bretagne
--C0000006	Côte d' Afrique [de l' Ouest]	Côte d'Afrique [de l'Ouest]		multi-Etat
--C0000019	Cap Vert	Cap-Vert	Cape Verde	Portugal
--A1967566	Sur Mahon			
--A1965228	25 à 30 lieues du Cap Sicié			
--A1964702	A la hauteur d' Alicante			
--A1965021	Sur Carthagène			
--A1965240	Sur les parages d' Arzeoun			
--B0000715	Banc de Terre Neuve	Grands Bancs de Terre-Neuve		zone maritime
--A1964251	Dans le Golfe de Lyon			
--A1969437	Au sud est de l' Alboran		Spain	
--A1964250	A la hauteur d' Yvice			
--A1968859	27 lieues au sud de Malte	Malte (27 lieues au sud)	Malta	zone maritime
--D0000004	Inde	Indes orientales		multi-Etat
--A1968875	35 degrés de latitude nord et 45 degrés de longitude du méridien de Paris	35 lat. nord et 45 long. du méridien de Paris		zone maritime
--B0000979	Grand Baye	Grande Baie		
--A1969357	Dix lieues à l' ouest de Palamos			
--A1968880	à 18 lieues au nord de Mayorque	Majorque (18 lieues au nord)	Spain	zone maritime
--A1964164	Au sud de Mahon			
--A1969436	entre Zante et Tunis			
--B0000713	Baÿe de Hara en Terreneuve	Baie de Hara	Canada	Grande-Bretagne
--A0220290	Cote des Bouq	Port-de-Bouc (sur)	France	France
--D3674030	Bien Dong	Chine		Chine
--A1965005	Cotte d' Afrique	Côtes de Barbarie	Algeria	multi-Etat
--A1965020	Par le travers de Carthagène	Par le travers de Carthagène	Spain	zone maritime
--B0000935	Groenland	Groenland		Danemark
--A1968882	à dix lieues du cap Saint Antoine	cap Saint-Antoine (à 2 lieues)	Spain	zone maritime
--C0000013	Mozambique	Mozambique		Portugal
--B0000970	Colonies américaines	Colonies françaises en Amérique	Guadeloupe	France
--A1963986	Baltique	Mer Baltique		zone maritime

/*
alter table ports.port_points add column port_on_shoreline geometry;
alter table ports.port_points add column id_shoreline  text;
alter table ports.port_points add column distance_shoreline float;
alter table ports.port_points  add column type_cote text;
alter table ports.port_points add column port_on_line_1mile geometry;

select count(*) from ports.port_points
-- 1205
-- 1706 en v8

-- calculer id_shoreline 
select count(*) from ports.port_points where port_on_shoreline is null; --299 -- , id_shoreline, distance_cote, type_cote 
select count(*) from ports.port_points where ogc_fid is null; --273
select count(*) from ports.port_points where point3857 is not null -- 1699/1706
select * from ports.port_points where ogc_fid is not null; --273

sudo -u postgres psql -U postgres -d portic_v8 -f /home/plumegeo/portic/portic_V8/portic_V8_shoreline16jan2023.sql

sudo -u postgres psql -U postgres -d portic_v8 -f /home/plumegeo/portic/portic_V8/portic_V8_shoreline20miles16jan2023.sql

-- en V8  : calculer id_shoreline - Pour les ports à terre ou proches de la cote
update  ports.port_points ports set port_on_shoreline = k2.port_on_shoreline, id_shoreline = k2.id, distance_shoreline=k1.dmin, type_cote='1mile'
from (
	select p.uhgs_id, min(st_distance(c.shoreline, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline c
			  where port_on_shoreline is null and st_contains(c.buffer_1mile, point3857)   
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.id, st_distance(c.shoreline , point3857) as d, ST_ClosestPoint(shoreline, point3857) as port_on_shoreline, '1mile' as type_cote
			  from ports.port_points p, navigoviz.shoreline c
			  where port_on_shoreline is null and st_contains(c.buffer_1mile, point3857) 
    ) as k2
where ports.port_on_shoreline is null and ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- 210 

-- en V8  : calculer id_shoreline - Pour les ports en mer ou loin de la cote
update  ports.port_points ports set port_on_shoreline = k2.port_on_shoreline, id_shoreline = k2.id, distance_shoreline=k1.dmin, type_cote='20mile'
from (
	select p.uhgs_id, min(st_distance(c.line_20_mile, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline_valid_20mile  c
			  where point3857 is not null and port_on_shoreline is null and st_contains(c.wkb_geometry, point3857)   
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.id, st_distance(c.line_20_mile , point3857) as d, ST_ClosestPoint(c.line_20_mile, point3857) as port_on_shoreline, '1mile' as type_cote
			  from ports.port_points p, navigoviz.shoreline_valid_20mile c
			  where point3857 is not null and port_on_shoreline is null and st_contains(c.wkb_geometry, point3857) 
    ) as k2
where ports.port_on_shoreline is null and ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- 48 en  4 min : 2min30 sur la nouvelle machine

-- correction de id_shoreline avec ogc_fid (qui sinon vaut toujours 1)
update  ports.port_points ports set port_on_shoreline = k2.port_on_shoreline, id_shoreline = k2.ogc_fid, distance_shoreline=k1.dmin, type_cote='20mile'
from (
	select p.uhgs_id, min(st_distance(c.line_20_mile, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline_valid_20mile  c
			  where point3857 is not null and type_cote='20mile' and st_contains(c.wkb_geometry, point3857)   
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.ogc_fid, st_distance(c.line_20_mile , point3857) as d, ST_ClosestPoint(c.line_20_mile, point3857) as port_on_shoreline
			  from ports.port_points p, navigoviz.shoreline_valid_20mile c
			  where point3857 is not null and type_cote='20mile' and st_contains(c.wkb_geometry, point3857) 
    ) as k2
where type_cote='20mile'  and ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- correction : 103 en  6min

-- en V8  : calculer id_shoreline - Pour les ports très loin de la cote (34 dont / 3 à 50 km, 14 à 100 km

update  ports.port_points ports set port_on_shoreline = k2.port_on_shoreline, id_shoreline = k2.id, distance_shoreline=k1.dmin, type_cote='50km'
from (
	select p.uhgs_id, min(st_distance(c.shoreline, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline c
			  where port_on_shoreline is null and st_contains(st_buffer(c.buffer_1mile, 50000), point3857)   
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.id, st_distance(c.shoreline , point3857) as d, ST_ClosestPoint(shoreline, point3857) as port_on_shoreline, '50km' as type_cote
			  from ports.port_points p, navigoviz.shoreline c
			  where port_on_shoreline is null and st_contains(st_buffer(c.buffer_1mile,50000), point3857) 
    ) as k2
where ports.port_on_shoreline is null and ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- 3 en 3 min
update  ports.port_points ports set port_on_shoreline = k2.port_on_shoreline, id_shoreline = k2.id, distance_shoreline=k1.dmin, type_cote='100km'
from (
	select p.uhgs_id, min(st_distance(c.shoreline, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline c
			  where port_on_shoreline is null and st_contains(st_buffer(c.buffer_1mile, 100000), point3857)   
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.id, st_distance(c.shoreline , point3857) as d, ST_ClosestPoint(shoreline, point3857) as port_on_shoreline, '100km' as type_cote
			  from ports.port_points p, navigoviz.shoreline c
			  where port_on_shoreline is null and st_contains(st_buffer(c.buffer_1mile,100000), point3857) 
    ) as k2
where ports.port_on_shoreline is null and ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- 14 en 3 s

----------------------------------------
-- IMPORTANT : port_on_line_1mile
----------------------------------------
update  ports.port_points ports set port_on_line_1mile = k2.port_on_line_1mile
from (
	select p.uhgs_id, min(st_distance(c.line_1mile, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline c
			  where  point3857 is not null and st_contains(c.buffer_1mile , point3857)     
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.id, st_distance(c.line_1mile , point3857) as d, ST_ClosestPoint(line_1mile, point3857) as port_on_line_1mile, '1mile' as type_cote
			  from ports.port_points p, navigoviz.shoreline c
			  where point3857 is not null and st_contains(buffer_1mile, point3857) 
    ) as k2
where  ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- 1573 en 1 min

update  ports.port_points ports set port_on_line_1mile = k2.port_on_line_1mile, id_shoreline = k2.id, distance_shoreline=k1.dmin
from (
	select p.uhgs_id, min(st_distance(c.line_1mile, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline  c
			  where port_on_line_1mile is null and  point3857 is not null    
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.id, st_distance(c.line_1mile , point3857) as d, ST_ClosestPoint(c.line_1mile, point3857) as port_on_line_1mile, '1mile' as type_cote
			  from ports.port_points p, navigoviz.shoreline c
			  where port_on_line_1mile is null and point3857 is not null 
    ) as k2
where ports.port_on_line_1mile is null and ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- 126 pour compléter

*/