

-- ca merdouille à cause des tonnages
select p.pointcall_uhgs_id, count(p.pkid) as total , count(tonnage) as nb_tonnage_filled, count(homeport) as nb_homeport_filled,
sum(tonnage::float) as bad_sum_tonnage
from navigoviz.pointcall p 
where state_1789_fr = 'France' and pointcall_function = 'O'
group by  p.pointcall_uhgs_id 




select distinct tonnage_unit, count(*)  from navigoviz.pointcall p group by tonnage_unit
-- tx ou quintaux

select  (tonnage::float)/24  from navigoviz.pointcall p where tonnage_unit = 'quintaux';

select pkid, tonnage, homeport, (case when tonnage_unit = 'quintaux' then (tonnage::float)/24  else tonnage::float end) as tonnage_numeric
from navigoviz.pointcall p
where tonnage_unit is not null


-- ca marche super
select pointcall_uhgs_id, source_suite,
count(pkid) as total , 
count(tonnage) as nb_tonnage_filled, 
count(homeport) as nb_homeport_filled,
count(commodity_purpose) as nb_product_filled, 
count(birthplace) as nb_birthplace_filled,
 count(citizenship) as nb_citizenship_filled,
sum(tonnage_numeric) as good_sum_tonnage
from 
(select source_suite, pointcall_uhgs_id, pkid, tonnage, homeport, commodity_purpose, birthplace, citizenship, 
(case when tonnage_unit = 'quintaux' then (tonnage::float)/24  else tonnage::float end) as tonnage_numeric
from navigoviz.pointcall p
where state_1789_fr = 'France' and pointcall_function = 'O'
) as k
-- where state_1789_fr = 'France' and pointcall_function = 'O'
group by  pointcall_uhgs_id , source_suite

select  * from navigoviz.pointcall p 
where source_doc_id = '00364008'
order by pointcall_rankfull 
-- marseille

select distinct source_suite, source_component from navigoviz.pointcall


select pp.* , toto.*
from ports.port_points pp ,
	(
		select pointcall_uhgs_id, source_suite,
		count(pkid) as total , 
		count(tonnage) as nb_tonnage_filled, 
		count(homeport) as nb_homeport_filled,
		count(commodity_purpose) as nb_product_filled, 
		count(birthplace) as nb_birthplace_filled,
		 count(citizenship) as nb_citizenship_filled,
		sum(tonnage_numeric) as good_sum_tonnage
		from (
			select source_suite, pointcall_uhgs_id, pkid, tonnage, homeport, commodity_purpose, birthplace, citizenship, 
			(case when tonnage_unit = 'quintaux' then (tonnage::float)/24  else tonnage::float end) as tonnage_numeric
			from navigoviz.pointcall p
			where state_1789_fr = 'France' and pointcall_function = 'O'
		) as k
	group by  pointcall_uhgs_id , source_suite
	) as toto
where toto.pointcall_uhgs_id = pp.uhgs_id


select nb_conges_1787_inputdone, nb_conges_1787_cr, nb_conges_1789_inputdone, nb_conges_1789_cr,
ogc_fid, uhgs_id, toponyme_standard_fr as  toponym, substate_1789_fr as substate, status, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    source_suite, pointcall_year, total, nb_tonnage_filled, nb_homeport_filled, nb_product_filled, nb_birthplace_filled, nb_citizenship_filled, good_sum_tonnage
from
    (
        select 
        pointcall_uhgs_id, source_suite, pointcall_year,
        --nb_conges_1787_inputdone, nb_conges_1787_cr, nb_conges_1789_inputdone, nb_conges_1789_cr,
        count(pkid) as total ,
        count(tonnage) as nb_tonnage_filled,
        count(homeport) as nb_homeport_filled,
        count(commodity_purpose) as nb_product_filled,
        count(birthplace) as nb_birthplace_filled,
        count(citizenship) as nb_citizenship_filled,
        sum(tonnage_numeric) as good_sum_tonnage
        from (
            select pointcall_uhgs_id, source_suite,
            (coalesce(extract(year from TO_DATE(pointcall_out_date2,'YYYY=MM=DD')), extract(year from TO_DATE(pointcall_in_date2,'YYYY=MM=DD')))) as pointcall_year,
            --nb_conges_1787_inputdone, nb_conges_1787_cr, nb_conges_1789_inputdone, nb_conges_1789_cr,
            pkid, tonnage, homeport, commodity_purpose, birthplace, citizenship,
            --nb_conges_1787_inputdone, nb_conges_1787_cr, nb_conges_1789_inputdone, nb_conges_1789_cr,
            (case when tonnage_unit = 'quintaux' then (tonnage::float)/24  else tonnage::float end) as tonnage_numeric,
             nb_conges_1787_inputdone, nb_conges_1787_cr, nb_conges_1789_inputdone, nb_conges_1789_cr
            from navigoviz.pointcall p
            where state_1789_fr = 'France' and pointcall_function = 'O'
        ) as k
    group by  pointcall_uhgs_id, source_suite, pointcall_year
    ) as toto,
    ports.port_points pp
where toto.pointcall_uhgs_id = pp.uhgs_id
-- aide le 23/12/2021


select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    source_suite, pointcall_year,
    (case when pointcall_year = '1787' then nb_conges_1787_inputdone else nb_conges_1789_inputdone end) as nb_conges_inputdone,    
    (case when pointcall_year = '1787' then nb_conges_1787_cr else nb_conges_1789_cr end) as nb_conges_cr,
    (case when pointcall_year = '1787' then nb_sante_1787 else nb_sante_1789 end) as nb_conges_sante,
    (case when pointcall_year = '1787' then nb_petitcabotage_1787 else nb_petitcabotage_1789 end) as nb_petitcabotage,
    nb_tonnage_filled, nb_homeport_filled, nb_product_filled, nb_birthplace_filled, nb_citizenship_filled, good_sum_tonnage
from ports.port_points pp ,
    (
        select 
        pointcall_uhgs_id, source_suite, pointcall_year, 
        count(tonnage) as nb_tonnage_filled,
        count(homeport) as nb_homeport_filled,
        count(commodity_purpose) as nb_product_filled,
        count(birthplace) as nb_birthplace_filled,
        count(citizenship) as nb_citizenship_filled,
        sum(tonnage_numeric) as good_sum_tonnage
        from (
            select pointcall_uhgs_id, source_suite,
            (coalesce(extract(year from TO_DATE(substring(pointcall_out_date2 for 4),'YYYY')), extract(year from TO_DATE(substring(pointcall_in_date2 for 4),'YYYY'))))::int4 as pointcall_year, 
            nb_conges_1787_inputdone, nb_conges_1787_cr, nb_conges_1789_inputdone, nb_conges_1789_cr, 
            tonnage, homeport, commodity_purpose, birthplace, citizenship,  
            (case when tonnage_unit = 'quintaux' then (tonnage::float)/24  else tonnage::float end) as tonnage_numeric  
            from navigoviz.pointcall p
            where state_1789_fr = 'France' and pointcall_function = 'O'
        ) as k
    group by  pointcall_uhgs_id, source_suite, pointcall_year
    ) as toto
where toto.pointcall_uhgs_id = pp.uhgs_id 
order by toponym, pointcall_year, source_suite

 


-- le 4 janvier
select ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    nb_sante_1787, nb_sante_1789, nb_petitcabotage_1789, nb_petitcabotage_1787
from ports.port_points pp    
    where uhgs_id = 'A0210797'
    
select ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    nb_sante_1787, nb_sante_1789, nb_petitcabotage_1789, nb_petitcabotage_1787
from ports.port_points pp    
    where uhgs_id = 'A0210797'
    
-- Vu avec Silvia le 05 janvier 2021 : il manque Vannes par exemple 

select distinct source_suite , source_subset from navigoviz.pointcall where source_suite = '%Marseille%'
 select distinct  source_subset from navigoviz.pointcall where source_suite = '%Marseille%'
-- Registre du petit cabotage (1786-1787)
-- Expéditions "coloniales" Marseille (1789)
-- Santé Marseille
 
update navigoviz.pointcall set source_suite = source_subset where source_suite = 'Marseille';

-- version 1
select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    source_suite, pointcall_year,
    (case when pointcall_year = '1787' then nb_conges_1787_inputdone else nb_conges_1789_inputdone end) as nb_conges_inputdone,    
    (case when pointcall_year = '1787' then nb_conges_1787_cr else nb_conges_1789_cr end) as nb_conges_cr,
    (case when pointcall_year = '1787' then nb_sante_1787 else nb_sante_1789 end) as nb_conges_sante,
    (case when pointcall_year = '1787' then nb_petitcabotage_1787 else nb_petitcabotage_1789 end) as nb_petitcabotage,
    nb_tonnage_filled, nb_homeport_filled, nb_product_filled, nb_birthplace_filled, nb_citizenship_filled, good_sum_tonnage
from ports.port_points pp , 
    (
        select 
        pointcall_uhgs_id, source_suite, pointcall_year, 
        count(tonnage) as nb_tonnage_filled,
        count(homeport) as nb_homeport_filled,
        count(commodity_purpose) as nb_product_filled,
        count(birthplace) as nb_birthplace_filled,
        count(citizenship) as nb_citizenship_filled,
        sum(tonnage_numeric) as good_sum_tonnage
        from (
            select pointcall_uhgs_id, source_suite,
            (coalesce(extract(year from TO_DATE(substring(pointcall_out_date2 for 4),'YYYY')), extract(year from TO_DATE(substring(pointcall_in_date2 for 4),'YYYY'))))::int4 as pointcall_year, 
            nb_conges_1787_inputdone, nb_conges_1787_cr, nb_conges_1789_inputdone, nb_conges_1789_cr, 
            tonnage, homeport, commodity_purpose, birthplace, citizenship,  
            (case when tonnage_unit = 'quintaux' then (tonnage::float)/24  else tonnage::float end) as tonnage_numeric  
            from navigoviz.pointcall p
            where state_1789_fr = 'France' and pointcall_function = 'O'
        ) as k
    group by  pointcall_uhgs_id, source_suite, pointcall_year
    ) as toto
-- on toto.pointcall_uhgs_id = pp.uhgs_id 
where toto.pointcall_uhgs_id = pp.uhgs_id and pp.state_1789_fr = 'France' -- and province = 'Bretagne' -- and pointcall_year = 1787 
union
(select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    'G5' as source_suite, 1787 as pointcall_year, 
    nb_conges_1787_inputdone as nb_conges_inputdone, 
    nb_conges_1787_cr as nb_conges_cr,
    nb_sante_1787 as nb_conges_sante,
    nb_petitcabotage_1787 as nb_petitcabotage,
    null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as good_sum_tonnage
from ports.port_points pp
where state_1789_fr = 'France' and nb_conges_1787_cr is not  null and nb_conges_1787_inputdone is null)
union 
(select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    'G5' as source_suite, 1789 as pointcall_year, 
    nb_conges_1789_inputdone as nb_conges_inputdone, 
    nb_conges_1789_cr as nb_conges_cr,
    nb_sante_1789 as nb_conges_sante,
    nb_petitcabotage_1789 as nb_petitcabotage,
    null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as good_sum_tonnage
from ports.port_points pp
where state_1789_fr = 'France' and nb_conges_1789_cr is not  null and nb_conges_1789_inputdone is null)
order by toponym, pointcall_year, source_suite

-- version 2
-- version avec flag et avec source_suite à null si le port n'a pas de données. 

select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    source_suite, pointcall_year,
    (case when pointcall_year = '1787' then nb_conges_1787_inputdone else nb_conges_1789_inputdone end) as nb_conges_inputdone,    
    (case when pointcall_year = '1787' then nb_conges_1787_cr else nb_conges_1789_cr end) as nb_conges_cr,
    (case when pointcall_year = '1787' then nb_sante_1787 else nb_sante_1789 end) as nb_conges_sante,
    (case when pointcall_year = '1787' then nb_petitcabotage_1787 else nb_petitcabotage_1789 end) as nb_petitcabotage,
    nb_tonnage_filled, nb_homeport_filled, nb_product_filled, nb_birthplace_filled, nb_citizenship_filled, nb_flag_filled, good_sum_tonnage
from ports.port_points pp, -- left outer join
    (
        select 
        pointcall_uhgs_id, source_suite, pointcall_year, 
        count(tonnage) as nb_tonnage_filled,
        count(homeport) as nb_homeport_filled,
        count(commodity_purpose) as nb_product_filled,
        count(birthplace) as nb_birthplace_filled,
        count(citizenship) as nb_citizenship_filled,
        count(flag) as nb_flag_filled,
        sum(tonnage_numeric) as good_sum_tonnage
        from (
            select pointcall_uhgs_id, source_suite,
            (coalesce(extract(year from TO_DATE(substring(pointcall_out_date2 for 4),'YYYY')), extract(year from TO_DATE(substring(pointcall_in_date2 for 4),'YYYY'))))::int4 as pointcall_year, 
            nb_conges_1787_inputdone, nb_conges_1787_cr, nb_conges_1789_inputdone, nb_conges_1789_cr, 
            tonnage, homeport, commodity_purpose, birthplace, citizenship,  flag,
            (case when tonnage_unit = 'quintaux' then (tonnage::float)/24  else tonnage::float end) as tonnage_numeric  
            from navigoviz.pointcall p
            where state_1789_fr = 'France' and pointcall_function = 'O'
        ) as k
    group by  pointcall_uhgs_id, source_suite, pointcall_year
    ) as toto
-- on toto.pointcall_uhgs_id = pp.uhgs_id 
 where toto.pointcall_uhgs_id = pp.uhgs_id and pp.state_1789_fr = 'France' -- and province = 'Bretagne' -- and pointcall_year = 1787 
-- where pp.state_1789_fr = 'France'
union
(select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    'G5' as source_suite, 1787 as pointcall_year, 
    nb_conges_1787_inputdone as nb_conges_inputdone, 
    nb_conges_1787_cr as nb_conges_cr,
    nb_sante_1787 as nb_conges_sante,
    nb_petitcabotage_1787 as nb_petitcabotage,
    null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled, null as good_sum_tonnage
from ports.port_points pp
where state_1789_fr = 'France' and nb_conges_1787_cr is not  null and nb_conges_1787_inputdone is null)
union 
(select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    null as source_suite, 1787 as pointcall_year, 
    nb_conges_1787_inputdone as nb_conges_inputdone, 
    nb_conges_1787_cr as nb_conges_cr,
    nb_sante_1787 as nb_conges_sante,
    nb_petitcabotage_1787 as nb_petitcabotage,
    null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled, null as good_sum_tonnage
from ports.port_points pp
where state_1789_fr = 'France' and nb_conges_1787_cr is  null and nb_conges_1787_inputdone is null and geom is not null)
union 
(select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    'G5' as source_suite, 1789 as pointcall_year, 
    nb_conges_1789_inputdone as nb_conges_inputdone, 
    nb_conges_1789_cr as nb_conges_cr,
    nb_sante_1789 as nb_conges_sante,
    nb_petitcabotage_1789 as nb_petitcabotage,
    null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled, null as good_sum_tonnage
from ports.port_points pp
where state_1789_fr = 'France' and nb_conges_1789_cr is not  null and nb_conges_1789_inputdone is null )
union 
(select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    null as source_suite, 1789 as pointcall_year, 
    nb_conges_1789_inputdone as nb_conges_inputdone, 
    nb_conges_1789_cr as nb_conges_cr,
    nb_sante_1789 as nb_conges_sante,
    nb_petitcabotage_1789 as nb_petitcabotage,
    null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled,  null as good_sum_tonnage
from ports.port_points pp
where state_1789_fr = 'France' and nb_conges_1789_cr is null and nb_conges_1789_inputdone is null and geom is not null)
order by toponym, pointcall_year, source_suite


-- version 3
-- version avec flag, has_a_clerk et avec source_suite et year à null si le port n'a pas de données. 
-- celle retenue pour l'API le 10 janvier 2022 (implication : si nbxxx = null then nbxxx = 0 côté client, dans le code de visualisation vizsources)

select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    source_suite, pointcall_year,
    (case when pointcall_year = '1787' then nb_conges_1787_inputdone else nb_conges_1789_inputdone end) as nb_conges_inputdone,    
    (case when pointcall_year = '1787' then nb_conges_1787_cr else nb_conges_1789_cr end) as nb_conges_cr,
    (case when pointcall_year = '1787' then nb_sante_1787 else nb_sante_1789 end) as nb_conges_sante,
    (case when pointcall_year = '1787' then nb_petitcabotage_1787 else nb_petitcabotage_1789 end) as nb_petitcabotage,
    nb_tonnage_filled, nb_homeport_filled, nb_product_filled, nb_birthplace_filled, nb_citizenship_filled, nb_flag_filled, good_sum_tonnage
from ports.port_points pp, -- left outer join
    (
        select 
        pointcall_uhgs_id, source_suite, pointcall_year, 
        count(tonnage) as nb_tonnage_filled,
        count(homeport) as nb_homeport_filled,
        count(commodity_purpose) as nb_product_filled,
        count(birthplace) as nb_birthplace_filled,
        count(citizenship) as nb_citizenship_filled,
        count(flag) as nb_flag_filled,
        sum(tonnage_numeric) as good_sum_tonnage
        from (
            select pointcall_uhgs_id, source_suite,
            (coalesce(extract(year from TO_DATE(substring(pointcall_out_date2 for 4),'YYYY')), extract(year from TO_DATE(substring(pointcall_in_date2 for 4),'YYYY'))))::int4 as pointcall_year, 
            nb_conges_1787_inputdone, nb_conges_1787_cr, nb_conges_1789_inputdone, nb_conges_1789_cr, 
            tonnage, homeport, commodity_purpose, birthplace, citizenship,  flag,
            (case when tonnage_unit = 'quintaux' then (tonnage::float)/24  else tonnage::float end) as tonnage_numeric  
            from navigoviz.pointcall p
            where state_1789_fr = 'France' and pointcall_function = 'O'
        ) as k
    group by  pointcall_uhgs_id, source_suite, pointcall_year
    ) as toto
 where toto.pointcall_uhgs_id = pp.uhgs_id and pp.state_1789_fr = 'France' 
union
(select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    'G5' as source_suite, 1787 as pointcall_year, 
    nb_conges_1787_inputdone as nb_conges_inputdone, 
    nb_conges_1787_cr as nb_conges_cr,
    nb_sante_1787 as nb_conges_sante,
    nb_petitcabotage_1787 as nb_petitcabotage,
    null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled, null as good_sum_tonnage
from ports.port_points pp
where state_1789_fr = 'France' and nb_conges_1787_cr is not  null and nb_conges_1787_inputdone is null)
/*union 
(select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    null as source_suite, 1787 as pointcall_year, 
    nb_conges_1787_inputdone as nb_conges_inputdone, 
    nb_conges_1787_cr as nb_conges_cr,
    nb_sante_1787 as nb_conges_sante,
    nb_petitcabotage_1787 as nb_petitcabotage,
    null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled, null as good_sum_tonnage
from ports.port_points pp
where state_1789_fr = 'France' and nb_conges_1787_cr is  null and nb_conges_1787_inputdone is null and geom is not null)*/
union 
(select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    'G5' as source_suite, 1789 as pointcall_year, 
    nb_conges_1789_inputdone as nb_conges_inputdone, 
    nb_conges_1789_cr as nb_conges_cr,
    nb_sante_1789 as nb_conges_sante,
    nb_petitcabotage_1789 as nb_petitcabotage,
    null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled, null as good_sum_tonnage
from ports.port_points pp
where state_1789_fr = 'France' and nb_conges_1789_cr is not  null and nb_conges_1789_inputdone is null )
union 
(select
    ogc_fid, uhgs_id, toponyme_standard_fr as toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
    null as source_suite, null as pointcall_year, 
    nb_conges_1789_inputdone as nb_conges_inputdone, 
    nb_conges_1789_cr as nb_conges_cr,
    nb_sante_1789 as nb_conges_sante,
    nb_petitcabotage_1789 as nb_petitcabotage,
    null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled,  null as good_sum_tonnage
from ports.port_points pp
where state_1789_fr = 'France' 
and nb_conges_1787_cr is null and nb_conges_1787_inputdone is null 
and nb_conges_1789_cr is null and nb_conges_1789_inputdone is null 
and geom is not null)
order by toponym, pointcall_year, source_suite


-- version 4 
-- le 03 nov 2022 pour calculer la part des déduits (xx_deduced)
-- nb_tonnage_deduced, nb_homeport_deduced, nb_birthplace_deduced,nb_citizenship_deduced, nb_flag_deduced, good_sum_tonnage_deduced
-- version avec flag, has_a_clerk et avec source_suite et year à null si le port n'a pas de données. 
-- celle retenue pour l'API le 03 novembre 2022 (implication : si nbxxx = null then nbxxx = 0 côté client, dans le code de visualisation vizsources)
-- 543 ports distincts sur V8 

 select
        ogc_fid, uhgs_id, toponyme_standard_fr as  toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
        source_suite, pointcall_year,
        (case when pointcall_year = '1787' then nb_conges_1787_inputdone else nb_conges_1789_inputdone end) as nb_conges_inputdone,
        (case when pointcall_year = '1787' then nb_conges_1787_cr else nb_conges_1789_cr end) as nb_conges_cr,
        (case when pointcall_year = '1787' then nb_sante_1787 else nb_sante_1789 end) as nb_conges_sante,
        (case when pointcall_year = '1787' then nb_petitcabotage_1787 else nb_petitcabotage_1789 end) as nb_petitcabotage,        
    (case when pointcall_year = '1787' then null else nb_longcours_marseille_1789 end) as nb_longcours_marseille,
    total,
        nb_tonnage_filled, nb_homeport_filled, nb_product_filled, nb_birthplace_filled, nb_citizenship_filled, nb_flag_filled, good_sum_tonnage,
    nb_tonnage_deduced, nb_homeport_deduced, nb_birthplace_deduced,nb_citizenship_deduced, nb_flag_deduced, good_sum_tonnage_deduced

        from ports.port_points pp ,
                (
                        select pointcall_uhgs_id, source_suite, pointcall_year,
            count(k.pkid) as total,
                        count(k.tonnage) as nb_tonnage_filled,
                        count(k.homeport) as nb_homeport_filled,
                        count(k.commodity_purpose) as nb_product_filled,
                        count(k.birthplace) as nb_birthplace_filled,
                        count(k.citizenship) as nb_citizenship_filled,
            count(k.flag) as nb_flag_filled,
                        sum(k.tonnage_numeric) as good_sum_tonnage,

            count(tonnage_deduced.tonnage) as nb_tonnage_deduced,
            count(homeport_deduced.homeport) as nb_homeport_deduced,
            count(birthplace_deduced.birthplace) as nb_birthplace_deduced,
            count(citizenship_deduced.citizenship) as nb_citizenship_deduced,
            count(flag_deduced.ship_flag) as nb_flag_deduced,
            sum(tonnage_deduced.tonnage_numeric) as good_sum_tonnage_deduced

                        from (
                                select pointcall_uhgs_id, source_suite, pkid,
                                (coalesce(extract(year from TO_DATE(substring(pointcall_out_date2 for 4),'YYYY')), extract(year from TO_DATE(substring(pointcall_in_date2 for 4),'YYYY'))))::int4 as pointcall_year,
                                nb_conges_1787_inputdone, nb_conges_1787_cr, nb_conges_1789_inputdone, nb_conges_1789_cr,
                                tonnage, homeport, (all_cargos::json->>0)::json->>'commodity_purpose' as commodity_purpose, birthplace, citizenship, flag,
                                (case when tonnage_unit = 'quintaux' then (tonnage::float)/24  else tonnage::float end) as tonnage_numeric
                        from navigoviz.pointcall p
                                where true and state_1789_fr = 'France' and pointcall_function = 'O' and data_block_leader_marker = 'A'
                        ) as k

            left join
            (
                select p.pkid, tonnage,
                (case when tonnage_unit = 'quintaux' then (tonnage::float)/24  else tonnage::float end) as tonnage_numeric        
                from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2
                where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.ship_tonnage = -2     
            ) as tonnage_deduced on tonnage_deduced.pkid = k.pkid

            left join
            (
                select p.pkid, p.homeport
                from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2
                where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.ship_homeport  = -2   
            ) as homeport_deduced
            on homeport_deduced.pkid = k.pkid

            left join
            (
                select p.pkid, p.birthplace
                from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2
                where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.captain_birthplace  = 
-2
            ) as birthplace_deduced
            on birthplace_deduced.pkid = k.pkid

            left join
            (
                select p.pkid, p.citizenship
                from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2
                where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.captain_citizenship  = -2
            ) as citizenship_deduced
            on citizenship_deduced.pkid = k.pkid


            left join
            (
                select p.pkid, ship_flag
                from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2
                where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.ship_flag  = -2       
            ) as flag_deduced
            on flag_deduced.pkid = k.pkid

                group by  pointcall_uhgs_id , source_suite, pointcall_year
                ) as toto
        where toto.pointcall_uhgs_id = pp.uhgs_id and pp.state_1789_fr = 'France'
    union (
        select
    ogc_fid, uhgs_id, toponyme_standard_fr as  toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as 
admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point, 'G5' as source_suite, 1787 as pointcall_year, nb_conges_1787_inputdone as nb_conges_inputdone, nb_conges_1787_cr as nb_conges_cr, nb_sante_1787 as nb_conges_sante, nb_petitcabotage_1787 as nb_petitcabotage, null as nb_longcours_marseille, null as total, null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled, null as good_sum_tonnage,null as nb_tonnage_deduced, null as nb_homeport_deduced, null as nb_birthplace_deduced, null as nb_citizenship_deduced, null as nb_flag_deduced, null as good_sum_tonnage_deduced
     from ports.port_points pp
        where state_1789_fr = 'France' and nb_conges_1787_cr is not null and nb_conges_1787_inputdone is null
    ) union (
        select
    ogc_fid, uhgs_id, toponyme_standard_fr as  toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as 
admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point, 'G5' as source_suite, 1789 as pointcall_year, nb_conges_1789_inputdone as nb_conges_inputdone, nb_conges_1789_cr as nb_conges_cr, nb_sante_1789 as nb_conges_sante, nb_petitcabotage_1789 as nb_petitcabotage, nb_longcours_marseille_1789 as nb_longcours_marseille, null as total, null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled, null as good_sum_tonnage, null as nb_tonnage_deduced, null as nb_homeport_deduced, null as nb_birthplace_deduced, null as nb_citizenship_deduced, null as nb_flag_deduced, null as good_sum_tonnage_deduced
     from ports.port_points pp
        where state_1789_fr = 'France' and nb_conges_1789_cr is not  null and nb_conges_1789_inputdone is null
    ) union (
        select
    ogc_fid, uhgs_id, toponyme_standard_fr as  toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as 
admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point, null as source_suite, null as pointcall_year, nb_conges_1787_inputdone as nb_conges_inputdone, nb_conges_1787_cr as nb_conges_cr, nb_sante_1787 as nb_conges_sante, nb_petitcabotage_1787 as nb_petitcabotage, null as nb_longcours_marseille, null as total, null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled, null as good_sum_tonnage,null as nb_tonnage_deduced, null as nb_homeport_deduced, null as nb_birthplace_deduced, null as nb_citizenship_deduced, null as nb_flag_deduced, null as good_sum_tonnage_deduced
         from ports.port_points pp
        where state_1789_fr = 'France' and nb_conges_1787_cr is null and nb_conges_1787_inputdone is null and nb_conges_1789_cr is null and nb_conges_1789_inputdone is null and geom is not null
    ) 
   
-- mail du 7 février 2023 de Silvia   
update ports.port_points set nb_conges_1787_cr = 4681 where uhgs_id = 'A0210797';

update ports.port_points set nb_sante_1787 = null;
update ports.port_points set nb_sante_1787 = 3295  where uhgs_id = 'A0210797';

select uhgs_id, nb_conges_1787_cr , nb_conges_1789_cr , nb_petitcabotage_1789 , nb_petitcabotage_1787 , nb_longcours_marseille_1789 , nb_sante_1787 , nb_sante_1789 
from port_points pp where toponyme_standard_fr = 'Marseille';
-- A0210797
-- A0210797	4681	4353	2416	2526	119	3295	3075

alter table port_points pp add column nb_conges_1749_cr int; -- 3316 pour Marseille
alter table port_points pp add column nb_conges_1759_cr int; -- 2182 pour Marseille
alter table port_points pp add column nb_conges_1769_cr int; -- 3930 pour Marseille
alter table port_points pp add column nb_conges_1779_cr int; -- 4644 pour Marseille

alter table port_points pp add column nb_sante_1749 int; -- 2291 pour Marseille
alter table port_points pp add column nb_sante_1759 int; -- 1926 pour Marseille
alter table port_points pp add column nb_sante_1769 int; -- 2320 pour Marseille
alter table port_points pp add column nb_sante_1779 int; -- 2329 pour Marseille
alter table port_points pp add column nb_sante_1799 int; -- 2629 pour Marseille


-- la Santé registre de patentes de Marseille	1787	3 296
-- la Santé registre de patentes de Marseille	1789	3 203




select nb_sante_1787 from  ports.port_points where uhgs_id = 'A0210797';

select p.suite_description__suite_title , p.component_description__component_id , p.component_description__component_short_title , p.component_description__component_source , p.component_description__suite_id 
from navigo.pointcall p 
where p.pointcall_indate like '1779%'
-- 1038 en 1799 au lieu de 2629
-- 3749 en 1779 au lieu de 2329
-- Registres de santé de Marseille (an VIII) ou Registres de santé de Marseille (1779) == component_description__component_short_title
-- ADBdR, 200E, 555/ ou ADBdR, 200E, 535/ == component_description__component_source

select pointcall_function, extract(year from indate_fixed), array_agg(distinct p.navigo_status), count(*) 
from navigoviz.pointcall p 
where p.source_suite like 'la Santé registre de patentes de Marseille' and p.pointcall_uhgs_id = 'A0210797' and  position('=' in p.pointcall_in_date) > 0
group by p.pointcall_function, extract(year from indate_fixed);

-- fix mis en place pour le debug sur la V8 (Silvia a dit que sur FileMaker elle a corrigé tous les bugs, donc ce sera bon sur la V9)

select pointcall_function, extract(year from indate_fixed), array_agg(distinct p.navigo_status), count(*) 
from navigoviz.pointcall p 
where p.source_suite like 'la Santé registre de patentes de Marseille' and p.pointcall_uhgs_id = 'A0210797' and  position('=' in p.pointcall_in_date) > 0
group by p.pointcall_function, extract(year from indate_fixed);

update navigoviz.pointcall p set pointcall_function = 'O' 
where p.source_suite like 'la Santé registre de patentes de Marseille' and p.pointcall_uhgs_id = 'A0210797' and  position('=' in p.pointcall_in_date) > 0
and navigo_status = 'PC-RS'
and pointcall_function is null ;
-- 3119

drop table navigoviz.decompte_sources_saisies;

create table navigoviz.decompte_sources_saisies as (
select  toponyme_standard_fr , uhgs_id, 1787 as annee, nb_conges_1787_cr as nb_conges_cr, nb_conges_1787_inputdone as nb_conges_inputdone
from port_points p
where  p.nb_conges_1787_cr is not null or  p.nb_conges_1787_inputdone is not null
union 
(select  toponyme_standard_fr , uhgs_id, 1789 as annee, nb_conges_1789_cr as nb_conges_cr, nb_conges_1789_inputdone as nb_conges_inputdone
from port_points p
where  p.nb_conges_1789_cr is not  null or  p.nb_conges_1789_inputdone is not null )
); --291

select  toponyme_standard_fr , uhgs_id, 1787 as annee, nb_conges_1787_cr as nb_conges_cr, nb_conges_1787_inputdone as nb_conges_inputdone
from port_points p
where  p.nb_conges_1787_cr is  null and p.nb_conges_1787_inputdone is not null

-- columns valables que pour Marseille
alter table  navigoviz.decompte_sources_saisies add column nb_conges_sante int;
alter table  navigoviz.decompte_sources_saisies add column nb_petitcabotage int;
alter table  navigoviz.decompte_sources_saisies add column nb_longcours_marseille int;

update navigoviz.decompte_sources_saisies set nb_conges_sante = 3296 where annee = 1787 and uhgs_id = 'A0210797';
-- update navigoviz.decompte_sources_saisies set nb_conges_sante = 3203 where annee = 1789 and uhgs_id = 'A0210797'; -- faux, voir le mail du 15 février de Silvia
update navigoviz.decompte_sources_saisies set nb_conges_sante = 3084  where annee = 1789 and uhgs_id = 'A0210797';

update navigoviz.decompte_sources_saisies set nb_petitcabotage = 2416 where annee = 1789 and uhgs_id = 'A0210797';
update navigoviz.decompte_sources_saisies set nb_petitcabotage = 2526 where annee = 1787 and uhgs_id = 'A0210797';
update navigoviz.decompte_sources_saisies set nb_longcours_marseille = 119 where annee = 1789 and uhgs_id = 'A0210797';

select * from navigoviz.decompte_sources_saisies;

insert into navigoviz.decompte_sources_saisies(toponyme_standard_fr , uhgs_id, annee, nb_conges_sante)
values ('Marseille', 'A0210797', 1749, 2291), ('Marseille', 'A0210797', 1759, 1926), ('Marseille', 'A0210797', 1769, 2320), ('Marseille', 'A0210797', 1779, 2329) ;
insert into navigoviz.decompte_sources_saisies(toponyme_standard_fr , uhgs_id, annee, nb_conges_sante)
values ('Marseille', 'A0210797', 1799, 2629);

select * from navigoviz.decompte_sources_saisies where uhgs_id = 'A0210797';


-- version 5 
-- le 08 fev 2023 pour récupérer les années à Marseille 1749, 1759, 1769, 1779,  1799
-- calculer la part des déduits (xx_deduced)
-- nb_tonnage_deduced, nb_homeport_deduced, nb_birthplace_deduced,nb_citizenship_deduced, nb_flag_deduced, good_sum_tonnage_deduced
-- version avec flag, has_a_clerk et avec source_suite et year à null si le port n'a pas de données. 
-- celle retenue pour l'API le 03 novembre 2022 (implication : si nbxxx = null then nbxxx = 0 côté client, dans le code de visualisation vizsources)

-- 543 ports distincts
 select
        ogc_fid, pp.uhgs_id, pp.toponyme_standard_fr as  toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
        source_suite, pointcall_year,
        d.nb_conges_inputdone as nb_conges_inputdone,
        d.nb_conges_cr as nb_conges_cr,
        d.nb_conges_sante as nb_conges_sante,
        d.nb_petitcabotage as nb_petitcabotage,        
    	d.nb_longcours_marseille as nb_longcours_marseille,
    total,
        nb_tonnage_filled, nb_homeport_filled, nb_product_filled, nb_birthplace_filled, nb_citizenship_filled, nb_flag_filled, good_sum_tonnage,
    nb_tonnage_deduced, nb_homeport_deduced, nb_birthplace_deduced,nb_citizenship_deduced, nb_flag_deduced, good_sum_tonnage_deduced

        from ports.port_points pp ,
                (
                        select pointcall_uhgs_id, source_suite, pointcall_year,
            count(k.pkid) as total,
                        count(k.tonnage) as nb_tonnage_filled,
                        count(k.homeport) as nb_homeport_filled,
                        count(k.commodity_purpose) as nb_product_filled,
                        count(k.birthplace) as nb_birthplace_filled,
                        count(k.citizenship) as nb_citizenship_filled,
            count(k.flag) as nb_flag_filled,
                        sum(k.tonnage_numeric) as good_sum_tonnage,

            count(tonnage_deduced.tonnage) as nb_tonnage_deduced,
            count(homeport_deduced.homeport) as nb_homeport_deduced,
            count(birthplace_deduced.birthplace) as nb_birthplace_deduced,
            count(citizenship_deduced.citizenship) as nb_citizenship_deduced,
            count(flag_deduced.ship_flag) as nb_flag_deduced,
            sum(tonnage_deduced.tonnage_numeric) as good_sum_tonnage_deduced

                        from (
                                select pointcall_uhgs_id, source_suite, pkid,
                                (coalesce(extract(year from TO_DATE(substring(pointcall_out_date2 for 4),'YYYY')), extract(year from TO_DATE(substring(pointcall_in_date2 for 4),'YYYY'))))::int4 as pointcall_year,
                                nb_conges_1787_inputdone, nb_conges_1787_cr, nb_conges_1789_inputdone, nb_conges_1789_cr,
                                tonnage, homeport, (all_cargos::json->>0)::json->>'commodity_purpose' as commodity_purpose, birthplace, citizenship, flag,
                                (case when tonnage_unit = 'quintaux' then (tonnage::float)/24  else tonnage::float end) as tonnage_numeric
                        from navigoviz.pointcall p
                                where true and state_1789_fr = 'France' and pointcall_function = 'O' and data_block_leader_marker = 'A'
                        ) as k

            left join
            (
                select p.pkid, tonnage,
                (case when tonnage_unit = 'quintaux' then (tonnage::float)/24  else tonnage::float end) as tonnage_numeric        
                from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2
                where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.ship_tonnage = -2     
            ) as tonnage_deduced on tonnage_deduced.pkid = k.pkid

            left join
            (
                select p.pkid, p.homeport
                from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2
                where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.ship_homeport  = -2   
            ) as homeport_deduced
            on homeport_deduced.pkid = k.pkid

            left join
            (
                select p.pkid, p.birthplace
                from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2
                where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.captain_birthplace  = 
-2
            ) as birthplace_deduced
            on birthplace_deduced.pkid = k.pkid

            left join
            (
                select p.pkid, p.citizenship
                from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2
                where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.captain_citizenship  = -2
            ) as citizenship_deduced
            on citizenship_deduced.pkid = k.pkid


            left join
            (
                select p.pkid, ship_flag
                from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2
                where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.ship_flag  = -2       
            ) as flag_deduced
            on flag_deduced.pkid = k.pkid

                group by  pointcall_uhgs_id , source_suite, pointcall_year
                ) as toto,
                navigoviz.decompte_sources_saisies d
        where pp.state_1789_fr = 'France' and toto.pointcall_uhgs_id = pp.uhgs_id and d.uhgs_id = pp.uhgs_id and toto.pointcall_year = d.annee 
    union ( 
        select
    ogc_fid, pp.uhgs_id, pp.toponyme_standard_fr as  toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as 
admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point, 'G5' as source_suite, d.annee as pointcall_year, nb_conges_inputdone, nb_conges_cr, nb_conges_sante, nb_petitcabotage, null as nb_longcours_marseille, null as total, null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled, null as good_sum_tonnage,null as nb_tonnage_deduced, null as nb_homeport_deduced, null as nb_birthplace_deduced, null as nb_citizenship_deduced, null as nb_flag_deduced, null as good_sum_tonnage_deduced
     from ports.port_points pp, navigoviz.decompte_sources_saisies d
        where state_1789_fr = 'France' and d.uhgs_id = pp.uhgs_id and (nb_conges_cr is  null or  nb_conges_inputdone is null)
    ) 
     union (
        select
    ogc_fid, uhgs_id, toponyme_standard_fr as  toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as 
admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point, null as source_suite, null as pointcall_year, nb_conges_1787_inputdone as nb_conges_inputdone, nb_conges_1787_cr as nb_conges_cr, nb_sante_1787 as nb_conges_sante, nb_petitcabotage_1787 as nb_petitcabotage, null as nb_longcours_marseille, null as total, null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled, null as good_sum_tonnage,null as nb_tonnage_deduced, null as nb_homeport_deduced, null as nb_birthplace_deduced, null as nb_citizenship_deduced, null as nb_flag_deduced, null as good_sum_tonnage_deduced
         from ports.port_points pp
        where state_1789_fr = 'France' and nb_conges_1787_cr is null and nb_conges_1787_inputdone is null and nb_conges_1789_cr is null and nb_conges_1789_inputdone is null and geom is not null
    )
    
    -- requete OK
    
--- Pour Marseille  , décomptes  
select
        ogc_fid, pp.uhgs_id, pp.toponyme_standard_fr as  toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point,
        source_suite, pointcall_year,
        d.nb_conges_inputdone as nb_conges_inputdone,
        d.nb_conges_cr as nb_conges_cr,
        d.nb_conges_sante as nb_conges_sante,
        d.nb_petitcabotage as nb_petitcabotage,        
    	d.nb_longcours_marseille as nb_longcours_marseille,
    total,
        nb_tonnage_filled, nb_homeport_filled, nb_product_filled, nb_birthplace_filled, nb_citizenship_filled, nb_flag_filled, good_sum_tonnage,
    nb_tonnage_deduced, nb_homeport_deduced, nb_birthplace_deduced,nb_citizenship_deduced, nb_flag_deduced, good_sum_tonnage_deduced

        from ports.port_points pp ,
                (
                        select pointcall_uhgs_id, source_suite, pointcall_year,
            count(k.pkid) as total,
                        count(k.tonnage) as nb_tonnage_filled,
                        count(k.homeport) as nb_homeport_filled,
                        count(k.commodity_purpose) as nb_product_filled,
                        count(k.birthplace) as nb_birthplace_filled,
                        count(k.citizenship) as nb_citizenship_filled,
            count(k.flag) as nb_flag_filled,
                        sum(k.tonnage_numeric) as good_sum_tonnage,

            count(tonnage_deduced.tonnage) as nb_tonnage_deduced,
            count(homeport_deduced.homeport) as nb_homeport_deduced,
            count(birthplace_deduced.birthplace) as nb_birthplace_deduced,
            count(citizenship_deduced.citizenship) as nb_citizenship_deduced,
            count(flag_deduced.ship_flag) as nb_flag_deduced,
            sum(tonnage_deduced.tonnage_numeric) as good_sum_tonnage_deduced

                        from (
                                select pointcall_uhgs_id, source_suite, pkid,
                                (coalesce(extract(year from TO_DATE(substring(pointcall_out_date2 for 4),'YYYY')), extract(year from TO_DATE(substring(pointcall_in_date2 for 4),'YYYY'))))::int4 as pointcall_year,
                                nb_conges_1787_inputdone, nb_conges_1787_cr, nb_conges_1789_inputdone, nb_conges_1789_cr,
                                tonnage, homeport, (all_cargos::json->>0)::json->>'commodity_purpose' as commodity_purpose, birthplace, citizenship, flag,
                                (case when tonnage_unit = 'quintaux' then (tonnage::float)/24  else tonnage::float end) as tonnage_numeric
                        from navigoviz.pointcall p
                                where true and state_1789_fr = 'France' and pointcall_function = 'O' and data_block_leader_marker = 'A'
                        ) as k

            left join
            (
                select p.pkid, tonnage,
                (case when tonnage_unit = 'quintaux' then (tonnage::float)/24  else tonnage::float end) as tonnage_numeric        
                from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2
                where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.ship_tonnage = -2     
            ) as tonnage_deduced on tonnage_deduced.pkid = k.pkid

            left join
            (
                select p.pkid, p.homeport
                from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2
                where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.ship_homeport  = -2   
            ) as homeport_deduced
            on homeport_deduced.pkid = k.pkid

            left join
            (
                select p.pkid, p.birthplace
                from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2
                where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.captain_birthplace  = 
-2
            ) as birthplace_deduced
            on birthplace_deduced.pkid = k.pkid

            left join
            (
                select p.pkid, p.citizenship
                from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2
                where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.captain_citizenship  = -2
            ) as citizenship_deduced
            on citizenship_deduced.pkid = k.pkid


            left join
            (
                select p.pkid, ship_flag
                from navigoviz.pointcall p, navigocheck.uncertainity_pointcall p2
                where p.state_1789_fr = 'France' and p.pointcall_function = 'O' and p.pkid = p2.pkid and p2.ship_flag  = -2       
            ) as flag_deduced
            on flag_deduced.pkid = k.pkid

                group by  pointcall_uhgs_id , source_suite, pointcall_year
                ) as toto,
                navigoviz.decompte_sources_saisies d
        where pp.state_1789_fr = 'France' and toto.pointcall_uhgs_id = pp.uhgs_id and d.uhgs_id = pp.uhgs_id and toto.pointcall_year = d.annee 
        and pp.uhgs_id = 'A0210797'
    union ( 
        select
    ogc_fid, pp.uhgs_id, pp.toponyme_standard_fr as  toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as 
admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point, 'G5' as source_suite, d.annee as pointcall_year, nb_conges_inputdone, nb_conges_cr, nb_conges_sante, nb_petitcabotage, null as nb_longcours_marseille, null as total, null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled, null as good_sum_tonnage,null as nb_tonnage_deduced, null as nb_homeport_deduced, null as nb_birthplace_deduced, null as nb_citizenship_deduced, null as nb_flag_deduced, null as good_sum_tonnage_deduced
     from ports.port_points pp, navigoviz.decompte_sources_saisies d
        where state_1789_fr = 'France' and d.uhgs_id = pp.uhgs_id and (nb_conges_cr is  null or  nb_conges_inputdone is null)
        and pp.uhgs_id = 'A0210797'
    ) 
     union (
        select
    ogc_fid, uhgs_id, toponyme_standard_fr as  toponym, substate_1789_fr as substate, status, has_a_clerk, geonameid, amiraute as 
admiralty, province, shiparea , ST_AsGeoJSON(ST_Transform(geom,900913)) as point, null as source_suite, null as pointcall_year, nb_conges_1787_inputdone as nb_conges_inputdone, nb_conges_1787_cr as nb_conges_cr, nb_sante_1787 as nb_conges_sante, nb_petitcabotage_1787 as nb_petitcabotage, null as nb_longcours_marseille, null as total, null as nb_tonnage_filled, null as nb_homeport_filled, null as nb_product_filled, null as nb_birthplace_filled, null as nb_citizenship_filled, null as nb_flag_filled, null as good_sum_tonnage,null as nb_tonnage_deduced, null as nb_homeport_deduced, null as nb_birthplace_deduced, null as nb_citizenship_deduced, null as nb_flag_deduced, null as good_sum_tonnage_deduced
         from ports.port_points pp
        where state_1789_fr = 'France' and nb_conges_1787_cr is null and nb_conges_1787_inputdone is null and nb_conges_1789_cr is null and nb_conges_1789_inputdone is null and geom is not null
        and pp.uhgs_id = 'A0210797'
    )
        order by source_suite, pointcall_year