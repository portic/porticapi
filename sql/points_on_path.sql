-- drop function ports.points_on_path(text,text,double precision,integer)
create or replace function ports.points_on_path(p1_uhgsid text, p2_uhgsid text, m_offset double precision, p_distance_ratio integer) 
returns ports.portic_computedPath as
$BODY$
/*
Calcule les points intermédiaires permettant de tracer des segments qui relient deux ports sans croiser de terre
Donner les identifiant UHGS des deux ports , un offset (5 km) pour l'éloignement à la côte, 
et une distance (10 km) pour l'espacement entre les points sur l'offset
https://trac.osgeo.org/postgis/wiki/UsersWikiExamplesInterpolateWithOffset
requires : 
CREATE TYPE ports.portic_computedPath AS
(
    pointslist geometry[]      ,
    nbPoints  int,
    duration float,
    tampon float,
    distance float
);
*/
	declare m_seg geometry;
	declare p1 geometry;
	declare p2 geometry;
	declare finished boolean;
	declare f1 double precision;
	declare f2 double precision;
	declare longueur_segment double precision;
	declare ratio double precision;
	declare pointslist geometry[];
	declare pointslistnew geometry[];
	declare pointslistnew2 geometry[];
	declare pointslistpart geometry[];
	declare pointslistsimplified geometry[];
	declare test geometry[];
	declare iter int;
	declare iter2 int;
	declare nbPoints int;
	declare rec record;
	declare shoreid text;
	declare starttime timestamptz;
	declare endtime timestamptz;
	declare previousPoint geometry;
	declare pointp1prime geometry;
	declare sameOffset boolean;
	declare distance_ratio double precision;
	declare tampon double precision;
	DECLARE nbPointsPart int;
	DECLARE maxiter int;
	DECLARE k int;
	declare resultat ports.portic_computedPath;
	declare duration float;
	declare f2prime double precision;
	declare recurse boolean;
	
begin
	RAISE notice '###################################### % p1 % p2 %', now(), p1_uhgsid, p2_uhgsid ;
	starttime:= clock_timestamp();
	iter:= 0;
	iter2:= 0;
	nbPoints:=0;

	
	--p1.point3857
	select ports.order_intersection(coalesce(p1.port_on_line_1mile, p1.point3857), st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile)) into test
	from ports.port_points p1 , ports.port_points p2
	where   p1.uhgs_id = p1_uhgsid and p2.uhgs_id = p2_uhgsid ;

	
	if test[1] is null 
	then 
		--RAISE notice '% STOP here, direct',NOW() ;
		finished:= true;
		-- Mettre le premier point : le port de départ, mais à 1 mile de la cote (IMPORTANT)
		pointslist[0]:=  coalesce(port_on_line_1mile, point3857) from  ports.port_points p where   p.uhgs_id = p1_uhgsid ;
		-- Mettre le dernier point : le port d'arrivée, mais à 1 mile de la cote (IMPORTANT)
		pointslist[1]:=  coalesce(port_on_line_1mile, point3857) from  ports.port_points p where   p.uhgs_id = p2_uhgsid ;
		nbPoints:=2;
	
		-- bug Marseille-Sète
		endtime:= clock_timestamp();
		duration:=1000 * ( extract(epoch from endtime) - extract(epoch from starttime) );
		resultat.pointslist := pointslist;
		resultat.nbPoints := array_length(pointslist, 1);
		resultat.duration := duration;
		resultat.tampon := m_offset;
		resultat.distance := p_distance_ratio;
	
		-- enregister le résultat
		/*
		update  navigoviz.routepaths r set m_offset= resultat.tampon, p_distance_ratio = resultat.distance,
		pointpath =resultat.pointslist , elapseTime=resultat.duration ,nbPoints = resultat.nbPoints
		where  r.from_uhgs_id = p1_uhgsid and r.to_uhgs_id = p2_uhgsid
		or (r.to_uhgs_id = p1_uhgsid and r.from_uhgs_id = p2_uhgsid);
		
		
		insert into  navigoviz.routepaths 
		(from_uhgs_id, to_uhgs_id, m_offset, p_distance_ratio, pointpath, elapseTime, nbPoints)
		values (p1_uhgsid, p2_uhgsid, resultat.tampon, resultat.distance, resultat.pointslist,resultat.duration, resultat.nbPoints);
		*/
		--commit;
	
		return resultat;
	else 
		RAISE notice ' Test p1 % p2 %', st_asewkt(test[1]), st_asewkt(test[2]);
		finished:= false;
		-- Mettre le premier point : le port de départ
		pointslist[0]:=  coalesce(port_on_line_1mile, point3857)  from  ports.port_points p where   p.uhgs_id = p1_uhgsid ;
		p1:=pointslist[0]; 
		-- ajout le 14/03/2022 pour test Marennes-Honfleur avec 100 km
		previousPoint:= pointslist[0];
		nbPoints:= nbPoints+1;
	end if;

	select  coalesce(port_on_line_1mile, point3857) into p2 from  ports.port_points  where   uhgs_id = p2_uhgsid ;

	RAISE notice  'DECOUPE de p1 % p2 %',  st_asewkt(p1), st_asewkt(p2) ;

	distance_ratio:= p_distance_ratio;
	tampon:=m_offset;
	resultat.tampon := tampon;
	resultat.distance := distance_ratio;
	-- avant iter2 = 10, mais maintenant on itère une seule fois. Test
	while finished is false and iter2 < 1 loop
	
		--k:= 2;
		k:= array_length(test, 1);
		m_seg:= NULL;
		longueur_segment:= st_length(st_setsrid(st_makeline(test[1], test[k]), 3857));
		--- ADAPTATION automatique de la densité de points et du buffer à la longueur approximée de la route
		if (longueur_segment > 1000000 and tampon!=50000) then
			distance_ratio := ((longueur_segment/1000)/(round((longueur_segment/1000)^(1/2.5))+1))*1000;--en mètres
			tampon:=50000;--50 km pour d>1700 km (75 % des observatons sont sous ce seuil)
		elseif (longueur_segment > 500000 and tampon!=36000) then
			tampon:=36000;
			distance_ratio := 40000;--40 km pour d>500 km 
		elseif (longueur_segment > 120000 and tampon!=20000) then
			tampon:=20000;
			distance_ratio := 20000;--20 km pour d>120 km 
		elseif (longueur_segment <= 120000 and tampon!=16000) then
			tampon:=16000; --recalculer avec ce tampon 16 km pour d<=120 km
			distance_ratio := 16000;
		end if;
		raise notice 'LONGUEUR DIRECTE % TAMPON % DISTANCE %',round(longueur_segment/1000),tampon, distance_ratio;
		resultat.tampon := tampon;
		resultat.distance := distance_ratio;
		
		while m_seg IS NULL AND k > 1 loop
			
			RAISE notice 'Grande boucle iter2 % test[1] % test[2] %', iter2, st_asewkt(test[1]) , st_asewkt(test[k]);
			RAISE notice 'Distance k % entre test[1] et test[2] %', k, ST_Distance(test[1], test[k]);
			shoreid:=  s.id from navigoviz.shoreline s  where s.shoreline is not null and s.area > 1 
				and st_intersects(s.shoreline, st_buffer(st_setsrid(st_makeline(test[1], test[k]), 3857), 10)) 
				order by st_distance(test[1],ST_ClosestPoint(shoreline, test[1])) asc 
				limit 1;
			RAISE notice 'Identifiant du shoreline à contourner %', shoreid ;
			-- assertion : les deux points test partagent la même shoreline car deux points successifs appartiennent à la même shoreline qu'ils coupent par l'intérieur
			f1 :=  ST_LineLocatePoint(s.shoreline, test[1]) 
				from navigoviz.shoreline s 
				where s.id = shoreid;
			f2 :=  ST_LineLocatePoint(s.shoreline , test[k]) 
				from navigoviz.shoreline s 
				where s.id = shoreid;
			RAISE notice 'cas comme AVANT , f1 % f2 % f1-f2 = %', f1, f2, case when f1> f2 then f1-f2 else f2-f1 end ;
			f2prime :=  ST_LineLocatePoint(st_addpoint(s.shoreline, test[1], 0) , test[k]) 
				from navigoviz.shoreline s 
				where s.id = shoreid;
			--RAISE notice 'f1 % f2 %', f1, f2 ;
			RAISE notice 'CAS où test1 devient le début, f1 % f2 % f1-f2 = %', 0, f2prime, f2prime ;
			if (f2prime < Abs(f2-f1)) then 			
				-- parfois ST_OffsetCurve renvoie des polylines, ce qui ne marche pas. Donc on prend la plus petite.
				select g into m_seg from (
					select ST_NumGeometries(geom),  (st_dump(geom)).geom as g
					from (
					select ST_OffsetCurve(ST_LineSubstring(st_addpoint(s.shoreline, test[1], 0), 0, f2prime), tampon) as geom
								from navigoviz.shoreline s 
								where s.id = shoreid
					) as k
					) as q
				--where st_intersects(g, test[1]) and st_intersects(g, test[2]);
				order by  st_length(g) asc limit 1; 	
				f1:=0;	
				f2:=f2prime;
			else
				select g into m_seg from (
					select ST_NumGeometries(geom),  (st_dump(geom)).geom as g
					from (
					select ST_OffsetCurve(ST_LineSubstring(s.shoreline,case when f1<f2 then f1 else f2 end, case when f1<f2 then f2 else f1 end), tampon) as geom
								from navigoviz.shoreline s 
								where s.id = shoreid
					) as k
					) as q
				--where st_intersects(g, test[1]) and st_intersects(g, test[2]);
				order by  st_length(g) desc limit 1; --comme avant
			end if;
			longueur_segment:= st_length(m_seg); 
			RAISE notice 'Longueur du segment en offset %', longueur_segment ;
			if m_seg is null then
				k:=k-1;
			/*else 
				--- ADAPTATION automatique de la densité de points et du buffer à la longueur approximée de la route
				if (longueur_segment > 1700000 and tampon!=50000) then
					distance_ratio := ((longueur_segment/1000)/(round((longueur_segment/1000)^(1/2.5))+1))*1000;--en mètres
					tampon:=50000;--50 km pour d>1700 km (75 % des observatons sont sous ce seuil)
				elseif (longueur_segment > 500000 and tampon!=36000) then
					tampon:=36000;
					distance_ratio := 40000;
				elseif (longueur_segment > 120000 and tampon!=20000) then
					tampon:=20000;
					distance_ratio := 20000;
				elseif (longueur_segment <= 120000 and tampon!=16000) then
					tampon:=16000; --recalculer avec ce tampon 16 km pour d<=120 km
					distance_ratio := 16000;
				end if;
				raise notice 'TAMPON % DISTANCE %',tampon, distance_ratio;
				resultat.tampon := tampon;
				resultat.distance := distance_ratio;*/
			end if;
		END loop;
	
		--return resultat;
	
		iter:= 0;
		maxiter:=300;--300
		
		sameOffset:=true;
	
		
		--distance_ratio:= longueur_segment/20;
		--distance_ratio:=p_distance_ratio/(2*iter2+1) ; -- on réduit l'allonge vers l'arrivée
		
		if maxiter * distance_ratio < longueur_segment then 
			maxiter := round(longueur_segment/distance_ratio) +2;
			raise notice 'INCREASE le nombre max iterations % ',maxiter;
		end if;
	
		while sameOffset is true and finished is false and m_seg is not null and iter < maxiter loop 
		
			-- ratio progresse de 0 à 1 jusqu'à l'arrivée
			ratio:= case when f1> f2 then 1 - (distance_ratio* (iter+1) / st_Length(m_seg)) else (distance_ratio* (iter+1) / st_Length(m_seg)) end;
			-- raise notice 'valeur du ratio % - doit etre dans [0, 1]',ratio;
			--ratio:= (distance_ratio* (iter+1) / st_Length(m_seg)) ;
			
		
			if ratio > 1 or ratio < 0 then 
				raise notice 'CHANGE OFFSET valeur du ratio % - doit etre dans [0, 1]',ratio;
				-- distance_ratio:=distance_ratio/2; -- a voir si c'est ca qui merde : oui, ca fait merder
				-- précaution 
				--ratio:= case when ratio > 1 then ratio-1 else -ratio end;
				--ratio:= case when ratio < 0 then 0 else ratio end;
				sameOffset:= false; -- a voir si c'est ca qui merde
				--ratio:=1.0/maxiter*iter;
				--raise notice 'CHANGE ratio : new ratio % ',ratio;
			else
			
				-- Place a point on segment that is parallel  to shoreline
				select ST_LineInterpolatePoint(m_seg, ratio) into p1; 
				--raise notice '### points_on_path 1 ST_LineInterpolatePoint moved to %', st_asewkt(p1);
			
				-- move this point if required (test if it intersects the previous point and do the job)
				-- attention, présuppose que pointslist contient toujours le premier point, 
				-- premier point à 1 mile des cotes pour éviter une intersection.
			
				-- OPTION 
				-- Cette action bouge le point p1 de manière à ne pas couper la côte sur le troncon qui précède.
				-- C'est fin mais ca boucle infiniment. Il vaut mieux rappeler redo_point_on_path 
				-- select  ports.move_pointp1(previousPoint, p1, m_offset, p_distance_ratio ) into p1;
			
				-- Cette action agit grosso modo : on place des points à intervalles réguliers sur le grand segmnet
				-- si ils tombent dans la terre, on les déplace
				select ports.move_pointp1_intosea(p1) into pointp1prime;
				--raise notice '### points_on_path 1 move_pointp1_intosea moved to %', st_asewkt(p1);
				--select ports.move_pointp1(previousPoint, p1, m_offset, round(distance_ratio)::integer  ) into p1;
			
				/*
				-- logguer les essais				
				insert into navigoviz.temp_points (iter2, maxiter, iter, previousPoint, pointp1, pointp1prime, shoreid, segment, longueur_segment, f1, f2, newoffset, distance_ratio, ratio) 
				values (iter2, maxiter, iter, previousPoint, p1, pointp1prime, shoreid, m_seg, longueur_segment, f1, f2, m_offset, distance_ratio, ratio);
				*/
			
				p1:= pointp1prime;

				-- test if finished for this shoreline
				select ports.order_intersection(p1, st_makeline(p1, p2)) into test;
				-- and s.id = shoreid and st_intersects(st_makeline(p1, p2.port_on_line_1mile), shoreline);
			

				if test[1] is null 
					then finished:= true;
				end if;
				-- comment
				--raise notice 'Segment direct ? % Iteration % ', finished, iter;
				
				
				pointslist[nbPoints]:=p1;
				nbPoints:= nbPoints+1;
				previousPoint:=p1;
				iter:= iter+1;
			end if;--change offset sur ratio
			
		end loop;
	
		if sameOffset is false then
			raise notice 'Liste des points à tester pour prendre intersection Offset :  p1 %, p2 %', st_asewkt(p1), st_asewkt(p2);
		end if;
		-- fini, vraiment ? Il y a une autre shoreline à contourner
		select ports.order_intersection(p1, st_makeline(p1, p2)) into test;
		
	
		if test[1] is null 
		then 
			RAISE notice 'STOP here,  iteration2  % ', iter2 ;
			finished:= true;
		else 
			RAISE notice 'INACHEVE Test p1 % p2 % iter2 %', st_asewkt(test[1]), st_asewkt(test[2]), iter2;
			finished:= false;
		end if;
	
		iter2:= iter2+1;
	end loop;

	-- Mettre le dernier point : le port d'arrivée
	pointslist[nbPoints]:=  p2;
	nbPoints:= nbPoints+1;
	
	/*
	-- simplifier
	pointslist := ports.simplifypath(pointslist);
	-- rappeler la correction sur tous les segments et insérer la correction
	
	pointslistnew[0] := pointslist[0];
	FOR iter IN 0 .. array_length(pointslist, 1)-1 LOOP 
   		pointslistpart := ports.subpoints_on_path(pointslist[iter], pointslist[iter+1],  m_offset, p_distance_ratio);
   		
   		nbPointsPart := array_length(pointslistpart, 1);
   		--pointslistnew := pointslist array_cat(anyarray, anyarray)
   		RAISE notice 'BOUCLE iter % subpoints_on_path p1 % p2 %  nbPointsPart %',iter, st_asewkt(pointslist[iter]), st_asewkt(pointslist[iter+1]), nbPointsPart ;
   		IF nbPointsPart = 2 THEN
   			pointslistnew := array_append(pointslistnew, pointslist[iter+1]);
   		ELSE 
   			pointslistnew := array_cat(pointslistnew, pointslistpart[1:nbPointsPart-1]);
   			pointslistnew := array_append(pointslistnew, pointslist[iter+1]);
   		END IF;
	END LOOP;
	
	-- simplifier au max (enlever les boucles). algo en n log n
	pointslistsimplified := ports.simplifypathbetter(pointslistnew);
	*/
	

	endtime:= clock_timestamp();
	duration:=1000 * ( extract(epoch from endtime) - extract(epoch from starttime) );
	resultat.pointslist := pointslist;
	resultat.nbPoints := array_length(pointslist, 1);
	resultat.duration := duration;


	---------------------------------
	-- Introduire ici la recursivité
	---------------------------------
	
	recurse:=false;
	iter:=0;
	
	select ST_Intersects (st_makeline(pointslist),s.polygone3857 ) into recurse
	from navigoviz.shoreline s  
	where ST_Intersects(st_makeline(pointslist),s.polygone3857 ) ;

	raise notice '_______________________________ recursivité iter % need to recurse ? % ', iter, recurse;
	while recurse is true and iter < 4 loop 
		-- distance_ratio := round(p_distance_ratio/power(2,iter+1));-- select power(iter,2) iter+2
		-- tampon := m_offset/(power(2, iter+1));
		distance_ratio := round(distance_ratio/2);
		tampon := tampon/2;
		raise notice '_______________________________ recursivité distance_ratio % tampon ? % ', distance_ratio, tampon;
		if (mod(iter, 2) = 0) then
			raise notice '_______________________________ recursivité start SIMPLIFY %', clock_timestamp();
			pointslist := ports.simplifypath(pointslist);
			raise notice '_______________________________ recursivité end SIMPLIFY %', clock_timestamp();
		end if;
	
		select ports.portic_redo_points_on_path(pointslist, tampon, distance_ratio::integer) into pointslist;
	
		select ST_Intersects (st_makeline(pointslist),s.polygone3857 ) into recurse
		from navigoviz.shoreline s  
		where ST_Intersects(st_makeline(pointslist),s.polygone3857 ) ;
		
		raise notice '_______________________________ recursivité iter % need to recurse ? % ', iter, recurse;
		iter:=iter+1;
	end loop;
	

	-- Tester les boucles 
	if ST_IsSimple(st_makeline(pointslist)) IS FALSE THEN
		raise notice '_______________________________ SIMPLIFY BETTER on nbpoints %', array_length(pointslist, 1);
		pointslist := ports.simplifypathbetter(pointslist);
	END IF;
	

	endtime:= clock_timestamp();
	--return pointslistsimplified;
	duration:=1000 * ( extract(epoch from endtime) - extract(epoch from starttime) );
	resultat.duration := duration;
	resultat.pointslist := pointslist;
	resultat.nbPoints := array_length(pointslist, 1);
	
	
	raise notice '% DURATION % NB POINTS %', NOW(), 1000 * ( extract(epoch from endtime) - extract(epoch from starttime) ), array_length(pointslist, 1);

	-- enregistrer le résultat
	/*insert into  navigoviz.routepaths 
	(from_uhgs_id, to_uhgs_id, m_offset, p_distance_ratio, pointpath, elapseTime, nbPoints)
	values (p1_uhgsid, p2_uhgsid, resultat.tampon, resultat.distance, resultat.pointslist,resultat.duration, resultat.nbPoints);
	
	--commit;
	update  navigoviz.routepaths r set m_offset= resultat.tampon, p_distance_ratio = resultat.distance,
		pointpath =resultat.pointslist , elapseTime=resultat.duration ,nbPoints = resultat.nbPoints
		where  r.from_uhgs_id = p1_uhgsid and r.to_uhgs_id = p2_uhgsid
		or (r.to_uhgs_id = p1_uhgsid and r.from_uhgs_id = p2_uhgsid);
	*/
	return resultat;

END;
$BODY$
language plpgsql;