-- DROP FUNCTION ports.simplifypathbetter(geometry[])
create or replace function ports.simplifypathbetter(itineraire geometry[]) 
returns geometry[] as
$BODY$
/*
Simplifie le chemin calculé en supprimant les ronds dans l'eau. 
Présuppose l'existance d'une table shoreline qui correspond à de GSHHS_i_L1.shp avec une linestring pour modéliser la cote
et la column shoreline = ST_LineMerge(st_boundary(st_setsrid(st_transform(st_setsrid(wkb_geometry, 4326), 3857), 3857)))
https://trac.osgeo.org/postgis/wiki/UsersWikiExamplesInterpolateWithOffset
*/
	declare previousPoint geometry;
	declare currentPoint geometry;
	declare pointp1 geometry;	
	declare pointslist geometry[];
	declare test geometry[];
	declare iter int;
	declare jiter int;
	declare previousindex int;
	declare nbPoints int;
	declare taille int;
begin
	nbPoints:=0;
	--nbPoints:= nbPoints+1;
	--pointslist[0]:=  itineraire[0];
	previousindex:=0;
	iter:=0;
	previousPoint:=itineraire[0];
	currentPoint:=previousPoint;
	taille:=array_length(itineraire, 1);--2
	raise notice 'array_length(itineraire, 1) %', 	taille;
	while iter < taille-1 loop
		--raise notice 'iter % currentPoint %', iter, st_asewkt(currentPoint) ;
		jiter := iter+1;
		pointp1:=itineraire[jiter];
		select ports.order_intersection(currentPoint, st_makeline(currentPoint, pointp1)) into test;
		previousPoint:=pointp1;--oo
		previousindex:=jiter;
		while jiter < taille-1 loop
			if test[1] is null then 
				previousPoint:=pointp1;
				previousindex:=jiter;
			end if;
			jiter:=jiter+1;
			pointp1:=itineraire[jiter];
			--raise notice 'boucle interne jiter % pointp1 %', jiter, st_asewkt(previousPoint);
			select ports.order_intersection(currentPoint, st_makeline(currentPoint, pointp1)) into test;
		end loop;
		pointslist[nbPoints]:=currentPoint;
		nbPoints:= nbPoints+1;
		currentPoint:=previousPoint;
		iter:=previousindex;
	end loop;
	pointslist[nbPoints]:=currentPoint;
	nbPoints:= nbPoints+1;
	--pointslist[nbPoints]:=  itineraire[taille-1];
	return pointslist;
END;
$BODY$
language plpgsql;