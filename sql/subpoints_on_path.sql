-- drop function ports.subpoints_on_path(text,text,double precision,integer)
create or replace function ports.subpoints_on_path(p1_start geometry, p2_end geometry, m_offset double precision, p_distance_ratio integer) 
returns geometry[] as
$BODY$
/*
Calcule les points intermédiaires permettant de tracer des segments qui relient deux ports sans croiser de terre
Donner les identifiant UHGS des deux ports , un offset (5 km) pour l'éloignement à la côte, 
et une distance (10 km) pour l'espacement entre les points sur l'offset
https://trac.osgeo.org/postgis/wiki/UsersWikiExamplesInterpolateWithOffset
*/
	declare m_seg geometry;
	declare p1 geometry;
	declare p2 geometry;
	declare pointp1prime geometry;
	declare finished boolean;
	declare f1 double precision;
	declare f2 double precision;
	declare longueur_segment double precision;
	declare ratio double precision;
	declare pointslist geometry[];
	declare test geometry[];
	declare iter int;
	declare iter2 int;
	declare nbPoints int;
	declare rec record;
	declare shoreid text;
	declare starttime timestamptz;
	declare endtime timestamptz;
	declare previousPoint geometry;
	declare sameOffset boolean;
	declare distance_ratio double precision;
	declare aterre text; -- identifiant de la shoreline dans laquelle pourrait tomber le point.

begin
	--RAISE notice '###################################### subpoints_on_path %', now();
	starttime:= clock_timestamp();
	iter:= 0;
	iter2:= 0;
	nbPoints:=0;

	-- précaution
	IF p1_start IS NULL OR p2_end IS NULL THEN
		RAISE notice '###### BUG subpoints_on_path : un des deux points null p[1] % p[2] %', st_asewkt(p1_start) , st_asewkt(p2_end);
		nbPoints:=1;
		IF p1_start IS NULL THEN
			pointslist[1]:=  p2_end ;
		ELSE
			pointslist[1]:=  p1_start ;
		END IF;
		return pointslist;
	END IF;

	select ports.order_intersection(p1_start , st_makeline(p1_start, p2_end)) into test;


	if test[1] is null 
	then 
		--RAISE notice '% STOP here, direct',NOW() ;
		finished:= true;
		-- Mettre le premier point : le port de départ, mais à 1 mile de la cote (IMPORTANT)
		pointslist[0]:=  p1_start;
		-- Mettre le dernier point : le port d'arrivée, mais à 1 mile de la cote (IMPORTANT)
		pointslist[1]:=  p2_end ;
		nbPoints:=2;
		return pointslist;
	else 
		--RAISE notice ' Test p1 % p2 %', test[1], test[2];
		finished:= false;
		-- Mettre le premier point : le port de départ
		pointslist[0]:=  p1_start ;
		-- p1:=pointslist[0]; 
		-- ajout le 14/03/2022 pour test Marennes-Honfleur avec 100 km
		previousPoint:= pointslist[0];
		nbPoints:= nbPoints+1;
	end if;

	--select  coalesce(port_on_line_1mile, point3857) into p2 from  ports.port_points  where   uhgs_id = p2_uhgsid ;

	distance_ratio:= p_distance_ratio;
	-- iter2 = 1 : faisait bien marcher
	while finished is false and iter2 < 10 loop
	
		RAISE notice '###### subpoints_on_path : Grand boucle iter2 % test[1] % test[2] %', iter2, st_asewkt(test[1]) , st_asewkt(test[2]);
		shoreid:=  s.id from navigoviz.shoreline s  where s.shoreline is not null and s.area > 1 
			and st_intersects(s.shoreline, st_buffer(st_setsrid(st_makeline(test[1], test[2]), 3857), 10)) 
			order by st_distance(test[1],ST_ClosestPoint(shoreline, test[1])) asc 
			limit 1;
		RAISE notice '###### subpoints_on_path : Identifiant du shoreline à contourner %', shoreid ;
		-- assertion : les deux points test partagent la même shoreline car deux points successifs appartiennent à la même shoreline qu'ils coupent par l'intérieur
		f1 :=  ST_LineLocatePoint(s.shoreline, test[1]) 
			from navigoviz.shoreline s 
			where s.id = shoreid;
		-- st_addpoint(s.shoreline, test[1], 0)
		f2 :=  ST_LineLocatePoint(s.shoreline, test[2]) 
			from navigoviz.shoreline s 
			where s.id = shoreid;
		
		-- On reduit l'offset au fur et à mesure des itérations iter2
		select g into m_seg from (
			-- parfois ST_OffsetCurve renvoie des polylines, ce qui ne marche pas. Donc on prend la plus GRANDE.
			select ST_NumGeometries(geom),  (st_dump(geom)).geom as g
			from (
			select ST_OffsetCurve(ST_LineSubstring(s.shoreline, case when f1<f2 then f1 else f2 end, case when f1<f2 then f2 else f1 end), m_offset/(iter2+1)) as geom
						from navigoviz.shoreline s 
						where s.id = shoreid
			) as k
			) as q
		order by  st_length(g) desc limit 1;--desc avant
		longueur_segment:= st_length(m_seg);
		RAISE notice '###### subpoints_on_path : Longueur du segment en offset %', longueur_segment ;
		
		-- On itère 10 fois max sur un dixième du segment.
		iter:= 0;
		distance_ratio:= longueur_segment/10;
		--distance_ratio:=p_distance_ratio/(2*iter2+1) ; -- on réduit l'allonge vers l'arrivée
		while finished is false and m_seg is not null and iter < 9 loop 
		
			ratio:= case when f1> f2 then 1 - (distance_ratio* (iter+1) / st_Length(m_seg)) else (distance_ratio* (iter+1) / st_Length(m_seg)) end;
			--ratio:= (distance_ratio* (iter+1) / st_Length(m_seg)) ;
			-- raise notice 'valeur du ratio % - doit etre dans [0, 1]',ratio;
			
			if ratio >= 0 and ratio <= 1   then 
						
				-- Place a point on segment that is parallel  to shoreline
				select ST_LineInterpolatePoint(m_seg, ratio) into p1; 
				IF p1 IS NULL THEN
					RAISE notice '###### BUG BUG 1 subpoints_on_path : ST_LineInterpolatePoint donne null ratio % m_seg %', ratio, st_asewkt(m_seg);
				END IF;
			
				-- test si le point p1 est bien en mer
				aterre:= s.id from navigoviz.shoreline s where st_contains(s.polygone3857, p1);
		
				-- empêche que le nouveau point soit sur terre
				/*
				select ports.move_pointp1_intosea(p1) into pointp1prime;
				IF pointp1prime IS NULL THEN
					RAISE notice '###### BUG BUG 2 subpoints_on_path : move_pointp1_intosea donne null en bougeant p1 % ',  st_asewkt(p1);
				END IF;
				*/
			
				-- move this point if required (test if it intersects the previous point and do the job)
				-- attention, présuppose que pointslist contient toujours le premier point, 
				-- premier point à 1 mile des cotes pour éviter une intersection.
				-- select  ports.move_pointp1(previousPoint, p1, m_offset, p_distance_ratio ) into p1;
				/*
				select ports.move_pointp1(previousPoint, p1, m_offset, round(distance_ratio)::integer  ) into p1;
				IF p1 IS NULL THEN
					RAISE notice '###### BUG BUG 3 subpoints_on_path : move_pointp1 donne null en bougeant p1 % avec m_offset % et distance % et previousPoint %', st_asewkt(p1),  m_offset, round(distance_ratio)::integer, st_asewkt(previousPoint);
				END IF;
				*/
				
				/*
				-- logguer les essais
				insert into navigoviz.temp_points (iter2, maxiter, iter, previousPoint, pointp1, pointp1prime, shoreid, segment, longueur_segment, f1, f2, newoffset, distance_ratio, ratio) 
				values (iter2, 9, iter, previousPoint, p1, pointp1prime, shoreid, m_seg, longueur_segment, f1, f2, m_offset/(iter2+1), distance_ratio, ratio);
				*/
				
				
				-- on ne tient pas compte de move_pointp1_intosea (test)
				--p1 := pointp1prime;
				
				-- test if finished for this shoreline
				select ports.order_intersection(p1, st_makeline(p1, p2_end)) into test;
			
				if test[1] is null  THEN
					finished:= true;
				end if;
				-- comment
				-- raise notice '###### subpoints_on_path - Segment direct ? % Iteration % ', finished, iter;
				
				-- On ajoute le point à la liste que si il n'est pas sur une terre. 
				if p1 is NOT NULL AND aterre IS NULL THEN
					pointslist[nbPoints]:=p1;
					nbPoints:= nbPoints+1;
					previousPoint:=p1;
				end if;
				iter:= iter+1;
			end if;
			
		end loop;
	
		-- fini, vraiment ? Il y a une autre shoreline à contourner 
		-- fix 18 juillet previousPoint au lieu de p1
		select ports.order_intersection(previousPoint, st_makeline(previousPoint, p2_end)) into test;
		
	
		if test[1] is null 
		then 
			RAISE notice '###### subpoints_on_path : STOP here,  iteration2  % ', iter2 ;
			finished:= true;
		else 
			RAISE notice '###### subpoints_on_path : INCREMENT SUR iter2 Test p1 % p2 % iter2 %', st_asewkt(test[1]), st_asewkt(test[2]), iter2;
			finished:= false;
		end if;
	
		iter2:= iter2+1;--offset va diminuer pour la prochaine itération.
	end loop;

	-- Mettre le dernier point : le port d'arrivée
	pointslist[nbPoints]:=  p2_end;
	nbPoints:= nbPoints+1;

	pointslist := ports.simplifypath(pointslist);

	return pointslist;
END;
$BODY$
language plpgsql;