-- drop function ports.portic_redo_points_on_path(pointslist geometry[], m_offset double precision, p_distance_ratio integer) 
create or replace function ports.portic_redo_points_on_path(pointslist geometry[], m_offset double precision, p_distance_ratio integer) 
returns geometry[] as
--returns ports.portic_computedPath as
$BODY$
/*
Calcule les points intermédiaires permettant de tracer des segments qui relient deux ports sans croiser de terre
Donner les identifiant UHGS des deux ports , un offset (5 km) pour l'éloignement à la côte, 
et une distance (10 km) pour l'espacement entre les points sur l'offset
https://trac.osgeo.org/postgis/wiki/UsersWikiExamplesInterpolateWithOffset
*/
	declare pointslistclean geometry[];
	declare pointslistnew geometry[];
	declare pointslistpart geometry[];
	declare pointslistsimplified geometry[];
	declare starttime timestamptz;
	declare endtime timestamptz;	
	DECLARE nbPointsPart int;
	
	--declare resultat ports.portic_computedPath;
	declare res geometry[];
	declare duration float;
begin
	--starttime:= clock_timestamp();
	
	RAISE notice 'portic_redo_points_on_path Longueur, %', array_length(pointslist, 1);
	pointslistclean[0] := pointslist[0];
	FOR iter IN 0 .. array_length(pointslist, 1)-1 LOOP 
		if pointslist[iter+1] is null then
   			--RAISE notice 'p2 IS NULL ? %', st_asewkt(pointslist[iter+1]);
   			--RAISE notice 'Pour la longueur, %, A iter+1, %, p2 IS NULL ? %', array_length(pointslist, 1), iter+1, st_asewkt(pointslist[iter+1]);
   			--iter:=iter+2;
   		else
   			pointslistclean := array_append(pointslistclean, pointslist[iter+1]);
   		end if;
   	
   	END LOOP;
   	RAISE notice 'portic_redo_points_on_path Longueur clean, %', array_length(pointslistclean, 1);
	
   
	pointslistnew[0] := pointslistclean[0];
	FOR iter IN 0 .. array_length(pointslistclean, 1)-2 LOOP 
		if pointslistclean[iter+1] is null then
   			--RAISE notice 'p2 IS NULL ? %', st_asewkt(pointslist[iter+1]);
   			RAISE notice 'portic_redo_points_on_path Pour la longueur, %, A iter+1, %, p2 IS NULL ? %', array_length(pointslistclean, 1), iter+1, st_asewkt(pointslistclean[iter+1]);
   			--iter:=iter+2;
   		else
	   		pointslistpart := ports.subpoints_on_path(pointslistclean[iter], pointslistclean[iter+1],  m_offset, p_distance_ratio);
	
	   		nbPointsPart := array_length(pointslistpart, 1);
	   		--RAISE notice 'BOUCLE iter % subpoints_on_path p1 % p2 %  nbPointsPart %',iter, st_asewkt(pointslistclean[iter]), st_asewkt(pointslistclean[iter+1]), nbPointsPart ;
	
	   		IF nbPointsPart = 2 then
	   			-- RAISE notice 'NOTHING TO DO ';
	   			pointslistnew := array_append(pointslistnew, pointslistclean[iter+1]);
	   		else
	   			RAISE notice 'SEGMENT iter % de portic_redo_points_on_path  [p1 % p2 % ] : nbPointsPart %',iter, st_asewkt(pointslistclean[iter]), st_asewkt(pointslistclean[iter+1]), nbPointsPart ;

	   			pointslistnew := array_cat(pointslistnew, pointslistpart[1:nbPointsPart-1]);
	   			pointslistnew := array_append(pointslistnew, pointslistclean[iter+1]);
	   		END IF;
   	
   		end if;
	END LOOP;
	
	--endtime:= clock_timestamp();
	--duration:=1000 * ( extract(epoch from endtime) - extract(epoch from starttime) );

	--raise notice '% DURATION % NB POINTS %', NOW(), duration, array_length(pointslistnew, 1);

	

	--resultat.pointslist := pointslistnew;
	--resultat.nbPoints := array_length(pointslistnew, 1);
	--resultat.duration := duration;
	--return resultat;

	res:=pointslistnew;
	return res;
END;
$BODY$
language plpgsql;