-- DROP FUNCTION ports.move_pointp1_intosea(geometry,geometry)
create or replace function ports.move_pointp1_intosea(pointp1 geometry) 
returns geometry as
$BODY$
/*
Bouge le point p1 qui appartient au polygone de la shoreline sur Mer
Présuppose l'existance d'une table shoreline qui correspond à de GSHHS_i_L1.shp avec une linestring pour modéliser la cote
et la column shoreline = ST_LineMerge(st_boundary(st_setsrid(st_transform(st_setsrid(wkb_geometry, 4326), 3857), 3857)))
https://trac.osgeo.org/postgis/wiki/UsersWikiExamplesInterpolateWithOffset
*/
	declare pointp1prime geometry;
	declare iter int;
	declare truc record;
	declare azimuth  double precision;
	declare aterre text; -- identifiant de la shoreline
	declare marge1mile int;
	declare geomline geometry;
begin
	marge1mile:=1854;
	pointp1prime:= pointp1;
	aterre:= s.id from navigoviz.shoreline s where st_contains(s.polygone3857, pointp1prime);


	if aterre is not null then
	-- Attention, toutes les shoreline n'avaient pas été calculées
		geomline := shoreline from navigoviz.shoreline where id = aterre;
		if geomline is null then 
			RAISE notice '\t ------------ MOVE P1 INTO SEA move_pointp1_intosea Compute a missing shoreline with id % ', aterre;
			update navigoviz.shoreline set shoreline = ST_LineMerge(st_boundary(polygone3857)) 
			where id = aterre;
		end if;
	end if;

	iter:= 0;
	while aterre is not null and iter < 10 loop 
		--RAISE notice '\t ------------ MOVE P1 INTO SEA  Bouger  pointp1 qui est à Terre % dans %', st_asewkt(pointp1prime), aterre;

		-- le replacer sur la cote la plus proche
		select st_length(st_shortestline(pointp1prime, shoreline)) as d, st_closestpoint(shoreline, pointp1prime) as proche into truc
		from navigoviz.shoreline where id = aterre  limit 1; --order by d asc
		--RAISE notice '\t ------------ Bouger de la distance % avec la projection sur la ligne % ', truc.d, st_asewkt(truc.proche);
	
		azimuth:= st_azimuth(pointp1prime, truc.proche);
		--RAISE notice '\t ------------ Bouger de azimuth % ', azimuth;

		--greatest(truc.d*2, truc.d+marge1mile)
		-- truc.d+marge1mile
		select st_transform(st_GeomFromEWKT(ST_AsEWKT (k.x)),3857) into pointp1prime from
		(
			select  st_setsrid(st_project(st_transform(pointp1prime, 4326)::geography, 2*truc.d, azimuth), 4326) as x
		) as k;
		-- pointp1prime est modifié , placé en mer, symétrique à la cote
		--RAISE notice '\t ------------ MOVE P1 INTO SEA -  resultat pointp1prime % ', st_asewkt(pointp1prime);
	
		aterre:= s.id from navigoviz.shoreline s where st_contains(s.polygone3857, pointp1prime);
		if aterre is null  then 
			return pointp1prime; 
		else
			-- Attention, toutes les shoreline n'avaient pas été calculées
			geomline := shoreline from navigoviz.shoreline where id = aterre;
			if geomline is null then 
				RAISE notice '\t ------------ MOVE P1 INTO SEA move_pointp1_intosea Compute a missing shoreline with id % ', aterre;
				update navigoviz.shoreline set shoreline = ST_LineMerge(st_boundary(polygone3857)) 
				where id = aterre;
			end if;
		end if;
		-- La je n'imagine pas que pointp1prime est à nouveau dans la terre.
		iter:=iter+1;
	end loop;
	--if iter=10 then 
		raise notice '\t ------------ MOVE P1 INTO SEA - IMPOSSIBLE DE BOUGER p1 % pointp1prime %', st_asewkt(pointp1), st_asewkt(pointp1prime);
		return pointp1;
	--end if;
	--return pointp1prime;

END;
$BODY$
language plpgsql;