-- Christine Plumejeaud-Perreau, UMR 7301 MIGRINTER
-- date de création : 21 fev 2022
-- Scripts pour calculer les chemins maritimes reliant les ports
-- Archivé dans C:\Travail\ULR_owncloud\ANR_PORTIC\Data\ports\archives_sql\
-- GIT : C:\Travail\Dev\portic_humanum\porticapi\sql\maritime_paths-21fev2022.sql
-- Date de dernière mise à jour : 21 fev 2022
-- Licence : GPL v3


--------------------------------------------------------------------------------------------
-- Reprendre portic_navigo.sql à partir de la ligne 1736
--------------------------------------------------------------------------------------------

-- Faire des segments tout droits

alter table navigoviz.travel add segment geometry;

alter table navigoviz.travel add column id serial;

update navigoviz.travel t set segment = ST_MakeLine(k.fromg, q.tog)
from 
( select  id, from_port_id, point3857 as fromg from  navigoviz.port_points ,  navigoviz.travel
where from_port_id = uhgs_id ) as k,
( select id,  to_port_id, point3857 as tog from  navigoviz.port_points ,  navigoviz.travel
where to_port_id = uhgs_id ) as q
where t.id = k.id and t.id = q.id and t.from_port_id = k.from_port_id and t.to_port_id = q.to_port_id

SELECT jsonb_build_object('type','FeatureCollection','features', jsonb_agg(feature))
FROM (SELECT jsonb_build_object(
                                   'type',       'Feature',
                                   'id',         id,
                                   'geometry',   ST_AsGeoJSON(segment)::jsonb,
                                   'properties', to_jsonb(row) - 'id' - 'segment'
                                 ) AS feature
                                 FROM (
                                 SELECT id, segment, ship_id, captain_id
                                 FROM navigoviz.travel
                                 limit 100
                                 ) row) features;
								 




--------------------------------------------------------------------------------------------
-- Reprendre portic_navigo.sql à partir de la ligne 1856
--------------------------------------------------------------------------------------------

-- today 
-- la table world_borders contient la column mpolygone3857
-- Pas la peine de faire cela. Pour la traçabilité d'où ils viennent ces mpolygone3857

-- import le 16 mai des frontières du monde dans public

ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=navigo dbname=portic password=navigocorpus2018 schemas=navigoviz" /home/plumegeo/navigo/ETL/data_15mai19/TM_WORLD_BORDERS-0.3.shp -append -a_srs EPSG:4326 -nln world_borders_2019 -nlt MULTIPOLYGON

alter table navigoviz.world_borders_2019 add column mpolygone3857 geometry;
update navigoviz.world_borders_2019 set mpolygone3857 = st_setsrid(st_transform(wkb_geometry, 3857), 3857) 
where region in (150, 2, 19, 142);
-- 150 : Europe
-- 2 : Afrique
-- 19 : Amériques
-- 142 : Orient / Asie (de la turquie à l'inde)
-- 9 : Océanie

update navigoviz.world_borders_2019 set mpolygone3857 = st_setsrid(st_transform(wkb_geometry, 3857), 3857) 
where region in (9);

select distinct region from navigoviz.world_borders_2019 where mpolygone3857 is null
-- 0
select * from navigoviz.world_borders_2019   where region = 0

update navigoviz.world_borders_2019 set mpolygone3857 = st_setsrid(st_transform(wkb_geometry, 3857), 3857) 
where iso2 in ('CC'); 
update navigoviz.world_borders_2019 set mpolygone3857 = st_setsrid(st_transform(wkb_geometry, 3857), 3857) 
where region = 0 and iso2 <>'AQ'; 

select * from navigoviz.world_borders_2019 where mpolygone3857 is null
-- 145;"AY";"AQ";"ATA";10;"Antarctica";0;0;0;0;21.304;-80.446

--------------------------------------------------------------------------------------------
-- Reprendre portic_navigo.sql à partir de la ligne 2101
--------------------------------------------------------------------------------------------



-- today : 
-- hypothèse : le segment d'un port à un autre coupe une ou deux fois la cote (un buffer de cote à 1 mile en fait).
-- donc on calcule le point de jonction avec ce buffer au large au départ P1 et à l'arrivée P2,
-- puis on calcule un segment du départ à P1, on découpe le buffer de cote de P1 à P2, puis un segment de P2 à l'arrivée
-- calculs repris de Bochaca / portulans. Il n'y a pas de table trait_cote ni coteline dans Portic.


-- Utiliser la cote pour construire des segments mieux



-- mise à jour de coteline pour simplifier les opérations de géométrie avec une multiligne plutot qu'un polygone.
alter table trait_cote add column coteline geometry;
update trait_cote set coteline = ST_LineMerge(st_boundary(wkb_geometry)) 
where id = '0-E';

CREATE INDEX coteline_geom_idx
  ON public.trait_cote
  USING gist
  (coteline);

CREATE INDEX segment_geom_idx
  ON public.long_trajet
  USING gist
  (segment);
  
alter table long_trajet add column intersection_cote geometry ;

create table intersections_trajet_cote as (
select t.id, st_intersection(segment, coteline) as multiligne, st_startpoint(st_intersection(segment, coteline)) as startP, st_endpoint(st_intersection(segment, coteline)) as endP, st_NPoints(st_intersection(segment, coteline)) as nbpoints
from long_trajet t, trait_cote c where c.id = '0-E' and coteline::box2d && segment::box2d )

select id, st_astext(multiligne), nbpoints, st_astext(ST_StartPoint(ST_LineFromMultiPoint(multiligne))) as start, st_astext(ST_PointN(ST_LineFromMultiPoint(multiligne), 2)) as end  from intersections_trajet_cote
where nbpoints > 1

alter table intersections_trajet_cote add column intersectionPoint geometry
update intersections_trajet_cote set intersectionPoint= (st_dump(multiligne)).geom where nbpoints = 1

-- utiliser le dump sur une multiligne
select st_astext((st_dump(intersection_cote)).geom), (st_dump(intersection_cote)).path[1] from long_trajet where id = 75

update long_trajet t set intersection_cote = point_ligne 
from (
select id, nbpoints, point_ligne, ordre from (
select id, nbpoints, st_astext((st_dump(multiligne)).geom) as point_ligne, (st_dump(multiligne)).path[1] as ordre from intersections_trajet_cote where nbpoints > 1 order by id, ordre desc ) as k
where k.ordre = 1) as i
where i.nbpoints > 1 and t.id = i.id


alter table long_trajet add column f1 float;
alter table long_trajet add column f2 float;

update long_trajet t set f1 = ST_Line_Locate_Point(coteline, intersection_cote)
-- select t.id, ST_Line_Locate_Point(coteline, intersection_cote), ST_Line_Locate_Point(coteline, a2.point) 
-- st_length(ST_Line_Substring(coteline, ST_Line_Locate_Point(coteline, intersection_cote), ST_Line_Locate_Point(coteline, a2.point))) / 1000.0
from trait_cote c,  amers a2  
where c.id = '0-E' and t.amer_arrivee=a2.identifiant 



update long_trajet t set f2 = ST_Line_Locate_Point(coteline, a2.point)
-- select t.id, ST_Line_Locate_Point(coteline, intersection_cote), ST_Line_Locate_Point(coteline, a2.point) 
-- st_length(ST_Line_Substring(coteline, ST_Line_Locate_Point(coteline, intersection_cote), ST_Line_Locate_Point(coteline, a2.point))) / 1000.0
from trait_cote c,  amers a2  
where c.id = '0-E' and t.amer_arrivee=a2.identifiant 

alter table long_trajet add column cabotinage geometry;
update long_trajet set cabotinage = ST_Line_Substring(coteline, CASE WHEN f1<f2 THEN f1 ELSE f2 END,  CASE WHEN f1<f2 THEN f2 ELSE f1 END)
from trait_cote c where c.id = '0-E'

--- IMPORTANT : generalisation de la cote
update long_trajet t set cabotinage = ST_SimplifyPreserveTopology(ST_Line_Substring(coteline, CASE WHEN f1<f2 THEN f1 ELSE f2 END,  CASE WHEN f1<f2 THEN f2 ELSE f1 END), 0.01)
from trait_cote c where c.id = '0-E'  ;
-- c'est bien (plus on baisse plus c'est précis) mais inutile
update long_trajet t set cabotinage = ST_ConcaveHull(ST_Line_Substring(coteline, CASE WHEN f1<f2 THEN f1 ELSE f2 END,  CASE WHEN f1<f2 THEN f2 ELSE f1 END), 0.80)
from trait_cote c where c.id = '0-E'  ;
-- OK




--------------------------------------------------------------------------------------------
-- Reprendre cleaning.sql à partir de la ligne 24
--------------------------------------------------------------------------------------------

-- today : les tables navigoviz.shoreline et navigoviz.sea20mile étaient trop grosses, elles ont été supprimées

 "navigoviz.shoreline";"29 GB"
  "navigoviz.sea20mile";"3333 MB"
"navigoviz.coastal_areas";"408 MB"

alter table navigoviz.shoreline drop column valid_poly_20_mile;
alter table navigoviz.shoreline drop column line_20_mile;
alter table navigoviz.shoreline drop column wkb_geometry;


--------------------------------------------------------------------------------------------
-- Reprendre portic_navigo.sql à partir de la ligne 1736
--------------------------------------------------------------------------------------------

-- today : 
-- d'où venait coastal_areas : de GSHHS_f_L1_3857.shp
-- d'où venait shoreline : de GSHHS_i_L1.shp
-- Sauvegarde des données dans des shapefiles bien utiles qui permettent de reconstruire
-- navigoviz.shoreline avec buffer_1mile, buffer_20mile, shoreline (?), valid_poly_20_mile

export PGCLIENTENCODING=utf8
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=navigo dbname=portic password=navigocorpus2018 schemas=navigoviz" /home/plumegeo/navigo/ETL/data_27fev2019/GSHHS_f_L1_3857.shp -append -a_srs EPSG:3857 -nln coastal_areas
comment on table navigoviz.coastal_areas is 'Import from NOAH shapefile : GSHHS_f_L1_3857.shp'
-- note : en fait la projection est 4326

export PGCLIENTENCODING=utf8
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=navigo dbname=portic password=navigocorpus2018 schemas=navigoviz" /home/plumegeo/navigo/ETL/data_21mai19/GSHHS_i_L1.shp -append -a_srs EPSG:4326 -nln shoreline
comment on table navigoviz.shoreline is 'Import from NOAH shapefile : GSHHS_i_L1.shp'




pgsql2shp -f "/home/plumegeo/navigo/buffer_1mile.shp" -u navigo -P navigocorpus2018 portic navigoviz.shoreline
pgsql2shp -f "/home/plumegeo/navigo/buffer_1mile.shp" -u navigo -P navigocorpus2018 portic "SELECT ogc_fid, id, region, buffer_1mile FROM navigoviz.shoreline where buffer_1mile is not null"
pgsql2shp -f "/home/plumegeo/navigo/buffer_20mile.shp" -u navigo -P navigocorpus2018 portic "SELECT ogc_fid, id, region, buffer_20mile FROM navigoviz.shoreline where buffer_20mile is not null"
pgsql2shp -f "/home/plumegeo/navigo/shoreline.shp" -u navigo -P navigocorpus2018 portic "SELECT ogc_fid, id, region, shoreline FROM navigoviz.shoreline where shoreline is not null"
pgsql2shp -f "/home/plumegeo/navigo/sea_20mile.shp" -u navigo -P navigocorpus2018 portic "SELECT  id, valid_poly_20_mile FROM navigoviz.sea20mile where valid_poly_20_mile is not null "

--------------------------------------------------------------------------------------------
-- Reprendre travel.sql ligne 289
--------------------------------------------------------------------------------------------

-- today : comment shoreline a été améliorée et ce qu'elle contient. 

-- polygone3857 = st_setsrid(st_transform(st_setsrid(wkb_geometry, 4326), 3857), 3857)
-- shoreline.region : pays ou code region (continent ) de world borders 2019 
-- buffer_1mile = st_buffer(polygone3857, 1852)
-- buffer_20mile = st_buffer(polygone3857, 40000)
-- shoreline = ST_LineMerge(st_boundary(polygone3857)) 
-- valid_poly_20_mile = sea20mile.valid_poly_20_mile
-- line_20_mile = sea20mile.line_20_mile

-- sea20mile : valid_poly_20_mile qui ressemble à buffer_20mile


-- 21 mai 2019 / trajectoiress

-- préprocessing de coastal_area : ajouter region, pays, et 2 buffers
alter table  navigoviz.coastal_areas add column region numeric;
alter table  navigoviz.shoreline add column region numeric;

select count(*) from  navigoviz.coastal_areas
-- 179819

alter table navigoviz.coastal_areas alter column  wkb_geometry type geometry 
-- using wkb_geometry::st_setsrid(wkb_geometry, 4326)
alter table navigoviz.coastal_areas add column polygone3857 geometry
update navigoviz.coastal_areas  set polygone3857 = st_setsrid(st_transform(st_setsrid(wkb_geometry, 4326), 3857), 3857)
-- Query returned successfully: 179819 rows affected, 12.0 secs execution time.

comment on table navigoviz.shoreline is 'Import from NOAH shapefile : GSHHS_i_L1.shp'
alter table navigoviz.shoreline alter column  wkb_geometry type geometry 
alter table navigoviz.shoreline add column polygone3857 geometry
update navigoviz.shoreline  set polygone3857 = st_setsrid(st_transform(st_setsrid(wkb_geometry, 4326), 3857), 3857)
-- Query returned successfully: 32834 rows affected, 1.0 secs execution time.


select ST_ConcaveHull(polygone3857, 0.80) 
from navigoviz.coastal_areas  c
where c.id = '0-E' 
-- 7 s


select ST_buffer(polygone3857, 1852) 
from navigoviz.shoreline  c
where c.id = '0-E' 

 
select c.ogc_fid, b.name, b.iso2, b.region,  st_area(st_intersection(c.polygone3857, b.mpolygone3857))
from  navigoviz.port_points p, navigoviz.world_borders_2019 b, navigoviz.shoreline c
where p.country2019_iso2code is not null and p.country2019_iso2code = b.iso2 and c.id = '0-E' and st_intersects(c.polygone3857, b.mpolygone3857)  

update navigoviz.shoreline cotes set region = k.region
from (
select c.ogc_fid, b.name, b.iso2, b.region, st_intersects(c.wkb_geometry, b.mpolygone3857), st_area(st_intersection(c.wkb_geometry, b.mpolygone3857))
from  navigoviz.port_points p, navigoviz.world_borders_2019 b, navigoviz.shoreline c
where p.country2019_iso2code is not null and p.country2019_iso2code = b.iso2 and st_intersects(c.polygone3857, b.mpolygone3857) ) as k
where cotes.ogc_fid = ogc_fid

select  distinct c.id, p.country2019_iso2code 
from  navigoviz.port_points p, navigoviz.shoreline c 
where p.country2019_region='150'  and st_contains(c.polygone3857, point3857)  

select  distinct c.id, p.country2019_iso2code 
from  navigoviz.port_points p, navigoviz.shoreline c 
where st_contains(c.polygone3857, point3857) 

-- calculer le buffer1 à 1 mile nautique (soit 1852 m ) 
alter table navigoviz.shoreline add column buffer_1mile geometry;

update  navigoviz.shoreline  set buffer_1mile = st_buffer(polygone3857, 1852)
where id in (select  distinct c.id
from  navigoviz.port_points p, navigoviz.shoreline c 
where st_contains(c.polygone3857, point3857) )
-- Query returned successfully: 111 rows affected, 14.2 secs execution time.

-- et le buffer2 à 20 miles nautiques (soit 40 km )
alter table navigoviz.shoreline add column buffer_20mile geometry;

update  navigoviz.shoreline  set buffer_20mile = st_buffer(polygone3857, 40000)
where id in (select  distinct c.id
from  navigoviz.port_points p, navigoviz.shoreline c 
where st_contains(c.polygone3857, point3857) )
-- Query returned successfully: 111 rows affected, 29.8 secs execution time.

update navigoviz.shoreline  set buffer_20mile = buffer_1mile
-- UPDATE 32834 Query returned successfully in 836 msec. Fix d'un bug

alter table navigoviz.shoreline add column shoreline geometry;
update navigoviz.shoreline set shoreline = ST_LineMerge(st_boundary(polygone3857)) 
where id in (select  distinct c.id
from  navigoviz.port_points p, navigoviz.shoreline c 
where st_contains(c.polygone3857, point3857) )
-- Query returned successfully: 111 rows affected, 12.5 secs execution time.

update navigoviz.shoreline set shoreline = ST_LineMerge(st_boundary(polygone3857)) 
where id in (select  distinct c.id
from  navigoviz.port_points p, navigoviz.shoreline c 
where shoreline is null and st_contains(st_buffer(c.polygone3857, 40000), point3857) )
-- UPDATE 1548 Query returned successfully in 12 secs 3 msec.

-- il en manque trop et ca fait buguer l'algo
-- découvert le 11 juillet 2022

update navigoviz.shoreline set region = p.country2019_region :: integer
from navigoviz.port_points p
where st_contains(polygone3857, point3857)
-- UPDATE 111 Query returned successfully in 13 secs 537 msec.

CREATE INDEX shoreline_buffer_1mile_idx
    ON navigoviz.shoreline USING gist
    (buffer_1mile)
    TABLESPACE pg_default;

CREATE INDEX port_points3857_idx
    ON navigoviz.port_points USING gist
    (point3857)
    TABLESPACE pg_default;

CREATE INDEX shoreline_polygone3857_idx
    ON navigoviz.shoreline USING gist
    (polygone3857)
    TABLESPACE pg_default;
	
vacuum analyze navigoviz.shoreline
vacuum analyze navigoviz.port_points
	
alter table navigoviz.port_points add column port_on_shoreline geometry;
alter table navigoviz.port_points add column id_shoreline integer;
alter table navigoviz.port_points alter column id_shoreline type text;
alter table navigoviz.port_points add column distance_shoreline float;


update  navigoviz.port_points ports set port_on_shoreline = k.port_on_shoreline, id_shoreline = k.id
from (
	select p.ogc_fid, p.toponyme, p.country2019_name, c.region, c.id, ST_ClosestPoint(shoreline, point3857) as port_on_shoreline
			  from navigoviz.port_points p, navigoviz.shoreline c 
			  where p.country2019_region::int = c.region and st_contains(c.buffer_1mile, point3857) 
    ) as k
where ports.ogc_fid = k.ogc_fid
-- UPDATE 1232 Query returned successfully in 2 secs 284 msec.

update  navigoviz.port_points ports set port_on_shoreline = k.port_on_shoreline, id_shoreline = k.id
from (
	select p.ogc_fid, p.toponyme, p.country2019_name, c.region, c.id, ST_ClosestPoint(shoreline, point3857) as port_on_shoreline
			  from navigoviz.port_points p, navigoviz.shoreline c 
			  where p.country2019_region::int = c.region and st_contains(c.buffer_20mile, point3857) 
    ) as k
where ports.ogc_fid = k.ogc_fid
-- UPDATE 1364 Query returned successfully in 2 secs 40 msec.

select count(*) from navigoviz.port_points where port_on_shoreline is null
-- 128
select * from navigoviz.port_points where port_on_shoreline is null

update  navigoviz.port_points ports set port_on_shoreline = k.port_on_shoreline, id_shoreline = k.id
from (
	select p.ogc_fid, p.toponyme, p.country2019_name, c.region, c.id, ST_ClosestPoint(shoreline, point3857) as port_on_shoreline
			  from navigoviz.port_points p, navigoviz.shoreline c 
			  where port_on_shoreline is null and st_contains(c.buffer_1mile, point3857) 
    ) as k
where ports.ogc_fid = k.ogc_fid
-- UPDATE 28 Query returned successfully in 296 msec.

update  navigoviz.port_points ports set port_on_shoreline = k.port_on_shoreline, id_shoreline = k.id
from (
	select p.ogc_fid, p.toponyme, p.country2019_name, c.region, c.id, ST_ClosestPoint(shoreline, point3857) as port_on_shoreline
			  from navigoviz.port_points p, navigoviz.shoreline c 
			  where port_on_shoreline is null and st_contains(c.buffer_20mile, point3857) 
    ) as k
where ports.ogc_fid = k.ogc_fid
-- 0

CREATE INDEX shoreline_polygone3857_idx
    ON navigoviz.shoreline USING gist
    (polygone3857)
    TABLESPACE pg_default;


-- 
update  navigoviz.port_points ports set port_on_shoreline = k2.port_on_shoreline, id_shoreline = k2.id, distance_shoreline=k1.dmin
from (
	select p.ogc_fid, min(st_distance(shoreline, point3857)) as dmin 
			  from navigoviz.port_points p, navigoviz.shoreline c 
			  where port_on_shoreline is null and st_contains(st_buffer(polygone3857, 100000), point3857) 
			group by p.ogc_fid
    ) as k1,
	(select p.ogc_fid, p.toponyme, p.country2019_name, c.region, c.id, st_distance(shoreline, point3857) as d, ST_ClosestPoint(shoreline, point3857) as port_on_shoreline
			  from navigoviz.port_points p, navigoviz.shoreline c 
			  where port_on_shoreline is null and st_contains(st_buffer(polygone3857, 100000), point3857) 
    ) as k2
where ports.ogc_fid = k2.ogc_fid and k1.ogc_fid=k2.ogc_fid and k2.d = k1.dmin
-- UPDATE 65 : Query returned successfully in 1 hr 15 min.
-- Il existe encore des points à plus de 100 km

select max(distance_shoreline) from navigoviz.port_points 
-- "1 145 144.18125221"
select avg(distance_shoreline) from navigoviz.port_points 
-- "11 625.8679020365" 

select count(*) from navigoviz.port_points where distance_shoreline > 11000
-- 299

select count(*), stddev(distance_shoreline), avg(distance_shoreline), min(distance_shoreline)
 from navigoviz.port_points where distance_shoreline > 11000 
 299;91705.8785666979;48908.03511765;11184.5463820759

 select count(*), stddev(distance_shoreline), avg(distance_shoreline), min(distance_shoreline)
 from navigoviz.port_points where distance_shoreline > 11000 and distance_shoreline < 50000 
 -- 220;10314.9115122314;24881.6341865354;11184.5463820759
 
 select count(*), stddev(distance_shoreline), avg(distance_shoreline), min(distance_shoreline)
 from navigoviz.port_points where distance_shoreline >  50000 
 -- 79;160216.303347737;115816.999735944;50283.0014149017

  select count(*), stddev(distance_shoreline), avg(distance_shoreline), min(distance_shoreline)
 from navigoviz.port_points where distance_shoreline >  50000 and distance_shoreline < 120000 
67;17156.6072102021;71930.8511683385;50283.0014149017

select uhgs_id, toponyme, country2019_name, country2019_iso2code, country2019_region, id_shoreline, distance_shoreline from navigoviz.port_points where distance_shoreline > 120000
/*
"A0164829";"Isle of France";"France";"FR";"150";"0-E";208789.64674423
"A0355726";"Portugal";"Portugal";"PT";"150";"0-E";120101.923012505
"A1964109";"Russie";"Russia";"RU";"150";"0-E";1145144.18125221
"A0840357";"Norge";"Norway";"NO";"150";"0-E";228971.04074508
"A0760751";"Oldenbourg";"Germany";"DE";"150";"0-E";145916.653408184
"A0167415";"Frankrig";"France";"FR";"150";"0-E";320097.285027709
"A1632045";"Moriske kyst";"Morocco";"MA";"2";"1";336180.778941555
"B2829143";"Natches";"United States";"US";"19";"2";200455.25315318
"B2488604";"Guayana";"Venezuela";"VE";"19";"3";168801.064276664
"B2681643";"Rio de Janeiro";"Peru";"PE";"19";"3";901496.654470597
"A1081238";"Suede";"Sweden";"SE";"150";"0-E";243480.720213092
"A1467456";"Levissy en Natolie";"Turkey";"TR";"142";"0-E";310740.749615857
*/




-- ligne 677 ??



--------------------------------------------------------------------------------------------
-- Reprendre routing.sql
--------------------------------------------------------------------------------------------

-- today : crée la table sea20mile

-- créer des buffer propres à 40 km des cotes


-- https://postgis.net/docs/ST_ClusterIntersecting.html
-- https://georezo.net/forum/viewtopic.php?pid=283598#p283598
create table navigoviz.sea20mile as (
SELECT row_number() OVER() as id, ST_LineMerge(st_boundary(unnest(ST_ClusterIntersecting(buffer_20mile)) )) as line_20_mile
FROM shoreline
);

create table navigoviz.sea20mile as (
SELECT row_number() OVER() as id, unnest(ST_ClusterIntersecting(buffer_20mile)) as poly_20_mile
FROM shoreline
);

select st_collectionextract(poly_20_mile, 3) from navigoviz.sea20mile limit 10
-- SELECT 447 Query returned successfully in 5 secs 771 msec.

create table navigoviz.sea20miles as (
select id, (st_dump(st_collectionextract(poly_20_mile,3))).geom from sea20mile
)
-- SELECT 32834 Query returned successfully in 576 msec.
drop table navigoviz.sea20miles

alter table navigoviz.sea20mile s add column valid_poly_20_mile geometry
update navigoviz.sea20mile s set valid_poly_20_mile = k.geom
from (
SELECT id, st_union(st_makevalid(geom)) as geom 
FROM sea20miles
GROUP BY id) as k
where k.id = s.id

alter table navigoviz.sea20mile  add column line_20_mile geometry
update navigoviz.sea20mile set line_20_mile = ST_LineMerge(st_boundary(valid_poly_20_mile))
-- UPDATE 447 en 20 s

-- today : calcul de line_1mile et port_on_line_1mile

-- calculer le point d'intersection avec le buffer à 1 mile du port (point le plus proche projeté en ligne droite vers la cote)
update navigoviz.shoreline set line_1mile = ST_LineMerge(st_boundary(buffer_1mile)) 
-- UPDATE 32834, 3 sec
alter table navigoviz.port_points add column port_on_line_1mile geometry;
--
update navigoviz.port_points set port_on_line_1mile = ST_ClosestPoint(line_1mile, port_on_shoreline), distance_shoreline = st_distance(port_on_shoreline, point3857) 
from navigoviz.shoreline s 
where port_on_shoreline is not null and s.id = id_shoreline 

--------------------------------------------------------------------------------------------
-- Reprendre travel.sql ligne 658
--------------------------------------------------------------------------------------------
-- today : ajoute line_20_mile et port_on_line_1mile
-- Extrait de ligne 658 de travel.sql

alter table shoreline add column valid_poly_20_mile geometry;
alter table shoreline add column line_20_mile geometry;

update shoreline set valid_poly_20_mile = s.valid_poly_20_mile , line_20_mile=s.line_20_mile
from  sea20mile s
where st_contains(s.valid_poly_20_mile, buffer_20mile) 
-- requete en attente

update sea20mile set valid_poly_20_mile = st_setsrid(valid_poly_20_mile, 3857)
-- Query returned successfully: 447 rows affected, 25.7 secs execution time.
update sea20mile set line_20_mile = st_setsrid(line_20_mile, 3857)

select st_srid(valid_poly_20_mile) from sea20mile
-- 0
select st_srid(buffer_20mile) from shoreline limit 10


--------------------------------------------------------------------------------------------
-- Reprendre update_shoreline.sql
--------------------------------------------------------------------------------------------

-- today : ok, vu dans travel.sql avant. Script pour faire un batch. Long
-- Script pour mettre à jour line_20_mile et valid_poly_20_mile dans shoreline

update shoreline set valid_poly_20_mile = s.valid_poly_20_mile , line_20_mile=s.line_20_mile
from  sea20mile s
where st_contains(s.valid_poly_20_mile, buffer_20mile) ;

--------------------------------------------------------------------------------------------
-- Suite le 21 février 2022
--------------------------------------------------------------------------------------------

-- C:\Travail\Dev\portic_humanum\porticapi\sql\maritime_paths-21fev2022.sql
export PGCLIENTENCODING=utf8
ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v7 password=mesange17 schemas=navigoviz" /home/plumegeo/navigo/GSHHS_i_L1.shp -overwrite -a_srs EPSG:4326 -nln shoreline
comment on table navigoviz.shoreline is 'Import from NOAH shapefile : GSHHS_i_L1.shp'

ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v7 password=mesange17 schemas=navigoviz" /home/plumegeo/navigo/shoreline.shp -append -a_srs EPSG:3857 -nln shoreline_line_3847
comment on table navigoviz.shoreline_line_3847 is 'Import from NOAH shapefile : GSHHS_i_L1.shp avec wkb_geometry =  la ligne cotière en projection 3857'

ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v7 password=mesange17 schemas=navigoviz" /home/plumegeo/navigo/buffer_1mile.shp -append -a_srs EPSG:3857 -nln shoreline_buffer_1mile
comment on table navigoviz.shoreline_buffer_1mile is 'Import from buffer_1mile : wkb_geometry = buffer de 1852 m autour de GSHHS_i_L1.shp , en projection 3857'

ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=postgres dbname=portic_v7 password=mesange17 schemas=navigoviz" /home/plumegeo/navigo/sea_20mile.shp -overwrite -a_srs EPSG:3857 -nln shoreline_valid_20mile -nlt PROMOTE_TO_MULTI
comment on table navigoviz.shoreline_valid_20mile is 'Import from sea_20mile : wkb_geometry = buffer de 40 km (20 miles) autour de GSHHS_i_L1.shp , en projection 3857, unioné, et rendu valide'
-- attention, il y aura plein de ligne avec le même id, mais un ogc_fid différents (multi-polygones)

alter table navigoviz.shoreline add column  region  int ;
update navigoviz.shoreline s set region = w.region 
from shoreline_line_3847 w where s.id = w.id

alter table navigoviz.shoreline alter column  wkb_geometry type geometry ;
alter table navigoviz.shoreline add column polygone3857 geometry;
update navigoviz.shoreline  set polygone3857 = st_setsrid(st_transform(st_setsrid(wkb_geometry, 4326), 3857), 3857);

alter table navigoviz.shoreline add column shoreline geometry;
update navigoviz.shoreline s set shoreline = w.wkb_geometry 
from shoreline_line_3847 w where s.id = w.id

alter table navigoviz.shoreline add column buffer_1mile geometry;
update navigoviz.shoreline s set buffer_1mile = w.wkb_geometry 
from shoreline_buffer_1mile w where s.id = w.id


drop table shoreline_line_3847;
drop table shoreline_buffer_1mile;

select count(distinct id) from shoreline --32834 lignes
select count(distinct id) from shoreline_line_3847 -- 1659
select count(distinct id) from shoreline_valid_20mile -- 1659

comment on column navigoviz.shoreline.shoreline is ' shoreline = ST_LineMerge(st_boundary(polygone3857)) ';
comment on column navigoviz.shoreline.wkb_geometry  is ' wkb_geometry = geometry polygonale de GSHHS_i_L1.shp en 4326 (wgs 84) ';
comment on column navigoviz.shoreline.polygone3857  is ' polygone3857 = st_setsrid(st_transform(st_setsrid(wkb_geometry, 4326), 3857), 3857) ';
comment on column navigoviz.shoreline.buffer_1mile  is ' buffer_1mile = st_buffer(polygone3857, 1852) ';

select * from shoreline_line_3847

select count(*) from navigoviz.shoreline_valid_20mile c --447

alter table navigoviz.shoreline_valid_20mile  add column line_20_mile geometry
update navigoviz.shoreline_valid_20mile set line_20_mile = ST_LineMerge(st_boundary(wkb_geometry))
-- UPDATE 447 en 20 s



-- alter table ports.port_points add column port_on_line_1mile geometry;
-- alter table ports.port_points  drop port_on_line_1mile

alter table ports.port_points add column port_on_shoreline geometry;
alter table ports.port_points add column id_shoreline  text;
alter table ports.port_points add column distance_shoreline float;
alter table ports.port_points  add column type_cote text;

select count(*) from ports.port_points
-- 1205
-- 1706 en v8

CREATE INDEX shoreline_polygone3857_idx
    ON navigoviz.shoreline USING gist
    (polygone3857)
    TABLESPACE pg_default;

CREATE INDEX shoreline_buffer_1mile_idx
    ON navigoviz.shoreline USING gist
    (buffer_1mile)
    TABLESPACE pg_default;
   
CREATE INDEX port_points3857_idx
    ON ports.port_points USING gist
    (point3857)
    TABLESPACE pg_default;
   
CREATE INDEX shoreline_valid_20mile_wkb_geometry_idx
    ON navigoviz.shoreline_valid_20mile USING gist
    (wkb_geometry)
    TABLESPACE pg_default;
 
CREATE INDEX shoreline_valid_20mile_line_20_mile_idx
    ON navigoviz.shoreline_valid_20mile USING gist
    (line_20_mile)
    TABLESPACE pg_default;
   
-- calculer id_shoreline 
select count(*) from ports.port_points where port_on_shoreline is null; --299 -- , id_shoreline, distance_cote, type_cote 
select count(*) from ports.port_points where ogc_fid is null; --273
select count(*) from ports.port_points where point3857 is not null -- 1699/1706
select * from ports.port_points where ogc_fid is not null; --273

-- en V8  : calculer id_shoreline - Pour les ports à terre ou proches de la cote
update  ports.port_points ports set port_on_shoreline = k2.port_on_shoreline, id_shoreline = k2.id, distance_shoreline=k1.dmin, type_cote='1mile'
from (
	select p.uhgs_id, min(st_distance(c.shoreline, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline c
			  where port_on_shoreline is null and st_contains(c.buffer_1mile, point3857)   
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.id, st_distance(c.shoreline , point3857) as d, ST_ClosestPoint(shoreline, point3857) as port_on_shoreline, '1mile' as type_cote
			  from ports.port_points p, navigoviz.shoreline c
			  where port_on_shoreline is null and st_contains(c.buffer_1mile, point3857) 
    ) as k2
where ports.port_on_shoreline is null and ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- 210 

-- en V8  : calculer id_shoreline - Pour les ports en mer ou loin de la cote
update  ports.port_points ports set port_on_shoreline = k2.port_on_shoreline, id_shoreline = k2.id, distance_shoreline=k1.dmin, type_cote='20mile'
from (
	select p.uhgs_id, min(st_distance(c.line_20_mile, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline_valid_20mile  c
			  where point3857 is not null and port_on_shoreline is null and st_contains(c.wkb_geometry, point3857)   
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.id, st_distance(c.line_20_mile , point3857) as d, ST_ClosestPoint(c.line_20_mile, point3857) as port_on_shoreline, '1mile' as type_cote
			  from ports.port_points p, navigoviz.shoreline_valid_20mile c
			  where point3857 is not null and port_on_shoreline is null and st_contains(c.wkb_geometry, point3857) 
    ) as k2
where ports.port_on_shoreline is null and ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- 48 en  4 min

-- en V8  : calculer id_shoreline - Pour les ports très loin de la cote (34 dont / 3 à 50 km, 14 à 100 km
update  ports.port_points ports set port_on_shoreline = k2.port_on_shoreline, id_shoreline = k2.id, distance_shoreline=k1.dmin, type_cote='100km'
from (
	select p.uhgs_id, min(st_distance(c.shoreline, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline c
			  where port_on_shoreline is null and st_contains(st_buffer(c.buffer_1mile, 100000), point3857)   
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.id, st_distance(c.shoreline , point3857) as d, ST_ClosestPoint(shoreline, point3857) as port_on_shoreline, '50km' as type_cote
			  from ports.port_points p, navigoviz.shoreline c
			  where port_on_shoreline is null and st_contains(st_buffer(c.buffer_1mile,100000), point3857) 
    ) as k2
where ports.port_on_shoreline is null and ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;

-- Pour les ports à terre ou proches de la cotes
update  ports.port_points ports set port_on_shoreline = k.port_on_shoreline, id_shoreline = k.id
from (
	select p.ogc_fid, p.toponyme, p.country2019_name, c.region, c.id, ST_ClosestPoint(shoreline, point3857) as port_on_shoreline
			  from ports.port_points p, navigoviz.shoreline c 
			  where port_on_shoreline is null and st_contains(c.buffer_1mile, point3857) 
    ) as k
where ports.ogc_fid = k.ogc_fid;
-- 1127 lignes en 1.7 s +225

update ports.port_points ports set distance_shoreline = k.distance_cote, type_cote='1mile'
from (
	select p.ogc_fid, p.toponyme, p.country2019_name, c.id, st_distance(shoreline, point3857) as distance_cote
			  from ports.port_points p, navigoviz.shoreline c 
			  where  type_cote is null and st_contains(c.buffer_1mile, point3857) 
    ) as k
where ports.ogc_fid = k.ogc_fid;

-- Pour les ports en mer ou loin de la cote
update  ports.port_points ports set port_on_shoreline = k.port_on_shoreline, id_shoreline = k.id
from (
	select  p.ogc_fid, p.toponyme, p.country2019_name, c.ogc_fid as identifiant_poly, c.id, ST_ClosestPoint(line_20_mile, point3857) as port_on_shoreline
		from ports.port_points p, navigoviz.shoreline_valid_20mile c 
	 where  point3857 is not null and port_on_shoreline is null and st_contains(c.wkb_geometry, point3857)  
    ) as k
where ports.ogc_fid = k.ogc_fid;
-- 63 lignes en 3 min + 7

update  ports.port_points ports set distance_shoreline = k.distance_cote, type_cote='20mile'
from (
	select  p.ogc_fid, p.toponyme, p.country2019_name, c.ogc_fid as identifiant_poly, c.id, st_distance(line_20_mile, point3857) as distance_cote
		from ports.port_points p, navigoviz.shoreline_valid_20mile c 
	 where  point3857 is not null and type_cote is null and port_on_shoreline is not null and st_contains(c.wkb_geometry, point3857)  
    ) as k
where ports.ogc_fid = k.ogc_fid; 
-- 52 en 2 min 33 + 3


-- Vérif
select  p.ogc_fid, count(distinct c.id) --, ST_ClosestPoint(line_20_mile, point3857) as port_on_shoreline
		from ports.port_points p, navigoviz.shoreline_valid_20mile c 
	 where point3857 is not null  and st_contains(c.wkb_geometry, point3857) 
	 group by p.ogc_fid
-- 63 lignes, toutes avec count = 1 -- and port_on_shoreline is null

select uhgs_id, toponyme , toponyme_standard_fr, country2019_name , state_1789_fr , province , distance_shoreline, port_on_shoreline, id_shoreline
from ports.port_points where type_cote is null and point3857 is not null; 
-- A1965005	Côtes de Barbarie	Algeria
-- A1964713	Mer Mediterranée
-- B0000715	Grands Bancs de Terre-Neuve
-- C0000019	Cap-Vert

/*
 * C0000019	Cap-Vert	Cape Verde	Portugal
A1965005	Côtes de Barbarie	Algeria	multi-Etat
A1965020	Par le travers de Carthagène	Spain	zone maritime
A1963986	Mer Baltique		zone maritime
A1968880	Majorque (18 lieues au nord)	Spain	zone maritime
A1968859	Malte (27 lieues au sud)	Malta	zone maritime
A1968882	cap Saint-Antoine (à 2 lieues)	Spain	zone maritime
B0000935	Groenland		Danemark
D0000004	Indes orientales		multi-Etat
B0000715	Grands Bancs de Terre-Neuve		zone maritime
B0000713	Baie de Hara	Canada	Grande-Bretagne
D3674030	Chine		Chine
A1964713	Mer Mediterranée		zone maritime
C0000006	Côte d'Afrique [de l'Ouest]		multi-Etat
A0037077	Canal de Malte	Italy	Malte
C0000013	Mozambique		Portugal
B0000978	colonies anglaises [en Amérique]		Grande-Bretagne
B0000970	Colonies françaises en Amérique	Guadeloupe	France
A1968875	35 lat. nord et 45 long. du méridien de Paris		zone maritime
 */

/* V8 
 * select uhgs_id, toponyme , toponyme_standard_fr, country2019_name , state_1789_fr , province , distance_shoreline, port_on_shoreline, id_shoreline
from ports.port_points where type_cote is null and point3857 is not null; 
 */
--A1964713	Mediterranée	Mer Mediterranée		zone maritime
--A0037077	Canal de Malte	Canal de Malte	Italy	Malte
--A1969359	A 12 lieues du Cap Saint Sébastien			
--A1964261	Au sud de Mayorque			
--B0000978	Colonies anglaises	colonies anglaises [en Amérique]		Grande-Bretagne
--C0000006	Côte d' Afrique [de l' Ouest]	Côte d'Afrique [de l'Ouest]		multi-Etat
--C0000019	Cap Vert	Cap-Vert	Cape Verde	Portugal
--A1967566	Sur Mahon			
--A1965228	25 à 30 lieues du Cap Sicié			
--A1964702	A la hauteur d' Alicante			
--A1965021	Sur Carthagène			
--A1965240	Sur les parages d' Arzeoun			
--B0000715	Banc de Terre Neuve	Grands Bancs de Terre-Neuve		zone maritime
--A1964251	Dans le Golfe de Lyon			
--A1969437	Au sud est de l' Alboran		Spain	
--A1964250	A la hauteur d' Yvice			
--A1968859	27 lieues au sud de Malte	Malte (27 lieues au sud)	Malta	zone maritime
--D0000004	Inde	Indes orientales		multi-Etat
--A1968875	35 degrés de latitude nord et 45 degrés de longitude du méridien de Paris	35 lat. nord et 45 long. du méridien de Paris		zone maritime
--B0000979	Grand Baye	Grande Baie		
--A1969357	Dix lieues à l' ouest de Palamos			
--A1968880	à 18 lieues au nord de Mayorque	Majorque (18 lieues au nord)	Spain	zone maritime
--A1964164	Au sud de Mahon			
--A1969436	entre Zante et Tunis			
--B0000713	Baÿe de Hara en Terreneuve	Baie de Hara	Canada	Grande-Bretagne
--A0220290	Cote des Bouq	Port-de-Bouc (sur)	France	France
--D3674030	Bien Dong	Chine		Chine
--A1965005	Cotte d' Afrique	Côtes de Barbarie	Algeria	multi-Etat
--A1965020	Par le travers de Carthagène	Par le travers de Carthagène	Spain	zone maritime
--B0000935	Groenland	Groenland		Danemark
--A1968882	à dix lieues du cap Saint Antoine	cap Saint-Antoine (à 2 lieues)	Spain	zone maritime
--C0000013	Mozambique	Mozambique		Portugal
--B0000970	Colonies américaines	Colonies françaises en Amérique	Guadeloupe	France
--A1963986	Baltique	Mer Baltique		zone maritime
/*
-- Calculer la distance minimale pour ces cas (19)

update  navigoviz.port_points ports set port_on_shoreline = k2.port_on_shoreline, id_shoreline = k2.id, distance_shoreline=k1.dmin, type_cote='100km'
from (
	select p.ogc_fid, min(st_distance(c.line_20_mile, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline_valid_20mile c
			  where port_on_shoreline is null and st_contains(st_buffer(c.wkb_geometry, 100000), point3857)  
			group by p.ogc_fid
    ) as k1,
	(select p.ogc_fid, p.toponyme, p.country2019_name, c.id, st_distance(c.line_20_mile , point3857) as d, ST_ClosestPoint(line_20_mile, point3857) as port_on_shoreline, '20mile' as type_cote
			  from ports.port_points p, navigoviz.shoreline_valid_20mile c
			  where port_on_shoreline is null and st_contains(st_buffer(c.wkb_geometry, 100000), point3857) 
    ) as k2
where ports.ogc_fid = k2.ogc_fid and k1.ogc_fid=k2.ogc_fid and k2.d = k1.dmin

select 52 + 19 + 1127
*/

-- Tous les points sont positionnés sur un des buffers
-- sauf 19 en pleine mer. On les garde
-- exporter les géométries des port_on_shoreline, distances_shoreline, id_shoreline 

-- Reste plus qu'à construire la triangulation entre ces points et tous les autres ==> graphe
-- calculer le plus court chemin de chaque port à chaque port ?
-- récupérer le path (suite de points) et l'affecter à une matrice
   
   
-- today : calcul de line_1mile et port_on_line_1mile
alter table navigoviz.shoreline  add column line_1mile geometry;


alter table ports.port_points add port_on_line_1mile  geometry;

-- calculer le point d'intersection avec le buffer à 1 mile du port (point le plus proche projeté en ligne droite vers la cote)
update navigoviz.shoreline set line_1mile = ST_LineMerge(st_boundary(buffer_1mile)) 
-- UPDATE 32834, 3 sec
-- crée des multilinestring aussi
select ogc_fid, id, ST_NumGeometries (line_1mile) from navigoviz.shoreline s  where GeometryType(s.line_1mile) ilike 'MultiLineString'
-- pareil pour les 20 miles. 
select ogc_fid, id, ST_NumGeometries (line_20_mile) from navigoviz.shoreline_valid_20mile   s  where GeometryType(s.line_20_mile) ilike 'MultiLineString'

select ogc_fid, id, shoreline, GeometryType(shoreline), ST_NumGeometries (line_1mile) from navigoviz.shoreline   s  where GeometryType(s.line_1mile) ilike 'MultiLineString'
select ogc_fid, id, st_asText(shoreline), GeometryType(shoreline), ST_NumGeometries (line_1mile) from navigoviz.shoreline   s  
where GeometryType(s.line_1mile) ilike 'MultiLineString'
select distinct(GeometryType(shoreline)) from navigoviz.shoreline where shoreline is not null
select distinct(GeometryType(line_1mile)) from navigoviz.shoreline where shoreline is not null

update ports.port_points set port_on_line_1mile = ST_ClosestPoint(line_1mile, port_on_shoreline)
from navigoviz.shoreline s 
where port_on_shoreline is not null and s.id = id_shoreline AND port_on_line_1mile IS null
-- 1179 + 228

--  sudo -u postgres psql -U postgres -d portic_v7 -c "grant usage on schema ports, navigoviz, navigo, public to navigo;"
sudo -u postgres psql -U postgres -d portic_v7 -c "GRANT SELECT ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, public, ports TO navigo"
sudo -u postgres psql -U postgres -d portic_v7  -c "grant SELECT on all sequences in schema navigoviz, navigo, navigocheck, public, ports to navigo"
sudo -u postgres psql -U postgres -d portic_v7 -c "GRANT USAGE ON SCHEMA navigoviz, navigo, navigocheck, public, ports TO navigo"


pgsql2shp -f "/home/plumegeo/navigo/ports_on_shoreline.shp" -u navigo -P navigocorpus2018 portic_v7 "SELECT ogc_fid, uhgs_id, toponyme_standard_fr, country2019_name , state_1789_fr, substate_1789_fr , province , amiraute, id_shoreline, distance_shoreline, type_cote, port_on_shoreline FROM ports.port_points "
pgsql2shp -f "/home/plumegeo/navigo/ports_on_line_1mile.shp" -u navigo -P navigocorpus2018 portic_v7 "SELECT ogc_fid, uhgs_id, toponyme_standard_fr, country2019_name , state_1789_fr, substate_1789_fr , province , amiraute, id_shoreline, distance_shoreline, type_cote, port_on_line_1mile FROM ports.port_points"

-- pour qgis
ssh -N -L 8007:localhost:5432 -v navigo@134.158.33.179 
-- corpus2018

-- quels sont les ports qui n'ont pas de position sur la shoreline
select * from  port_points pp where pp.port_on_shoreline is null and point3857 is not null --19

select * from  port_points pp where pp.port_on_line_1mile is null and point3857 is not null --19

-- creation de l'extension sfcgal pour bénéficier de la triangulation de Delaunay avec contrainte
CREATE EXTENSION postgis_sfcgal; --ok en local aussi
-- https://postgis.net/docs/ST_ConstrainedDelaunayTriangles.html
-- ST_DelaunayTriangles of 2 polygons. Triangle edges cross polygon boundaries. 
CREATE EXTENSION postgis_pgrouting; 
-- SQL Error [58P01]: ERREUR: n'a pas pu ouvrir le fichier de contrôle d'extension « C:/Program Files/PostgreSQL/14/share/extension/postgis_pgrouting.control » : No such file or directory

select ST_ConstrainedDelaunayTriangles(
               ST_Union(
                       'POLYGON((175 150, 20 40, 50 60, 125 100, 175 150))'::geometry,
                       ST_Buffer('POINT(110 170)'::geometry, 20)
                   )
           );
SELECT
    ST_DelaunayTriangles(
        ST_Union(ST_GeomFromText('POLYGON((175 150, 20 40,
            50 60, 125 100, 175 150))'),
        ST_Buffer(ST_GeomFromText('POINT(110 170)'), 20)
        ))
     As  dtriag;
          
select ST_ConstrainedDelaunayTriangles(s.buffer_1mile) from navigoviz.shoreline s where s.id = '523' -- '0-E'
-- LONG et ... ?


     
SELECT
    ST_DelaunayTriangles(
        ST_Union(buffer_1mile))
     As  dtriag from navigoviz.shoreline s where s.id = '523' 

SELECT 
    ST_DelaunayTriangles(
        ST_Union(buffer_1mile))
     As  dtriag from navigoviz.shoreline s where s.id in ('523' , '235')

SELECT (ST_Dump(dtriag)).geom
FROM (
	SELECT  ST_DelaunayTriangles(ST_Union(buffer_1mile)) As  dtriag 
     from navigoviz.shoreline s where s.id in ('523' , '235')
) as foo 

select count(*)  from (
SELECT (ST_Dump(dtriag)).geom
FROM (
	SELECT  ST_DelaunayTriangles(ST_Union(buffer_1mile), 1) As  dtriag 
     from navigoviz.shoreline s where s.id in ('523' , '235')
) as foo ) as k, navigoviz.shoreline s
where not st_intersects(k.geom, s.polygone3857)



   

-- toutes les lignes point à point
create table segments as 
(select row_number() OVER() as segid, p1.ogc_fid as id1, p1.uhgs_id as uhgs_id1, p1.toponyme_standard_fr as topo1, 
	ST_MakeLine(p1.port_on_line_1mile, p2.port_on_line_1mile),
	p2.ogc_fid as id2, p2.uhgs_id as uhgs_id2, p2.toponyme_standard_fr as topo2
from ports.port_points p1, ports.port_points p2 )
-- non : y'a trop de segments -- 1452025
select count(*) from (select row_number() OVER() as segid, p1.ogc_fid as id1, p1.uhgs_id as uhgs_id1, p1.toponyme_standard_fr as topo1, 
	ST_MakeLine(p1.port_on_line_1mile, p2.port_on_line_1mile),
	p2.ogc_fid as id2, p2.uhgs_id as uhgs_id2, p2.toponyme_standard_fr as topo2
from ports.port_points p1, ports.port_points p2 ) as f


-- via Delaunay


select  ST_DelaunayTriangles( ST_Union(p.port_on_line_1mile))
     As  dtriag from ports.port_points p 

-- 2115 en poly, 3179 en lignes ( 0.001)
select count(*) from (
	SELECT (ST_Dump(geom)).geom
	FROM (
		select (ST_DelaunayTriangles( ST_collect(p.port_on_line_1mile), 0.001, 1)) as geom
     	from ports.port_points p  ) as foo
) as k
  
select row_number() OVER() as segid, geom from (
	SELECT (ST_Dump(geom)).geom
	FROM (
		select (ST_DelaunayTriangles( ST_collect(p.port_on_line_1mile), 0.001, 1)) as geom
     	from ports.port_points p  ) as foo
) as k

create table segments as (
select row_number() OVER() as segid, geom as line3857 from (
	SELECT (ST_Dump(geom)).geom
	FROM (
		select (ST_DelaunayTriangles( ST_collect(p.port_on_line_1mile), 0.001, 1)) as geom
     	from ports.port_points p  ) as foo
) as k
) 


pg_dump --verbose --host=127.0.0.1 --port=5432 --username=postgres --format=p --encoding=UTF-8  --no-privileges --no-owner -n "navigoviz" portic_v7 > /home/plumegeo/navigo/portic_v7_shorelines.sql

pg_dump --verbose --host=127.0.0.1 --port=5432 --username=postgres --format=p --encoding=UTF-8  --no-privileges --no-owner -n "ports" -t "ports.port_points" portic_v7 > /home/plumegeo/navigo/ports_points_encours_21fev2022.sql
pg_dump --verbose --host=127.0.0.1 --port=5432 --username=postgres --format=p --encoding=UTF-8  --no-privileges --no-owner -n "ports" -t "ports.segments" portic_v7 > /home/plumegeo/navigo/segments_encours_21fev2022.sql

-- supprimer ceux qui passent dans ploygone3857 (dans les terres)
	-- 1. calculer leur intersection avec line_20_mile
	-- 2. retriangulier avec ces points
	-- 3. supprimer les segments qui intersectent 

-- sur ma machine
select st_length (st_intersection(s.polygone3857, seg.line3857)) / st_length(seg.line3857) *100 as prop_terre
from navigoviz.shoreline s, ports.segments seg
where st_intersects(s.polygone3857, seg.line3857) is true
order by prop_terre desc
-- SQL Error [XX000]: ERREUR: lwgeom_intersection_prec: GEOS Error: TopologyException: Input geom 0 is invalid: Self-intersection at -7763428.1965160612 5430130.5657902854


select st_intersection(s.line_20_mile, seg.line3857)
from navigoviz.shoreline_valid_20mile  s, ports.segments seg
where st_intersects(s.line_20_mile, seg.line3857) is true
-- 3.82 s

select count(*)
from navigoviz.shoreline_valid_20mile  s, ports.segments seg
where st_intersects(s.line_20_mile, seg.line3857) is true
-- 248085 

-- ajouter ces points à ceux requis pour la retriangulation, quand ils ne sont pas dans la terre
select count(*) from
(select st_intersection(s.line_20_mile, seg.line3857) as point
from navigoviz.shoreline_valid_20mile  s, ports.segments seg
where st_intersects(s.line_20_mile, seg.line3857) is true) as k,
navigoviz.shoreline s
where not st_contains(s.polygone3857, k.point)

----------------------------------------
-- calculer les points des ports proches de la ligne des 20miles

update sea20mile set valid_poly_20_mile = st_setsrid(valid_poly_20_mile, 3857)
-- Query returned successfully: 447 rows affected, 25.7 secs execution time.
update sea20mile set line_20_mile = st_setsrid(line_20_mile, 3857)

alter table ports.port_points add column port_on_line_20mile geometry;
alter table ports.port_points add column id_valid_20mile int;

-- identifiant du polygone d'appartenance du port
update ports.port_points p set id_valid_20mile = s.ogc_fid
from navigoviz.shoreline_valid_20mile  s
where st_contains(s.wkb_geometry,p.point3857 )
-- 1 min 11

-- Point sur la shoreline des 20 miles
update ports.port_points set port_on_line_20mile = ST_ClosestPoint(line_20_mile, port_on_shoreline) 
from navigoviz.shoreline_valid_20mile  s
where port_on_line_20mile is  null and s.ogc_fid = id_valid_20mile 


SELECT ST_Line_Interpolate_Point(ST_Line_Locate_Point(ST_OffsetCurve(roadcenter_geom, pointoffset), point_geometry ) 
from xx where yyy

SELECT GeometryType(s.line_1mile), s.line_1mile::geometry(LineString, 3857) ,GeometryType(p.port_on_line_1mile)
--select ST_Line_Locate_Point(st_makeline(s.line_1mile), p.port_on_line_1mile  ) 
from ports.port_points p, navigoviz.shoreline s 
where p.uhgs_id = 'A0148900' and p.id_shoreline = s.id 

select ogc_fid, id, ST_NumGeometries (line_1mile) from navigoviz.shoreline s  where GeometryType(s.line_1mile) ilike 'MultiLineString'
select ogc_fid, id, ST_NumGeometries (line_20_mile) from navigoviz.shoreline_valid_20mile   s  where GeometryType(s.line_20_mile) ilike 'MultiLineString'

select distinct id , ogc_fid from navigoviz.shoreline
order by id

 
-- GeometryType(s.line_1mile), s.line_1mile::geometry(LineString, 3857) ,GeometryType(p.port_on_line_1mile)
select ST_Line_Locate_Point(s.shoreline_line::geometry(LineString, 3857), p.port_on_shoreline::Point  ) 
from ports.port_points p, navigoviz.shoreline s 
where p.uhgs_id = 'A0148900' and s.shoreline_line is not null and p.id_shoreline = s.id  

alter table shoreline add COLUMN shoreline_line  geometry(LineString, 3857) ;
update shoreline set shoreline_line = shoreline::geometry(LineString, 3857)
/*
create table shoreline_line as 
(select * from shoreline where shoreline is not null)
-- 1659

alter table shoreline drop column shoreline_line

set search_path=public, navigoviz, ports

select ST_LineLocatePoint(s.shoreline_line, p.port_on_shoreline  ) 
from ports.port_points p, navigoviz.shoreline_line s 
where p.uhgs_id = 'A0148900'  and p.id_shoreline = s.id  

SELECT ST_AsEWKT(ST_LineInterpolatePoint(the_line, 0.20))
	FROM (SELECT ST_GeomFromEWKT('LINESTRING(25 50, 100 125, 150 190)') as the_line) As foo;

select distinct GeometryType(s.shoreline_line) from navigoviz.shoreline_line s 
-- LINESTRING
alter table shoreline_line alter  COLUMN shoreline_line type Linestring ;

drop table shoreline_line
*/

select ST_LineLocatePoint(s.shoreline, p.port_on_shoreline  ) 
from ports.port_points p, navigoviz.shoreline s 
where p.uhgs_id = 'A0148900'  and p.id_shoreline = s.id 
-- 0.46142756791463385

select ST_LineLocatePoint(s.shoreline, p.port_on_shoreline  ) 
from ports.port_points p, navigoviz.shoreline s 
where p.uhgs_id = 'A0195511'  and p.id_shoreline = s.id 
-- 0.46109265745074013

-- ST_OffsetCurve() : ligne parallele à la cote qui relie les deux ports (sauf au départ et à l'arrivée)
select ST_OffsetCurve(ST_LineSubstring(shoreline, ST_LineLocatePoint(s.shoreline, p1.port_on_shoreline) , ST_LineLocatePoint(s.shoreline, p2.port_on_shoreline)), 2000)
from ports.port_points p1, navigoviz.shoreline s , ports.port_points p2
where p1.uhgs_id = 'A0195511'  and p1.id_shoreline = s.id and p2.uhgs_id = 'A0148900'  and p2.id_shoreline = s.id 

-- afficher dans QGIS
select row_number() over () as id, ST_OffsetCurve(ST_LineSubstring(shoreline, ST_LineLocatePoint(s.shoreline, p1.port_on_shoreline) , ST_LineLocatePoint(s.shoreline, p2.port_on_shoreline)), 2000)
from ports.port_points p1, navigoviz.shoreline s , ports.port_points p2
where p1.uhgs_id = 'A0195511'  and p1.id_shoreline = s.id and p2.uhgs_id = 'A0148900'  and p2.id_shoreline = s.id 

-- crée le point intermédiaire
select row_number() over () as id, ST_LineInterpolatePoint(ST_OffsetCurve(ST_LineSubstring(shoreline, ST_LineLocatePoint(s.shoreline, p1.port_on_shoreline) , ST_LineLocatePoint(s.shoreline, p2.port_on_shoreline)), 5000), 0.3)
from ports.port_points p1, navigoviz.shoreline s , ports.port_points p2
where p1.uhgs_id = 'A0195511'  and p1.id_shoreline = s.id and p2.uhgs_id = 'A0148900'  and p2.id_shoreline = s.id 

-- retracer la ligne droite depuis le point intermédiaire à l'arrivée
-- prednre la première intersection 
-- si intersection avec la cote, prendre 
SELECT ST_LineInterpolatePoint(ST_LineLocatePoint(ST_OffsetCurve(roadcenter_geom, pointoffset), point_geometry ) 
from xx where yyy


------------------
create or replace function points_on_path(p1 text, p2 text, m_offset double precision) 
returns geometry as
$BODY$
/*
comment
https://trac.osgeo.org/postgis/wiki/UsersWikiExamplesInterpolateWithOffset
*/

declare m_seg geometry;
declare p1 geometry;
declare p2 geometry;
declare finished boolean;
declare f1 double precision;
declare f2 double precision;
BEGIN

-- m_seg :=  st_linesubstring(m_geom,0, m_percent);
-- select point3857 from ports.port_points where p2.uhgs_id = 'A0195511' into p2;
f1 :=  ST_LineLocatePoint(s.shoreline, p1.port_on_shoreline) 
from ports.port_points p1, navigoviz.shoreline s 
where p1.uhgs_id = 'A0148900'  and p1.id_shoreline = s.id;

f2 :=  ST_LineLocatePoint(s.shoreline, p1.port_on_shoreline) 
from ports.port_points p1, navigoviz.shoreline s 
where p1.uhgs_id = 'A0195511'  and p1.id_shoreline = s.id;

/*
m_seg := ST_OffsetCurve(ST_LineSubstring(shoreline, case when f1<f2 then f1 else f2 end, case when f2<f1 then f2 else f1 end), m_offset)
from ports.port_points p1, navigoviz.shoreline s , ports.port_points p2
where p1.uhgs_id = 'A0148900'  and p1.id_shoreline = s.id and p2.uhgs_id = 'A0195511'  and p2.id_shoreline = s.id ;
*/


m_seg := ST_OffsetCurve(ST_LineSubstring(shoreline, ST_LineLocatePoint(s.shoreline, p1.port_on_shoreline) , ST_LineLocatePoint(s.shoreline, p2.port_on_shoreline)), m_offset)
from ports.port_points p1, navigoviz.shoreline s , ports.port_points p2
where p1.uhgs_id = 'A0195511'  and p1.id_shoreline = s.id and p2.uhgs_id = 'A0148900'  and p2.id_shoreline = s.id ;

select ST_LineInterpolatePoint(m_seg, 0.75) into p1;
/* Grab the 2 points of the matching line segment */
-- p1:=st_pointn(m_seg,st_npoints(m_seg)-1);

select st_intersects(st_makeline(p1, p2),coastline)
from navigoviz.shoreline s , ports.port_points p2
where   p2.uhgs_id = 'A0195511'  and p2.id_shoreline = s.id into finished ;

raise notice '% Dois je continuer % ? ', NOW(), finished;

return p1;


END;

$BODY$
language plpgsql;

select row_number() over () as id, (points_on_path('A0148900', 'A0195511', 5000, 0.75))[0]
-- https://postgis.net/docs/ST_LineToCurve.html
-- (-289742.2054512607,6002679.545039418) si 0.75
-- (-260113.23324453886,5977004.637371258) si 0.25

-- cas direct : le segment peut se tracer de la ligne des 1 miles à l'autre directement
select row_number() over () as id, (ports.points_on_path('A0123033', 'A0124715', 5000, 10000))
-- return null 

select row_number() over () as id, (points_on_path('A0148900', 'A0195511', 5000, 10))[0]
select row_number() over () as id, (points_on_path('A0148900', 'A0195511', 5000, 10))

select row_number() over () as id, geom
from (select unnest(points_on_path('A0148900', 'A0195511', 5000, 10)) as geom) as k

select row_number() over () as id, geom
from (select unnest(points_on_path('A0148900', 'A0195511', 5000, 7000)) as geom) as k

set search

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- CAS TESTS
------------------------------------------------------------------------------
------------------------------------------------------------------------------

---------------------------------------------
-- cas direct
---------------------------------------------
create schema paths;

select row_number() over () as id, (ports.points_on_path('A0123033', 'A0124715', 5000, 10000))

select s.id , p1.id_shoreline, p2.id_shoreline
	from ports.port_points p1, ports.port_points p2, navigoviz.shoreline s 
	where   p1.uhgs_id = 'A0123033'  and p2.uhgs_id = 'A0124715' and shoreline is not null and st_intersects(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline)

select row_number() over () as id, geom
from (select unnest(ports.points_on_path('A0123033', 'A0124715', 5000, 10000)) as geom) as k

---------------------------------------------
-- cas premier normal : même shoreline
---------------------------------------------
select s.id , p1.id_shoreline, p2.id_shoreline
	from ports.port_points p1, ports.port_points p2, navigoviz.shoreline s 
	where   p1.uhgs_id = 'A0195511'  and p2.uhgs_id = 'A0148900' and shoreline is not null and st_intersects(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline)
-- 0-E	0-E	0-E

-- dans un sens 
select row_number() over () as id, geom
from (select unnest(ports.points_on_path('A0148900', 'A0195511', 5000, 10000)) as geom) as k

-- dans le sens inverse
select row_number() over () as id, geom
from (select unnest(ports.points_on_path('A0195511', 'A0148900', 5000, 10000)) as geom) as k

drop table paths.STNazaire_Mesquer2;
create table paths.STNazaire_Mesquer2 as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A0195511', 'A0148900', 5000, 10000)) as geom) as k
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0148900', 'A0195511', 5000, 10000)) as geom) as k
)
)

-- voir la route
select idtrack as id, st_makeline(geom) as geom from
(select idtrack , geom from paths.STNazaire_Mesquer2  order by idtrack, id) as k
group by idtrack

-- voir les path points
select id , geom from paths.STNazaire_Mesquer2  where idtrack=2

------------------------------------------------
-- autre test avec point sur iles différentes (pas le meme shoreline)
-- cas particulier 1
------------------------------------------------

drop table paths.Quiberon_BelleIle;
create table paths.Quiberon_BelleIle as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A0197884', 'A0131657', 5000, 10000)) as geom) as k
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0131657', 'A0197884', 5000, 10000)) as geom) as k
)
)

select idtrack as id, st_makeline(geom) as geom from
(select idtrack , geom from paths.Quiberon_BelleIle  order by idtrack, id) as k
group by idtrack

select id , geom from paths.Quiberon_BelleIle  where idtrack=1 order by id


select row_number() over () as id, geom
from (select unnest(ports.points_on_path('A0197884', 'A0131657', 5000, 10000)) as geom) as k

select row_number() over () as id, geom
from (select unnest(ports.points_on_path('A0131657', 'A0197884', 5000, 10000)) as geom) as k


select s.id , p1.id_shoreline, p2.id_shoreline, st_closestpoint(p2.port_on_line_1mile,st_intersection(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline))
	from ports.port_points p1, ports.port_points p2, navigoviz.shoreline s 
	where   p1.uhgs_id = 'A0197884'  and p2.uhgs_id = 'A0131657' and shoreline is not null and st_intersects(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline)
--- 1668	0-E	1668 POINT (-347431.04415916285 6017719.81832922)

------------------------------------------------
-- cas particulier 2 : une ile entre les 2 ports
------------------------------------------------

drop table paths.BelleIle_Rhuys;
create table paths.BelleIle_Rhuys as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A0123033', 'A0131657', 5000, 10000)) as geom) as k
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0131657', 'A0123033', 5000, 10000)) as geom) as k
)
)

select row_number() over () as id, geom
from (select unnest(ports.points_on_path('A0123033', 'A0131657', 5000, 10000)) as geom) as k
-- 2 puis 14 points (ligne bizarre)
select ST_makeLine(ports.points_on_path('A0123033', 'A0131657', 5000, 10000)) 

select row_number() over () as id, geom
from (select unnest(ports.points_on_path('A0131657', 'A0123033', 5000, 10000)) as geom) as k
-- 1 point
select ST_LineToCurve(ST_makeLine(ports.points_on_path('A0131657', 'A0123033', 5000, 10000)) )

------------------------------------------------
-- tres grand trajet : st nazaire, Marseille
------------------------------------------------

drop table paths.marseille_st_nazaire2;
-- marseille_st_nazaire2 10 000 et 100 000 
create table paths.marseille_st_nazaire2 as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A0210797', 'A0195511', 10000, 50000)) as geom) as k
	-- ok
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0195511', 'A0210797', 10000, 50000)) as geom) as k
		-- pas ok
)
)

select idtrack as id, st_makeline(geom) as geom from
(select idtrack , geom from paths.marseille_st_nazaire2  order by idtrack, id) as k
group by idtrack

truncate table paths.temp_points; 
--  20000, 50000
create table paths.marseille_st_nazaire2 as (
select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0210797', 'A0195511', 20000, 50000)) as geom) as k
	)

create table paths.marseille_st_nazaire2 as (
select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0195511', 'A0210797', 20000, 50000)) as geom) as k
	)
	
select row_number() over () as id, geom
from (select unnest(ports.points_on_path('A0210797', 'A0195511', 10000, 100000)) as geom) as k
-- 38 points

select row_number() over () as id, geom
from (select unnest(ports.points_on_path('A0195511', 'A0210797', 10000, 100000)) as geom) as k
-- 45 points
-- 10 points et un null

------------------------------------------------
--  trajet : st nazaire, La Rochelle
------------------------------------------------
drop table paths.laRochelle_st_nazaire;
create table paths.laRochelle_st_nazaire as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A0198999', 'A0195511', 5000, 10000)) as geom) as k
	-- ok
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0195511', 'A0198999', 5000, 10000)) as geom) as k
		-- pas ok
)
);

truncate table navigoviz.temp_points; 


drop table navigoviz.laRochelle_st_nazaire2;
create table navigoviz.laRochelle_st_nazaire2 as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A0198999', 'A0195511', 5000, 10000)) as geom) as k
	-- ok
union (

	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0195511', 'A0198999', 5000, 10000)) as geom) as k
		--  ok mais difficile pour shore id = '2188'
)
);

create table navigoviz.laRochelle_st_nazaire2 as (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0198999', 'A0195511', 5000, 10000)) as geom) as k
	)
	
select row_number() over () as id, iter, previouspoint as geom, longueur_segment, sensDirect , newoffset, distance_ratio 
from navigoviz.temp_points 
where shoreid = '0-E' and iter = 0

select *
from navigoviz.temp_points 
where shoreid = '0-E' and iter = 0

select test[1], test[2] from 
(select (ports.order_intersection(previouspoint , st_makeline(previouspoint, pointp1prime) )) as test
from navigoviz.temp_points
where shoreid = '0-E' and iter = 0) as k
	
select idtrack, st_makeline(geom) from
(select idtrack, geom from navigoviz.laRochelle_st_nazaire where sens='direct' order by id) as k
group by idtrack

select idtrack as id, st_makeline(geom) as geom from
(select idtrack , geom from navigoviz.laRochelle_st_nazaire where sens='inverse' order by id) as k
group by idtrack


select idtrack, st_makeline(geom) from
(select idtrack, geom from navigoviz.marseille_st_nazaire where sens='direct' order by id) as k
group by idtrack

select idtrack, st_makeline(geom) from navigoviz.marseille_st_nazaire where sens='direct' group by idtrack order by id; --101 pts
select idtrack, st_makeline(geom) from navigoviz.marseille_st_nazaire where sens='inverse' group by idtrack order by id; --101 pts

select count(*) from navigoviz.marseille_st_nazaire where sens='inverse';--10 pts


select row_number() over () as id, geom
from (select unnest(ports.points_on_path('A0195511', 'A0210797', 10000, 50000)) as geom) as k
-- 101 et on est pas arrivés

create table navigoviz.temp_points (
	iter int, 
	previousPoint geometry, 
	pointp1 geometry, 
	pointp1prime geometry, 
	sensDirect boolean, 
	shoreid text, 
	longueur_segment double precision, 
	newoffset double precision, 
	distance_ratio double precision, 
	ratio double precision);

select row_number() over () as id, iter, pointp1 as geom, longueur_segment, sensDirect 
from navigoviz.temp_points where shoreid = '2188' and iter = 0;


select row_number() over () as id, iter, previousPoint as geom, longueur_segment, newoffset, sensDirect 
from navigoviz.temp_points where shoreid = '2188' and iter = 0;

select row_number() over () as id, iter, pointp1prime as geom, longueur_segment, sensDirect 
from navigoviz.temp_points where shoreid = '2188' ;



------------------------------------------------
--  grand trajet : st nazaire, Pontevedra
------------------------------------------------

drop table paths.pontevedra_st_nazaire;
create table paths.pontevedra_st_nazaire as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A0103067', 'A0195511', 20000, 50000)) as geom) as k
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0195511', 'A0103067', 20000, 50000)) as geom) as k
)
)


-- pontevedra_st_nazaire3 : 10000 offset / 10000 km 

select  s.id
from navigoviz.shoreline s 
where st_contains(s.polygone3857, st_setsrid(st_makepoint(-1025982.7401599308, 5350414.238468682), 3857))

select  s.id
from navigoviz.shoreline s 
where st_contains(s.polygone3857, st_setsrid(st_makepoint(-971826.25451444,5210716.404569135), 3857))



select idtrack, st_makeline(geom) from
(select idtrack, id, geom from navigoviz.pontevedra_st_nazaire3 where sens='direct' order by id) as k
group by idtrack

select idtrack, st_makeline(geom) from
(select idtrack, id, geom from navigoviz.pontevedra_st_nazaire where sens='inverse' order by id) as k
group by idtrack

------------------------------------------------
--  SUPER grand trajet : Amsterdam / St nazaire
------------------------------------------------

drop table paths.amsterdam_st_nazaire; -- 20000 en offset bugait
create table paths.amsterdam_st_nazaire as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A0620777', 'A0195511', 20000, 50000)) as geom) as k
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0195511', 'A0620777', 20000, 50000)) as geom) as k
)
)
-- 1 min et ok avec offset de 10000 : ok aussi avec 20000

drop table paths.amsterdam_st_nazaire2;
create table paths.amsterdam_st_nazaire2 as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A0620777', 'A0195511', 5000, 10000)) as geom) as k
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0195511', 'A0620777', 5000, 10000)) as geom) as k
)
) -- ok

create table paths.amsterdam_st_nazaire3 as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A0620777', 'A0195511', 20000, 50000)) as geom) as k
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0195511', 'A0620777', 20000, 50000)) as geom) as k
)
) --2 min33
------------------------------------------------
--  SUPER grand trajet : Riga (pays balte - A1010107, Sulina (mer noire - A1220285)
------------------------------------------------

drop table paths.riga_sulina;
create table paths.riga_sulina as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A1010107', 'A1220285', 10000, 200000)) as geom) as k
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A1220285', 'A1010107', 10000, 200000)) as geom) as k
)
)
-- marche pas car riga (le point sur la ligne des 1 miles) est dans la terre 

drop table navigoviz.riga_rostov;
create table navigoviz.riga_rostov as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A1010107', 'A0671198', 10000, 50000)) as geom) as k
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0671198', 'A1010107', 10000, 50000)) as geom) as k
)
)

------------------------------------------
-- autre SUPER SUPER grand trajet : A1797542 (Kronstadt en Russie) - Sulina (mer noire - A1220285)
------------------------------------------
drop table paths.kronstadt_sulina;
create table paths.kronstadt_sulina as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A1797542', 'A1220285', 10000, 200000)) as geom) as k
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A1220285', 'A1797542', 10000, 200000)) as geom) as k
)
) -- NOK

-- A0828834 - AARHUS

drop table paths.kronstadt_aarhus;
create table paths.kronstadt_aarhus as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A1797542', 'A0828834', 10000, 100000)) as geom) as k
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0828834', 'A1797542', 10000, 100000)) as geom) as k
)
); -- NOK

--- A0924524 Helsinki
drop table paths.kronstadt_helsinki;

create table paths.kronstadt_helsinki as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A1797542', 'A0924524', 10000, 50000)) as geom) as k
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0924524', 'A1797542', 10000, 50000)) as geom) as k
)
); -- ok


-- A1029831 Kristianstad 

create table paths.kronstadt_kristianstad as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A1797542', 'A1029831', 10000, 50000)) as geom) as k
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A1029831', 'A1797542', 10000, 50000)) as geom) as k
)
); -- NOK

-- A1102661 Stockholm


create table paths.kronstadt_stockholm as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A1797542', 'A1102661', 10000, 50000)) as geom) as k
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A1102661', 'A1797542', 10000, 50000)) as geom) as k
)
);

------------------------------------------------
-- trajet très délicat avec pleins d'iles kristianstad  A1029831 --> stockholm A1102661
------------------------------------------------
drop table paths.kristianstad_stockholm;
create table paths.kristianstad_stockholm as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A1029831', 'A1102661', 10000, 50000)) as geom) as k
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A1102661', 'A1029831', 10000, 50000)) as geom) as k
)
);

select ST_LineSubstring(shoreline, 0.5645722215390745, 0.5646243681872446) from navigoviz.shoreline s where s.id = '0-E';
select g into m_seg from (
			select ST_NumGeometries(geom),  (st_dump(geom)).geom as g
			from (
			select ST_OffsetCurve(ST_LineSubstring(shoreline, 0.5645722215390745, 0.5646243681872446), 500) as geom
						from navigoviz.shoreline s 
						where s.id = '0-E'
			) as k
			) as q
		--where st_intersects(g, test[1]) and st_intersects(g, test[2]);
		order by  st_length(g) desc limit 1;

	select st_geometrytype(line_1mile) from navigoviz.shoreline where id = '0-E'
	-- ST_MultiLineString
	select st_geometrytype(shoreline) from navigoviz.shoreline where id = '0-E'
	-- ST_LineString
		select st_geometrytype(line_20_mile) from navigoviz.shoreline_valid_20mile  
	-- ST_MultiLineString
	select count(*), avg(st_area(polygone3857)) from navigoviz.shoreline where region is null -- 32724
	-- 2084756781.2550442
		select  (st_area(polygone3857)) from navigoviz.shoreline where id = '29987' -- 32724

------------------------------------------------
-- tres grand trajet : Honfleur, Marennes
------------------------------------------------
SELECT uhgs_id FROM ports.port_points WHERE toponyme = 'Honfleur'
drop table paths.marennes_honfleur;
-- marseille_st_nazaire2 10 000 et 100 000 
create table paths.marennes_honfleur as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A0136930', 'A0187836', 10000, 50000)) as geom) as k
	-- ok
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0187836', 'A0136930', 10000, 50000)) as geom) as k
		-- pas ok
)
)

select idtrack as id, st_makeline(geom) as geom from
(select idtrack , geom from paths.marennes_honfleur  order by idtrack, id) as k
group by idtrack

------------------------------------------------
--  trajet delicat: Honfleur, La Tremblade
------------------------------------------------
SELECT uhgs_id FROM ports.port_points WHERE toponyme = 'Honfleur';--A0187836
SELECT uhgs_id FROM ports.port_points WHERE toponyme = 'La Tremblade'; -- A0186515

drop table paths.LaTremblade_honfleur;
create table paths.LaTremblade_honfleur as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A0186515', 'A0187836', 10000, 50000)) as geom) as k
	-- ok
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0187836', 'A0186515', 10000, 50000)) as geom) as k
	-- ok
)
)
-- 2 min
select idtrack as id, st_makeline(geom) as geom from
(select idtrack , geom from paths.LaTremblade_honfleur  order by idtrack, id) as k
group by idtrack


---------------------------------------------------------------------------------------------------
-- une petite merde au retour vers pontevedra
st_makepoint(-971826.25451444, 5210716.404569135) 
st_makepoint(-971826.25451444, 5210716.404569135)
st_makepoint(-968246.3938887308, 5223787.405682824)

select ports.order_intersection(st_setsrid(st_makepoint(-971826.25451444, 5210716.404569135), 3857), 
	st_setsrid(st_makeline(st_makepoint(-971826.25451444, 5210716.404569135), st_makepoint(-968246.3938887308, 5223787.405682824)), 3857))
	
select  st_intersection(linethatcut, shoreline) as intersectionPoints
			from navigoviz.shoreline s 
			where  shoreline is not null and st_intersects(st_buffer(linethatcut, 10), shoreline

select ST_LineToCurve()

-- Point pres de pontevedra  dans la terre
select 1 as id, st_makepoint(-972181.1174617943, 5216620.711860664)

select 2 as id, st_project(st_setsrid(st_makepoint(-972181.1174617943, 5216620.711860664), 3857), 
st_length(st_shortestline(st_setsrid(st_makepoint(-972181.1174617943, 5216620.711860664), 3857), shoreline)),
st_azimuth(st_setsrid(st_makepoint(-972181.1174617943, 5216620.711860664), 3857)::geometry, st_closestpoint(shoreline, st_setsrid(st_makepoint(-972181.1174617943, 5216620.711860664), 3857))))
from navigoviz.shoreline;

select st_length(st_shortestline(st_setsrid(st_makepoint(-972181.1174617943, 5216620.711860664), 3857), shoreline)) as d, st_closestpoint(shoreline, st_setsrid(st_makepoint(-972181.1174617943, 5216620.711860664), 3857))
from navigoviz.shoreline order by d asc limit 1
-- 298.6830105715183
-- POINT (-972448.5527726162 5216753.715226523)


select st_transform(ST_GeomFromText(ST_AsEWKT (k.x)),3857) from
(
select 2 as id, st_setsrid(st_project(st_transform(st_setsrid(st_makepoint(-972181.1174617943, 5216620.711860664), 3857), 4326)::geography, 
300*2,
5.1738975976700505), 4326) as x
) as k

select st_azimuth(st_setsrid(st_makepoint(-972181.1174617943, 5216620.711860664), 3857), st_setsrid(st_makepoint (-972448.5527726162, 5216753.715226523), 3857))


select ST_LineFromMultiPoint(st_union(geom))
from (select row_number() over () as id, geom from unnest(ports.points_on_path('A0195511', 'A0210797', 10000, 100000)) ) as k
order by id

select ST_makeLine(ports.points_on_path('A0195511', 'A0210797', 10000, 100000))
from (select row_number() over () as id, geom from unnest(ports.points_on_path('A0195511', 'A0210797', 10000, 100000)) ) as k
order by id

/*
 *  Test p1 0101000020110F0000A0A3E74AE8E42141306E484F00775441 p2 0101000020110F000094DEDE98FC590DC1F131B1E80AD65641
Identifiant du shoreline à contourner 0-E
Longueur du segment en offset 5 813 787.476707185
-- line_interpolate_point: 1st arg isn't a line
  Où : instruction SQL « select ST_LineInterpolatePoint(m_seg, ratio) »
 */

select s.id from navigoviz.shoreline s  
where s.shoreline is not null 
and st_intersects(s.shoreline, st_buffer(st_setsrid(st_makeline(ST_Point(10088.075783164126, 4873665.893616792) , ST_Point(-139794.66758539784, 5535378.317537028)), 3857), 10));

select s.id from navigoviz.shoreline s  
where s.shoreline is not null and st_intersects(s.shoreline,st_buffer(st_setsrid(ST_Point(10088.075783164126, 4873665.893616792), 3857), 10))

select ST_NumGeometries(geom),  st_length(st_geometryn(geom, 1)), st_geometryn(geom, 1)
from (
select ST_OffsetCurve(ST_LineSubstring(shoreline,ST_LineLocatePoint(s.shoreline, p2.port_on_shoreline),  ST_LineLocatePoint(s.shoreline, p1.port_on_shoreline) ), 5000) as geom
from ports.port_points p1, navigoviz.shoreline s , ports.port_points p2
where p1.uhgs_id = 'A0195511'  and p1.id_shoreline = s.id and p2.uhgs_id = 'A0210797'  and p2.id_shoreline = s.id 
) as k
-- multilinestring
-- ok d'après la doc https://postgis.net/docs/ST_OffsetCurve.html. Il retourne linestring ou multilinestring

select st_length(g) as longueur, g from (
select ST_NumGeometries(geom),  (st_dump(geom)).geom as g
from (
select ST_OffsetCurve(ST_LineSubstring(shoreline,ST_LineLocatePoint(s.shoreline, p2.port_on_shoreline),  ST_LineLocatePoint(s.shoreline, p1.port_on_shoreline) ), 5000) as geom
from ports.port_points p1, navigoviz.shoreline s , ports.port_points p2
where p1.uhgs_id = 'A0195511'  and p1.id_shoreline = s.id and p2.uhgs_id = 'A0210797'  and p2.id_shoreline = s.id 
) as k
) as q
order by longueur desc limit 1

select ST_LineSubstring(shoreline,ST_LineLocatePoint(s.shoreline, p2.port_on_shoreline),  ST_LineLocatePoint(s.shoreline, p1.port_on_shoreline) )
from ports.port_points p1, navigoviz.shoreline s , ports.port_points p2
where p1.uhgs_id = 'A0195511'  and p1.id_shoreline = s.id and p2.uhgs_id = 'A0210797'  and p2.id_shoreline = s.id ;
-- linestring


/*
 * -- intersection de la shoreline de p2

select s.id , p1.id_shoreline, p2.id_shoreline, st_geometryn(st_intersection(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline), 0)
	from ports.port_points p1, ports.port_points p2, navigoviz.shoreline s 
	where   p1.uhgs_id = 'A0197884'  and p2.uhgs_id = 'A0131657' and shoreline is not null and st_intersects(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline)
-- MULTIPOINT ((-352914.703791917 5991715.895864345), (-351107.94504628703 6000283.680424398))
st_pointn(m_seg,st_npoints(m_seg)-1)	

select point3857 as depart, (st_dump(intersectionpoints)).geom, st_distance(point3857, (st_dump(intersectionpoints)).geom) as d
from 
(select s.id , p1.point3857, p1.id_shoreline, p2.id_shoreline, st_intersection(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline) as intersectionpoints
	from ports.port_points p1, ports.port_points p2, navigoviz.shoreline s 
	where   p1.uhgs_id = 'A0197884'  and p2.uhgs_id = 'A0131657' and shoreline is not null and st_intersects(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline)
) as k
order by d asc limit 2;


select point3857 as depart, (st_dump(intersectionpoints)).geom, st_distance(point3857, (st_dump(intersectionpoints)).geom) as d
from 
(select s.id , p1.point3857, p1.id_shoreline, p2.id_shoreline, st_intersection(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline) as intersectionpoints
	from ports.port_points p1, ports.port_points p2, navigoviz.shoreline s 
	where   p1.uhgs_id = 'A0123033'  and p2.uhgs_id = 'A0131657' and shoreline is not null and st_intersects(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline)
) as k
order by d asc limit 2;


select array_agg(geom) from (
select  (st_dump(intersectionpoints)).geom
from 
(select s.id , p1.point3857, p1.id_shoreline, p2.id_shoreline, st_intersection(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline) as intersectionpoints
	from ports.port_points p1, ports.port_points p2, navigoviz.shoreline s 
	where   p1.uhgs_id = 'A0123033'  and p2.uhgs_id = 'A0131657' and shoreline is not null and st_intersects(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline)
) as k
order by st_distance(point3857, (st_dump(intersectionpoints)).geom) asc limit 2
) as q

-- cas de plusieurs intersections
select row_number() over() as id, test[0]::geometry as firstp, test[1] as secondp, st_makeline(test[0], test[1] ) as premier_segment
from 
(
select ports.order_intersection(p1.point3857, st_intersection(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline)) as test
from ports.port_points p1 , ports.port_points p2, navigoviz.shoreline s 
where   p1.uhgs_id = 'A0123033' and p2.uhgs_id = 'A0131657' and shoreline is not null and st_intersects(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline)
) as k

1	POINT (-345789.5331974394 5994971.013075842)	POINT (-351257.74984824716 5991157.183400798)	LINESTRING (-345789.5331974394 5994971.013075842, -351257.74984824716 5991157.183400798)
2	POINT (-328486.0921129474 6007039.367587483)	POINT (-330766.4865405139 6005448.897331083)	LINESTRING (-328486.0921129474 6007039.367587483, -330766.4865405139 6005448.897331083)

select row_number() over() as id, test[1]::geometry as firstp, test[2] as secondp, st_makeline(test[1], test[2] ) as premier_segment
from 
(
select ports.order_intersection(p1.point3857, st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile)) as test
from ports.port_points p1 , ports.port_points p2
where   p1.uhgs_id = 'A0123033' and p2.uhgs_id = 'A0131657'
)as k
-- test version 2 ok



	
		select   (st_dump(intersectionpoints)).geom
		from 
		(select s.id , p1.point3857, p1.id_shoreline, p2.id_shoreline, st_intersection(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline) as intersectionpoints
			from ports.port_points p1, ports.port_points p2, navigoviz.shoreline s 
			where   p1.uhgs_id = 'A0123033'  and p2.uhgs_id = 'A0131657' and shoreline is not null and st_intersects(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline)
		) as k
		order by st_distance(point3857, (st_dump(intersectionpoints)).geom) asc limit 2
	
	
	
-- cas de une seule intersection
select row_number() over() as id, test[1] as firstp, test[2] as secondp
from 
(
select ports.order_intersection(p1.point3857, st_intersection(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline)) as test
from ports.port_points p1 , ports.port_points p2, navigoviz.shoreline s 
where   p1.uhgs_id = 'A0197884' and p2.uhgs_id = 'A0131657' and shoreline is not null and st_intersects(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline)
) as k
-- POINT (-351107.94504628703 6000283.680424398)	POINT (-352914.703791917 5991715.895864345)

select row_number() over() as id, test[1] as firstp, test[2] as secondp
from 
(
select ports.order_intersection(p1.point3857, st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile)) as test
from ports.port_points p1 , ports.port_points p2
where   p1.uhgs_id = 'A0197884' and p2.uhgs_id = 'A0131657' 
) as k --test version 2 ok

-- cas direct
select ports.order_intersection(p1.point3857, st_intersection(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline)) as test
	from ports.port_points p1, ports.port_points p2, navigoviz.shoreline s 
	where   p1.uhgs_id = 'A0123033'  and p2.uhgs_id = 'A0124715' and shoreline is not null and st_intersects(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline)

select test[1] as firstp, test[2] as secondp
from 
(
select ports.order_intersection(p1.point3857, st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile)) as test
from ports.port_points p1 , ports.port_points p2
where   p1.uhgs_id = 'A0123033' and p2.uhgs_id = 'A0124715' 
) as k --test version 2 ok


-- test is null
*/	
	


----------------------------------------------------------------


select st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile)
-- , not st_intersects(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile),shoreline)
	from ports.port_points p1, navigoviz.shoreline s , ports.port_points p2
	where   p1.uhgs_id = 'A0123033'  and p1.id_shoreline = s.id and p2.uhgs_id = 'A0131657'  and p2.id_shoreline = s.id  ;
select port_on_line_1mile from ports.port_points where uhgs_id = 'A0123033'
select port_on_line_1mile from ports.port_points where uhgs_id = 'A0131657'

select st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile)
	from ports.port_points p1, ports.port_points p2
	where   p1.uhgs_id = 'A0123033'  and p2.uhgs_id = 'A0131657' ;
select s.id , p1.id_shoreline, p2.id_shoreline
	from ports.port_points p1, ports.port_points p2, navigoviz.shoreline s 
	where   p1.uhgs_id = 'A0123033'  and p2.uhgs_id = 'A0131657' and shoreline is not null and st_intersects(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline)

	st_intersects(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline) is not null  ;

select count(line_1mile ) from navigoviz.shoreline s where line_1mile is not null
select count(shoreline ) from navigoviz.shoreline s where shoreline is not null

-- https://postgis.net/docs/ST_LineToCurve.html
-- (-289742.2054512607,6002679.545039418) si 0.75
-- (-260113.23324453886,5977004.637371258) si 0.25
select row_number() over () as id, (points_on_path('A0123033', 'A0124715', 5000, 0.75))[0]


select ST_OffsetCurve(ST_LineSubstring(shoreline, ST_LineLocatePoint(s.shoreline, p1.port_on_shoreline) , ST_LineLocatePoint(s.shoreline, p2.port_on_shoreline)), 5000)
from ports.port_points p1, navigoviz.shoreline s , ports.port_points p2
where p1.uhgs_id = 'A0148900'  and p1.id_shoreline = s.id and p2.uhgs_id = 'A0195511'  and p2.id_shoreline = s.id ;


select st_intersects(st_makeline(p1.port_on_line_1mile , p2.port_on_line_1mile),shoreline)
	from ports.port_points p1, navigoviz.shoreline s , ports.port_points p2
	where   p1.uhgs_id = 'A0123033'  and p1.id_shoreline = s.id and p2.uhgs_id = 'A0124715'  and p2.id_shoreline = s.id  ;

--------------------------------------------------------------------------------------------------------------------------------
-- reprise le 13 mars :cartographie d'un exemple
-- ship 0002537N 
--------------------------------------------------------------------------------------------------------------------------------

select * from public.pointcall2 order by pointcall_rankfull;
drop table public.pointcall2;
create table public.pointcall2 as (
	select t.ship_id, t.ship_name, t.pointcall_rankfull,t.pointcall_uhgs_id,t.pointcall,t.pointcall_function,coalesce(v.net_route_marker, t.net_route_marker) as net_route_marker,
	t.pointcall_out_date,t.pointcall_in_date,t.pointcall_action,t.date_fixed,
	t.has_input_done,t.pointcall_admiralty,t.pointcall_province,t.navigo_status,t.date_precise,t.source_suite,t.tonnage,t.list_sup_uhgs_id,
	t.record_id,t.source_doc_id,t.pkid,t.pointcall_rank_dedieu,v.certitude,v.certitude_reason,v.verif
    from public.test2 t left join public.verifs2 v on v.pkid = t.pkid
    order by ship_id, pointcall_rankfull 
)-- 29 pour le ship 0002537N

alter table public.pointcall2 add column pointcall_uncertainity int;

-- couleur verte
update public.pointcall2 set pointcall_uncertainity = 0 where certitude='Observé';
update public.pointcall2 set pointcall_uncertainity = -1 where certitude='Confirmé';
-- couleur grise
update public.pointcall2 set pointcall_uncertainity = -4 where certitude is NULL;
update public.pointcall2 set pointcall_uncertainity = -2 where certitude='Déclaré';
-- couleur orange
update public.pointcall2 set pointcall_uncertainity = -3 where verif like '%#net_route_marker%'; -- debat
-- couleur rouge
update public.pointcall2 set pointcall_uncertainity = -5 where certitude='Infirmé';


SELECT * FROM public.pointcall2
SELECT net_route_marker, certitude , verif FROM public.pointcall2 WHERE verif like '%#net_route_marker%';


drop table public.travels2
create table public.travels2 as 
 (
            select  depart.id, depart.ship_id, depart.ship_name, 
            depart.pointcall as departure, arrivee.pointcall as destination, 
            depart.pointcall_uhgs_id as depart_pointcall_uhgs_id, arrivee.pointcall_uhgs_id as arrivee_pointcall_uhgs_id,
            depart.date_fixed as outdate_fixed, arrivee.date_fixed as indate_fixed, arrivee.date_fixed- depart.date_fixed as duration, 
            depart.pointcall_out_date as departure_out_date, arrivee.pointcall_in_date as destination_in_date,
            depart.net_route_marker as depart_net_route_marker , arrivee.net_route_marker as arrivee_net_route_marker,
            depart.pointcall_action as departure_action, 
            depart.pointcall_uncertainity as departure_uncertainity, 
            arrivee.pointcall_action as destination_action, 
            arrivee.pointcall_uncertainity as destination_uncertainity,
            
            depart.navigo_status as departure_navstatus, depart.pointcall_function as departure_function, 
            arrivee.navigo_status  as destination_navstatus, arrivee.pointcall_function as destination_function, 
            
            arrivee.ship_name as arrivee_shipname, 
            --depart.captain_name as depart_captainname, arrivee.captain_name as arrivee_captainname, 
            --depart.captain_id as depart_captainid, arrivee.captain_id as arrivee_captainid,
            --depart.flag as depart_flag, arrivee.flag as arrivee_flag, 
            --depart.ship_flag_id as depart_flagid, arrivee.ship_flag_id as arrivee_flagid, 
            --depart.homeport as depart_homeport, arrivee.homeport as arrivee_homeport, 
            --depart.homeport_uhgs_id as depart_homeportid, arrivee.homeport_uhgs_id as arrivee_homeportid,
            arrivee.pointcall_out_date as arrivee_outdate,
            
            depart.source_suite as source_depart, depart.source_doc_id as doc_depart, 
            arrivee.source_suite as source_destination, arrivee.source_doc_id as doc_destination,
            depart.pkid as departure_pkid, arrivee.pkid as destination_pkid 
            from 
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite, d.source_doc_id, d.pkid, d.pointcall_out_date, d.pointcall_in_date, navigo_status, pointcall_function,
            pointcall_uhgs_id
            -- captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id
            from public.pointcall2 d
            where   net_route_marker = 'A' or pointcall_uncertainity = -3
            order by d.ship_id, d.pointcall_rankfull) as depart, 
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite , d.source_doc_id, d.pkid, d.pointcall_in_date, navigo_status, pointcall_function, 
            pointcall_out_date, pointcall_uhgs_id
            -- captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id, 
            from public.pointcall2 d
            where   net_route_marker = 'A' or pointcall_uncertainity = -3
            order by d.ship_id, d.pointcall_rankfull) as arrivee 
            
            where depart.ship_id = arrivee.ship_id and depart.id + 1  = arrivee.id 
            --or (depart.id + 2  = arrivee.id and depart.net_route_marker  = 'A' and arrivee.net_route_marker  = 'A')
            order by depart.ship_id, depart.id 
    ) 

-- select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
--	from (select unnest(ports.points_on_path('A0620777', 'A0195511', 20000, 50000)) as geom) as k

select * from public.travels2 order by id
    
alter table public.travels2 add column pointpath geometry[] ;
update public.travels2 set pointpath = ports.points_on_path(depart_pointcall_uhgs_id , arrivee_pointcall_uhgs_id, 5000, 10000)
-- 12 min


select id, departure, destination, duration, 
departure_out_date, destination_in_date, departure_uncertainity, destination_uncertainity ,
st_makeline(pointpath) 
from public.travels2 
-- where sens='inverse' group by idtrack order by id; --101 pts

select idtrack, st_makeline(geom) from
(select idtrack, id, geom from navigoviz.pontevedra_st_nazaire where sens='inverse' order by id) as k
group by idtrack

alter table public.travels2 add column pointpath2 geometry[] ;
update public.travels2 set pointpath = ports.points_on_path(depart_pointcall_uhgs_id , arrivee_pointcall_uhgs_id, 20000, 50000)
where id = 6
-- select array_length(pointpath, 1) from public.travels2 where id = 6
-- A0136930	A0187836 -- 402 points simplifié à 20 pts avec 20000, 50000
update public.travels2 set pointpath = ports.points_on_path(depart_pointcall_uhgs_id , arrivee_pointcall_uhgs_id, 20000, 100000)
where id = 6

select * from ports.port_points where uhgs_id='A0136930'
select * from ports.port_points where uhgs_id='A0187836'


update public.travels2 set pointpath2 = ports.points_on_path(depart_pointcall_uhgs_id , arrivee_pointcall_uhgs_id, 20000, 50000)
where id = 17
-- 9min pour tout (sans simplifier)
-- 5 min avec simplification

alter table public.travels2 add column pointpath3 geometry[] ;
update public.travels2 set pointpath3 = ports.points_on_path(depart_pointcall_uhgs_id , arrivee_pointcall_uhgs_id, 10000, 10000);
-- 37 min


select id, departure, destination, duration, 
departure_out_date, destination_in_date, departure_uncertainity, destination_uncertainity ,
st_makeline(pointpath3) 
from public.travels2 

-- bugs sur 7, 18, 21 : Honfleur Marennes ou la Tremblade, ou  Marennes --> Dunkerque. 

alter table public.travels2 add column pointpath3 geometry[] ;
update public.travels2 set pointpath3 = ports.points_on_path(depart_pointcall_uhgs_id , arrivee_pointcall_uhgs_id, 10000, 50000) WHERE id = 21;

create table public.travels3 as 
 (
            select  depart.id, depart.ship_id, depart.ship_name, 
            depart.pointcall as departure, arrivee.pointcall as destination, 
            depart.pointcall_uhgs_id as depart_pointcall_uhgs_id, arrivee.pointcall_uhgs_id as arrivee_pointcall_uhgs_id,
            depart.date_fixed as outdate_fixed, arrivee.date_fixed as indate_fixed, arrivee.date_fixed- depart.date_fixed as duration, 
            depart.pointcall_out_date as departure_out_date, arrivee.pointcall_in_date as destination_in_date,
            depart.net_route_marker as depart_net_route_marker , arrivee.net_route_marker as arrivee_net_route_marker,
            depart.pointcall_action as departure_action, 
            depart.certitude as departure_certitude,
            depart.pointcall_uncertainity as departure_uncertainity, 
            arrivee.pointcall_action as destination_action, 
            arrivee.certitude as destination_certitude,
            arrivee.pointcall_uncertainity as destination_uncertainity,
            
            depart.navigo_status as departure_navstatus, depart.pointcall_function as departure_function, 
            arrivee.navigo_status  as destination_navstatus, arrivee.pointcall_function as destination_function, 
            
            arrivee.ship_name as arrivee_shipname, 
            --depart.captain_name as depart_captainname, arrivee.captain_name as arrivee_captainname, 
            --depart.captain_id as depart_captainid, arrivee.captain_id as arrivee_captainid,
            --depart.flag as depart_flag, arrivee.flag as arrivee_flag, 
            --depart.ship_flag_id as depart_flagid, arrivee.ship_flag_id as arrivee_flagid, 
            --depart.homeport as depart_homeport, arrivee.homeport as arrivee_homeport, 
            --depart.homeport_uhgs_id as depart_homeportid, arrivee.homeport_uhgs_id as arrivee_homeportid,
            arrivee.pointcall_out_date as arrivee_outdate,
            
            depart.source_suite as source_depart, depart.source_doc_id as doc_depart, 
            arrivee.source_suite as source_destination, arrivee.source_doc_id as doc_destination,
            depart.pkid as departure_pkid, arrivee.pkid as destination_pkid 
            from 
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite, d.source_doc_id, d.pkid, d.pointcall_out_date, d.pointcall_in_date, navigo_status, pointcall_function,
            pointcall_uhgs_id, certitude
            -- captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id
            from public.pointcall2 d
            --where   net_route_marker = 'A' or pointcall_uncertainity = -3
            order by d.ship_id, d.pointcall_rankfull) as depart, 
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite , d.source_doc_id, d.pkid, d.pointcall_in_date, navigo_status, pointcall_function, 
            pointcall_out_date, pointcall_uhgs_id, certitude
            -- captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id, 
            from public.pointcall2 d
            --where   net_route_marker = 'A' or pointcall_uncertainity = -3
            order by d.ship_id, d.pointcall_rankfull) as arrivee 
            
            where depart.ship_id = arrivee.ship_id and depart.id + 1  = arrivee.id and depart.pointcall_uhgs_id<>arrivee.pointcall_uhgs_id
            --or (depart.id + 2  = arrivee.id and depart.net_route_marker  = 'A' and arrivee.net_route_marker  = 'A')
            order by depart.ship_id, depart.id 
    ) 
    
-- 
select * from navigocheck.check_pointcall where data_block_local_id = '00149865'
-- normal, vu dans Filemaker : il a payé un droit de délestage en entrant à la Tremblade. 


alter table public.travels3 add column direct boolean default false;
update public.travels3 set direct = true where departure_uncertainity = 0;
-- mais ceux qui disent venir de Dunkerque pour aller à Bordeaux (Out de Dunkerque, In-Out La Rochelle, In Bordeaux) ?

alter table public.travels3 add column couleur text default 'gris';

-- 3
update public.travels3 set couleur = 'grey' ;
update public.travels3 set couleur = 'green' where destination_uncertainity in (0, -1) and departure_uncertainity in (0, -1); --4 / 10 
update public.travels3 set couleur = 'green' where departure_uncertainity = -1 and destination_uncertainity in (0, -1);-- 0
update public.travels3 set couleur = 'orange' where departure_uncertainity = -3 or destination_uncertainity =-3;-- 4
update public.travels3 set couleur = 'red' where destination_uncertainity = -5 or departure_uncertainity = -5; --6


select * from public.travels3 
select departure, destination, depart_net_route_marker, arrivee_net_route_marker, direct, couleur from public.travels3 
-- where departure_uncertainity = -5 or destination_uncertainity=-5;
order by id

alter table public.travels3 add column pointpath3 geometry[] ;
update public.travels3 set pointpath3 = ports.points_on_path(depart_pointcall_uhgs_id , arrivee_pointcall_uhgs_id, 10000, 50000) ;

/*
 * -- couleur verte
update public.pointcall2 set pointcall_uncertainity = 0 where certitude='Observé';
update public.pointcall2 set pointcall_uncertainity = -1 where certitude='Confirmé';
-- couleur grise
update public.pointcall2 set pointcall_uncertainity = -4 where certitude is NULL;
update public.pointcall2 set pointcall_uncertainity = -2 where certitude='Déclaré';
-- couleur orange
update public.pointcall2 set pointcall_uncertainity = -3 where verif like '%#net_route_marker%'; -- debat
-- couleur rouge
update public.pointcall2 set pointcall_uncertainity = -5 where certitude='Infirmé';
*/

select count(*) from public.travels3 where direct is true; --15

select distinct departure_function from public.travels3 where direct is true; -- O
select distinct departure_function from public.travels3 where direct is false; -- A, T

SELECT * FROM public.travels3

select distinct departure_function from public.travels2 where direct is true; -- O
select distinct departure_function from public.travels2 where direct is false; -- A, T


select p.pointcall_rank_dedieu , * from navigoviz.pointcall p where ship_id = '0014815N' and source_doc_id = '00189968'
-- ship_name like '%Yverdun%'
order by pointcall_rank_dedieu
-- p.pointcall_rankfull 
-- 0014815N Ville d' Yverdun 00189968

select p.pointcall_rank_dedieu , ship_name from navigoviz.pointcall p where ship_id = '0021517N' 
-- ship_name like '%Yverdun%'
order by pointcall_rank_dedieu

--- Pour la carte sur QGIS avec le cas de test les Bons amis , filtrer les ports en question
-- 'A0213139','A0122932','A0133403','A0167057','A0187995','A0186515','A0136930','A0187836','A0187101','A0180923','A0185385','A0189462','A0189364','A0204180'

select id, departure, destination, depart_net_route_marker, arrivee_net_route_marker, direct, couleur, pointpath3 as geom 
from public.travels3 

select id, departure, destination, depart_net_route_marker, arrivee_net_route_marker, direct, couleur, pointpath3 as geom 
from public.travels2
where departure = 'Granville' and destination='La Tremblade' 
-- id = 4 (couleur : gris, en tirets car supputée mais non car départ = 'O'...)
select id, departure, destination, depart_net_route_marker, arrivee_net_route_marker, direct, couleur, pointpath3 as geom 
from public.travels2
where departure = 'Honfleur' and destination='La Tremblade'   
-- id = 7 (couleur : gris, en tirets car supputée mais non car départ = 'O'...)
select id, departure, destination, depart_net_route_marker, arrivee_net_route_marker, direct, couleur, pointpath3 as geom 
from public.travels2
where departure = 'Le Havre' and destination='Bordeaux'  
-- id = 10 (couleur : gris, en tirets car supputée mais non car départ = 'O'...)
select id, departure, destination, depart_net_route_marker, arrivee_net_route_marker, direct, couleur, pointpath3 as geom 
from public.travels2
where departure = 'Gravelines' and destination='Marennes'  -- absent : si on garde un tri avec un filtre sur les -3 -enlevés, et on les ajoute, ca marche peut-être. 

-- comment calculer les alternatives

-- D'abord calculer sans les Z,  puis calculer les -3 avec Z et -5 et faire l'union ?

select  row_number() over () as id, id as tid, outdate_fixed, departure, destination, depart_net_route_marker, arrivee_net_route_marker, direct, couleur, st_makeline(pointpath3) as geom  
from public.travels2 order by tid 

select row_number() over () as tid, * from (
select id, departure, destination, outdate_fixed, departure_uncertainity, destination_uncertainity, depart_net_route_marker, arrivee_net_route_marker, direct, couleur, st_makeline(pointpath3) as geom 
from public.travels2
union
select id, departure, destination, outdate_fixed, departure_uncertainity, destination_uncertainity, depart_net_route_marker, arrivee_net_route_marker, direct, couleur, st_makeline(pointpath3) as geom 
from public.travels3 where couleur = 'red'
order by outdate_fixed, id 
) as k

update public.travels2 set couleur = 'grey' ;
update public.travels2 set couleur = 'green' where destination_uncertainity in (0, -1) and departure_uncertainity in (0, -1); --10 /12
update public.travels2 set couleur = 'green' where departure_uncertainity = -1 and destination_uncertainity in (0, -1);-- 0
update public.travels2 set couleur = 'orange' where departure_uncertainity = -3 or destination_uncertainity =-3;-- 4
update public.travels2 set couleur = 'red' where destination_uncertainity = -5 or departure_uncertainity = -5; --0


--------------------------------------------------------------------------------------------------------------------------------
-- reprise le 19 avril :cartographie des exemples de l'article
-- Aimable Ursule - ship 0007498N 
-- Fidèle Marianne - ship 0002931N
-- Marie Louise - ship 0004155N
-- Turgot, ship id 0021517N
-- Femmes et Enfants, 0003951N
-- Notre Dame des Carmes 0004171N
--------------------------------------------------------------------------------------------------------------------------------

drop table public.test_heloise;

create table public.test_heloise as (
            select pkid, ship_id , pointcall_rank_dedieu, p.pointcall_rankfull, record_id, source_suite, source_doc_id , pointcall_function,
            pointcall,  p.pointcall_uhgs_id , p.pointcall_admiralty,  p.pointcall_province, ship_name, tonnage::float, tonnage_unit, p.navigo_status , p.pointcall_out_date , p.pointcall_in_date ,
            pointcall_action ,  net_route_marker ,data_block_leader_marker, pointcall_uncertainity,
            (case when position('=' in coalesce(p.pointcall_out_date , p.pointcall_in_date)) > 0 then true else false end) as date_precise,
            list_sup_uhgs_id  ,list_inf_uhgs_id, date_fixed,
            (case when p.state_1789_fr = 'France' then
                (case when p.source_suite != 'G5' then
                    true
                else
                    (case when extract(year from coalesce(p.outdate_fixed, p.indate_fixed)) = 1787 then
                        nb_conges_1787_inputdone is not null and p.nb_conges_1787_inputdone > 0
                    else p.nb_conges_1789_inputdone is not null and p.nb_conges_1789_inputdone > 0
                    end)
                end)
                else false
                end) as has_input_done
            from navigoviz.pointcall p left join

            (select ughs_id, array_agg(ughs_id_sup) as list_sup_uhgs_id
            from ports.generiques_inclusions_geo_csv gigc
            group by ughs_id) as k on k.ughs_id = p.pointcall_uhgs_id

            left join
            (select ughs_id_sup, array_agg(ughs_id) as list_inf_uhgs_id
            from ports.generiques_inclusions_geo_csv gigc
            group by ughs_id_sup) as k2 on k2.ughs_id_sup = p.pointcall_uhgs_id

            where -- p.ship_id = '0014650N' --'0000447N' -- '0000305N' / -- '0014815N' -- 0012800N
            -- p.source_doc_id in ('00332297', '00310958', '00307963', '00352540') or
            -- ship_id in ( '0014650N', '0000011N', '0012406N', '0002344N', '0014815N', '0000063N', '0000447N', '0004355N', '0015074N', '0000068N', '0000144N','0000684N', '0002454N', '0000039N', '0000131N', '0012925N', '0000305N', '0000125N', '0007004N', '0021517N')
            ship_id is not null and net_route_marker != 'Q' -- and ship_id >= '0000388N'
             and ship_id in ( '0007498N', '0002931N', '0004155N', '0021517N', '0003951N', '0004171N', '0002537N', '0002350N', '0014650N', '0000011N', '0012406N', '0002344N', '0014815N', '0000063N', '0000447N', '0004355N', '0015074N', '0000068N', '0000144N','0000684N', '0002454N', '0000039N', '0000131N', '0012925N', '0000305N', '0000125N', '0007004N', '0021517N')
            order by ship_id, pointcall_rankfull
            )
-- Etapes : 1. pointcall_heloise 2. travel_heloise

select * from public.test_heloise
--1) test_heloise + verifs_heloise --> pointcall_heloise 
create table public.pointcall_heloise as (
	select t.ship_id, t.ship_name, t.pointcall_rankfull,t.pointcall_uhgs_id,t.pointcall,t.pointcall_function,coalesce(v.net_route_marker, t.net_route_marker) as net_route_marker,
	t.pointcall_out_date,t.pointcall_in_date,t.pointcall_action,t.date_fixed,
	t.has_input_done,t.pointcall_admiralty,t.pointcall_province,t.navigo_status,t.date_precise,t.source_suite,t.tonnage,t.list_sup_uhgs_id,
	t.record_id,t.source_doc_id,t.pkid,t.pointcall_rank_dedieu,v.certitude,v.certitude_reason,v.verif
    from public.test_heloise t left join public.verifs_heloise v on v.pkid = t.pkid
    order by ship_id, pointcall_rankfull 
)-- 29 pour le ship 0002537N

alter table public.pointcall_heloise add column pointcall_uncertainity int;

-- couleur verte
update public.pointcall_heloise set pointcall_uncertainity = 0 where certitude='Observé';
update public.pointcall_heloise set pointcall_uncertainity = -1 where certitude='Confirmé';
-- couleur grise
update public.pointcall_heloise set pointcall_uncertainity = -4 where certitude is NULL;
update public.pointcall_heloise set pointcall_uncertainity = -2 where certitude='Déclaré';
-- couleur orange
update public.pointcall_heloise set pointcall_uncertainity = -3 where verif like '%#net_route_marker%'; -- debat
-- couleur rouge
update public.pointcall_heloise set pointcall_uncertainity = -5 where certitude='Infirmé';


--2) --> travel_heloise_historien + travel_heloise_algo

drop table public.travel_heloise_historien
create table public.travel_heloise_historien as 
 (
            select  depart.id, depart.ship_id, depart.ship_name, 
            depart.pointcall as departure, arrivee.pointcall as destination, 
            depart.pointcall_uhgs_id as depart_pointcall_uhgs_id, arrivee.pointcall_uhgs_id as arrivee_pointcall_uhgs_id,
            depart.date_fixed as outdate_fixed, arrivee.date_fixed as indate_fixed, arrivee.date_fixed- depart.date_fixed as duration, 
            depart.pointcall_out_date as departure_out_date, arrivee.pointcall_in_date as destination_in_date,
            depart.net_route_marker as depart_net_route_marker , arrivee.net_route_marker as arrivee_net_route_marker,
            depart.pointcall_action as departure_action, 
            depart.certitude as departure_certitude,
            depart.pointcall_uncertainity as departure_uncertainity, 
            arrivee.pointcall_action as destination_action, 
            arrivee.certitude as destination_certitude,
            arrivee.pointcall_uncertainity as destination_uncertainity,
            
            depart.navigo_status as departure_navstatus, depart.pointcall_function as departure_function, 
            arrivee.navigo_status  as destination_navstatus, arrivee.pointcall_function as destination_function, 
            
            arrivee.ship_name as arrivee_shipname, 
            arrivee.pointcall_out_date as arrivee_outdate,
            
            depart.source_suite as source_depart, depart.source_doc_id as doc_depart, 
            arrivee.source_suite as source_destination, arrivee.source_doc_id as doc_destination,
            depart.pkid as departure_pkid, arrivee.pkid as destination_pkid 
            from 
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite, d.source_doc_id, d.pkid, d.pointcall_out_date, d.pointcall_in_date, navigo_status, pointcall_function,
            pointcall_uhgs_id, certitude
            from public.pointcall_heloise d
            where   net_route_marker = 'A' 
            order by d.ship_id, d.pointcall_rankfull) as depart, 
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite , d.source_doc_id, d.pkid, d.pointcall_in_date, navigo_status, pointcall_function, 
            pointcall_out_date, pointcall_uhgs_id, certitude
            from public.pointcall_heloise d
            where   net_route_marker = 'A' 
            order by d.ship_id, d.pointcall_rankfull) as arrivee 
            
            where depart.ship_id = arrivee.ship_id and depart.id + 1  = arrivee.id and depart.pointcall_uhgs_id<>arrivee.pointcall_uhgs_id
            order by depart.ship_id, depart.id 
    ) -- travel_heloise_historien
    
-- comment calculer les alternatives
-- D'abord calculer sans les Z (travel_heloise_historien),  
-- puis calculer travel_heloise_algo : les -3 avec Z et -5 et faire l'union ?
-- fix le 10 mai : puis calculer l'autojointure sans filtrer les Z, et retenir seulement les troncons avec -5 (INFIRMED) à un bout ou l'autre
-- puis faire l'union travel_heloise_historien + travel_heloise_algo ?
    
-- drop table travel_heloise_algo
create table public.travel_heloise_algo as 
 (
            select  depart.id, depart.ship_id, depart.ship_name, 
            depart.pointcall as departure, arrivee.pointcall as destination, 
            depart.pointcall_uhgs_id as depart_pointcall_uhgs_id, arrivee.pointcall_uhgs_id as arrivee_pointcall_uhgs_id,
            depart.date_fixed as outdate_fixed, arrivee.date_fixed as indate_fixed, arrivee.date_fixed- depart.date_fixed as duration, 
            depart.pointcall_out_date as departure_out_date, arrivee.pointcall_in_date as destination_in_date,
            depart.net_route_marker as depart_net_route_marker , arrivee.net_route_marker as arrivee_net_route_marker,
            depart.pointcall_action as departure_action, 
            depart.certitude as departure_certitude,
            depart.pointcall_uncertainity as departure_uncertainity, 
            arrivee.pointcall_action as destination_action, 
            arrivee.certitude as destination_certitude,
            arrivee.pointcall_uncertainity as destination_uncertainity,
            
            depart.navigo_status as departure_navstatus, depart.pointcall_function as departure_function, 
            arrivee.navigo_status  as destination_navstatus, arrivee.pointcall_function as destination_function, 
            
            arrivee.ship_name as arrivee_shipname, 
            arrivee.pointcall_out_date as arrivee_outdate,
            
            depart.source_suite as source_depart, depart.source_doc_id as doc_depart, 
            arrivee.source_suite as source_destination, arrivee.source_doc_id as doc_destination,
            depart.pkid as departure_pkid, arrivee.pkid as destination_pkid 
            from 
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite, d.source_doc_id, d.pkid, d.pointcall_out_date, d.pointcall_in_date, navigo_status, pointcall_function,
            pointcall_uhgs_id, certitude
            from public.pointcall_heloise d
            --where   (net_route_marker = 'A' and  pointcall_uncertainity = 0) or (net_route_marker = 'Z' and  pointcall_uncertainity = -1)-- or pointcall_uncertainity = -5
            order by d.ship_id, d.pointcall_rankfull) as depart, 
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uncertainity, d.net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite , d.source_doc_id, d.pkid, d.pointcall_in_date, navigo_status, pointcall_function, 
            pointcall_out_date, pointcall_uhgs_id, certitude
            from public.pointcall_heloise d
            --where   ( pointcall_uncertainity = -5 and net_route_marker = 'Z' ) --or (pointcall_uncertainity = -3 and net_route_marker = 'Z')
            order by d.ship_id, d.pointcall_rankfull) as arrivee 
            
            where depart.ship_id = arrivee.ship_id and depart.id + 1  = arrivee.id and depart.pointcall_uhgs_id<>arrivee.pointcall_uhgs_id
            AND (depart.pointcall_uncertainity = -5 OR arrivee.pointcall_uncertainity= -5)
            order by depart.ship_id, depart.id 
    ) -- 88 
    
select * from travel_heloise_algo where departure_uncertainity = -3 or destination_uncertainity =-3

/*
select departure, travel_rank, destination, cargo_item_action, commodity_purpose, source_entry ,  *
from navigoviz.built_travels where ship_id = '0004171N' order by travel_rank

select cargo_item_action, count(*) from navigoviz.pointcall
group by cargo_item_action


select cargo_item_action, pointcall_rankfull, pointcall, commodity_purpose  from navigoviz.pointcall where ship_id = '0004171N' 
order by pointcall_rankfull
*/

alter table travel_heloise_historien add column direct boolean default false;
update public.travel_heloise_historien set direct = true where departure_uncertainity = 0;--139

alter table public.travel_heloise_algo add column direct boolean default false;
update public.travel_heloise_algo set direct = true where departure_uncertainity = 0;--43


alter table public.travel_heloise_historien add column couleur text default 'grey';
alter table public.travel_heloise_algo add column couleur text default 'grey';

-- 3
update public.travel_heloise_historien set couleur = 'grey' ;
update public.travel_heloise_historien set couleur = 'green' where destination_uncertainity in (0, -1) and departure_uncertainity in (0, -1); --102
update public.travel_heloise_historien set couleur = 'orange' where departure_uncertainity = -3 or destination_uncertainity =-3;-- 14
update public.travel_heloise_historien set couleur = 'red' where destination_uncertainity = -5 or departure_uncertainity = -5; --6

update public.travel_heloise_algo set couleur = 'green' where destination_uncertainity in (0, -1) and departure_uncertainity in (0, -1); --0
update public.travel_heloise_algo set couleur = 'orange' where departure_uncertainity = -3 or destination_uncertainity =-3;-- 1
update public.travel_heloise_algo set couleur = 'red' where destination_uncertainity = -5 or departure_uncertainity = -5; --88 (finalement tout est rouge)


-- ship_id = '0004171N' -- st_makeline(pointpath3)
alter table public.travel_heloise_historien add column pointpath geometry[] ;
update public.travel_heloise_historien set pointpath = ports.points_on_path(depart_pointcall_uhgs_id , arrivee_pointcall_uhgs_id, 10000, 50000) where ship_id = '0004171N';
update public.travel_heloise_historien set pointpath = ports.points_on_path(depart_pointcall_uhgs_id , arrivee_pointcall_uhgs_id, 10000, 50000) where ship_id = '0004155N';
update public.travel_heloise_historien set pointpath = ports.points_on_path(depart_pointcall_uhgs_id , arrivee_pointcall_uhgs_id, 10000, 50000) where ship_id = '0002931N';

alter table public.travel_heloise_algo add column pointpath geometry[] ;
update public.travel_heloise_algo set pointpath = ports.points_on_path(depart_pointcall_uhgs_id , arrivee_pointcall_uhgs_id, 10000, 50000) where ship_id = '0004171N';
update public.travel_heloise_algo set pointpath = ports.points_on_path(depart_pointcall_uhgs_id , arrivee_pointcall_uhgs_id, 10000, 50000) where ship_id = '0004155N';
update public.travel_heloise_algo set pointpath = ports.points_on_path(depart_pointcall_uhgs_id , arrivee_pointcall_uhgs_id, 10000, 50000) where ship_id = '0002931N';

-- faire l'union
select row_number() over () as tid, * from (
select id, departure, destination, outdate_fixed, departure_uncertainity, destination_uncertainity, depart_net_route_marker, arrivee_net_route_marker, direct, couleur, st_makeline(pointpath) as geom 
from public.travel_heloise_historien where ship_id = '0004171N'
union
select id, departure, destination, outdate_fixed, departure_uncertainity, destination_uncertainity, depart_net_route_marker, arrivee_net_route_marker, direct, couleur, st_makeline(pointpath) as geom 
from public.travel_heloise_algo where couleur = 'red' and ship_id = '0004171N'
order by outdate_fixed, id 
) as k

select distinct pointcall_uhgs_id from navigoviz.pointcall where ship_id = '0004171N'
select distinct pointcall_uhgs_id from navigoviz.pointcall where ship_id = '0004155N'
select distinct pointcall_uhgs_id from navigoviz.pointcall where ship_id = '0002931N'


select id, departure, destination, outdate_fixed, departure_uncertainity, destination_uncertainity, depart_net_route_marker, arrivee_net_route_marker, direct, couleur, st_makeline(pointpath) as geom 
from public.travel_heloise_historien where ship_id = '0004155N'
order by outdate_fixed, id 

-- Fidèle Marianne

select id, departure, destination, outdate_fixed, departure_uncertainity, destination_uncertainity, depart_net_route_marker, arrivee_net_route_marker, direct, couleur, st_makeline(pointpath) as geom 
from public.travel_heloise_historien where ship_id = '0002931N'
order by outdate_fixed, id 


select * from public.pointcall2

-- traitement long lancé le soir du 12 mai 2022 : terminé
update public.travel_heloise_historien set pointpath = ports.points_on_path(depart_pointcall_uhgs_id , arrivee_pointcall_uhgs_id, 10000, 50000) where pointpath IS null;
select count(*) from public.travel_heloise_historien where pointpath is null;

-------------------------------------------------------------------------------------
-- POST Heloise, travail d'extraction pour Silvia
-- Mais le fichier stages n'est pas le bon
-- Il faudrait utiliser la table de geoassembling et pas pointcall_stages qui est archi fausse
-- Extraire les segments de Built_travels dont il manque les distances en miles
-------------------------------------------------------------------------------------

select departure_uhgs_id, departure_fr, destination_uhgs_id, destination_fr, distance_dep_dest_miles 
from navigoviz.built_travels, navigo.stages s
where departure_uhgs_id!=destination_uhgs_id and source_entry!='both-to'
and distance_dep_dest_miles is null

select 'a', 'b' except (select 'a', 'c');

-- allers
select distinct departure_uhgs_id, destination_uhgs_id 
from navigoviz.built_travels
where departure_uhgs_id!=destination_uhgs_id and source_entry!='both-to'
and distance_dep_dest_miles is null --1554
except 
(select distinct pointcall_uhgs_id, next_point__pointcall_uhgs_id from navigo.stages s where stages__length_miles is not  null) --4072
-- 1513

-- retour
select distinct departure_uhgs_id, destination_uhgs_id 
from navigoviz.built_travels
where departure_uhgs_id!=destination_uhgs_id and source_entry!='both-to'
and distance_dep_dest_miles is null --1554
except 
(select distinct next_point__pointcall_uhgs_id, pointcall_uhgs_id  from navigo.stages s where stages__length_miles is not  null) --4072
-- 1549

-- INTERSECTION des 2 ensembles

select distinct departure_uhgs_id, destination_uhgs_id 
from navigoviz.built_travels
where departure_uhgs_id!=destination_uhgs_id and source_entry!='both-to'
and distance_dep_dest_miles is null --1554
except 
(select distinct pointcall_uhgs_id, next_point__pointcall_uhgs_id from navigo.stages s where stages__length_miles is not  null) --4072
UNION
(select distinct departure_uhgs_id, destination_uhgs_id 
from navigoviz.built_travels
where departure_uhgs_id!=destination_uhgs_id and source_entry!='both-to'
and distance_dep_dest_miles is null --1554
except 
(select distinct next_point__pointcall_uhgs_id, pointcall_uhgs_id  from navigo.stages s where stages__length_miles is not  null) --4072
)--1554

-- La bonne requête (mais que sur inferred)

select departure_uhgs_id, p.toponyme_standard_fr , destination_uhgs_id, p2.toponyme_standard_fr
from ports.port_points p, ports.port_points p2,
(
select distinct departure_uhgs_id, destination_uhgs_id 
from navigoviz.built_travels
where departure_uhgs_id!=destination_uhgs_id and source_entry!='both-to'
and distance_dep_dest_miles is null --1554
except (
(select distinct pointcall_uhgs_id, next_point__pointcall_uhgs_id from navigo.stages s where stages__length_miles is not  null) --4072
UNION 
(select distinct next_point__pointcall_uhgs_id as pointcall_uhgs_id, pointcall_uhgs_id as next_point__pointcall_uhgs_id from navigo.stages s where stages__length_miles is not  null) --4072
)
--1508
) as k 
where k.departure_uhgs_id = p.uhgs_id and  k.destination_uhgs_id = p2.uhgs_id
-- 1508


select * from navigo.stages s 
where pointcall_uhgs_id='A0167057' and next_point__pointcall_uhgs_id = 'B0000953' 
or (next_point__pointcall_uhgs_id = 'A0167057' and pointcall_uhgs_id='B0000953')
--A0210797	Marseille	A0249999	Pozzallo --pas renseigné
--A0180068	Courseulles	A0205141	Cancale -- inferred leg
--A0167057	Granville	B0000953	Le Four -- inferred leg


-- Extraction pour Silvia et Pierre Nicolo le 13 mai 2022 
-- Fichier C:\Travail\ULR_owncloud\ANR_PORTIC\datasprint3_Marseille\Data\missing_miles_legs.csv

select departure_uhgs_id, p.toponyme_standard_fr , destination_uhgs_id, p2.toponyme_standard_fr
from ports.port_points p, ports.port_points p2,
(
(select distinct departure_uhgs_id, destination_uhgs_id 
from navigoviz.raw_flows 
where departure_uhgs_id!=destination_uhgs_id and source_entry!='both-to'
and distance_dep_dest_miles is null --844
union
(select distinct departure_uhgs_id, destination_uhgs_id 
from navigoviz.built_travels
where departure_uhgs_id!=destination_uhgs_id and source_entry!='both-to'
and distance_dep_dest_miles is null )
)--2141
except (
(select distinct pointcall_uhgs_id, next_point__pointcall_uhgs_id from navigo.stages s where stages__length_miles is not  null) --4072
UNION 
(select distinct next_point__pointcall_uhgs_id as pointcall_uhgs_id, pointcall_uhgs_id as next_point__pointcall_uhgs_id from navigo.stages s where stages__length_miles is not  null) --4072
)
--1508
) as k 
where k.departure_uhgs_id = p.uhgs_id and  k.destination_uhgs_id = p2.uhgs_id
-- 2070 au TOTAL




select homeport_fr, departure, distance_homeport_dep_miles  from navigoviz.built_travels


select departure_action, source_doc_id, * from navigoviz.built_travels 
where departure_fr = destination_fr and departure_action != 'Sailing around' limit 1
'Sailing around'

-- Identifier des problèmes sur les travels et les A/Z
select departure_action, destination_action, count(*) from navigoviz.built_travels 
where departure_uhgs_id=destination_uhgs_id
group by departure_action, destination_action

select pointcall_action from navigoviz.pointcall where source_doc_id = '00108472'
----------------------------------------------------------------------------------------------------------------
-- dev : copie de sauvegarde
-------------------------------------------------------------------------------------


'''
        query = """create table navigoviz.uncertainity_travels as (
            select depart.ship_id||'-'||to_char(depart.id, '09') as travel_id, depart.id, depart.ship_id, depart.ship_name, depart.pointcall as departure, arrivee.pointcall as destination, 
            depart.pointcall_uhgs_id as departure_uhgs_id, arrivee.pointcall_uhgs_id as destination_uhgs_id, 
            
            depart.date_fixed as outdate_fixed, arrivee.date_fixed as indate_fixed, arrivee.date_fixed- depart.date_fixed as duration, 
            depart.pointcall_out_date as departure_out_date, arrivee.pointcall_in_date as destination_in_date,
            
            depart.pointcall_action as departure_action, depart.pointcall_uncertainity as departure_uncertainity, 
            arrivee.pointcall_action as destination_action, arrivee.pointcall_uncertainity as destination_uncertainity,
            
            depart.navigo_status as departure_navstatus, depart.pointcall_function as departure_function, depart.net_route_marker as depart_net_route_marker,
            arrivee.navigo_status  as destination_navstatus, arrivee.pointcall_function as destination_function, arrivee.net_route_marker as arrivee_net_route_marker,
            
            depart.source_suite as source_depart, depart.source_doc_id as doc_depart, 
            arrivee.source_suite as source_destination, arrivee.source_doc_id as doc_destination,
            depart.pkid as departure_pkid, arrivee.pkid as destination_pkid ,
            
            arrivee.ship_name as arrivee_shipname, 
            depart.captain_name as depart_captainname, arrivee.captain_name as arrivee_captainname, 
            depart.captain_id as depart_captainid, arrivee.captain_id as arrivee_captainid,
            depart.flag as depart_flag, arrivee.flag as arrivee_flag, 
            depart.ship_flag_id as depart_flagid, arrivee.ship_flag_id as arrivee_flagid, 
            depart.homeport as depart_homeport, arrivee.homeport as arrivee_homeport, 
            depart.homeport_uhgs_id as depart_homeportid, arrivee.homeport_uhgs_id as arrivee_homeportid,
            arrivee.pointcall_out_date as arrivee_outdate,
            depart.tonnage_class as depart_tonnage_class, arrivee.tonnage_class as arrivee_tonnage_class
            
            from 
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uhgs_id , d.pointcall_uncertainity, d.net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite, d.source_doc_id, d.pkid, d.pointcall_out_date, d.pointcall_in_date, navigo_status, pointcall_function,
            captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id, tonnage_class
            from navigoviz.pointcall d
            --where   net_route_marker = 'A' and pointcall_uncertainity > -2
            order by d.ship_id, d.pointcall_rankfull) as depart,
            
            (select ROW_NUMBER () OVER (PARTITION BY ship_id order by pointcall_rankfull) as id, d.pointcall_rankfull, 
            d.ship_id, d.ship_name, d.pointcall, d.pointcall_uhgs_id , d.pointcall_uncertainity, d.net_route_marker,  d.date_fixed , d.pointcall_action, 
            d.source_suite , d.source_doc_id, d.pkid, d.pointcall_in_date, navigo_status, pointcall_function, d.pointcall_out_date,
            captain_name, captain_id, flag, ship_flag_id, homeport, homeport_uhgs_id, tonnage_class
            from navigoviz.pointcall d
            --where   net_route_marker = 'A' and pointcall_uncertainity > -2
            order by d.ship_id, d.pointcall_rankfull, d.source_doc_id) as arrivee
            
            where depart.ship_id = arrivee.ship_id and depart.id + 1  = arrivee.id 
            order by depart.ship_id, depart.id 
        )  """
        self.loader.execute_sql(query)
        '''
            
        '''
        select doc_depart, source_depart, doc_destination , source_destination, departure , ship_id, ship_name, departure_out_date, destination_in_date, pp.amiraute , destination , pp2.amiraute,  duration , distance_dep_dest_miles , duration,
        (case when duration is not null and duration > 0 then (distance_dep_dest_miles/duration)  else -1 end ) as vitesse_miles_par_jour , 
        pp.amiraute != pp2.amiraute as amirautes_differentes
        from navigoviz.uncertainity_travels ut , ports.port_points pp , ports.port_points pp2
        where destination_navstatus like 'PC%' and destination_function = 'O' 
        and depart_net_route_marker ='Z' and arrivee_net_route_marker='A' 
        and pp.uhgs_id = departure_uhgs_id
        and pp2.uhgs_id = destination_uhgs_id 
        and departure != destination 
        and departure_uhgs_id not in (select geo.ughs_id_sup from ports.generiques_inclusions_geo_csv geo where geo.ughs_id = destination_uhgs_id) 
        -- 562 lignes à vérifier. Pointcall_uncertainity de départ prend -3 et les autres attributs aussi.
        '''
        
        self.loader.execute_sql("alter table navigoviz.uncertainity_travels  add column travel_uncertainity int")
        self.loader.execute_sql("update navigoviz.uncertainity_travels set travel_uncertainity = 0 where departure_uncertainity=0 and destination_uncertainity=0")
        self.loader.execute_sql("update navigoviz.uncertainity_travels set travel_uncertainity = -1 where departure_uncertainity=-1 or destination_uncertainity=-1")
        self.loader.execute_sql("update navigoviz.uncertainity_travels set travel_uncertainity = -2 where departure_uncertainity=-2 or destination_uncertainity=-2")
        self.loader.execute_sql("update navigoviz.uncertainity_travels set travel_uncertainity = -3 where departure_uncertainity=-3 or destination_uncertainity=-3")
--------------------------------------------------------------------------------------------
-- reprise et calcul sur le server de tous les paths manquant
-- le 6 juillet 2022
-- On a calculé une table navigoviz.routepaths qui contient les paths
---------------------------------------------------------------------------------------------        
create table navigoviz.routepaths (
	from_uhgs_id text,
	from_toponyme_fr text, 
	to_uhgs_id text,
	to_toponyme_fr text, 
	m_offset double precision, 
	p_distance_ratio integer,
	pointpath geometry[],
	elapseTime float,
	nbPoints int
);

alter table navigoviz.routepaths add column broader_length double precision;
alter table navigoviz.routepaths add column direct_length double precision;
alter table navigoviz.routepaths add column endouble boolean default false;
alter table navigoviz.routepaths add column redonepath geometry[] ;

-- truncate navigoviz.routepaths;
select count(travel_id) from navigoviz.built_travels bt where source_entry != 'both-to' and pointpath is null;
-- 61339 / 64268
select distinct(departure_uhgs_id, destination_uhgs_id)
from navigoviz.built_travels bt where source_entry != 'both-to' and pointpath is null;
select count(*) from navigoviz.pointcall p  ;

(A0025828,A0249463)
(A0025828,A0242553)
(A0025828,A0249463)
(A0025828,A9999997)
(A0026844,A0007338)
(A0026844,A0011401)
(A0026844,A0018049)

CREATE TYPE ports.portic_computedPath AS
(
    pointslist geometry[]      ,
    nbPoints  int,
    duration float
);

select (c).nbPoints,(c).duration
from 
(select ports.portic_points_on_path('A0103067', 'A0195511', 20000, 50000) as c) as r

create schema paths;
drop schema paths cascade;


create table paths.test as select 'A0103067' as departid, 'Pontevedra' as departname, 'A0195511' as destid, 'St nazaire' as destname , 20000 as moffset, 50000 as distance,  ports.portic_points_on_path('A0103067', 'A0195511', 20000, 50000) as calcul
select (calcul).nbPoints,(calcul).duration, (calcul).pointslist  from paths.test

create table paths.pontevedra_st_nazaire as (
	select 1 as idtrack, row_number() over () as id, geom, 'direct' as sens
	from (select unnest(ports.points_on_path('A0103067', 'A0195511', 20000, 50000)) as geom) as k
union (
	select 2 as idtrack, row_number() over () as id, geom, 'inverse' as sens
	from (select unnest(ports.points_on_path('A0195511', 'A0103067', 20000, 50000)) as geom) as k
)
)

-- A faire dans ETL
insert into navigoviz.routepaths (from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr)
select distinct departure_uhgs_id, departure_fr, destination_uhgs_id, destination_fr 
from navigoviz.built_travels bt
where source_entry != 'both-to' and pointpath is null  
-- 6752
-- sur serveur et localement


insert into navigoviz.routepaths (from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr)
select distinct bt.departure_uhgs_id, bt.departure_fr, bt.destination_uhgs_id, bt.destination_fr 
from navigoviz.built_travels bt 
where source_entry != 'both-to' and bt.pointpath is null 
-- left outer join  navigoviz.routepaths r2 
	--on (r2.from_uhgs_id =bt.departure_uhgs_id  and r2.to_uhgs_id  = bt.destination_uhgs_id)


-- A faire dans ETL
-- corriger : marquer ceux qu'on ne calculera pas en doublon
alter table navigoviz.routepaths add column endouble boolean default false;

update navigoviz.routepaths r  set endouble = true
from navigoviz.routepaths r2
WHERE r.from_uhgs_id=r2.to_uhgs_id AND r.to_uhgs_id=r2.from_uhgs_id;

select * from navigoviz.routepaths where endouble is true ;
-- A0046492	Bruges	A0198999	La Rochelle


update  navigoviz.routepaths r set pointpath = null, m_offset=null, p_distance_ratio=null, nbPoints=null;
-- 6797

update  navigoviz.routepaths r 
set m_offset = 10000, p_distance_ratio = 50000, pointpath=bt.pointpath , nbPoints = array_length(bt.pointpath, 1)
from navigoviz.built_travels  bt 
where bt.geom is not null 
and bt.departure_uhgs_id = r.from_uhgs_id and bt.destination_uhgs_id =r.to_uhgs_id
or (bt.departure_uhgs_id = r.to_uhgs_id and bt.destination_uhgs_id =r.from_uhgs_id);
-- 3457

select * from navigoviz.routepaths where nbPoints is not null ;
--45, 6 juil.  21:05 log_06juin2022.txt

-- test 1
update  navigoviz.routepaths r set m_offset = 20000, p_distance_ratio = 50000,
 	pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
from (
	select from_uhgs_id, to_uhgs_id, ports.portic_points_on_path(from_uhgs_id, to_uhgs_id, 20000, 50000) as c 
	from navigoviz.routepaths r2
	where r2.pointpath is null  
	) as r2 
where r.from_uhgs_id=r2.from_uhgs_id and r.to_uhgs_id = r2.to_uhgs_id	
	and r.from_uhgs_id = 'A0007338' and r.to_uhgs_id = 'A0026844';

'A0148900', 'A0390929'
-- requete OK à mettre en script sur le serveur
update  navigoviz.routepaths r set m_offset = 20000, p_distance_ratio = 50000,
 	pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
from (
	select from_uhgs_id, to_uhgs_id, ports.portic_points_on_path(from_uhgs_id, to_uhgs_id, 20000, 50000) as c 
	from navigoviz.routepaths r2
	where r2.pointpath is null 
	) as r2 
where r.from_uhgs_id=r2.from_uhgs_id and r.to_uhgs_id = r2.to_uhgs_id	
	--or (r.to_uhgs_id=r2.from_uhgs_id and r.from_uhgs_id = r2.to_uhgs_id	)
	and r.from_uhgs_id = 'A0046492' and r.to_uhgs_id = 'A0198999';



Cayenne -> Détroit de Gibraltar
'B2009606', 'A0354220'
Mesquer -> Angleterre
'A0148900', 'A0390929'
select * from navigoviz.routepaths where nbpoints > 2
from_uhgs_id='A0148900'

 update  navigoviz.routepaths r set m_offset = 10000.000000, p_distance_ratio = 50000,
                    pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
                    from (
                        select ports.portic_points_on_path('A1968950', 'A0198999', 10000.000000, 50000) as c
                    ) as calcul
                    where  r.from_uhgs_id = 'A1968950' and r.to_uhgs_id = 'A0198999' or (r.to_uhgs_id = 'A1968950' and r.from_uhgs_id = 'A0198999');
select * from navigoviz.routepaths where from_uhgs_id='A1968950'
                   
                   
update  navigoviz.routepaths r set m_offset = 10000.000000, p_distance_ratio = 50000,
                    pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
                    from (
                        select ports.portic_points_on_path('A0221139', 'A0210797', 10000.000000, 50000) as c
                    ) as calcul
where  r.from_uhgs_id = 'A0221139' and r.to_uhgs_id = 'A0210797' or (r.to_uhgs_id = 'A0221139' and r.from_uhgs_id = 'A0210797');



select * from navigoviz.routepaths where from_uhgs_id='B2009606'
select from_uhgs_id, to_uhgs_id, from_toponyme_fr, to_toponyme_fr from navigoviz.routepaths where pointpath is null
--(A0007338,Bizerte ,A0026844,Tunis)
select * from navigoviz.routepaths where from_uhgs_id = 'A0007338' and to_uhgs_id = 'A0026844';
-- (A0007338,Bizerte ,A0210797,Marseille)
select * from navigoviz.routepaths where from_uhgs_id = 'A0007338' and to_uhgs_id = 'A0210797';
-- A0046492	Bruges	A0198999	La Rochelle
select * from navigoviz.routepaths where from_uhgs_id = 'A0046492' and to_uhgs_id = 'A0198999';
-- A0180923	Bordeaux	B2048070	Cap-Français
select * from navigoviz.routepaths where from_uhgs_id = 'A0180923' and to_uhgs_id = 'B2048070';
-- A0196771	Saint-Valéry-sur-Somme	A1964694	Marans
select *, ST_IsSimple(st_makeline(pointpath)) from navigoviz.routepaths where from_uhgs_id = 'A0196771' and to_uhgs_id = 'A1964694';



select row_number() OVER () AS id, *, ST_IsSimple(st_makeline(pointpath)), st_makeline(pointpath), st_intersects(s.shoreline, st_makeline(pointpath)) AS intersects 
from navigoviz.routepaths , navigoviz.shoreline s 
where pointpath IS NOT NULL and from_toponyme_fr = 'Cork' and s.shoreline is not null and st_intersects(s.shoreline, st_makeline(pointpath));

row_number() OVER() as id

select ST_GeometryType(s.shoreline) from  navigoviz.shoreline s -- linestring


select uhgs_id, toponyme, s.id, st_contains(s.polygone3857, p.port_on_line_1mile )
from navigoviz.shoreline s , ports.port_points p
where uhgs_id in ('A0614744', 'A0605259') and id = '26'

-- ports.portic_redo_points_on_path() 

SELECT ports.portic_redo_points_on_path(pointpath, 10000, 50000) 
-- , *, ST_IsSimple(st_makeline(pointpath)) 
from navigoviz.routepaths
WHERE from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744';

select * from ports.port_points pp where uhgs_id in ('A0204180', 'A0614744');
-- Kinsale en Irlande, Dunkerque

alter table navigoviz.routepaths drop column  redonepath;
alter table navigoviz.routepaths add column  redonepath geometry[];

--alter table navigoviz.routepaths add column redonepath ports.portic_computedPath;

-- update navigoviz.routepaths set redonepath = ports.portic_redo_points_on_path(pointpath, 10000, 50000) 
-- where from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744';


-----------------------------------------------------------------------------
-- DEBUG  Dunkerque --> Kinsale
-- le 11 juillet 2022
-----------------------------------------------------------------------------


------------------------------------------
--- CAS Dunkerque -->  kingsale
------------------------------------------

update  navigoviz.routepaths r set m_offset = 10000, p_distance_ratio = 50000,
                    redonepath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
                    from (
                        select ports.points_on_path('A0204180', 'A0614744', 50000, 50000) as c
                    ) as calcul 
where  r.from_uhgs_id = 'A0204180' and r.to_uhgs_id = 'A0614744' or (r.to_uhgs_id = 'A0204180' and r.from_uhgs_id = 'A0614744');

select s2.id, ST_Intersects (st_makeline(pointpath),s2.polygone3857 ) 
from navigoviz.routepaths, navigoviz.shoreline s2  
	where from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744' and ST_Intersects(st_makeline(pointpath),s2.polygone3857 ) 
-- 15 et 628
	
-- Dunkerque -> Kinsale
-- regarder le résultat Dunkerque -> Kinsale
select row_number() over () as id, st_makeline(redonepath) as geom  
from  navigoviz.routepaths r where  from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744' ;

select row_number() over () as id,unnest(pointpath)  
from  navigoviz.routepaths r where  from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744';


update navigoviz.routepaths set redonepath = (c).pointslist, nbpoints=(c).nbPoints, elapsetime = (c).duration+elapsetime
from (select ports.portic_redo_points_on_path(pointpath, 5000, 25000) as c 
	from navigoviz.routepaths 
	where from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744' ) as calcul
where from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744';
-- 
select s2.id, ST_Intersects (st_makeline(redonepath),s2.polygone3857 ) 
from navigoviz.routepaths, navigoviz.shoreline s2  
	where from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744' and ST_Intersects(st_makeline(redonepath),s2.polygone3857 ) 
-- 628	true

-- appel récursif
update navigoviz.routepaths set redonepath = (c).pointslist, nbpoints=(c).nbPoints, elapsetime = (c).duration+elapsetime
from (select ports.portic_redo_points_on_path(redonepath, 10000, 50000) as c 
	from navigoviz.routepaths 
	where from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744' ) as calcul
where from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744';

-- appel récursif
update navigoviz.routepaths set redonepath = (c).pointslist, nbpoints=(c).nbPoints, elapsetime = (c).duration+elapsetime
from (select ports.portic_redo_points_on_path(redonepath, 5000, 25000) as c 
	from navigoviz.routepaths 
	where from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744' ) as calcul
where from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744';

update navigoviz.routepaths set redonepath = c
from (select ports.simplifypathbetter(redonepath) as c 
	from navigoviz.routepaths 
	where from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744' ) as calcul
where from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744';

select array_length(redonepath, 1) from navigoviz.routepaths 
	where from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744'
-- 6
-- pointslistsimplified := ports.simplifypathbetter(pointslistnew);

	
	

			select st_length(g), g from (
				select ST_NumGeometries(geom),  (st_dump(geom)).geom as g
				from (
				select ST_OffsetCurve(ST_LineSubstring(shoreline, 0.17453252234451658, 0.9865466731612956), 10000) as geom
							from navigoviz.shoreline s 
							where s.id = '15'
				) as k
				) as q
			--where st_intersects(g, test[1]) and st_intersects(g, test[2]);
			order by  st_length(g) desc limit 1;
			longueur_segment:= st_length(m_seg);

		select row_number() over () as id, shoreline as g
-- , st_scroll(shoreline, st_setsrid(st_makepoint(-149867.18758124145, 6642776.836827128), 3857)) as geom
--ST_LineSubstring(st_reverse(shoreline), 0.17453252234451658, 0.9865466731612956)  as geom
from  navigoviz.shoreline s where s.id = '15'


select st_length(g), g from (
				select ST_NumGeometries(geom),  (st_dump(geom)).geom as g
				from (
				select ST_OffsetCurve(ST_LineSubstring(st_addpoint(shoreline, st_setsrid(st_makepoint(262807.53625297826, 6632586.0688358955), 3857) , 0), 0, 0.22798033641643423), 10000) as geom
							from navigoviz.shoreline s 
							where s.id = '15'
				) as k
				) as q
			--where st_intersects(g, test[1]) and st_intersects(g, test[2]);
			order by  st_length(g) desc limit 1;
		
		
select pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
                    from (
                        select ports.portic_redo_points_on_path('A0204180', 'A0614744', 10000, 50000) as c
                    ) as calcul
                    where  r.from_uhgs_id = 'A1968950' and r.to_uhgs_id = 'A0198999'
                    
navigoviz.routepaths
-- A0204180


select row_number() over () as id,unnest(redonepath) from navigoviz.routepaths
where from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744';


select 1 as id, st_setsrid(st_makepoint(-669419.4511218165, 7736951.636980291), 3857) as geom
union
select 2 as id,st_setsrid(st_makepoint(-696416.3517657017, 7716830.109400979), 3857) as geom
 
SELECT k.p1, st_makeline(k.p1,k.p2) as linethatcut, ports.order_intersection(k.p1, st_makeline(k.p1,k.p2)) 
FROM 
(select  st_setsrid(st_makepoint(-669419.4511218165, 7736951.636980291), 3857) as p1,
		st_setsrid(st_makepoint(-696416.3517657017, 7716830.109400979), 3857)  AS p2) as k

		
	select s.id
	from navigoviz.shoreline s 
	where  shoreline is not null and s.area > 1 and st_intersects(st_buffer(linethatcut, 10), shoreline)
	order by st_distance(p_depart,ST_ClosestPoint (shoreline, p_depart)) asc limit 1;

/*
select row_number() over () as id, redonepath, st_makeline((redonepath).pointslist) from navigoviz.routepaths
where from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744';
*/

select row_number() over () as id, *, (redonepath)[67:], st_makeline((redonepath)) from navigoviz.routepaths
where from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744';


select row_number() over () as id,unnest(redonepath[66:]) from navigoviz.routepaths
where from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744';
-- POINT (-832640.364444307 7370635.716013506)
-- POINT (-845223.7838971685 7373848.6488388935)
-- POINT (-948115.2160067303 6741849.347803193)
-- POINT (-834464.2291263783 7391472.736155571)

select 0 as id, st_setsrid(st_makepoint(-832640.364444307, 7370635.716013506), 3857) as geom
union
select 1 as id, st_setsrid(st_makepoint(-845223.4511218165, 7373848.636980291), 3857) as geom
union
select 2 as id,st_setsrid(st_makepoint(-948115.2160067303, 6741849.347803193), 3857) as geom
 union
select 3 as id,st_setsrid(st_makepoint(-834464.2291263783, 7391472.736155571), 3857) as geom
union
select 4 as id,st_setsrid(st_makepoint(-1007151.4998571195, 6709226.827596462), 3857) as geom
union
select 5 as id,st_setsrid(st_makepoint(-937801.9097061742, 7366727.586163488), 3857) as geom

-- POINT(-622127.746011726 7653388.011543957)
select 7 as id, st_setsrid(st_makepoint(-622127.746011726, 7653388.011543957), 3857) as geom
select row_number() over () as id,unnest(k.tab) from 
(select ports.subpoints_on_path(st_setsrid(st_makepoint(-832640.364444307, 7370635.716013506), 3857), st_setsrid(st_makepoint(-948115.2160067303, 6741849.347803193), 3857), 10000, 50000) as tab
) as k

-- SRID=3857;POINT(-937801.9097061742 7366727.586163488)
select ports.move_pointp1_intosea(st_setsrid(st_makepoint(-937801.9097061742, 7366727.586163488), 3857)) 

select * from navigoviz.shoreline where id = '10980'
select st_area(polygone3857) from navigoviz.shoreline where shoreline is null and polygone3857 is not null ;

select st_length(st_shortestline(st_setsrid(st_makepoint(-937801.9097061742, 7366727.586163488), 3857), shoreline)) as d, st_closestpoint(shoreline, st_setsrid(st_makepoint(-937801.9097061742, 7366727.586163488), 3857)) as proche 
from navigoviz.shoreline where id = '10980'  limit 1;
	
select row_number() over () as id, st_makeline((c).pointslist) from 
(
SELECT ports.portic_redo_points_on_path(pointpath, 10000, 50000) as c
-- , *, ST_IsSimple(st_makeline(pointpath)) 
from navigoviz.routepaths
WHERE from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744') as calcul
;

update  navigoviz.routepaths r set m_offset = 10000, p_distance_ratio = 50000,
                    pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
                    from (
                        select ports.points_on_path( 'A0204180','A0614744', 10000, 50000) as c
                    ) as calcul 
where  r.to_uhgs_id = 'A0614744' and r.from_uhgs_id = 'A0204180';
-- celui ci sera à simplifier dans redonepath par exemple

-- timing on 
update navigoviz.routepaths set redonepath = c, nbpoints=array_length(c, 1)
from (select ports.simplifypathbetter(pointpath) as c 
	from navigoviz.routepaths 
	where from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744' ) as calcul
where from_uhgs_id = 'A0204180' and to_uhgs_id = 'A0614744';


select row_number() over () as id, from_toponyme_fr, to_toponyme_fr, nbpoints, elapseTime, st_makeline(redonepath) as geom  
from  navigoviz.routepaths r 
--where  from_uhgs_id = 'A0152606' and to_uhgs_id = 'A0210797'
--where  to_uhgs_id = 'A0187101' and from_uhgs_id = 'A0614744' ;
where  r.to_uhgs_id = 'A0614744' and r.from_uhgs_id = 'A0204180';

------------------------------
--- Cas de test Kinsale -> le havre
-------------------------------

update  navigoviz.routepaths r set m_offset = 10000, p_distance_ratio = 50000,
                    pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
                    from (
                        select ports.points_on_path( 'A0614744','A0187101', 10000.000000, 50000) as c
                    ) as calcul 
where  r.from_uhgs_id = 'A0187101' and r.to_uhgs_id = 'A0614744' or (r.to_uhgs_id = 'A0187101' and r.from_uhgs_id = 'A0614744');
-- A0614744
-- A0187101

select row_number() over () as id, to_uhgs_id, st_makeline(pointpath) as geom  
from  navigoviz.routepaths r 
where  to_uhgs_id = 'A0187101' and from_uhgs_id = 'A0614744' ;

-- appel récursif
update navigoviz.routepaths set redonepath = (c).pointslist, nbpoints=(c).nbPoints, elapsetime = (c).duration+elapsetime
from (select ports.portic_redo_points_on_path(pointpath, 5000, 25000) as c 
	from navigoviz.routepaths 
	where from_uhgs_id = 'A0614744' and to_uhgs_id = 'A0187101' ) as calcul
where from_uhgs_id = 'A0614744' and to_uhgs_id = 'A0187101';


select row_number() over () as id, to_uhgs_id, st_makeline(redonepath) as geom  
from  navigoviz.routepaths r 
where  to_uhgs_id = 'A0187101' and from_uhgs_id = 'A0614744' ;

select s2.id, ST_Intersects (st_makeline(redonepath),s2.polygone3857 ) 
from navigoviz.routepaths, navigoviz.shoreline s2  
	where to_uhgs_id = 'A0187101' and from_uhgs_id = 'A0614744'  and ST_Intersects(st_makeline(redonepath),s2.polygone3857 ) 
-- 15	true

-- appel récursif
update navigoviz.routepaths set redonepath = (c).pointslist, nbpoints=(c).nbPoints, elapsetime = (c).duration+elapsetime
from (select ports.portic_redo_points_on_path(redonepath, 2500, 12500) as c 
	from navigoviz.routepaths 
	where from_uhgs_id = 'A0614744' and to_uhgs_id = 'A0187101' ) as calcul
where from_uhgs_id = 'A0614744' and to_uhgs_id = 'A0187101';	
-- terminé

-- on simplifie
update navigoviz.routepaths set redonepath = c
from (select ports.simplifypathbetter(redonepath) as c 
	from navigoviz.routepaths 
	where from_uhgs_id = 'A0614744' and to_uhgs_id = 'A0187101' ) as calcul
where from_uhgs_id = 'A0614744' and to_uhgs_id = 'A0187101';

select array_length(redonepath, 1) from navigoviz.routepaths 
	where from_uhgs_id = 'A0614744' and to_uhgs_id = 'A0187101'
-- 6 pts
drop function  ports.portic_redo_points_on_path(pointslist geometry[], m_offset double precision, p_distance_ratio integer) ;	

-- ICI 11 juillet
update  navigoviz.routepaths r set m_offset = 10000, p_distance_ratio = 50000,
                    pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
                    from (
                        select ports.points_on_path( 'A0614744','A0187101', 10000, 50000) as c
                    ) as calcul 
where  r.to_uhgs_id = 'A0187101' and r.from_uhgs_id = 'A0614744';


-------------------------------
-- cas très long : A0152606	Boulogne-sur-Mer --> Marseille
-------------------------------

select * from navigoviz.routepaths r where r.to_toponyme_fr = 'Marseille' ;--A0210797
-- A0152606	Boulogne-sur-Mer
update  navigoviz.routepaths r set m_offset = 20000, p_distance_ratio = 100000,
                    pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
                    from (
                        select ports.points_on_path( 'A0152606','A0210797', 10000.000000, 50000) as c
                    ) as calcul 
where   r.from_uhgs_id = 'A0152606' and r.to_uhgs_id = 'A0210797' ;

select row_number() over () as id, nbpoints, elapseTime, st_makeline(pointpath) as geom  
from  navigoviz.routepaths r where  from_uhgs_id = 'A0152606' and to_uhgs_id = 'A0210797'  ;

select s.id, ST_Intersects (st_makeline(pointpath),s.polygone3857 ) 
		from navigoviz.shoreline s  , navigoviz.routepaths r
		where from_uhgs_id = 'A0152606' and to_uhgs_id = 'A0210797'  and ST_Intersects(st_makeline(r.pointpath),s.polygone3857 ) ;

	
	-------------------------------
select ports.points_on_path('A0207141', 'A0185385', 10000.000000, 50000) 


------------------------------------------
-- DEBUG:BuildNavigoviz:Marseille -> Sète
	
update  navigoviz.routepaths r set m_offset = 10000, p_distance_ratio = 50000,
        -- pointpath =c , elapseTime=null ,nbPoints = array_length(c, 1)
		pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
        from (
            select ports.points_on_path('A0210797', 'A0170986', 10000, 50000) as c 
        ) as calcul
where  r.from_uhgs_id = 'A0210797' and r.to_uhgs_id = 'A0170986' or (r.to_uhgs_id = 'A0210797' and r.from_uhgs_id = 'A0170986');

----------------------------------------------------------
-- DEBUG:BuildNavigoviz:La Pria / Pietra Ligure -> Divers ports en Europe et en Méditerranée
update  navigoviz.routepaths r set m_offset = 10000, p_distance_ratio = 50000,
                     -- pointpath =c , elapseTime=null ,nbPoints = array_length(c, 1)
	pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
                    from (
                        select ports.points_on_path('A0238624', 'A9999997', 10000.000000, 50000) as c 
                    ) as calcul
                    where  r.from_uhgs_id = 'A0238624' and r.to_uhgs_id = 'A9999997' or (r.to_uhgs_id = 'A0238624' and r.from_uhgs_id = 'A9999997');

select st_astext(st_makeline(pointpath))  from navigoviz.routepaths r 
where  r.from_uhgs_id = 'A0238624' and r.to_uhgs_id = 'A9999997'

select * from ports.port_points pp where uhgs_id in ('A9999997', 'A0170986')




-------------------------------------------------------
-- DEBUG:BuildNavigoviz:Dunkerque -> Setúbal 
update  navigoviz.routepaths r set m_offset = 10000.000000, p_distance_ratio = 50000,
    pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
    from (
        select ports.points_on_path('A0204180', 'A0364551', 10000.000000, 50000) as c
    ) as calcul
    where  r.from_uhgs_id = 'A0204180' and r.to_uhgs_id = 'A0364551' or (r.to_uhgs_id = 'A0204180' and r.from_uhgs_id = 'A0364551');
-------------------------------------------------------





/*
select ports.points_on_path('B2009606', 'A0354220', 10000.000000, 50000)
update  navigoviz.routepaths r set m_offset = 10000.000000, p_distance_ratio = 50000,
                    pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
                    from (
                        select ports.portic_points_on_path('A1965253', 'A0210797', 20000, 50000) as c 
                    ) as calcul
                    where  r.from_uhgs_id = 'A1965253' and r.to_uhgs_id = 'A0210797' or (r.to_uhgs_id = 'A1965253' and r.from_uhgs_id = 'A0210797');

                   

select * from navigoviz.routepaths r where 
r.from_uhgs_id='A0046492' AND r.to_uhgs_id='A0198999' 
or (r.from_uhgs_id='A0198999' AND r.to_uhgs_id='A0046492')
SELECT r.* FROM navigoviz.routepaths r, navigoviz.routepaths r2
WHERE r.from_uhgs_id=r2.to_uhgs_id AND r.to_uhgs_id=r2.from_uhgs_id;
-- 3434

-- marche pas
update  navigoviz.routepaths r set m_offset = 20000, p_distance_ratio = 50000,
 	pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
from ports.portic_points_on_path(from_uhgs_id, to_uhgs_id, 20000, 50000) as c 
where  r.from_uhgs_id = 'A0007338' and r.to_uhgs_id = 'A0210797'
	

select * from ports.port_points pp where pp.uhgs_id = 'A0103067'
--(A0007338,Bizerte ,A0026844,Tunis)
-- (A0007338,Bizerte ,A0210797,Marseille)
select distinct(bt.departure_uhgs_id, bt.departure_fr , bt.destination_uhgs_id, bt.destination_fr) 
from navigoviz.built_travels bt , navigoviz.built_travels bt2
where bt.source_entry != 'both-to' and bt.pointpath is null and bt2.source_entry != 'both-to' and bt2.pointpath is null
and bt.departure_uhgs_id = bt2.destination_uhgs_id and bt.destination_uhgs_id =bt2.departure_uhgs_id 

union
select distinct(destination_uhgs_id, destination_fr , departure_uhgs_id, departure_fr) 
from navigoviz.built_travels bt 
where source_entry != 'both-to' and pointpath is null

select distinct(departure_uhgs_id, departure_fr , destination_uhgs_id, destination_fr) 
from navigoviz.built_travels bt 
where source_entry != 'both-to' and pointpath is null
 and departure_uhgs_id='A0025828' and destination_uhgs_id in ('A0019937', 'A0026844', 'A0210797', 'A1549874')
-- and destination_uhgs_id='A0025828' 
-- and departure_uhgs_id='A0249463'

(A0019937,A0025828)
(A0026844,A0025828)
(A0210797,A0025828)
(A1549874,A0025828)


select  depart_pointcall_uhgs_id, departure, arrivee_pointcall_uhgs_id, destination, pointpath , array_length(pointpath, 1)
from navigoviz.uncertainity_travels where pointpath is not null --45

insert into navigoviz.routepaths (from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr, m_offset, p_distance_ratio, pointpath, nbPoints) 
select  depart_pointcall_uhgs_id, departure, arrivee_pointcall_uhgs_id, destination, 10000, 50000, pointpath , array_length(pointpath, 1)
from navigoviz.uncertainity_travels where pointpath is not null;
    */

---------------------------------------------------------------------------------------
-- BILAN global
---------------------------------------------------------------------------------------


select count(*), avg(elapsetime), min(elapsetime), max(elapsetime), stddev(elapsetime)  
from navigoviz.routepaths 
WHERE pointpath IS not null;

'A0180923', 'B2048070',
-- 1351.2070498537348	330.9028148651123	2172325.991153717	28119.37554754068
-- 2644	29105.976821074146	329.61201667785645	2172325.991153717	80446.42495036074
-- 128	270392.8561862558	2.3899078369140625	8964804.63385582	1013483.6610238234
-- 169	289185.8257118767	2.3899078369140625	8964804.63385582	946352.2398432613
-- 244	415685.50811146124	2.3899078369140625	16993010.54906845	1786081.8420317257
-- après refonte algo et adaptation distances et tampon
-- 286	176424.13134841653	139.9221420288086	7550033.018112183	660668.6882319736
-- 330	198571.49809490552	139.9221420288086	7550033.018112183	661947.4742002309
-- 461	164043.3035941548	139.9221420288086	7550033.018112183	572321.315683029
-- 562	162292.1908729017	139.9221420288086	7550033.018112183	547202.2069623098
-- 621 167 
-- 621	167477.17245803747	139.9221420288086	7550033.018112183	548365.0126363937

-- Tester des intersections sur les chemins
select * from navigoviz.routepaths 
where ST_IsSimple(st_makeline(pointpath)) is false; -- 136 true, 16 false
--196 true / 30 false
-- 27 true


--select from_uhgs_id, to_uhgs_id, from_toponyme_fr, to_toponyme_fr from navigoviz.routepaths where pointpath is null
-- 6669

select * from navigoviz.routepaths 
WHERE pointpath IS NOT null and array_length(pointpath, 1)!= nbpoints 
-- 108
-- A0912818	Bergue 	A0354220	Détroit de Gibraltar

update  navigoviz.routepaths set m_offset = null, p_distance_ratio = null , 
pointpath = null, elapsetime = null, redonepath = null ;
-- where nbpoints = 71;
-- 3336
-- 6797 - fait sur le server le 18 juillet

-- (from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr, m_offset, p_distance_ratio, pointpath, nbPoints)
-- select  departure_uhgs_id, departure, destination_uhgs_id, destination, 10000, 50000, pointpath , array_length(pointpath, 1)
update navigoviz.routepaths set  m_offset = 10000, p_distance_ratio=50000, pointpath=t.pointpath, nbPoints=array_length(t.pointpath, 1)
from navigoviz.built_travels t where pointpath is not null
and departure_uhgs_id = from_uhgs_id and to_uhgs_id = destination_uhgs_id;
-- update  navigoviz.routepaths set nbpoints = array_length(pointpath, 1)
-- where pointpath IS NOT null and nbpoints != array_length(pointpath, 1);

-- diagnostique
select from_toponyme_fr, to_toponyme_fr,   ports.portic_test_pathlength(from_uhgs_id, to_uhgs_id, 30000, 50000)
from  navigoviz.routepaths r ;

select nbpoints, elapsetime , direct_length, broader_length, st_length(st_makeline(pointpath))
-- ,  ports.portic_test_pathlength(from_uhgs_id, to_uhgs_id, 30000, 50000)
from navigoviz.routepaths 
WHERE pointpath  IS not null;

--  p1 A0868259 p2 A0162516
select from_toponyme_fr, to_toponyme_fr, nbpoints, elapsetime, round(st_length(st_makeline(pointpath))/1000), ports.portic_test_pathlength(from_uhgs_id, to_uhgs_id, 30000, 50000) as broader_length 
from navigoviz.routepaths 
WHERE -- from_uhgs_id = 'A0189462' and to_uhgs_id = 'A0187995 '
-- from_uhgs_id = 'A0205141' and to_uhgs_id = 'A1964988 '
-- from_uhgs_id = 'A0868259' and to_uhgs_id = 'A0162516 '
-- from_uhgs_id = 'A0189462' and to_uhgs_id = 'A0187995 '
 pointpath  IS not null and broader_length is null limit 3;

select from_toponyme_fr, to_toponyme_fr, nbpoints, elapsetime, round(st_length(st_makeline(pointpath))/1000), direct_length , broader_length 
from navigoviz.routepaths 
WHERE 
-- from_uhgs_id = 'A0189462' and to_uhgs_id = 'A0187995'
-- from_uhgs_id = 'A0205141' and to_uhgs_id = 'A1964988'
-- from_uhgs_id = 'A0868259' and to_uhgs_id = 'A0162516'
-- from_uhgs_id = 'A0189462' and to_uhgs_id = 'A0187995'
from_uhgs_id = 'A0078410' and to_uhgs_id = 'A0104036'

--- p1 A0078410 p2 A0104036

/*
 * Gravelines	Bayonne	7	20001.27100944519	1911.0	2336.224184964968
Saint Malo	Saint Brieux	3	1255.336046218872	118.0	121.2376971330883
Bayonne	Dunkerque	17	38386.76190376282	2042.0	2372.5546499126385

Le Havre	Bordeaux	10	323509.7830295563	1435.0		1769.0947522724634
Saint Brieux	Côte de Bretagne	23	65758.30793380737	3961.0		5112.546966616676
Saint Malo	Sables d' Olonne	8	10752.661943435669	901.0		1017.9495423453875

 */
select nbpoints, elapsetime , m_offset , p_distance_ratio ,  st_length(st_makeline(pointpath)) as path_length, direct_length, broader_length
--, st_length(st_makeline(pointpath))
-- ,  ports.portic_test_pathlength(from_uhgs_id, to_uhgs_id, 30000, 50000)
from navigoviz.routepaths 
WHERE from_uhgs_id = 'A0205141' and to_uhgs_id = 'A1964988 '

-- p1 A0205141 p2 A1964988

update navigoviz.routepaths set direct_length=st_length(st_makeline(p1.point3857 ,p2.point3857)),  broader_length = st_length(st_makeline(p1.point3857 ,p2.point3857)) 
from ports.port_points p1, ports.port_points p2 
where  from_uhgs_id = 'A0189462' and to_uhgs_id = 'A0187995 '
and p1.uhgs_id = from_uhgs_id and p2.uhgs_id = to_uhgs_id;
	
select departure_uhgs_id, to_uhgs_id
from navigoviz.built_travels t where pointpath is not null

-- a faire en local sur ma machine
update  navigoviz.routepaths r set m_offset = 30000.000000, p_distance_ratio = 50000,
pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
from (
    select departure_uhgs_id, to_uhgs_id, ports.points_on_path(departure_uhgs_id, to_uhgs_id, 30000.000000, 50000) as c
	from navigoviz.built_travels t where pointpath is not null
) as calcul
where  r.from_uhgs_id = calcul.departure_uhgs_id and r.to_uhgs_id = calcul.to_uhgs_id
or (r.to_uhgs_id = calcul.departure_uhgs_id and r.from_uhgs_id = calcul.to_uhgs_id);

------------------------------------------
-- CAS TESTS
------------------------------------------
--- Bordeaux -> Gèfle 
drop type ports.portic_computedPath cascade;
CREATE TYPE ports.portic_computedPath AS
(
    pointslist geometry[]      ,
    nbPoints  int,
    duration float,
    tampon float,
    distance float
);

update  navigoviz.routepaths r set m_offset = (c).tampon, p_distance_ratio = (c).distance,
pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
from (
    select ports.points_on_path('A0180923', 'A1064986', 30000, 50000) as c
) as calcul
where  r.from_uhgs_id = 'A0180923' and r.to_uhgs_id = 'A1064986' or (r.to_uhgs_id = 'A0180923' and r.from_uhgs_id = 'A1064986');
-- long 11 min
-- 2022-07-20 04:36:36.418391+02 DURATION 675063.6038780212 NB POINTS 27
 
select row_number() over () as id, r.from_toponyme_fr , r.to_toponyme_fr ,m_offset, p_distance_ratio, nbpoints, elapseTime, st_length(st_makeline(pointpath)), st_makeline(pointpath) as geom  
from  navigoviz.routepaths r where  from_uhgs_id = 'A0180923' and to_uhgs_id = 'A1064986'  ;


select ports.points_on_path('A0180923', 'A0128573', 30000, 50000) as c 

-- Bordeaux --> Oléron , 159 km
update  navigoviz.routepaths r set m_offset = (c).tampon, p_distance_ratio = (c).distance,
pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
from (
    select ports.points_on_path('A0180923', 'A0128573', 30000, 50000) as c
) as calcul
where  r.from_uhgs_id = 'A0180923' and r.to_uhgs_id = 'A0128573' or (r.to_uhgs_id = 'A0180923' and r.from_uhgs_id = 'A0128573');
-- 2022-07-20 03:13:34.904652+02 DURATION 229263.82899284363 NB POINTS 8
-- 2022-07-20 03:31:01.031465+02 DURATION 288893.728017807 NB POINTS 9 (30/50)
-- 2022-07-20 03:47:26.251438+02 DURATION 209599.19095039368 NB POINTS 6 -- 20/20
-- 2022-07-20 04:32:08.352134+02 DURATION 30270.074129104614 NB POINTS 10

-----------------------------------------
-- Dunkerque --> Kinsale
-----------------------------------------

update  navigoviz.routepaths r set m_offset = 30000, p_distance_ratio = 50000,
    pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
    from (
        select ports.points_on_path('A0204180', 'A0614744', 30000, 50000) as c
    ) as calcul
    where  r.from_uhgs_id = 'A0204180' and r.to_uhgs_id = 'A0614744' or (r.to_uhgs_id = 'A0204180' and r.from_uhgs_id = 'A0614744');


------------------------------------------------
--  trajet delicat: Honfleur, La Tremblade
------------------------------------------------
SELECT uhgs_id FROM ports.port_points WHERE toponyme = 'Honfleur';--A0187836
SELECT uhgs_id FROM ports.port_points WHERE toponyme = 'La Tremblade'; -- A0186515

select * from navigoviz.routepaths r where 
r.from_uhgs_id = 'A0187836' and r.to_uhgs_id = 'A0186515' or (r.to_uhgs_id = 'A0187836' and r.from_uhgs_id = 'A0186515');

update  navigoviz.routepaths r set m_offset = 30000, p_distance_ratio = 50000,
    pointpath  =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
    from (
        select ports.points_on_path('A0187836', 'A0186515', 30000, 50000) as c
    ) as calcul
    where  r.from_uhgs_id = 'A0187836' and r.to_uhgs_id = 'A0186515' or (r.to_uhgs_id = 'A0187836' and r.from_uhgs_id = 'A0186515');

select row_number() over () as id, r.from_toponyme_fr , r.to_toponyme_fr , nbpoints, elapseTime, st_length(st_makeline(pointpath)), st_makeline(pointpath) as geom  
from  navigoviz.routepaths r where  from_uhgs_id = 'A0187836' and to_uhgs_id = 'A0186515'  ;
-- OK le 13 juillet
 
select * from navigoviz.routepaths 
where ST_IsSimple(st_makeline(pointpath)) is false; 

update navigoviz.routepaths set redonepath = c
from (select ports.simplifypathbetter(pointpath) as c 
	from navigoviz.routepaths 
	where from_uhgs_id = 'A0187836' and to_uhgs_id = 'A0186515' ) as calcul
where from_uhgs_id = 'A0187836' and to_uhgs_id = 'A0186515';

select array_length(redonepath, 1) from navigoviz.routepaths 
where from_uhgs_id = 'A0187836' and to_uhgs_id = 'A0186515'
-- 6

update navigoviz.routepaths set redonepath = c
from (select ports.portic_redo_points_on_path(redonepath, 5000, 50000) as c 
	from navigoviz.routepaths 
	where from_uhgs_id = 'A0187836' and to_uhgs_id = 'A0186515' ) as calcul
where from_uhgs_id = 'A0187836' and to_uhgs_id = 'A0186515';

select row_number() over () as id, st_asewkt(geom) from
(select unnest(redonepath) as geom from navigoviz.routepaths
where from_uhgs_id = 'A0187836' and to_uhgs_id = 'A0186515') as k;


-- BOUCLE iter 4 subpoints_on_path 
-- p1 SRID=3857;POINT(-403686.1540057346 6248648.276503776) 
-- p2 SRID=3857;POINT(-128333.59511980404 5750793.466706636)  nbPointsPart 2
select st_setsrid(st_makepoint(-403686.1540057346, 6248648.276503776), 3857)
select ports.order_intersection(st_setsrid(st_makepoint(-403686.1540057346, 6248648.276503776), 3857) , 
st_makeline(st_setsrid(st_makepoint(-403686.1540057346, 6248648.276503776), 3857), st_setsrid(st_makepoint(-128333.59511980404, 5750793.466706636), 3857))) 

select st_makeline(st_setsrid(st_makepoint(-403686.1540057346, 6248648.276503776), 3857), st_setsrid(st_makepoint(-128333.59511980404, 5750793.466706636), 3857))

	select s.id
	from navigoviz.shoreline s 
	where  shoreline is not null and s.area > 1 and st_intersects(st_makeline(st_setsrid(st_makepoint(-403686.1540057346, 6248648.276503776), 3857), st_setsrid(st_makepoint(-128333.59511980404, 5750793.466706636), 3857)), shoreline)
	order by st_distance(st_setsrid(st_makepoint(-403686.1540057346, 6248648.276503776), 3857),ST_ClosestPoint (shoreline, st_setsrid(st_makepoint(-403686.1540057346, 6248648.276503776), 3857)))
	asc limit 1;

          
------------------------------------------------
-- marseille_st_nazaire2
------------------------------------------------
create schema paths

create table paths.routepaths (
	from_uhgs_id text,
	from_toponyme_fr text, 
	to_uhgs_id text,
	to_toponyme_fr text, 
	m_offset double precision, 
	p_distance_ratio integer,
	pointpath geometry[],
	elapseTime float,
	nbPoints int,
	redonepath geometry[]
);
-- truncate navigoviz.routepaths;

/*
CREATE TYPE ports.portic_computedPath AS
(
    pointslist geometry[]      ,
    nbPoints  int,
    duration float
);

drop type ports.portic_computedPath cascade;
CREATE TYPE ports.portic_computedPath AS
(
    pointslist geometry[]      ,
    nbPoints  int,
    duration float,
    tampon float,
    distance float
);
*/


insert into  paths.routepaths (from_uhgs_id, to_uhgs_id, m_offset, p_distance_ratio, pointpath, elapseTime, nbPoints, from_toponyme_fr,to_toponyme_fr ) 
	select 'A0210797',  'A0195511', 30000, 50000,(c).pointslist, (c).duration, (c).nbPoints, 'Marseille', 'St nazaire'
    from (
        select ports.points_on_path('A0210797', 'A0195511', 30000, 50000) as c
    ) as calcul
    
truncate paths.routepaths;


------------------------------------------------
------------------------------------------------
    
--  trajet : st nazaire, La Rochelle
-- 'A0198999', 'A0195511'
select * from  navigoviz.routepaths r
 where  r.from_uhgs_id = 'A0198999' and r.to_uhgs_id = 'A0195511' or (r.to_uhgs_id = 'A0198999' and r.from_uhgs_id = 'A0195511');

insert into  paths.routepaths (from_uhgs_id, to_uhgs_id, m_offset, p_distance_ratio, pointpath, elapseTime, nbPoints, from_toponyme_fr,to_toponyme_fr ) 
	select 'A0198999',  'A0195511', 30000, 50000,(c).pointslist, (c).duration, (c).nbPoints, 'La Rochelle', 'St nazaire'
    from (
        select ports.points_on_path('A0198999', 'A0195511', 30000, 50000) as c
    ) as calcul
 
truncate paths.routepaths;


-- grand trajet : st nazaire, Pontevedra
-- 'A0103067', 'A0195511'
select * from  navigoviz.routepaths r
 where  r.from_uhgs_id = 'A0103067' and r.to_uhgs_id = 'A0195511' or (r.to_uhgs_id = 'A0103067' and r.from_uhgs_id = 'A0195511');

insert into  paths.routepaths (from_uhgs_id, to_uhgs_id, m_offset, p_distance_ratio, pointpath, elapseTime, nbPoints, from_toponyme_fr,to_toponyme_fr ) 
	select   'A0195511','A0103067', 30000, 50000,(c).pointslist, (c).duration, (c).nbPoints, 'St nazaire', 'Pontevedra'
    from (
        select ports.points_on_path('A0195511', 'A0103067', 30000, 50000) as c
    ) as calcul; 
   
--  SUPER grand trajet : Amsterdam / St nazaire
-- 'A0620777', 'A0195511'
 select * from  navigoviz.routepaths r
 where  r.from_uhgs_id = 'A0620777' and r.to_uhgs_id = 'A0195511' or (r.to_uhgs_id = 'A0620777' and r.from_uhgs_id = 'A0195511');
 
insert into  paths.routepaths (from_uhgs_id, to_uhgs_id, m_offset, p_distance_ratio, pointpath, elapseTime, nbPoints, from_toponyme_fr,to_toponyme_fr ) 
	select 'A0620777',  'A0195511', 20000, 50000,(c).pointslist, (c).duration, (c).nbPoints, 'Amsterdam', 'St nazaire'
    from (
        select ports.points_on_path('A0620777', 'A0195511', 20000, 50000) as c
    ) as calcul; 

--  TRAJET d'iles kristianstad  A1029831 --> stockholm A1102661
 select * from  navigoviz.routepaths r
 where  r.from_uhgs_id = 'A1029831' and r.to_uhgs_id = 'A1102661' or (r.to_uhgs_id = 'A1029831' and r.from_uhgs_id = 'A1102661');

truncate paths.routepaths;

 insert into  paths.routepaths (from_uhgs_id, to_uhgs_id, m_offset, p_distance_ratio, pointpath, elapseTime, nbPoints, from_toponyme_fr,to_toponyme_fr ) 
	select 'A1029831',  'A1102661', 50000, 50000,(c).pointslist, (c).duration, (c).nbPoints, 'kristianstad', 'Stockholm'
    from (
        select ports.points_on_path('A1029831', 'A1102661', 50000, 50000) as c
    ) as calcul; 

 select 1 as id, from_toponyme_fr,to_toponyme_fr, m_offset, p_distance_ratio, elapseTime, nbPoints , st_makeline(pointpath) as geom
 from  paths.routepaths r
 where  r.from_uhgs_id = 'A1029831' and r.to_uhgs_id = 'A1102661'
 
/*
 * CHANGE ratio : new ratio 0.9933333333333333 
CHANGE OFFSET valeur du ratio -3.0375219569017196 - doit etre dans [0, 1]
CHANGE ratio : new ratio 0.9966666666666667 
INACHEVE Test p1 SRID=3857;POINT(1661361.1904555361 7590467.954584511) p2 SRID=3857;POINT(1842275.5277704424 7927439.963742879) iter2 1
Grande boucle iter2 2 test[1] SRID=3857;POINT(1661361.1904555361 7590467.954584511) test[2] SRID=3857;POINT(2018116.4812949023 8254962.261206355)
Distance k 20 entre test[1] et test[2] 754206.219196531
Identifiant du shoreline à contourner 0-E
cas comme AVANT , f1 0.5756838899478052 f2 0.5645335007435436 f1-f2 = 0.01115038920426159
CAS où test1 devient le début, f1 0 f2 0.5899374044078476 f1-f2 = 0.5899374044078476
Longueur du segment en offset 1254541.0991971574
 */
-- autre SUPER  grand trajet en BALTIQUE : 
-- A1797542 (Kronstadt en Russie) 
-- A1102661 Stockholm
select * from  navigoviz.routepaths r
 where  r.from_uhgs_id = 'A1797542' and r.to_uhgs_id = 'A1102661' 
or (r.to_uhgs_id = 'A1797542' and r.from_uhgs_id = 'A1102661');

insert into  paths.routepaths (from_uhgs_id, to_uhgs_id, m_offset, p_distance_ratio, pointpath, elapseTime, nbPoints, from_toponyme_fr,to_toponyme_fr ) 
	select 'A1797542',  'A1102661', 10000, 50000,(c).pointslist, (c).duration, (c).nbPoints, 'Kronstadt en Russie', 'Stockholm'
from (
    select ports.points_on_path('A1797542', 'A1102661', 10000, 50000) as c
) as calcul; 
   
-- debug sur QGIS
select row_number() over () as id, from_toponyme_fr, to_toponyme_fr, nbpoints, elapseTime,  st_length(st_makeline(pointpath)), st_makeline(pointpath) as geom  
from  paths.routepaths;

----------------------------------- 
-- Dieppe -> Hambourg

 update  navigoviz.routepaths r set m_offset = 50000.000000, p_distance_ratio = 50000,
                    pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
                    from (
                        select ports.points_on_path('A0130761', 'A0743522', 50000.000000, 50000) as c
                    ) as calcul
                    where  r.from_uhgs_id = 'A0130761' and r.to_uhgs_id = 'A0743522' or (r.to_uhgs_id = 'A0130761' and r.from_uhgs_id = 'A0743522');

select row_number() over () as id, nbpoints, elapseTime, st_makeline(pointpath) as geom  
from  navigoviz.routepaths r where  from_uhgs_id = 'A0130761' and to_uhgs_id = 'A0743522'  ;

--maxiter, iter, previousPoint, pointp1, pointp1prime, shoreid, longueur_segment, newoffset, distance_ratio, ratio
drop table navigoviz.temp_points;
truncate table navigoviz.temp_points;
create table navigoviz.temp_points (
	iter2 int,
	maxiter int,
	iter int, 
	previousPoint geometry, 
	pointp1 geometry, 
	pointp1prime geometry, 
	shoreid text, 
	segment geometry,
	longueur_segment double precision, 
	f1 double precision,
	f2 double precision,
	newoffset double precision, 
	distance_ratio double precision, 
	ratio double precision);

select row_number() over () as id, iter2, iter, shoreid, f1, f2, pointp1 as geom, segment as geom2,  longueur_segment, newoffset, distance_ratio, ratio
from navigoviz.temp_points
order by iter2, iter
--where shoreid = '2188' and iter = 0;

select max(iter) from navigoviz.temp_points where iter2 = 0
select distinct shoreid, segment,longueur_segment, newoffset, distance_ratio
from  navigoviz.temp_points
select row_number() over () as id, iter, shoreid, pointp1 as geom, longueur_segment, newoffset, distance_ratio, ratio
from navigoviz.temp_points where shoreid = '2188' and iter = 0;

select row_number() over () as id, iter2, iter, shoreid, f1, f2, pointp1prime as geom, segment as geom2,  longueur_segment, newoffset, distance_ratio, ratio
from navigoviz.temp_points

select distinct iter2, shoreid, segment as geom,  longueur_segment, newoffset, distance_ratio
from navigoviz.temp_points


select row_number() over () as id, nbpoints, elapseTime, st_makeline(pointpath) as geom  
from  navigoviz.routepaths r where  from_uhgs_id = 'A0130761' and to_uhgs_id = 'A0743522'  ;

----------------
-- DEBUG:BuildNavigoviz:Nantes -> Halifax
update  navigoviz.routepaths r set m_offset = 50000, p_distance_ratio = 50000,
pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
from (
    select ports.points_on_path('A0124817', 'B2120163', 50000, 50000) as c
) as calcul
where  r.from_uhgs_id = 'A0124817' and r.to_uhgs_id = 'B2120163' or (r.to_uhgs_id = 'A0124817' and r.from_uhgs_id = 'B2120163');


-- DEBUG:BuildNavigoviz:Constantinople -> Saint-Tropez

update  navigoviz.routepaths r set m_offset = 50000.000000, p_distance_ratio = 50000,
redonepath  =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
from (
    select ports.points_on_path('A1465893', 'A0147257', 50000.000000, 50000) as c
) as calcul
where  r.from_uhgs_id = 'A1465893' and r.to_uhgs_id = 'A0147257' or (r.to_uhgs_id = 'A1465893' and r.from_uhgs_id = 'A0147257');

select row_number() over () as id, from_toponyme_fr , to_toponyme_fr , nbpoints, elapseTime, st_makeline(pointpath) as geom  
from  navigoviz.routepaths r where  from_uhgs_id = 'A1465893' and to_uhgs_id = 'A0147257'  ;
                   
------------------------------------------------
-- trajet très délicat avec pleins d'iles kristianstad  A1029831 --> stockholm A1102661
------------------------------------------------
update  navigoviz.routepaths r set m_offset = 50000, p_distance_ratio = 50000,
pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
from (
    select ports.points_on_path('A0124817', 'B2120163', 50000, 50000) as c
) as calcul
where  r.from_uhgs_id = 'A0124817' and r.to_uhgs_id = 'B2120163' or (r.to_uhgs_id = 'A0124817' and r.from_uhgs_id = 'B2120163');

------------------------------------------------
-- DEBUG:BuildNavigoviz:Bergue  -> Détroit de Gibraltar
------------------------------------------------

update  navigoviz.routepaths r set m_offset = 30000.000000, p_distance_ratio = 50000,
pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
from (
    select ports.points_on_path('A0912818', 'A0354220', 30000.000000, 50000) as c
) as calcul
where  r.from_uhgs_id = 'A0912818' and r.to_uhgs_id = 'A0354220' or (r.to_uhgs_id = 'A0912818' and r.from_uhgs_id = 'A0354220');

-- 27 min, 13 pts

------------------------------------------
-- autre SUPER SUPER grand trajet : A1797542 (Kronstadt en Russie) - Sulina (mer noire - A1220285)
------------------------------------------
truncate paths.routepaths;
insert into  paths.routepaths (from_uhgs_id, to_uhgs_id, m_offset, p_distance_ratio, pointpath, elapseTime, nbPoints, from_toponyme_fr,to_toponyme_fr ) 
	select 'A1797542',  'A1220285', 30000, 50000,(c).pointslist, (c).duration, (c).nbPoints, 'Kronstadt en Russie', 'Sulina'
from (
    select ports.points_on_path('A1797542', 'A1220285', 30000, 50000) as c
) as calcul; 
-- 27 min. OK
   
-- debug sur QGIS
select row_number() over () as id, from_toponyme_fr, to_toponyme_fr, nbpoints, elapseTime,  st_length(st_makeline(pointpath)), st_makeline(pointpath) as geom  
from  paths.routepaths;
         
---------------------------------------------------------------------------------------
-- cas des ports sur fleuves
---------------------------------------------------------------------------------------

--- DEBUG Libourne--> Bordeaux p1.uhgs_id = 'A0215306' and p2.uhgs_id = 'A0180923' ;

select ports.points_on_path('A0215306', 'A0180923', 10000.000000, 50000)

update  navigoviz.routepaths r set m_offset = 10000, p_distance_ratio = 50000,
         -- pointpath =c , elapseTime=null ,nbPoints = array_length(c, 1)
	pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
        from (
            select ports.points_on_path('A0215306', 'A0180923', 10000.000000, 50000) as c 
        ) as calcul
        where  r.from_uhgs_id = 'A0215306' and r.to_uhgs_id = 'A0180923' or (r.to_uhgs_id = 'A0215306' and r.from_uhgs_id = 'A0180923');

-- Attention, on est sur la côte. Il faudrait prendre les projections sur le fleuve
-- Distance au fleuve par rapport à la cote : si plus courte, il faut mettre le port sur le fleuve (port on 1 mile)
-- Découper le fleuve par shoreline depuis la projection du fleuve sur le port
-- Rajouter une colonne segment fluvial dans route_paths.        
select st_length(st_makeline(pointpath)), * from navigoviz.routepaths r 
where  r.from_uhgs_id = 'A0215306' and r.to_uhgs_id = 'A0180923';



--:Libourne -> Bordeaux
	select coalesce(p1.port_on_line_1mile, p1.point3857), coalesce(p2.port_on_line_1mile, p2.point3857), ports.order_intersection(coalesce(p1.port_on_line_1mile, p1.point3857), st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile)) 
	from ports.port_points p1 , ports.port_points p2
	where   p1.uhgs_id = 'A0215306' and p2.uhgs_id = 'A0180923' ;
	
-- ICI
select type_cote, id_shoreline, id_valid_20mile , uhgs_id, toponyme_standard_fr , state_1789_fr , port_on_line_1mile, port_on_line_20mile
from ports.port_points pp 
where point3857 is not null and 
--port_on_shoreline is null  ;
port_on_line_1mile is null ;
-- port_on_line_20mile is NULL -- beaucoup
-- a corriger ce matin 12 juillet


                   

select * from navigoviz.routepaths r where r.from_uhgs_id = 'A0204180' and r.to_uhgs_id = 'A0364551';
-- elapsetime > 2172325
-- A0338382	Salonique 	A0348862	Port Sigre


select row_number() over () as id, from_toponyme_fr, to_toponyme_fr, nbpoints, elapseTime,  st_length(st_makeline(pointpath)), st_makeline(pointpath) as geom  
from  navigoviz.routepaths r 
--where  from_uhgs_id = 'A0152606' and to_uhgs_id = 'A0210797'
--where  to_uhgs_id = 'A0187101' and from_uhgs_id = 'A0614744' ;
-- where  r.to_uhgs_id = 'A0614744' and r.from_uhgs_id = 'A0204180';
-- where  r.from_uhgs_id = 'A0215306' and r.to_uhgs_id = 'A0180923';
where from_uhgs_id = 'A0187836' and to_uhgs_id = 'A0186515' ;

--- Localement le 20 juillet 2022
---------------------------------

update  navigoviz.routepaths r set m_offset= (c).tampon, p_distance_ratio = (c).distance,
pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
from (
    select distinct departure_uhgs_id, destination_uhgs_id, 
    ports.points_on_path(departure_uhgs_id, destination_uhgs_id, 36000, 50000) as c
	from navigoviz.built_travels t where pointpath is not null limit 2
) as calcul
where  r.from_uhgs_id = calcul.departure_uhgs_id and r.to_uhgs_id = calcul.to_uhgs_id
or (r.to_uhgs_id = calcul.departure_uhgs_id and r.from_uhgs_id = calcul.to_uhgs_id);

    select distinct departure_uhgs_id, destination_uhgs_id,
    ports.points_on_path(departure_uhgs_id, destination_uhgs_id, 36000, 50000)
	from navigoviz.built_travels t where pointpath is not null limit 2;

-----------------------
	
select * from navigoviz.routepaths where pointpath is not null;

insert into navigoviz.routepaths (from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr)
select from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr from navigoviz.routepaths_backup20juillet22;
-- 6842

create table navigoviz.routepaths_backup20juillet22 as (select * from navigoviz.routepaths)
truncate table navigoviz.routepaths;

select  * --ports.points_on_path(departure_uhgs_id, destination_uhgs_id, 36000, 50000) as c
from (
    select distinct departure_uhgs_id, destination_uhgs_id 
	from navigoviz.built_travels t 
	where pointpath is not null limit 2) as calcul ,
 navigoviz.routepaths r
 where -- r.pointpath is null and 
 r.from_uhgs_id = calcul.departure_uhgs_id and r.to_uhgs_id = calcul.destination_uhgs_id
or (r.to_uhgs_id = calcul.departure_uhgs_id and r.from_uhgs_id = calcul.destination_uhgs_id);
-- Le Havre A0122218	Rouen A0187101


update  navigoviz.routepaths r set m_offset= (c).tampon, p_distance_ratio = (c).distance,
pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
from (
	select  departure_uhgs_id, destination_uhgs_id, ports.points_on_path(departure_uhgs_id, destination_uhgs_id, 36000, 50000) as c
		from (
		    select distinct calcul.departure_uhgs_id, calcul.destination_uhgs_id 
			from navigoviz.built_travels calcul 
			where calcul.pointpath is not null 
			except 
			(select distinct  r.from_uhgs_id as departure_uhgs_id, r.to_uhgs_id as destination_uhgs_id
			from navigoviz.routepaths r 
			where r.pointpath is not null)
			--limit 25
		) as k
	) as calcul
 where  
 r.from_uhgs_id = calcul.departure_uhgs_id and r.to_uhgs_id = calcul.destination_uhgs_id
or (r.to_uhgs_id = calcul.departure_uhgs_id and r.from_uhgs_id = calcul.destination_uhgs_id);
-- fini le 20 juillet 2022 en local 5435

select distinct departure_uhgs_id, destination_uhgs_id 
			from navigoviz.built_travels calcul
			where calcul.pointpath is not null 
			and r.from_uhgs_id = calcul.departure_uhgs_id and r.to_uhgs_id = calcul.destination_uhgs_id
			or (r.to_uhgs_id = calcul.departure_uhgs_id and r.from_uhgs_id = calcul.destination_uhgs_id)
			
			
	select ports.order_intersection(coalesce(p1.port_on_line_1mile, p1.point3857), st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile)) 
	--into test
	from ports.port_points p1 , ports.port_points p2
	where   p1.uhgs_id = 'A0122218' and p2.uhgs_id = 'A0187101' ;

select 1, coalesce(p1.port_on_line_1mile, p1.point3857) as geom, st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile) as geom2
	--into test
	from ports.port_points p1 , ports.port_points p2
	where   p1.uhgs_id = 'A0122218' and p2.uhgs_id = 'A0187101' ;


select distinct departure_uhgs_id, destination_uhgs_id --, ports.points_on_path(departure_uhgs_id, destination_uhgs_id, 30000.000000, 50000) as c
	from navigoviz.built_travels t where pointpath is not null
except  
(select distinct destination_uhgs_id , departure_uhgs_id --, ports.points_on_path(departure_uhgs_id, destination_uhgs_id, 30000.000000, 50000) as c
	from navigoviz.built_travels t where pointpath is not null)
 	--- 7 :  ceux qui n'ont pas l'aller-retour 


select row_number() over () as id, k.departure_uhgs_id, k.destination_uhgs_id
from (select distinct departure_uhgs_id, destination_uhgs_id --, ports.points_on_path(departure_uhgs_id, destination_uhgs_id, 30000.000000, 50000) as c
	from navigoviz.built_travels t where pointpath is not null ) as k

select departure , destination , departure_out_date ,outdate_fixed , uncertainty_color , style_dashed , travel_uncertainity  
from built_travels bt where bt.ship_id = '0004171N' and source_entry != 'both-to' 
order by travel_rank 


DEBUG:BuildNavigoviz:Dieppe -> Glasgow

update  navigoviz.routepaths r set m_offset = (c).tampon, p_distance_ratio = (c).distance,
pointpath =(c).pointslist , elapseTime=(c).duration ,nbPoints = (c).nbPoints
from (
    select ports.points_on_path('A0130761', 'A0397114', 30000.000000, 50000) as c
) as calcul
where  r.from_uhgs_id = 'A0130761' and r.to_uhgs_id = 'A0397114' or (r.to_uhgs_id = 'A0130761' and r.from_uhgs_id = 'A0397114');


------------------------------------------------------------------------------------------------
-- 30 aout 2022
-- Création d'une linestring en WGS84
-- Report des calculs terminés sur le serveur dans built_travels
------------------------------------------------------------------------------------------------
alter table navigoviz.built_travels add column geom4326 geometry;
 
comment on column navigoviz.built_travels.geom4326 is 'linestring in WGS84 coordinates (long lat points)';

update navigoviz.built_travels t set pointpath = r.pointpath ,  geom = st_makeline(r.pointpath), geom4326 = st_setsrid(st_transform(st_makeline(r.pointpath), 4326), 4326)
            from navigoviz.routepaths r
            where (from_uhgs_id = departure_uhgs_id  and to_uhgs_id=destination_uhgs_id)
            --or (from_uhgs_id = destination_uhgs_id and to_uhgs_id=departure_uhgs_id)
            and r.pointpath is not null
-- 128537 lignes en 1 min 50      , sur le serveur le 30 aout
-- 126576 lignes en 17.816      , sur ma machine le 7 sept

-- pourquoi ce différentiel ?
select travel_id, travel_rank , shipcaptain_travel_rank, departure, destination,  ship_id, ship_name , departure_out_date, destination_in_date  , outdate_fixed , indate_fixed ,
geom4326, style_dashed, uncertainty_color, travel_uncertainity  
from built_travels t, navigoviz.routepaths r 
where (from_uhgs_id = departure_uhgs_id  and to_uhgs_id=destination_uhgs_id)
--(from_uhgs_id = destination_uhgs_id and to_uhgs_id=departure_uhgs_id) 
and r.pointpath is not null
and geom4326 is null 

update navigoviz.built_travels t set pointpath = r.pointpath ,  geom = st_makeline(r.pointpath), geom4326 = st_setsrid(st_transform(st_makeline(r.pointpath), 4326), 4326)
from navigoviz.routepaths r
 where (-- from_uhgs_id = departure_uhgs_id  and to_uhgs_id=destination_uhgs_id)
        (from_uhgs_id = destination_uhgs_id and to_uhgs_id=departure_uhgs_id)
and r.pointpath is not null

select departure, destination,  ship_id, ship_name , array_length(pointpath, 1), departure_out_date, destination_in_date  
from navigoviz.built_travels where source_entry <> 'both-to' and (ship_id = '0007200N' )

------------------------------------------------------------------------------------------------
-- 7 sept 2022 - reverse the order of points when required
------------------------------------------------------------------------------------------------

select shipcaptain_travel_rank , departure_fr , destination_fr , nb_cargo, style_dashed, uncertainty_color, travel_uncertainity ,
st_transform(st_makeline(pointpath), 4326), geom4326, 
st_distance(st_setsrid(st_transform(st_setsrid(st_makepoint(departure_longitude::float, departure_latitude::float), 4326), 3857), 3857),pointpath[0]) as d_depart,
st_distance(st_setsrid(st_transform(st_setsrid(st_makepoint(destination_longitude::float, destination_latitude::float), 4326), 3857), 3857),pointpath[0]) as d_arrivee
from navigoviz.built_travels bt 
where ship_id = '0002931N' and source_entry !='both-to' and shipcaptain_travel_rank = 1
order by shipcaptain_travel_rank;


alter table navigoviz.routepaths  add column reverse_order boolean default false;
comment on column navigoviz.routepaths.reverse_order is 'boolean that values true if the initial pointpath is not in the good order (from departure to destination)';

select from_toponyme_fr , to_toponyme_fr , m_offset , nbpoints , broader_length , direct_length , 
st_distance(p1.point3857,pointpath[0]) as d_depart,
st_distance(p2.point3857,pointpath[0]) as d_arrivee,
reverse_order
from navigoviz.routepaths r , ports.port_points p1, ports.port_points p2
where r.pointpath is not null and array_length(pointpath, 1)> 1 and not st_equals(pointpath[0], pointpath[1])
and p1.uhgs_id = r.from_uhgs_id and p2.uhgs_id =r.to_uhgs_id and reverse_order is true

update navigoviz.routepaths r 
set reverse_order = true 
from ports.port_points p1, ports.port_points p2
where r.pointpath is not null and array_length(pointpath, 1)> 1 and not st_equals(pointpath[0], pointpath[1])
and p1.uhgs_id = r.from_uhgs_id and p2.uhgs_id =r.to_uhgs_id 
and st_distance(p1.point3857,pointpath[0]) > st_distance(p2.point3857,pointpath[0])
-- 1722 à inverser

-- https://wiki.postgresql.org/wiki/Array_reverse
-- drop FUNCTION ports.portic_array_reverse(anyarray);
CREATE OR REPLACE FUNCTION ports.portic_array_reverse(anyarray) RETURNS geometry[] AS $BODY$
declare pointslist geometry[];
declare indexi int;
declare indexj int;
begin
    raise notice 'taille du tableau de points en entrée : %', array_length($1, 1);
	indexi:=array_length($1, 1)-1;
	indexj:=0;
	while indexi >= 0 loop
		pointslist[indexj] := $1[indexi];
		indexi:= indexi-1;
		indexj:=indexj+1;
	end loop;
	/*
    SELECT $1[i] into pointslist[i]
    FROM generate_subscripts($1,1) AS s(i)
    ORDER BY i desc;
    */
   	raise notice 'taille du tableau de points en sortie: %', array_length(pointslist, 1);
    return pointslist;
end
$BODY$ LANGUAGE 'plpgsql';

alter table navigoviz.routepaths add column reverse_pointpath geometry[];
update navigoviz.routepaths r set reverse_pointpath = ports.portic_array_reverse(pointpath)
where reverse_order = true ;
-- 1722

select * from navigoviz.routepaths where reverse_order = true;

update navigoviz.built_travels t set pointpath = r.reverse_pointpath ,  geom = st_makeline(r.reverse_pointpath), geom4326 = st_setsrid(st_transform(st_makeline(r.reverse_pointpath), 4326), 4326)
            from navigoviz.routepaths r
            where (from_uhgs_id = departure_uhgs_id  and to_uhgs_id=destination_uhgs_id)
            and r.pointpath is not null and r.reverse_order is true
-- 53604 lignes en 6s      , sur ma machine le 7 sept

-- controle de la correction (sens des flêches)
            
select shipcaptain_travel_rank , departure_fr , destination_fr , nb_cargo, style_dashed, uncertainty_color, travel_uncertainity ,
st_transform(st_makeline(pointpath), 4326), geom4326, 
st_distance(st_setsrid(st_transform(st_setsrid(st_makepoint(departure_longitude::float, departure_latitude::float), 4326), 3857), 3857),pointpath[0]) as d_depart,
st_distance(st_setsrid(st_transform(st_setsrid(st_makepoint(destination_longitude::float, destination_latitude::float), 4326), 3857), 3857),pointpath[0]) as d_arrivee
from navigoviz.built_travels bt 
where ship_id = '0002931N' and source_entry !='both-to' and shipcaptain_travel_rank = 1
order by shipcaptain_travel_rank;
-- LINESTRING (-1.796853534479123 46.484858342122145, -1.5387369833091693 43.53346110603902)
-- ok.

-- j'applique sur le serveur
-- fait le 7 sept. 2022

select from_toponyme_fr , to_toponyme_fr , m_offset , nbpoints , broader_length , direct_length , 
st_distance(p1.point3857,reverse_pointpath[0]) as d_depart,
st_distance(p2.point3857,reverse_pointpath[0]) as d_arrivee,
reverse_order
from navigoviz.routepaths r , ports.port_points p1, ports.port_points p2
where r.pointpath is not null and array_length(pointpath, 1)> 1 --and not st_equals(reverse_pointpath[0], reverse_pointpath[1])
and p1.uhgs_id = r.from_uhgs_id and p2.uhgs_id =r.to_uhgs_id and reverse_order is true


-- LINESTRING (-1.5387369833091693 43.53346110603902, -1.796853534479123 46.484858342122145)
-- LINESTRING (-1.5387369833091693 43.53346110603902, -1.796853534479123 46.484858342122145)

-- sur le serveur
-- LINESTRING (-1.5387369833091693 43.53346110603902, -1.796853534479123 46.484858342122145)
-- LINESTRING (-171291.41744675464 5393525.801201109, -200024.82048830984 5858391.488546229)

                   
------------------------------------------------------------------------------------------------
-- 2 nov 2022 - fix the style_dashed = true for Marseille for instance
------------------------------------------------------------------------------------------------

-- tous les trajets arrivants à Marseille sont direct
update navigoviz.uncertainity_travels set direct = true 
where  source_depart in ('Registre du petit cabotage (1786-1787)', 'la Santé registre de patentes de Marseille')
and departure_navstatus = 'PC-RS';

-- en prod, sur travels, le 23 nov : 
-- attention, source_suite = Santé Marseille et pas la Santé registre de patentes de Marseille comme dans Pointcall
update navigoviz.built_travels  set style_dashed = false 
where  source_suite in ('Registre du petit cabotage (1786-1787)', 'Santé Marseille')
and departure_navstatus = 'PC-RS';
-- 11440 Registre du petit cabotage (1786-1787)

-- en prod, sur travels, le 23 nov : 
update navigoviz.built_travels  set style_dashed = false 
where  source_suite in ('Santé Marseille')
and departure_navstatus = 'PC-RS';
-- 16755

-- en prod, sur travels, le 23 nov : 
update navigoviz.built_travels set departure_uncertainity = 0
where departure_uncertainity = -2 and source_suite = 'Santé Marseille' and source_entry != 'both-to'
and departure_navstatus = 'PC-RS'
and position('=' in coalesce( departure_out_date , destination_in_date)) > 0;
-- 3654 / Il en restera 13 avec date pas sure à analyser/comprendre

-- en prod, sur travels, le 23 nov : 
update navigoviz.built_travels set departure_uncertainity = 0
where departure_uncertainity = -2 and source_suite = 'Registre du petit cabotage (1786-1787)' and source_entry != 'both-to'
and departure_navstatus = 'PC-RS';
-- 3034

-- en prod, sur travels, le 23 nov : 
update navigoviz.built_travels 
set uncertainty_color = 'green', travel_uncertainity = 0 
where destination_uncertainity in (0, -1) and departure_uncertainity in (0, -1);
-- -- en prod, sur travels, le 23 nov : 

-----------------------------------------------------------------------
-- Ajouts depuis le 23 nov, - fait le 27 juillet 2023
-----------------------------------------------------------------------

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- CAS TESTS
------------------------------------------------------------------------------
------------------------------------------------------------------------------

---------------------------------------------
-- cas direct
---------------------------------------------
create schema paths;

select row_number() over () as id, (ports.points_on_path('A0123033', 'A0124715', 5000, 10000))

select s.id , p1.id_shoreline, p2.id_shoreline
	from ports.port_points p1, ports.port_points p2, navigoviz.shoreline s 
	where   p1.uhgs_id = 'A0123033'  and p2.uhgs_id = 'A0124715' and shoreline is not null and st_intersects(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline)

select row_number() over () as id, geom
from (select unnest(ports.points_on_path('A0123033', 'A0124715', 5000, 10000)) as geom) as k

---------------------------------------------
-- cas premier normal : même shoreline
---------------------------------------------
create table paths.straight_line as 
(select 'A0195511' as from_uhgs, 'Saint Nazaire' as from_name, 'A0148900' as to_uhgs, 'Mesquer' as to_name,  st_makeline(p1.point3857, p2.point3857)
from ports.port_points p1, ports.port_points p2
where   p1.uhgs_id = 'A0195511'  and p2.uhgs_id = 'A0148900'
);

select ports.portic_points_on_path('A0148900', 'A0195511', 5000.0, 10000).pointslist

select s.id , p1.id_shoreline, p2.id_shoreline
	from ports.port_points p1, ports.port_points p2, navigoviz.shoreline s 
	where   p1.uhgs_id = 'A0195511'  and p2.uhgs_id = 'A0148900' and shoreline is not null and st_intersects(st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile), shoreline)
-- 0-E	0-E	0-E

-- dans un sens 
select row_number() over () as id, geom
from (select unnest((ports.portic_points_on_path('A0148900', 'A0195511', 5000.0, 10000)).pointslist) as geom) as k

-- dans le sens inverse
select row_number() over () as id, geom
from (select unnest(ports.portic_points_on_path('A0195511', 'A0148900', 5000, 10000)) as geom) as k

-- drop table paths.test;
-- truncate table paths.test;
--create table paths.test (idtrack int, id int, point geometry, sens text, from_uhgs text,  from_name text, to_uhgs text, to_name text);

truncate table paths.routepaths;
create table paths.routepaths (idtrack serial, from_uhgs text, from_name text, to_uhgs text, to_name text, m_offset float, p_distance_ratio int, pointpath geometry[], nbPoints int, elapseTime float);

insert into paths.routepaths (from_uhgs, from_name, to_uhgs, to_name, m_offset, p_distance_ratio, pointpath, nbPoints, elapseTime)
	select 'A0195511', 'Saint Nazaire', 'A0148900', 'Mesquer', (c).tampon,(c).distance, (c).pointslist , (c).nbPoints, (c).duration 
    from (
         select ports.portic_points_on_path('A0195511', 'A0148900', 5000, 10000) as c
    ) as calcul;

   /*
insert into  paths.test (idtrack, id, point, sens, from_uhgs, from_name, to_uhgs, to_name) (
	select  1, row_number() over () , geom, 'direct', 'A0195511', 'Saint Nazaire', 'A0148900', 'Mesquer'
	from (select unnest((ports.portic_points_on_path('A0195511', 'A0148900', 5000, 10000)).pointslist) as geom) as k
union (
	select 2, row_number() over () , geom, 'inverse' , 'A0148900', 'Mesquer', 'A0195511', 'Saint Nazaire'
	from (select unnest((ports.portic_points_on_path('A0148900', 'A0195511', 5000, 10000)).pointslist) as geom) as k
)
)
*/
-- voir la route directe
/*
select idtrack as id, st_makeline(point) as geom from
(select idtrack , point from paths.test where sens = 'direct' order by idtrack, id) as k
group by idtrack
*/

select idtrack as id, st_makeline(pointpath) as geom, * from paths.routepaths

-- voir les  points

-- select id , point from paths.test  where idtrack=1

select idtrack, row_number() over (PARTITION BY idtrack),  geom
from (select idtrack, unnest(pointpath) as geom from paths.routepaths  ) as k


------------------------------------------------
-- autre test avec point sur iles différentes (pas le meme shoreline)
-- cas particulier 1
------------------------------------------------
-- from_uhgs, from_name, to_uhgs, to_name
insert into  paths.straight_line  
(select 'A0197884'  , 'Quiberon' , 'A0131657' , 'Belle-Ile-en-mer' ,  st_makeline(p1.point3857, p2.point3857)
from ports.port_points p1, ports.port_points p2
where   p1.uhgs_id = 'A0197884'  and p2.uhgs_id = 'A0131657'
);

/*
insert into  paths.test (idtrack, id, point, sens, from_uhgs, from_name, to_uhgs, to_name) (
	select 3, row_number() over () , geom, 'direct', 'A0197884', 'Quiberon', 'A0131657', 'Belle-Ile-en-mer'
	from (select unnest((ports.portic_points_on_path('A0197884', 'A0131657', 5000, 10000)).pointslist) as geom) as k
union (
	select 4, row_number() over () , geom, 'inverse' , 'A0131657', 'Belle-Ile-en-mer', 'A0197884', 'Quiberon'
	from (select unnest((ports.portic_points_on_path('A0131657', 'A0197884', 5000, 10000)).pointslist) as geom) as k
)
)*/

insert into paths.routepaths (from_uhgs, from_name, to_uhgs, to_name, m_offset, p_distance_ratio, pointpath, nbPoints, elapseTime)
	select 'A0197884', 'Quiberon', 'A0131657', 'Belle-Ile-en-mer', (c).tampon,(c).distance, (c).pointslist , (c).nbPoints, (c).duration 
    from (
         select ports.portic_points_on_path('A0197884', 'A0131657', 5000, 10000) as c
    ) as calcul;


------------------------------------------------
-- cas particulier 2 : une ile entre les 2 ports
------------------------------------------------

insert into  paths.straight_line  
(select 'A0123033'  , 'Rhuys' , 'A0131657' , 'Belle-Ile-en-mer' ,  st_makeline(p1.point3857, p2.point3857)
from ports.port_points p1, ports.port_points p2
where   p1.uhgs_id = 'A0123033'  and p2.uhgs_id = 'A0131657'
);

/*
insert into  paths.test (idtrack, id, point, sens, from_uhgs, from_name, to_uhgs, to_name) (
	select  5, row_number() over () , geom, 'direct', 'A0123033', 'Rhuys', 'A0131657', 'Belle-Ile-en-mer'
	from (select unnest((ports.portic_points_on_path('A0123033', 'A0131657', 5000, 10000)).pointslist) as geom) as k
union (
	select 6, row_number() over () , geom, 'inverse' , 'A0131657', 'Belle-Ile-en-mer', 'A0123033', 'Rhuys'
	from (select unnest((ports.portic_points_on_path('A0131657', 'A0123033', 5000, 10000)).pointslist) as geom) as k
)
)*/


insert into paths.routepaths (from_uhgs, from_name, to_uhgs, to_name, m_offset, p_distance_ratio, pointpath, nbPoints, elapseTime)
	select 'A0123033', 'Rhuys', 'A0131657', 'Belle-Ile-en-mer', (c).tampon,(c).distance, (c).pointslist , (c).nbPoints, (c).duration 
    from (
         select ports.portic_points_on_path('A0197884', 'A0131657', 5000, 10000) as c
    ) as calcul;
   
---------------------------------------------------
-- A0170819 st malo , A0122932 st Brieuc, A1964988 cote de bretagne
------------------------------------------------


insert into  paths.straight_line  
(select 'A0170819'  , p1.toponyme_standard_fr , 'A0122932' , p2.toponyme_standard_fr  ,  st_makeline(p1.point3857, p2.point3857)
from ports.port_points p1, ports.port_points p2
where   p1.uhgs_id = 'A0170819'  and p2.uhgs_id = 'A0122932'
);

/*
insert into  paths.test (idtrack, id, point, sens, from_uhgs, from_name, to_uhgs, to_name) (
	select  7, row_number() over () , geom, 'direct', 'A0170819', 'Saint-Malo', 'A0122932', 'Saint-Brieuc'
	from (select unnest((ports.portic_points_on_path('A0170819', 'A0122932', 5000, 10000)).pointslist) as geom) as k
union (
	select  8, row_number() over () , geom, 'inverse' , 'A0122932', 'Saint-Brieuc', 'A0170819', 'Saint-Malo'
	from (select unnest((ports.portic_points_on_path('A0122932', 'A0170819', 5000, 10000)).pointslist) as geom) as k
)
)*/

insert into paths.routepaths (from_uhgs, from_name, to_uhgs, to_name, m_offset, p_distance_ratio, pointpath, nbPoints, elapseTime)
	select 'A0170819', 'Saint-Malo', 'A0122932', 'Saint-Brieuc', (c).tampon,(c).distance, (c).pointslist , (c).nbPoints, (c).duration 
    from (
         select ports.portic_points_on_path('A0170819', 'A0122932', 5000, 10000) as c
    ) as calcul;
   
insert into  paths.straight_line  
(select 'A0122932'  , p1.toponyme_standard_fr , 'A1964988' , p2.toponyme_standard_fr  ,  st_makeline(p1.point3857, p2.point3857)
from ports.port_points p1, ports.port_points p2
where   p1.uhgs_id = 'A0122932'  and p2.uhgs_id = 'A1964988'
);

/*
insert into  paths.test (idtrack, id, point, sens, from_uhgs, from_name, to_uhgs, to_name) (
	select  7, row_number() over () , geom, 'direct', 'A0122932', 'Saint-Brieuc', 'A1964988', 'Côtes de Bretagne'
	from (select unnest((ports.portic_points_on_path('A0122932', 'A1964988', 5000, 10000)).pointslist) as geom) as k
union (
	select  8, row_number() over () , geom, 'inverse' , 'A1964988', 'Côtes de Bretagne', 'A0122932', 'Saint-Brieuc'
	from (select unnest((ports.portic_points_on_path('A1964988', 'A0122932', 5000, 10000)).pointslist) as geom) as k
)
)
*/

   
delete from paths.test  where from_name ='Côtes de Bretagne' or to_name = 'Côtes de Bretagne';
delete from paths.test  where idtrack in (7, 8);

select idtrack as id, st_makeline(point) as geom from
(select idtrack , point from paths.test where sens = 'direct' and from_name = 'Saint-Brieuc' order by idtrack, id) as k
group by idtrack

 
select idtrack , point from paths.test where sens = 'direct' order by idtrack, id 

insert into paths.routepaths (from_uhgs, from_name, to_uhgs, to_name, m_offset, p_distance_ratio, pointpath, nbPoints, elapseTime)
	select 'A0122932', 'Saint-Brieuc', 'A1964988', 'Côtes de Bretagne', (c).tampon,(c).distance, (c).pointslist , (c).nbPoints, (c).duration 
    from (
         select ports.portic_points_on_path('A0122932', 'A1964988', 5000, 10000) as c
    ) as calcul;


-- Pour QGIS

select idtrack as id, st_makeline(pointpath) as geom, * from paths.routepaths

 select idtrack, row_number() over (PARTITION BY idtrack),  geom
from (select idtrack, unnest(pointpath) as geom from paths.routepaths  ) as k

   
----------------------------------------------------------------------------------------------------
-- pb sur le placement de 'Côtes de Bretagne'
----------------------------------------------------------------------------------------------------

select * from ports.port_points pp where pp.toponyme_standard_fr = 'Côtes de Bretagne';
select * from ports.port_points pp where type_cote = '20mile';


-- CALCULER 
-- port_on_line_1mile
select m20.ogc_fid , m20.id, s.id , s.ogc_fid , st_contains(m20.wkb_geometry, s.polygone3857)
from navigoviz.shoreline_valid_20mile m20, navigoviz.shoreline s 
where st_contains(m20.wkb_geometry, s.polygone3857) and s.id = '2'
 limit 20
 
 select count(distinct id) from navigoviz.shoreline_valid_20mile; --447
 -- 1
  select count(distinct ogc_fid) from navigoviz.shoreline_valid_20mile; --447


update ports.port_points set port_on_line_1mile = null ;

update  ports.port_points ports set port_on_line_1mile = k2.port_on_line_1mile
from (
	select p.uhgs_id, min(st_distance(c.line_1mile, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline c
			  where  point3857 is not null and st_contains(c.buffer_1mile , point3857)     
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.id, st_distance(c.line_1mile , point3857) as d, ST_ClosestPoint(line_1mile, point3857) as port_on_line_1mile, '1mile' as type_cote
			  from ports.port_points p, navigoviz.shoreline c
			  where point3857 is not null and st_contains(buffer_1mile, point3857) 
    ) as k2
where  ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- 1573 en 1 min


update  ports.port_points ports set port_on_line_1mile = k2.port_on_line_1mile, id_shoreline = k2.id, distance_shoreline=k1.dmin, type_cote='20mile'
from (
	select p.uhgs_id, min(st_distance(c.line_1mile, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline  c
			  where p.uhgs_id='A1964988' and  point3857 is not null    
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.id, st_distance(c.line_1mile , point3857) as d, ST_ClosestPoint(c.line_1mile, point3857) as port_on_line_1mile, '1mile' as type_cote
			  from ports.port_points p, navigoviz.shoreline c
			  where point3857 is not null and p.uhgs_id='A1964988' 
    ) as k2
where ports.uhgs_id='A1964988' and ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- corrige la Bretagne mais calcul couteux



update  ports.port_points ports set port_on_line_1mile = k2.port_on_line_1mile, id_shoreline = k2.id, distance_shoreline=k1.dmin
from (
	select p.uhgs_id, min(st_distance(c.line_1mile, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline  c
			  where port_on_line_1mile is null and  point3857 is not null    
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.id, st_distance(c.line_1mile , point3857) as d, ST_ClosestPoint(c.line_1mile, point3857) as port_on_line_1mile, '1mile' as type_cote
			  from ports.port_points p, navigoviz.shoreline c
			  where port_on_line_1mile is null and point3857 is not null 
    ) as k2
where ports.port_on_line_1mile is null and ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- 126 pour compléter

select * from ports.port_points where port_on_line_1mile is null;

-- correction de ogc_fid dans cette requete 
update  ports.port_points ports set port_on_shoreline = k2.port_on_shoreline, id_shoreline = k2.ogc_fid, distance_shoreline=k1.dmin, type_cote='20mile'
from (
	select p.uhgs_id, min(st_distance(c.line_20_mile, point3857)) as dmin 
				from ports.port_points p, navigoviz.shoreline_valid_20mile  c
			  where point3857 is not null and type_cote='20mile' and st_contains(c.wkb_geometry, point3857)   
			group by p.uhgs_id
    ) as k1,
	(select p.uhgs_id, p.toponyme, p.country2019_name, c.ogc_fid, st_distance(c.line_20_mile , point3857) as d, ST_ClosestPoint(c.line_20_mile, point3857) as port_on_shoreline
			  from ports.port_points p, navigoviz.shoreline_valid_20mile c
			  where point3857 is not null and type_cote='20mile' and st_contains(c.wkb_geometry, point3857) 
    ) as k2
where type_cote='20mile'  and ports.uhgs_id = k2.uhgs_id and k1.uhgs_id=k2.uhgs_id and k2.d = k1.dmin;
-- correction : 103 en  6min

-- sur le serveur

select * from navigoviz.routepaths r where from_uhgs_id = 'A0122932' and to_uhgs_id='A1964988';
select * from navigoviz.routepaths r where to_uhgs_id = 'A0122932' and from_uhgs_id='A1964988';
delete from navigoviz.routepaths r where from_uhgs_id = 'A0122932' and to_uhgs_id='A1964988';

insert into navigoviz.routepaths (from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr, m_offset, p_distance_ratio, pointpath, nbPoints, elapseTime)
	select 'A0122932', 'Saint-Brieuc', 'A1964988', 'Côtes de Bretagne', (c).tampon,(c).distance, (c).pointslist , (c).nbPoints, (c).duration 
    from (
         select ports.portic_points_on_path('A0122932', 'A1964988', 5000, 10000) as c
    ) as calcul;
   
select * from navigoviz.built_travels where departure_uhgs_id ='A0122932' and destination_uhgs_id ='A1964988';

update navigoviz.built_travels t set geom=st_makeline(r.pointpath), pointpath = r.pointpath, geom4326 = st_setsrid(st_transform(st_setsrid(st_makeline(r.pointpath),3857) , 4326), 4326)
from navigoviz.routepaths r where r.from_uhgs_id ='A0122932' and r.to_uhgs_id ='A1964988'
and departure_uhgs_id ='A0122932' and destination_uhgs_id ='A1964988'


   
------------------------
-- le 20/02/2023 : bug signalé par Silvia sur le 0011334N entre Italie et Corse.
--------------------------

select source_suite , source_doc_id , departure, departure_uhgs_id  , destination , destination_uhgs_id , bt.travel_rank , ship_name , style_dashed , uncertainty_color , departure_function , destination_function 
from navigoviz.built_travels bt 
where ship_id = '0011334N'
order by bt.travel_rank ;
-- G5 : Ajaccio--> Italie
-- Santé : Marseille --> Ajaccio
-- G5 Italie	A1964975	Marseille	A0210797

insert into  paths.straight_line  
(select 'A0218974'  , 'Ajaccio' , 'A1964975' , 'Italie' ,  st_makeline(p1.point3857, p2.point3857)
from ports.port_points p1, ports.port_points p2
where   p1.uhgs_id = 'A0218974'  and p2.uhgs_id = 'A1964975'
);

select * from paths.routepaths where from_name='Ajaccio'
select * from paths.routepaths where to_name='Ajaccio'

insert into paths.routepaths (from_uhgs, from_name, to_uhgs, to_name, m_offset, p_distance_ratio, pointpath, nbPoints, elapseTime)
	select 'A0218974', 'Ajaccio', 'A1964975', 'Italie', (c).tampon,(c).distance, (c).pointslist , (c).nbPoints, (c).duration 
    from (
         select ports.portic_points_on_path('A0218974', 'A1964975', 10000, 20000) as c
    ) as calcul;

-- voir par point
select idtrack, row_number() over (PARTITION BY idtrack),  geom, description
from (select idtrack, unnest(pointpath) as geom, from_name||'->'||to_name as description from paths.routepaths  ) as k

-- update paths.routepaths r set reverse_pointpath = ports.portic_array_reverse(pointpath) 

insert into paths.routepaths (from_uhgs, from_name, to_uhgs, to_name, m_offset, p_distance_ratio, pointpath, nbPoints, elapseTime)
	select 'A1964975', 'Italie', 'A0218974', 'Ajaccio',  (c).tampon,(c).distance, (c).pointslist , (c).nbPoints, (c).duration 
    from (
         select ports.portic_points_on_path('A1964975', 'A0218974', 10000, 20000) as c
    ) as calcul;

insert into paths.routepaths (from_uhgs, from_name, to_uhgs, to_name, m_offset, p_distance_ratio, pointpath, nbPoints, elapseTime)
	select 'A1964975', 'Italie', 'A0210797', 'Marseille',  (c).tampon,(c).distance, (c).pointslist , (c).nbPoints, (c).duration 
    from (
         select ports.portic_points_on_path('A1964975', 'A0210797', 10000, 20000) as c
    ) as calcul;
   



-- à faire sur le serveur le 20/02/2023

select * from navigoviz.routepaths r where from_uhgs_id = 'A0218974' and to_uhgs_id='A1964975';
select * from navigoviz.routepaths r where to_uhgs_id = 'A1964975' and from_uhgs_id='A0218974';
delete from navigoviz.routepaths r where from_uhgs_id = 'A0218974' and to_uhgs_id='A1964975';
delete from navigoviz.routepaths r where from_uhgs_id = 'A1964975' and to_uhgs_id='A0218974';


insert into navigoviz.routepaths (from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr, m_offset, p_distance_ratio, pointpath, nbPoints, elapseTime)
	select 'A0218974', 'Ajaccio', 'A1964975', 'Italie', (c).tampon,(c).distance, (c).pointslist , (c).nbPoints, (c).duration 
    from (
         select ports.portic_points_on_path('A0218974', 'A1964975', 5000, 10000) as c
    ) as calcul;
   

   
update navigoviz.routepaths r set reverse_pointpath = ports.portic_array_reverse(pointpath) 
where from_uhgs_id = 'A0218974' and to_uhgs_id='A1964975';

update navigoviz.built_travels t set geom=st_makeline(r.pointpath), pointpath = r.pointpath, geom4326 = st_setsrid(st_transform(st_setsrid(st_makeline(r.pointpath),3857) , 4326), 4326)
from navigoviz.routepaths r where r.from_uhgs_id ='A0218974' and r.to_uhgs_id ='A1964975'
and departure_uhgs_id ='A0218974' and destination_uhgs_id ='A1964975';

update navigoviz.built_travels t set geom=st_makeline(r.reverse_pointpath), pointpath = r.reverse_pointpath, geom4326 = st_setsrid(st_transform(st_setsrid(st_makeline(r.reverse_pointpath),3857) , 4326), 4326)
from navigoviz.routepaths r where r.from_uhgs_id ='A0218974' and r.to_uhgs_id ='A1964975'
and departure_uhgs_id ='A1964975' and destination_uhgs_id ='A0218974';

-- select * from navigoviz.routepaths r where from_uhgs_id = 'A0218974' and to_uhgs_id='A1964975';
delete from navigoviz.routepaths r where from_uhgs_id = 'A1964975' and to_uhgs_id='A0210797';

-- Italie	A1964975	Marseille	A0210797

insert into navigoviz.routepaths (from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr, m_offset, p_distance_ratio, pointpath, nbPoints, elapseTime)
	select 'A1964975', 'Italie', 'A0210797', 'Marseille', (c).tampon,(c).distance, (c).pointslist , (c).nbPoints, (c).duration 
    from (
         select ports.portic_points_on_path('A1964975', 'A0210797', 5000, 10000) as c
    ) as calcul;

update navigoviz.routepaths r set reverse_pointpath = ports.portic_array_reverse(pointpath) 
where from_uhgs_id = 'A1964975' and to_uhgs_id='A0210797';

update navigoviz.built_travels t set geom=st_makeline(r.pointpath), pointpath = r.pointpath, geom4326 = st_setsrid(st_transform(st_setsrid(st_makeline(r.pointpath),3857) , 4326), 4326)
from navigoviz.routepaths r where r.from_uhgs_id ='A1964975' and r.to_uhgs_id ='A0210797'
and departure_uhgs_id ='A1964975' and destination_uhgs_id ='A0210797';

update navigoviz.built_travels t set geom=st_makeline(r.reverse_pointpath), pointpath = r.reverse_pointpath, geom4326 = st_setsrid(st_transform(st_setsrid(st_makeline(r.reverse_pointpath),3857) , 4326), 4326)
from navigoviz.routepaths r where r.from_uhgs_id ='A1964975' and r.to_uhgs_id ='A0210797'
and departure_uhgs_id ='A0210797' and destination_uhgs_id ='A1964975';

---------
-- A0122932	Saint-Brieuc ; A1964767	Ribérou ; A1964694	Marans
select * from navigoviz.routepaths r where to_uhgs_id = 'A1964988'; ---, 'Côtes de Bretagne'
-- A1964767
delete from navigoviz.routepaths r where from_uhgs_id = 'A1964767' and to_uhgs_id='A1964988';-- Ribérou
-- A1964767	Ribérou	A1964988	Côtes de Bretagne
delete from navigoviz.routepaths r where from_uhgs_id = 'A1964694' and to_uhgs_id='A1964988';-- Ribérou
-- A1964694	Marans	A1964988	Côtes de Bretagne

insert into navigoviz.routepaths (from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr, m_offset, p_distance_ratio, pointpath, nbPoints, elapseTime)
	select 'A1964767', 'Ribérou', 'A1964988', 'Côtes de Bretagne', (c).tampon,(c).distance, (c).pointslist , (c).nbPoints, (c).duration 
    from (
         select ports.portic_points_on_path('A1964767', 'A1964988', 5000, 10000) as c
    ) as calcul;

update navigoviz.routepaths r set reverse_pointpath = ports.portic_array_reverse(pointpath) 
where from_uhgs_id = 'A1964767' and to_uhgs_id='A1964988';

update navigoviz.built_travels t set geom=st_makeline(r.pointpath), pointpath = r.pointpath, geom4326 = st_setsrid(st_transform(st_setsrid(st_makeline(r.pointpath),3857) , 4326), 4326)
from navigoviz.routepaths r where r.from_uhgs_id ='A1964767' and r.to_uhgs_id ='A1964988'
and departure_uhgs_id ='A1964767' and destination_uhgs_id ='A1964988';

update navigoviz.built_travels t set geom=st_makeline(r.reverse_pointpath), pointpath = r.reverse_pointpath, geom4326 = st_setsrid(st_transform(st_setsrid(st_makeline(r.reverse_pointpath),3857) , 4326), 4326)
from navigoviz.routepaths r where r.from_uhgs_id ='A1964767' and r.to_uhgs_id ='A1964988'
and departure_uhgs_id ='A1964988' and destination_uhgs_id ='A1964767';


insert into navigoviz.routepaths (from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr, m_offset, p_distance_ratio, pointpath, nbPoints, elapseTime)
	select 'A1964694', 'Marans', 'A1964988', 'Côtes de Bretagne', (c).tampon,(c).distance, (c).pointslist , (c).nbPoints, (c).duration 
    from (
         select ports.portic_points_on_path('A1964694', 'A1964988', 5000, 10000) as c
    ) as calcul;

update navigoviz.routepaths r set reverse_pointpath = ports.portic_array_reverse(pointpath) 
where from_uhgs_id = 'A1964694' and to_uhgs_id='A1964988';

update navigoviz.built_travels t set geom=st_makeline(r.pointpath), pointpath = r.pointpath, geom4326 = st_setsrid(st_transform(st_setsrid(st_makeline(r.pointpath),3857) , 4326), 4326)
from navigoviz.routepaths r where r.from_uhgs_id ='A1964694' and r.to_uhgs_id ='A1964988'
and departure_uhgs_id ='A1964694' and destination_uhgs_id ='A1964988';

update navigoviz.built_travels t set geom=st_makeline(r.reverse_pointpath), pointpath = r.reverse_pointpath, geom4326 = st_setsrid(st_transform(st_setsrid(st_makeline(r.reverse_pointpath),3857) , 4326), 4326)
from navigoviz.routepaths r where r.from_uhgs_id ='A1964694' and r.to_uhgs_id ='A1964988'
and departure_uhgs_id ='A1964988' and destination_uhgs_id ='A1964694';



-- A0122932	Saint-Brieuc ; A1964767	Ribérou ; A1964694	Marans
select from_uhgs_id , from_toponyme_fr  from navigoviz.routepaths r 
where to_uhgs_id = 'A1964988' and from_uhgs_id not in ('A0122932', 'A1964767','A1964694')  ; ---, 'Côtes de Bretagne'
-- 16

delete from navigoviz.routepaths r where to_uhgs_id='A1964988'
and  from_uhgs_id not in ('A0122932', 'A1964767','A1964694');  
-- 23

select distinct departure_uhgs_id , departure_fr, ports.portic_points_on_path('A1964694', 'A1964988', 5000, 10000) as c from navigoviz.built_travels
where destination_uhgs_id = 'A1964988' and departure_uhgs_id not in ('A0122932', 'A1964767','A1964694');
	


insert into navigoviz.routepaths (from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr, m_offset, p_distance_ratio, pointpath, nbPoints, elapseTime)
	select calcul.departure_uhgs_id, calcul.departure_fr, 'A1964988', 'Côtes de Bretagne', (c).tampon,(c).distance, (c).pointslist , (c).nbPoints, (c).duration 
    from (
		select distinct departure_uhgs_id , departure_fr,  ports.portic_points_on_path(departure_uhgs_id, 'A1964988', 5000, 10000) as c 
		from navigoviz.built_travels
		where destination_uhgs_id = 'A1964988' and departure_uhgs_id not in ('A0122932', 'A1964767','A1964694') 
) as calcul; -- BUG, ne marche pas


insert into navigoviz.routepaths (from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr)
	select distinct departure_uhgs_id, departure_fr, 'A1964988', 'Côtes de Bretagne'		from navigoviz.built_travels
		where destination_uhgs_id = 'A1964988' and departure_uhgs_id not in ('A0122932', 'A1964767','A1964694') ;
		-- ,  ports.portic_points_on_path(departure_uhgs_id, 'A1964988', 5000, 10000) as c 

update navigoviz.routepaths r set m_offset = (c).tampon,
p_distance_ratio = (c).distance,
pointpath = (c).pointslist,
nbPoints = (c).nbPoints,
elapseTime = (c).duration 
from 
(	select from_uhgs_id, ports.portic_points_on_path(from_uhgs_id, 'A1964988', 5000, 10000) as c 
	from navigoviz.routepaths
	where to_uhgs_id = 'A1964988' and from_uhgs_id not in ('A0122932', 'A1964767','A1964694')
) as k
where r.to_uhgs_id = 'A1964988' and r.from_uhgs_id not in ('A0122932', 'A1964767','A1964694') 
and r.from_uhgs_id =k.from_uhgs_id; 
-- 5 min 

update navigoviz.routepaths r set reverse_pointpath = ports.portic_array_reverse(pointpath) 
where from_uhgs_id not in ('A0122932', 'A1964767','A1964694') and to_uhgs_id='A1964988';



update navigoviz.built_travels t set geom=st_makeline(r.pointpath), pointpath = r.pointpath, geom4326 = st_setsrid(st_transform(st_setsrid(st_makeline(r.pointpath),3857) , 4326), 4326)
from navigoviz.routepaths r 
where r.to_uhgs_id ='A1964988' and destination_uhgs_id ='A1964988'
and r.from_uhgs_id = t.departure_uhgs_id and from_uhgs_id not in ('A0122932', 'A1964767','A1964694') ;
-- 236 

update navigoviz.built_travels t set geom=st_makeline(r.reverse_pointpath), pointpath = r.reverse_pointpath, geom4326 = st_setsrid(st_transform(st_setsrid(st_makeline(r.reverse_pointpath),3857) , 4326), 4326)
from navigoviz.routepaths r where r.to_uhgs_id ='A1964988' and departure_uhgs_id ='A1964988' 
and destination_uhgs_id = r.from_uhgs_id and from_uhgs_id not in ('A0122932', 'A1964767','A1964694') ;
-- 144

--- reverse_order
update navigoviz.routepaths r set reverse_pointpath = ports.portic_array_reverse(pointpath) where reverse_order is true;
-- 1719

update navigoviz.routepaths r 
set reverse_order = true 
from ports.port_points p1, ports.port_points p2
where r.pointpath is not null and array_length(pointpath, 1)> 1 and not st_equals(pointpath[0], pointpath[1])
and p1.uhgs_id = r.from_uhgs_id and p2.uhgs_id =r.to_uhgs_id 
and st_distance(p1.point3857,pointpath[0]) > st_distance(p2.point3857,pointpath[0]);
-- 1718

update navigoviz.routepaths r set pointpath = reverse_pointpath where reverse_order is true;
-- 1719

select * from navigoviz.routepaths where to_toponyme_fr like 'Côtes de Bre%';
update navigoviz.routepaths r set reverse_pointpath = ports.portic_array_reverse(pointpath) ;
-- 6797

select distinct r.from_uhgs_id, r.to_uhgs_id
from navigoviz.routepaths r , navigoviz.built_travels t 
where r.from_uhgs_id = t.departure_uhgs_id and r.to_uhgs_id = t.destination_uhgs_id ;
-- 6469

select distinct departure_uhgs_id, destination_uhgs_id from built_travels bt 
where pointpath is null and (bt.departure_point is not null and bt.destination_point is not null)
; -- 653

update navigoviz.routepaths r set endouble = false;

-- vérifier si on peut alléger encore le calcul

select distinct departure_uhgs_id, destination_uhgs_id from built_travels bt where pointpath is null and (bt.departure_point is not null and bt.destination_point is not null)
except (
	select distinct departure_uhgs_id, destination_uhgs_id from built_travels bt where pointpath is null and (bt.departure_point is not null and bt.destination_point is not null)
	except 
	(select distinct  destination_uhgs_id, departure_uhgs_id from built_travels bt where pointpath is null and (bt.departure_point is not null and bt.destination_point is not null))
	-- 591 liste des distincts sans retour 
)
-- 62 avec retours
-- A0210797	A0194462
-- A0233023	A0148992

select * from built_travels bt where pointpath is null and (bt.departure_point is not null and bt.destination_point is not null)
and departure_uhgs_id='A1968869' and destination_uhgs_id='A0210797';
-- A0210797	A1968869
--2197

select distinct departure_uhgs_id, destination_uhgs_id from built_travels bt 
where pointpath is null and (bt.departure_point is not null and bt.destination_point is not null) and departure_uhgs_id='A0210797' and destination_uhgs_id='A1968869'
; 

select * from (
	select distinct departure_uhgs_id, destination_uhgs_id from built_travels bt where pointpath is null and (bt.departure_point is not null and bt.destination_point is not null)
	except 
	(select distinct  destination_uhgs_id, departure_uhgs_id from built_travels bt where pointpath is null and (bt.departure_point is not null and bt.destination_point is not null))
) as k 
where k.departure_uhgs_id='A1968869' and k.destination_uhgs_id='A0210797'
	

update navigoviz.routepaths r set endouble = true 
from 
(select distinct departure_uhgs_id, destination_uhgs_id from built_travels bt where pointpath is not null and (bt.departure_point is not null and bt.destination_point is not null)
except (
	select distinct departure_uhgs_id, destination_uhgs_id from built_travels bt where pointpath is not null and (bt.departure_point is not null and bt.destination_point is not null)
	except 
	(select distinct  destination_uhgs_id, departure_uhgs_id from built_travels bt where pointpath is not null and (bt.departure_point is not null and bt.destination_point is not null))
	-- 591 liste des distincts sans retour 
)) as doublons
where  r.from_uhgs_id = departure_uhgs_id and r.to_uhgs_id = destination_uhgs_id; --3291


alter table navigoviz.routepaths add column id serial;


select r1.id, r2.id, r1.endouble, r1.nbpoints , r2.nbpoints , r1.elapsetime , r2.elapsetime , r1.from_uhgs_id , r1.to_uhgs_id , r2.from_uhgs_id , r2.to_uhgs_id 
from navigoviz.routepaths r1, navigoviz.routepaths r2
where r1.from_uhgs_id = r2.to_uhgs_id and r1.to_uhgs_id = r2.from_uhgs_id and r1.nbpoints != r2.nbpoints;

delete from navigoviz.routepaths where id in (
select r1.id
from navigoviz.routepaths r1, navigoviz.routepaths r2
where r1.from_uhgs_id = r2.to_uhgs_id and r1.to_uhgs_id = r2.from_uhgs_id and r1.nbpoints != r2.nbpoints and r1.elapsetime > r2.elapsetime );
-- garde les plus courts en calcul qui ont été corrigés le 20 février 2023


select r1.id, r2.id, r1.endouble, r1.nbpoints , r2.nbpoints , r1.elapsetime , r2.elapsetime , r1.from_uhgs_id , r1.to_uhgs_id , r2.from_uhgs_id , r2.to_uhgs_id , ST_IsSimple(st_makeline(r1.pointpath)) 
from navigoviz.routepaths r1, navigoviz.routepaths r2
where r1.from_uhgs_id = r2.to_uhgs_id and r1.to_uhgs_id = r2.from_uhgs_id and r1.nbpoints = r2.nbpoints and ST_IsSimple(st_makeline(r2.pointpath))  is false and r1.id < r2.id;
-- 18 mais en double on passe à 9 à supprimer, les autres à recalculer

select distinct r1.id
from navigoviz.routepaths r1, navigoviz.routepaths r2
where r1.from_uhgs_id = r2.to_uhgs_id and r1.to_uhgs_id = r2.from_uhgs_id and r1.nbpoints = r2.nbpoints and ST_IsSimple(st_makeline(r2.pointpath))  is false;


update navigoviz.routepaths r set m_offset = (c).tampon,
p_distance_ratio = (c).distance,
pointpath = (c).pointslist,
nbPoints = (c).nbPoints,
elapseTime = (c).duration 
from 
(	
	
	select  r2.id, r2.from_uhgs_id , r2.to_uhgs_id, ports.portic_points_on_path(r2.from_uhgs_id, r2.to_uhgs_id, 5000, 10000) as c 
	from navigoviz.routepaths r1, navigoviz.routepaths r2
	where r1.from_uhgs_id = r2.to_uhgs_id and r1.to_uhgs_id = r2.from_uhgs_id and r1.nbpoints = r2.nbpoints and ST_IsSimple(st_makeline(r2.pointpath))  is false and r1.id < r2.id

) as k
where r.to_uhgs_id = k.to_uhgs_id and r.from_uhgs_id =k.from_uhgs_id;


select r1.id, r2.id, r1.endouble, r1.nbpoints , r2.nbpoints , r1.elapsetime , r2.elapsetime , r1.from_uhgs_id , r1.to_uhgs_id , r2.from_uhgs_id , r2.to_uhgs_id , ST_IsSimple(st_makeline(r1.pointpath)) 
from navigoviz.routepaths r1, navigoviz.routepaths r2
where r1.from_uhgs_id = r2.to_uhgs_id and r1.to_uhgs_id = r2.from_uhgs_id  and ST_IsSimple(st_makeline(r2.pointpath)) != ST_IsSimple(st_makeline(r1.pointpath)) and r1.id < r2.id;
-- 18 mais en double on passe à 9 à supprimer, les autres à recalculer

select  from navigoviz.routepaths r
delete from navigoviz.routepaths where id in 
(
select r1.id
from navigoviz.routepaths r1, navigoviz.routepaths r2
where r1.from_uhgs_id = r2.to_uhgs_id and r1.to_uhgs_id = r2.from_uhgs_id  and ST_IsSimple(st_makeline(r2.pointpath)) != ST_IsSimple(st_makeline(r1.pointpath)) and r1.id < r2.id
);

-- supprimer les doublons en général
select r1.id, r2.id, r1.endouble, r1.nbpoints , r2.nbpoints , r1.elapsetime , r2.elapsetime , r1.from_uhgs_id , r1.to_uhgs_id , r2.from_uhgs_id , r2.to_uhgs_id , ST_IsSimple(st_makeline(r1.pointpath)) 
from navigoviz.routepaths r1, navigoviz.routepaths r2
where r1.from_uhgs_id = r2.to_uhgs_id and r1.to_uhgs_id = r2.from_uhgs_id   and r1.id < r2.id;
-- 3384 --> 1692

delete from navigoviz.routepaths where id in (
select r1.id
from navigoviz.routepaths r1, navigoviz.routepaths r2
where r1.from_uhgs_id = r2.to_uhgs_id and r1.to_uhgs_id = r2.from_uhgs_id   and r1.id < r2.id);
-- 1688 

select id, round(elapsetime/1000.0) as elapsetimes, * from  navigoviz.routepaths
where ST_IsSimple(st_makeline(pointpath)) is false
order by elapsetime;
-- 2810	2729.602	A0134176	Redon	A0198999	La Rochelle

-- correction faite sur < 100 s et < 500 s en local V9


update navigoviz.routepaths r set m_offset = (c).tampon,
p_distance_ratio = (c).distance,
pointpath = (c).pointslist,
nbPoints = (c).nbPoints,
elapseTime = (c).duration 
from 
(	
	
	select  r2.id, r2.from_uhgs_id , r2.to_uhgs_id, ports.portic_points_on_path(r2.from_uhgs_id, r2.to_uhgs_id, 5000, 10000) as c 
	from  navigoviz.routepaths r2
	where  ST_IsSimple(st_makeline(r2.pointpath))  is false and r2.elapsetime <  500000

) as k
where r.to_uhgs_id = k.to_uhgs_id and r.from_uhgs_id =k.from_uhgs_id;

-- Suite

select count(*) from navigoviz.routepaths;
-- 5085

--- les paths qui coupent la terre

select r.id, max(st_length(st_intersection(st_makeline(r.pointpath), polygone3857))) as longueur
from navigoviz.routepaths r, navigoviz.shoreline s 
where st_intersects(st_makeline(r.pointpath), s.polygone3857) 
and r.id < 100
group by r.id
order by longueur;
-- 783
--818
--51


select ST_IsSimple(st_makeline(pointpath)), id, * from navigoviz.routepaths where id in (51, 17, 99);
-- true	51	A0204180	Dunkerque	A1963986	Mer Baltique
-- true	17	A1465893	Constantinople	A0147257	Saint-Tropez
-- true	99	A0354220	Détroit de Gibraltar	A1968882	cap Saint-Antoine (à 2 lieues)
select * from ports.port_points pp where uhgs_id = 'A1963986';


update navigoviz.routepaths r set m_offset = (c).tampon,
p_distance_ratio = (c).distance,
pointpath = (c).pointslist,
nbPoints = (c).nbPoints,
elapseTime = (c).duration 
from 
(	
	
	select  r2.id, r2.from_uhgs_id , r2.to_uhgs_id, ports.portic_points_on_path(r2.from_uhgs_id, r2.to_uhgs_id, 5000, 10000) as c 
	from  navigoviz.routepaths r2
	where  id in (51, 17, 99) 

) as k
where r.to_uhgs_id = k.to_uhgs_id and r.from_uhgs_id =k.from_uhgs_id;

alter table navigoviz.routepaths add column longueur_intersection float;
alter table navigoviz.routepaths add column taux_intersection float;

select k.id, k.longueur , k.longueur / st_length(st_makeline(r.pointpath)) * 100 
from 
(
select r.id, max(st_length(st_intersection(st_makeline(r.pointpath), polygone3857))) as longueur
from navigoviz.routepaths r, navigoviz.shoreline s 
where st_intersects(st_makeline(r.pointpath), s.polygone3857) 
and r.id < 500
group by r.id
) as k ,
navigoviz.routepaths r
where k.id = r.id;

update navigoviz.routepaths r set longueur_intersection = k.longueur, taux_intersection = k.longueur / st_length(st_makeline(r.pointpath)) * 100
from 
(
select r.id, max(st_length(st_intersection(st_makeline(r.pointpath), polygone3857))) as longueur
from navigoviz.routepaths r, navigoviz.shoreline s 
where st_intersects(st_makeline(r.pointpath), s.polygone3857) 
and r.id < 8000 and longueur_intersection is null
group by r.id
) as k 
where k.id = r.id;
-- 110 + 104 + 106 + 112 + 138 + 54 + 148 + 126 + 131

select max(id) from navigoviz.routepaths; -- 6797
select 110 + 104 + 106 + 112 + 138 + 54 + 148 + 126 + 131 -- 1029
-- 0.000015025921557	2.3638986246212226	11.582420098527	89.5077835583224

select min(taux_intersection), avg(taux_intersection), stddev(taux_intersection), max(taux_intersection) from navigoviz.routepaths
where longueur_intersection is not null;

select count(*) from navigoviz.routepaths where taux_intersection < 1
-- 933

select count(*), min(longueur_intersection) from navigoviz.routepaths where taux_intersection > 3
-- 975 / 54 avec 10 km min d'intersection

update navigoviz.routepaths r set m_offset = (c).tampon,
p_distance_ratio = (c).distance,
pointpath = (c).pointslist,
nbPoints = (c).nbPoints,
elapseTime = (c).duration 
from 
(	
	
	select  r2.id, r2.from_uhgs_id , r2.to_uhgs_id, ports.portic_points_on_path(r2.from_uhgs_id, r2.to_uhgs_id, 5000, 10000) as c 
	from  navigoviz.routepaths r2
	where  taux_intersection > 3 

) as k
where r.to_uhgs_id = k.to_uhgs_id and r.from_uhgs_id =k.from_uhgs_id;
-- 


alter table navigoviz.routepaths add column longueur_intersection2 float;
alter table navigoviz.routepaths add column taux_intersection2 float;

update navigoviz.routepaths r set longueur_intersection2 = k.longueur, taux_intersection2 = k.longueur / st_length(st_makeline(r.pointpath)) * 100
from 
(
select r.id, max(st_length(st_intersection(st_makeline(r.pointpath), polygone3857))) as longueur
from navigoviz.routepaths r, navigoviz.shoreline s 
where st_intersects(st_makeline(r.pointpath), s.polygone3857) 
and taux_intersection > 3 
group by r.id
) as k 
where k.id = r.id;

select  r2.id, r2.from_uhgs_id , r2.from_toponyme_fr , r2.to_uhgs_id, r2.to_toponyme_fr , elapsetime , nbpoints ,
	taux_intersection, taux_intersection2
	from  navigoviz.routepaths r2	 
	where  taux_intersection > 3 
	order by taux_intersection2 desc
	-- 12
	
-- 12 à revoir	
	
--2435	A1010107	Riga	A0354220	Détroit de Gibraltar	775.892	2	89.5077835583224	92.36251427326904
--4039	A1010107	Riga	A0180923	Bordeaux	1555.605	2	89.41394143785159	89.41394143785159
--300	A1069373	Norrköping	A0354220	Détroit de Gibraltar	1707.006	2	88.69242142123177	89.25626940090251
--6552	A1010107	Riga	A0187101	Le Havre	1572.064	2	69.58304261704407	69.66244272211681
--548	A0932725	Abo / Turku	A0354220	Détroit de Gibraltar	864.14	2	67.2555150573454	67.88239108142233
--4562	A0865461	Kristiansund 	A0354220	Détroit de Gibraltar	1590.786	2	47.05755738259408	47.837430241822915
--5042	A0348862	Port Sigre	A0210797	Marseille	1571.865	2	47.28284191211366	47.28284191211363
--2705	A1069373	Norrköping	A0395164	Ramsgate	717.287	2	33.91730940427139	33.9173094042714
--2533	C0000002	île de France / île Maurice	C0000013	Mozambique	8.463	2	25.468761741838964	26.960379057109822
--4704	A1470521	Foilleri / Yenifoça	A0348862	Port Sigre	928.264	4	9.756666340974784	19.841420762882027
--900	A0348862	Port Sigre	A0026844	Tunis	901.255	2	11.825985693294246	11.825985693294285
--1085	A0140266	Lorient	D0000003	Côte de Coromandel	67463.045	25	4.350617469431213	6.253128971008716
--783	A0140266	Lorient	D0000001	Pondichéry 	125231.233	24	4.350074112460364	4.332507287493473
	
	
update navigoviz.routepaths r set reverse_pointpath = ports.portic_array_reverse(pointpath) ;-- 5085
--- ICI backup du fichier et réinstallation sur le server le 22/02/2023, soir

-- Puis à mettre en place le service de ship, captain, 
-- OK vu avec bernard le 23/02

-- Puis à tester l'API (et remettre les droits à api_user) et en particulier vizsources
-- OK vu avec bernard le 23/02

select travel_rank,  departure_uhgs_id , departure_fr, destination_uhgs_id, destination_fr   from built_travels bt where bt.ship_id = '0011334N' and geom is null ;
--Ajaccio	A0218974	11
--Casteglione dans l' Etat Ecclésiastique [péninsule italienne	A0219830	12
-- 11	A0218974	Ajaccio	A0219830	Italie
-- 12	A0219830	Italie	A0210797	Marseille

select * from navigoviz.routepaths r  where from_uhgs_id ='A0218974' and to_uhgs_id = 'A0219830'


-- Italie	A1964975	Marseille	A0210797
-- a été remplacé par Casteglione dans l' Etat Ecclésiastique [péninsule italienne	A0219830  Marseille	A0210797
-- Changement d'UHGS_id pour Italie	A1964975 --> A0219830

-- 11	A0218974	Ajaccio	A0219830	Italie
insert into navigoviz.routepaths (from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr, m_offset, p_distance_ratio, pointpath, nbPoints, elapseTime)
	select 'A0218974', 'Ajaccio', 'A0219830', 'Italie', (c).tampon,(c).distance, (c).pointslist , (c).nbPoints, (c).duration 
    from (
         select ports.portic_points_on_path('A0218974', 'A0219830', 5000, 10000) as c
    ) as calcul;

update navigoviz.routepaths r set reverse_pointpath = ports.portic_array_reverse(pointpath) 
where from_uhgs_id = 'A0218974' and to_uhgs_id='A0219830';

update navigoviz.built_travels t set geom=st_makeline(r.pointpath), pointpath = r.pointpath, geom4326 = st_setsrid(st_transform(st_setsrid(st_makeline(r.pointpath),3857) , 4326), 4326), 
distance_dep_dest_km = round(st_length(st_makeline(r.pointpath))/1000)
from navigoviz.routepaths r where r.from_uhgs_id ='A0218974' and r.to_uhgs_id ='A0219830'
and departure_uhgs_id ='A0218974' and destination_uhgs_id ='A0219830';

update navigoviz.built_travels t set geom=st_makeline(r.reverse_pointpath), pointpath = r.reverse_pointpath, 
	geom4326 = st_setsrid(st_transform(st_setsrid(st_makeline(r.reverse_pointpath),3857) , 4326), 4326),
	distance_dep_dest_km = round(st_length(st_makeline(r.reverse_pointpath))/1000)
from navigoviz.routepaths r where r.from_uhgs_id ='A0218974' and r.to_uhgs_id ='A0219830'
and departure_uhgs_id ='A0219830' and destination_uhgs_id ='A0218974';

-- 12	A0219830	Italie	A0210797	Marseille
insert into navigoviz.routepaths (from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr, m_offset, p_distance_ratio, pointpath, nbPoints, elapseTime)
	select 'A0219830', 'Italie', 'A0210797', 'Marseille', (c).tampon,(c).distance, (c).pointslist , (c).nbPoints, (c).duration 
    from (
         select ports.portic_points_on_path('A0219830', 'A0210797', 5000, 10000) as c
    ) as calcul;

update navigoviz.routepaths r set reverse_pointpath = ports.portic_array_reverse(pointpath) 
where from_uhgs_id = 'A0219830' and to_uhgs_id='A0210797';

update navigoviz.built_travels t set geom=st_makeline(r.pointpath), pointpath = r.pointpath, geom4326 = st_setsrid(st_transform(st_setsrid(st_makeline(r.pointpath),3857) , 4326), 4326), 
distance_dep_dest_km = round(st_length(st_makeline(r.pointpath))/1000)
from navigoviz.routepaths r where r.from_uhgs_id ='A0219830' and r.to_uhgs_id ='A0210797'
and departure_uhgs_id ='A0219830' and destination_uhgs_id ='A0210797';

update navigoviz.built_travels t set geom=st_makeline(r.reverse_pointpath), pointpath = r.reverse_pointpath, 
	geom4326 = st_setsrid(st_transform(st_setsrid(st_makeline(r.reverse_pointpath),3857) , 4326), 4326),
	distance_dep_dest_km = round(st_length(st_makeline(r.reverse_pointpath))/1000)
from navigoviz.routepaths r where r.from_uhgs_id ='A0219830' and r.to_uhgs_id ='A0210797'
and departure_uhgs_id ='A0210797' and destination_uhgs_id ='A0219830';

-- Pour Vendredi, il restera à examiner quels chemins restent à calculer

-- même quand ship_id is null (remplacé par doc_id lu dans raw_flows)
select max(id) from navigoviz.routepaths r ;

select ST_IsSimple(st_makeline(pointpath)), * 
from navigoviz.routepaths where id in (6799, 6798);

select * from navigoviz.routepaths r where r.from_uhgs_id = 'A0219830' and r.to_uhgs_id = 'A0210797'
	
