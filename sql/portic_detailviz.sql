-----------------------------------------------------------------
-- API pour detailviz
-----------------------------------------------------------------

set search_path = navigoviz, ports, public;

select count(*) from built_travels where source_entry != 'both-to';
-- 63204 segments

select distinct p.captain_id , p.captain_name , p.captain_uncertainity , 
p.birthplace , p.birthplace_uhgs_id , p.birthplace_uncertainity ,  p.birthplace_uhgs_id_uncertainity ,
p.citizenship , p.citizenship_uncertainity 
-- ATTENTION, à ajouter dans travels : captain_citizenship_id
--captain_last_name, captain_age, captain_status,   
--captain_citizenship, captain_birthplace, captain_birthplace_id, captain_origin, captain_origin_id 
from  navigoviz.built_travels p
where source_entry != 'both-to'
order by captain_id;
-- 21994

-- drop table navigoviz.captain;
truncate TABLE navigoviz.captain;
CREATE TABLE navigoviz.captain (
		captain_id text,
		occurences_names json,
		occurences_birthplaces json,
		occurences_citizenships json
	);

INSERT INTO navigoviz.captain(captain_id)
	SELECT DISTINCT captain_id FROM navigoviz.built_travels where captain_id is not null;
--- 12329 
-- 12210 le 21 juillet 2022
-- 13153 le 23 février 2023

--  where source_entry != 'both-to'

update navigoviz.captain c set occurences_names = k.occurences_name
FROM
(select captain_local_id, array_to_json(array_agg(json_build_object(captain_name ,occurences))) as occurences_name
	FROM
	(
	Select captain_id as captain_local_id, trim(captain_name) as captain_name, count (distinct source_doc_id) as occurences
	FROM navigoviz.pointcall p
	WHERE p.captain_id in (select distinct captain_id from built_travels)
	GROUP BY trim(captain_name) , captain_id
	ORDER BY captain_id, occurences DESC
) t
WHERE occurences > 0
GROUP BY captain_local_id
) as k
WHERE c.captain_id = k.captain_local_id;
-- 12210 le 21 juillet 2022 
-- 13153 le 23 février 2023


-- bug signalé le 29 juin par Bernard
-- Par exemple (pour le captain_id 00011085), 
-- je suis tombé sur des variantes de noms de capitaines (occurences_names) qui ne sont pas vraiment toutes différentes :
select * from navigoviz.captain where captain_id = '00011085';
select * from navigoviz.pointcall where captain_id = '00011085';
select * from navigoviz.built_travels where captain_id = '00011085';


-- [{"Bordes, Jean" : 4},{"Border, Jean" : 1},{"Bordery, Jean" : 1},{"Bordery, Jean " : 1},{"Bordes, Jean " : 1}]

	Select captain_id as captain_local_id, trim(captain_name) as captain_name, count (distinct source_doc_id) as occurences
	FROM navigoviz.pointcall p
	WHERE p.captain_id in (select distinct captain_id from built_travels) and captain_id = '00011085'
	GROUP BY trim(captain_name) , captain_id
	ORDER BY captain_id, occurences desc
	-- ok, trim règle le pb
	
-- select * from navigoviz.captain where occurences_names is null;

update navigoviz.captain c set occurences_birthplaces = k.occurence_birthplace
FROM
(select captain_local_id, array_to_json(array_agg(json_build_object(birthplace ,occurences))) as occurence_birthplace
FROM
(
Select captain_id as captain_local_id, birthplace, count (distinct source_doc_id) as occurences
FROM navigoviz.pointcall p
	WHERE p.captain_id in (select distinct captain_id from built_travels)
	GROUP BY birthplace,captain_id
	ORDER BY captain_id, occurences DESC
) t
WHERE occurences > 0 and birthplace is not null
GROUP BY captain_local_id
) as k
WHERE c.captain_id = k.captain_local_id;
-- 2325
-- 2817 le 23 février 2023




update navigoviz.captain c set occurences_citizenships = k.occurences_citizenship
FROM
(select captain_local_id, array_to_json(array_agg(json_build_object(citizenship ,occurences))) as occurences_citizenship
FROM
(
Select captain_id as captain_local_id, citizenship, count (distinct source_doc_id) as occurences
FROM navigoviz.pointcall p
	WHERE p.captain_id in (select distinct captain_id from built_travels)
	GROUP BY citizenship,captain_id
	ORDER BY captain_id, occurences DESC
) t
WHERE occurences > 0 and citizenship is not null
GROUP BY captain_local_id
) as k
WHERE c.captain_id = k.captain_local_id;
-- 1085
-- 1517 le 23 février 2023


select * from navigoviz.captain where captain_id in ('00002765', '00002763', '00002472', '00014569', '00011637') ;
-- 00014569	[{"Accamo, Jean Baptiste" : 18},{"Accamo,Jean Baptiste" : 1},{"Accame, Jean Baptiste" : 1},{"Accamé, Jean Baptiste" : 1},{"Accamo, Jean BAptiste" : 1}]		[{"Génois" : 18},{"Genois" : 3}]
-- 00002763	[{"Taglierani, Jean Antoine" : 1}]		[{"Ragusois" : 1}]
-- 00002472	[{"Perron, Joseph" : 3},{"Person, Joseph" : 2},{"Le Person, Joseph" : 1},{"Porson, Joseph" : 1},{"Le Perron, Joseph" : 1}]	[{"Ile aux moines" : 2}]	
-- 00011637	[{"Rouilla, François" : 2},{"Rouilla, Fancois" : 1},{"Rolla, François" : 1}]	[{"Tossa" : 3},{"Tosse [Tossa]" : 1}]	[{"Catalan" : 3},{"Catalogne" : 1}]
-- 00002765	[{"Cazabon, Jean" : 1},{"Casaubon, Jean" : 1}]		

select captain_name, * from navigoviz.pointcall where captain_id  = '00002472'
order by captain_name

--- Pour les navires
drop table navigoviz.ship;
truncate table navigoviz.ship;

CREATE TABLE navigoviz.ship(
	ship_id text,
	occurences_names json,
	occurences_homeports_fr json,--use standardized homeports fr et en; should be only ONE ?
	occurences_homeports_en json,--use standardized homeports fr et en; should be only ONE ?
	occurences_flags_fr json,-- use standardized flag fr et en; should be only ONE ?
	occurences_flags_en json,-- use standardized flag fr et en; should be only ONE ?
	occurences_class json,
	occurences_tonnageclass json
);

INSERT INTO navigoviz.ship(ship_id,occurences_names)
select ship_id, array_to_json(array_agg(json_build_object(ship_name ,occurences))) as occurences_name
FROM
(
Select ship_id, trim(ship_name) as ship_name, count (distinct source_doc_id) as occurences
FROM navigoviz.pointcall p
	WHERE p.ship_id in (select distinct ship_id from built_travels)
	GROUP BY trim(ship_name),ship_id
	ORDER BY ship_id, occurences DESC
) as k
GROUP BY ship_id;
-- 11618
-- 11527 le 21 juillet 2022
-- 12608 le 23 février 2023

update navigoviz.ship s set occurences_names = k.occurences_name
FROM
(select ship_id, array_to_json(array_agg(json_build_object(ship_name ,occurences))) as occurences_name
	from (
		Select ship_id, trim(ship_name) as ship_name, count (distinct source_doc_id) as occurences
		FROM navigoviz.pointcall p
		WHERE p.ship_id in (select distinct ship_id from built_travels)
			GROUP BY trim(ship_name),ship_id
			ORDER BY ship_id, occurences DESC
		) as k
	GROUP BY ship_id
) as k
WHERE s.ship_id = k.ship_id; -- 11527
-- 12608 le 23 février 2023

select * from navigoviz.built_travels where ship_name is null ;

update navigoviz.ship s set occurences_flags_fr = k.flags
FROM
(SELECT t.ship_id, array_to_json(array_agg(json_build_object(t.ship_flag ,t.occurences))) as flags
	FROM(
		Select ship_id,
		ship_flag_standardized_fr as ship_flag, count (distinct source_doc_id) as occurences
		FROM navigoviz.pointcall p
		WHERE p.ship_id in (select distinct ship_id from built_travels)
		GROUP BY ship_id, p.ship_flag_standardized_fr
		ORDER BY ship_id, occurences DESC
		) t
	WHERE occurences > 0 and ship_flag is not null
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;
-- 11102
-- 12059 le 23 février 2023

update navigoviz.ship s set occurences_flags_en = k.flags
FROM
(SELECT t.ship_id, array_to_json(array_agg(json_build_object(t.ship_flag ,t.occurences))) as flags
	FROM(
		Select ship_id,
		ship_flag_standardized_en as ship_flag, count (distinct source_doc_id) as occurences
		FROM navigoviz.pointcall p
	WHERE p.ship_id in (select distinct ship_id from built_travels)
		GROUP BY ship_id, p.ship_flag_standardized_en
		ORDER BY ship_id, occurences DESC
		) t
	WHERE occurences > 0 and ship_flag is not null
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;
-- 11102
-- 12059 le 23 février 2023

-- Requête JSON à faire : existe-t-il des cas où le flag n'est pas consistent pour le même navire ?
-- OUI : 26 cas sur 11104
-- OUI : 46 sur 12608
-- taille du tableau ? 
select distinct json_array_length (occurences_flags_fr) from navigoviz.ship
select * from navigoviz.ship where  json_array_length (occurences_flags_fr) > 1
select * from navigoviz.ship where  json_array_length (occurences_flags_fr) > 1
select (occurences_flags_fr)::json->0->>'f1' from navigoviz.ship where occurences_flags_fr is not null
select (occurences_flags_fr)::json->0->>'f1' from navigoviz.ship where occurences_flags_fr is not null and (occurences_flags_fr)::json->0->>'f1' = ''


 

update navigoviz.ship s set occurences_homeports_fr = k.homeports
FROM
(SELECT t.ship_id, array_to_json(array_agg(json_build_object(t.ship_homeport ,t.occurences))) as homeports
	FROM(
		Select ship_id,
		homeport_toponyme_fr as ship_homeport, count (distinct source_doc_id) as occurences
		FROM navigoviz.pointcall p
	WHERE p.ship_id in (select distinct ship_id from built_travels)
		GROUP BY ship_id, homeport_toponyme_fr
		ORDER BY ship_id, occurences DESC
		) t
	 WHERE occurences > 0 and ship_homeport is not null
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;
---  7695
-- 6664 le 23 février 2023

update navigoviz.ship s set occurences_homeports_en = k.homeports
FROM
(SELECT t.ship_id, array_to_json(array_agg(json_build_object(t.ship_homeport ,t.occurences))) as homeports
	FROM(
		Select ship_id,
		homeport_toponyme_en as ship_homeport, count (distinct source_doc_id) as occurences
		FROM navigoviz.pointcall p
	WHERE p.ship_id in (select distinct ship_id from built_travels)
		GROUP BY ship_id, homeport_toponyme_en
		ORDER BY ship_id, occurences DESC
		) t
	 WHERE occurences > 0 and ship_homeport is not null
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;
--- 7695
-- 6664 le 23 février 2023

-- update navigoviz.ship s set occurences_class = null

update navigoviz.ship s set occurences_class = k.class
FROM
(SELECT t.ship_id, array_to_json(array_agg(json_build_object(t.ship_class ,t.occurences))) as class
	FROM(
		Select ship_id,
		trim(lower(ship_class_standardized)) as ship_class, count (distinct source_doc_id) as occurences
		FROM navigoviz.pointcall p
	WHERE p.ship_id in (select distinct ship_id from built_travels)
		GROUP BY ship_id, trim(lower(ship_class_standardized))
		ORDER BY ship_id, occurences DESC
		) t
	 WHERE occurences > 0 and ship_class is not null
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;
-- 4478
-- use ship_class_standardized (and no more class attribute) since 16.01.2023.
-- 5491 le 23 février 2023

select * from navigoviz.ship s

update navigoviz.ship s set occurences_tonnageclass = k.tonnageclass
FROM
(SELECT t.ship_id, array_to_json(array_agg(json_build_object(t.ship_tonnage ,t.occurences))) as tonnageclass
	FROM(
		Select ship_id,
		tonnage_class as ship_tonnage, count (distinct source_doc_id) as occurences
		FROM navigoviz.pointcall p
	WHERE p.ship_id in (select distinct ship_id from built_travels)
		GROUP BY ship_id, tonnage_class
		ORDER BY ship_id, occurences desc
		) t
	 WHERE occurences > 0 and ship_tonnage is not null
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;	
-- 8949
-- 9043 le 23 février 2023

select * from navigoviz.ship where  json_array_length (occurences_tonnageclass) > 1 --622 / 663 le 23 février 2022

select * from navigoviz.ship where  json_array_length (occurences_tonnageclass) > 1 --622
select (occurences_tonnageclass)::json->0->>'f1' from navigoviz.ship where occurences_tonnageclass is not null

select travel_id as step_id, source_doc_id, outdate_fixed, indate_fixed, departure_uhgs_id, departure_fr, destination_uhgs_id, destination_fr,
ship_id, ship_name, tonnage_class, ship_flag_standardized_fr, homeport_toponyme_fr, 
captain_id , captain_name, birthplace, citizenship, travel_rank
from navigoviz.built_travels
where ship_id = '0022221N' and source_entry!='both-to'
order by travel_rank;
-- and captain_id=''

select count(shipcaptain_travel_rank) from navigoviz.built_travels -- 128537
select count(travel_id) from navigoviz.uncertainity_travels ut --76792

-- Renvoyer les variantes de noms de navires ou de capitaines
-- role api_user / password portic
GRANT USAGE ON SCHEMA navigoviz TO api_user;
GRANT USAGE ON SCHEMA navigocheck to api_user;
GRANT USAGE ON SCHEMA navigo to api_user;
GRANT USAGE ON SCHEMA public to api_user;
GRANT USAGE ON SCHEMA ports to api_user;
GRANT SELECT ON ALL TABLES IN SCHEMA navigoviz, navigo, navigocheck, ports, public TO api_user;
grant CONNECT on database portic_v9 to api_user;
grant SELECT on all sequences in schema navigoviz, navigo, navigocheck, ports, public to api_user;

-------------------------------------------------------------------
-------------------------------------------------------------------

-- Travail sur les trajectoires

-- Aimable Ursule - ship 0007498N 
-- Fidèle Marianne - ship 0002931N
-- Marie Louise - ship 0004155N
-- Turgot, ship id 0021517N
-- Femmes et Enfants, 0003951N
-- Notre Dame des Carmes 0004171N


select travel_id as step_id, source_text, outdate_fixed, indate_fixed, departure_uhgs_id, departure_fr, destination_uhgs_id, destination_fr,
ship_id, ship_name, tonnage_class, ship_flag_standardized_fr, homeport_toponyme_fr, 
captain_id , captain_name, birthplace, citizenship, travel_rank
from navigoviz.built_travels
where ship_id in ( '0007498N', '0002931N', '0004155N', '0021517N', '0003951N', '0004171N', '0002537N', '0002350N', '0014650N', '0000011N', '0012406N', '0002344N', '0014815N', '0000063N', '0000447N', '0004355N', '0015074N', '0000068N', '0000144N','0000684N', '0002454N', '0000039N', '0000131N', '0012925N', '0000305N', '0000125N', '0007004N', '0021517N')
--= '0004171N' 
and source_entry!='both-to'
order by ship_id, travel_rank;

comment on table navigoviz.ship is 'List of ships''descriptions with variantes for the name, flag, homeport, tonnage_class and class of ship';
comment on table navigoviz.captain is 'List of captains''descriptions with variantes for the name, citizenship, birthplace of captain';

comment on column navigoviz.ship.ship_id is 'unique identifier of ship set by Silvia who identifies the same ships through out various source documents';
comment on column navigoviz.ship.occurences_names is 'JSON array of various names of the same ship through sources, with the frequecy of appearance in documents';
comment on column navigoviz.ship.occurences_homeports_fr is 'JSON array of various homeports (standardized name in FR lang) of the same ship through sources, with the frequecy of appearance in documents';
comment on column navigoviz.ship.occurences_homeports_en is 'JSON array of various homeports (standardized name in EN lang) of the same ship through sources, with the frequecy of appearance in documents';
comment on column navigoviz.ship.occurences_flags_fr is 'JSON array of various flag (standardized name in FR lang) of the same ship through sources, with the frequecy of appearance in documents';
comment on column navigoviz.ship.occurences_flags_en is 'JSON array of various flag (standardized name in EN lang) of the same ship through sources, with the frequecy of appearance in documents';
comment on column navigoviz.ship.occurences_class is 'JSON array of various class types of the same ship through sources, with the frequecy of appearance in documents';
comment on column navigoviz.ship.occurences_tonnageclass is 'JSON array of various categories of tonnage of the same ship through sources, with the frequecy of appearance in documents';

comment on column navigoviz.captain.captain_id is 'unique identifier of captain set by Silvia who identifies the same captain through out various source documents';
comment on column navigoviz.captain.occurences_names is 'JSON array of various names of the same captain through sources, with the frequecy of appearance in documents';
comment on column navigoviz.captain.occurences_birthplaces is 'JSON array of various birthplace of the same captain through sources, with the frequecy of appearance in documents';
comment on column navigoviz.captain.occurences_citizenships is 'JSON array of various citizenship of the same captain through sources, with the frequecy of appearance in documents';

-- suite le 16 juin : ajout de ship_id, captain_id et min et max date pour Bernard
alter table navigoviz.ship add column captain_list json;
alter table navigoviz.ship add column mindate date;
alter table navigoviz.ship add column maxdate date;
alter table navigoviz.ship add column nb_sourcedoc int;
alter table navigoviz.captain add column ship_list json;
alter table navigoviz.captain add column mindate date;
alter table navigoviz.captain add column maxdate date;
alter table navigoviz.captain add column nb_sourcedoc int;

alter table navigoviz.ship alter column mindate type text;
alter table navigoviz.ship alter column maxdate type text;
-- alter table navigoviz.ship alter column mindate type date using mindate::date;
-- alter table navigoviz.ship alter column maxdate type date using maxdate::date;
-- alter table navigoviz.captain alter column mindate type date using mindate::date;
-- alter table navigoviz.captain alter column maxdate type date using maxdate::date;
alter table navigoviz.captain alter column mindate type text;
alter table navigoviz.captain alter column maxdate type text;


comment on column navigoviz.ship.captain_list is 'JSON array of various captains having sailed on the same ship through sources, with the frequecy of appearance in documents';
comment on column navigoviz.ship.mindate is 'First date of appearance of this ship in the sources';
comment on column navigoviz.ship.maxdate is 'Last date of appearance of this ship in the sources';
comment on column navigoviz.ship.nb_sourcedoc is 'Number of documents mentionning this ship in the sources';
comment on column navigoviz.captain.ship_list is 'JSON array of various ships on which this captain has been sailing through sources, with the frequecy of appearance in documents';
comment on column navigoviz.captain.mindate is 'First date of appearance of this captain in the sources';
comment on column navigoviz.captain.maxdate is 'Last date of appearance of this captain in the sources';
comment on column navigoviz.captain.nb_sourcedoc is 'Number of documents mentionning this captain in the sources';


update navigoviz.ship s set captain_list = k.captain_list
FROM
(SELECT t.ship_id, array_to_json(array_agg(json_build_object(t.captain_id ,t.occurences))) as captain_list
	FROM(
		Select ship_id, captain_id , count (distinct source_doc_id) as occurences
		FROM navigoviz.pointcall p
	WHERE p.ship_id in (select distinct ship_id from navigoviz.built_travels)
		GROUP BY ship_id, captain_id
		ORDER BY ship_id, occurences desc
		) t
	 WHERE occurences > 0 and captain_id is not null
	 GROUP BY t.ship_id
) as k
WHERE s.ship_id = k.ship_id;	
-- 11523
-- 12605 le 23 février 2023


update navigoviz.ship s set mindate = to_char(k.mindate, 'yyyy-mm-dd'), maxdate=to_char(k.maxdate, 'yyyy-mm-dd'), nb_sourcedoc=k.nb
from (
		Select ship_id, min(p.date_fixed) as mindate, max(p.date_fixed) as maxdate, count(distinct source_doc_id) as nb
		FROM navigoviz.pointcall p
		WHERE p.ship_id in (select distinct ship_id from navigoviz.built_travels)
		GROUP BY ship_id
) as k
WHERE s.ship_id = k.ship_id;
-- 11527
-- 12608 le 23 février 2023


update navigoviz.captain s set ship_list = k.ship_list
FROM
(SELECT t.captain_id, array_to_json(array_agg(json_build_object(t.ship_id ,t.occurences))) as ship_list
	FROM(
		Select captain_id, ship_id , count (distinct source_doc_id) as occurences
		FROM navigoviz.pointcall p
	WHERE p.captain_id in (select distinct captain_id from navigoviz.built_travels)
		GROUP BY captain_id, ship_id
		ORDER BY captain_id, occurences desc
		) t
	 WHERE occurences > 0 and ship_id is not null
	 GROUP BY t.captain_id
) as k
WHERE s.captain_id = k.captain_id;
-- 12210
-- 13153 le 23 février 2023

update navigoviz.captain s set mindate = to_char(k.mindate, 'yyyy-mm-dd'), maxdate=to_char(k.maxdate, 'yyyy-mm-dd'), nb_sourcedoc=k.nb
from (
		Select captain_id, min(p.date_fixed) as mindate, max(p.date_fixed) as maxdate, count(distinct source_doc_id) as nb
		FROM navigoviz.pointcall p
		WHERE p.captain_id in (select distinct captain_id from navigoviz.built_travels)
		GROUP BY captain_id
) as k
WHERE s.captain_id = k.captain_id;
-- 12210
-- 13153 le 23 février 2023

select * from navigoviz.ship where ship_id in ('0010348N', '0011092N')
select * from navigoviz.captain where captain_id in ('00014569')

-----------------------------------------------------------------------------------------
-- Reprise du travail sur maritime_paths_21fev2022
---------------------------------------------------------------------------------------------

-- 1. Enrichir navigoviz.pointcall avec nouveaux attributs utilisés par l'algo pour vérifier les A/Z
-- date_precise : (case when position('=' in coalesce(p.pointcall_out_date , p.pointcall_in_date)) > 0 then true else false end) as date_precise,
-- list_sup_uhgs_id : 
-- list_inf_uhgs_id : 
-- has_input_done : vrai si il y a des sources pour ce port dans le G5 (faux à l'étranger toujours, et vrai pour Marseille)
/* rajouter ces attributs dans les commentaires des tables pour l'API pointcall */
-- fait sur le serveur le 15 juin 2022
-- l'algo fabrique public.test_pointcall automatiquement à partir de navigoviz.pointcall si on ne spécifie pas de filter

-- 2. L'algo va créer public.pointcall_verifs à partir de public.test_pointcall qui est fabriqué à partir navigoviz.pointcall
-- 3. On demande ensuite la création de navigoviz.pointcall_checked (public.pointcall_heloise) sont la colonne net_route_marker est corrigée
    -- et qui surtout qualifie le niveau de certitude qu'on a qu'un pointcall ait été fréquenté ou pas
-- 4. On corrige la fabrique de built_travels dans l'ETL (heloise_historiens) par autojointure sur navigoviz.pointcall_checked
-- 5. On fait fabriquer à l'algo la table invalidated_travels dans l'ETL (heloise_algo) 
	 -- en calculant l'autojointure sur navigoviz.pointcall_checked en retenant seulement les troncons avec -5 (INFIRMED) à un bout ou l'autre
-- 6. On calcule les couleurs et le style de troncons de built_travels (pt de vue historien) et de invalidated_travels (pt de vue algo)
	-- on calcule aussi la trajectoire parcourue dans pointpath
-- 7. On fait l'union de ces 2 tables built_travels + invalidated_travels
    -- et on donne un identifiant à chaque tronçon pour fabriquer un geosjon plus facilement : row_number() over () as tid


alter table navigoviz.pointcall add column date_precise boolean;
update navigoviz.pointcall p set date_precise =  (case when position('=' in coalesce(p.pointcall_out_date , p.pointcall_in_date)) > 0 then true else false end) ;
COMMENT ON COLUMN navigoviz.pointcall.date_precise IS 'For algo : true indicates that the date of the pointcall is precisely known ';

alter table navigoviz.pointcall add column has_input_done boolean;
update navigoviz.pointcall p set has_input_done =  (case when p.state_1789_fr = 'France' then
                (case when p.source_suite != 'G5' then
                    true
                else
                    (case when extract(year from coalesce(p.outdate_fixed, p.indate_fixed)) = 1787 then
                        nb_conges_1787_inputdone is not null and p.nb_conges_1787_inputdone > 0
                    else p.nb_conges_1789_inputdone is not null and p.nb_conges_1789_inputdone > 0
                    end)
                end)
                else false
                end);
COMMENT ON COLUMN navigoviz.pointcall.has_input_done IS 'For algo : true indicates that there is sources (G5 registries) for this pointcall';

alter table navigoviz.pointcall add column list_sup_uhgs_id _varchar;
alter table navigoviz.pointcall add column list_inf_uhgs_id _varchar;

COMMENT ON COLUMN navigoviz.pointcall.list_sup_uhgs_id IS 'For algo : list of entities whom pointcall could geographically belong to';
COMMENT ON COLUMN navigoviz.pointcall.list_inf_uhgs_id IS 'For algo : list of entities that could geographically belong to this pointcall';

update navigoviz.pointcall p set list_sup_uhgs_id = k.list_sup_uhgs_id
from (select ughs_id, array_agg(ughs_id_sup) as list_sup_uhgs_id
            from ports.generiques_inclusions_geo_csv gigc
            group by ughs_id) as k where k.ughs_id = p.pointcall_uhgs_id;
update navigoviz.pointcall p set list_inf_uhgs_id = k.list_inf_uhgs_id
from (select ughs_id_sup, array_agg(ughs_id) as list_inf_uhgs_id
            from ports.generiques_inclusions_geo_csv gigc
            group by ughs_id_sup) as k where k.ughs_id_sup = p.pointcall_uhgs_id;    
            
           
select travel_id as step_id, source_text, outdate_fixed, indate_fixed, departure_uhgs_id, departure_fr, destination_uhgs_id, destination_fr,
ship_id, ship_name, tonnage_class, ship_flag_standardized_fr, homeport_toponyme_fr, 
captain_id , captain_name, birthplace, citizenship, travel_rank
from navigoviz.built_travels
where ship_id in ('0004171N', '0004155N', '0002931N')
-- ship_id in ( '0007498N', '0002931N', '0004155N', '0021517N', '0003951N', '0004171N', '0002537N', '0002350N', '0014650N', '0000011N', '0012406N', '0002344N', '0014815N', '0000063N', '0000447N', '0004355N', '0015074N', '0000068N', '0000144N','0000684N', '0002454N', '0000039N', '0000131N', '0012925N', '0000305N', '0000125N', '0007004N', '0021517N')
--= '0004171N' 
and source_entry!='both-to'
order by ship_id, travel_rank;

-- la bonne requete pour faire l'union des historiens et de l'algo

select row_number() over (PARTITION BY ship_id order by outdate_fixed asc, id asc, couleur desc) as travel_rank, * from (
select id, ship_id, source_depart as source_text, departure, destination, outdate_fixed, departure_uncertainity, destination_uncertainity, depart_net_route_marker, arrivee_net_route_marker, direct, couleur, st_makeline(pointpath) as geom 
from public.travel_heloise_historien where ship_id in ('0004171N', '0004155N', '0002931N')
union
select id, ship_id, source_depart as source_text, departure, destination, outdate_fixed, departure_uncertainity, destination_uncertainity, depart_net_route_marker, arrivee_net_route_marker, direct, couleur, st_makeline(pointpath) as geom 
from public.travel_heloise_algo where couleur = 'red' and ship_id in ('0004171N', '0004155N', '0002931N')
order by ship_id, outdate_fixed, id 
) as k

create table navigoviz.built_travels_backup17juin22 as (select * from navigoviz.built_travels)

update navigoviz.pointcall p set pointcall_uncertainity = -2  
from public.pointcall_checked c where p.pkid=c.pkid and certitude='Déclaré' 



create table navigoviz.uncertainity_travels as (
	select row_number() over (PARTITION BY ship_id order by outdate_fixed asc, id asc, destination_uncertainity asc) as travel_rank, * 
   	from 
        (
   		select * from public.uncertainity_travels_historien
    	union
        select * from public.uncertainity_travels_algo 
        where destination_uncertainity = -5 or departure_uncertainity = -5
        order by ship_id, outdate_fixed, id  
    	) as k
)
 
drop table navigoviz.routepaths
create table navigoviz.routepaths (
	from_uhgs_id text,
	from_toponyme_fr text, 
	to_uhgs_id text,
	to_toponyme_fr text, 
	m_offset double precision, 
	p_distance_ratio integer,
	pointpath geometry[],
	elapseTime float,
	nbPoints int
);
truncate navigoviz.routepaths;

select count(*) from navigoviz.uncertainity_travels --228
select  depart_pointcall_uhgs_id, departure, arrivee_pointcall_uhgs_id, destination, pointpath , array_length(pointpath, 1)
from navigoviz.uncertainity_travels where pointpath is not null --45

insert into navigoviz.routepaths (from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr, m_offset, p_distance_ratio, pointpath, nbPoints) 
select  depart_pointcall_uhgs_id, departure, arrivee_pointcall_uhgs_id, destination, 10000, 50000, pointpath , array_length(pointpath, 1)
from navigoviz.uncertainity_travels where pointpath is not null;
/*
insert into navigoviz.routepaths (from_uhgs_id, from_toponyme_fr, to_uhgs_id, to_toponyme_fr, pointpath, nbPoints) 
select  arrivee_pointcall_uhgs_id, destination, depart_pointcall_uhgs_id, departure,  pointpath , array_length(pointpath, 1)
from navigoviz.uncertainity_travels where pointpath is not null;
*/

ports.points_on_path(depart_pointcall_uhgs_id , arrivee_pointcall_uhgs_id, 10000, 50000) 
update navigoviz.routepaths set 
select t.*, r.pointpath 
from navigoviz.routepaths r, navigoviz.uncertainity_travels t 
where (from_uhgs_id = t.depart_pointcall_uhgs_id and to_uhgs_id = t.arrivee_pointcall_uhgs_id)
or (to_uhgs_id=t.depart_pointcall_uhgs_id and from_uhgs_id=t.arrivee_pointcall_uhgs_id)


update navigoviz.built_travels  set pointpath = r.pointpath ,  geom = st_makeline(r.pointpath)
            from navigoviz.routepaths r
            where (from_uhgs_id = departure_uhgs_id and to_uhgs_id=destination_uhgs_id)
            or (from_uhgs_id = destination_uhgs_id and to_uhgs_id=departure_uhgs_id)
            and r.pointpath is not null
-- 5858

select * from navigoviz.built_travels 
where pointpath is not null and ship_id = '0004155N'
order by travel_id 

select distinct ship_id from navigoviz.built_travels  where pointpath is not null

CREATE TYPE ports.portic_computedPath AS
(
    pointslist geometry[]      ,
    nbPoints  int,
    duration float,
    tampon float,
    distance float
);
