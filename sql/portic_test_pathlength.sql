create or replace function ports.portic_test_pathlength(p1_uhgsid text, p2_uhgsid text, m_offset double precision, p_distance_ratio integer) 
returns double precision  as 
$BODY$
/*
Calcule les points intermédiaires permettant de tracer des segments qui relient deux ports sans croiser de terre
Donner les identifiant UHGS des deux ports , un offset (5 km) pour l'éloignement à la côte, 
et une distance (10 km) pour l'espacement entre les points sur l'offset
https://trac.osgeo.org/postgis/wiki/UsersWikiExamplesInterpolateWithOffset
requires : 
CREATE TYPE ports.portic_computedPath AS
(
    pointslist geometry[]      ,
    nbPoints  int,
    duration float
);
*/
	declare m_seg geometry;
	declare p1 geometry;
	declare p2 geometry;
	declare f1 double precision;
	declare f2 double precision;
	declare longueur_segment double precision;
	declare ratio double precision;
	declare pointslist geometry[];
	declare test geometry[];
	declare nbPoints int;
	declare shoreid text;
	declare previousPoint geometry;
	declare distance_ratio double precision;
	DECLARE k int;
	declare f2prime double precision;
	
	
begin
	RAISE notice '###################################### % p1 % p2 %', now(), p1_uhgsid, p2_uhgsid ;
	nbPoints:=0;

	
	--p1.point3857
	select ports.order_intersection(coalesce(p1.port_on_line_1mile, p1.point3857), st_makeline(p1.port_on_line_1mile, p2.port_on_line_1mile)) into test
	from ports.port_points p1 , ports.port_points p2
	where   p1.uhgs_id = p1_uhgsid and p2.uhgs_id = p2_uhgsid ;

	
	if test[1] is null 
	then 
		--RAISE notice '% STOP here, direct',NOW() ;
		-- Mettre le premier point : le port de départ, mais à 1 mile de la cote (IMPORTANT)
		pointslist[0]:=  coalesce(port_on_line_1mile, point3857) from  ports.port_points p where   p.uhgs_id = p1_uhgsid ;
		-- Mettre le dernier point : le port d'arrivée, mais à 1 mile de la cote (IMPORTANT)
		pointslist[1]:=  coalesce(port_on_line_1mile, point3857) from  ports.port_points p where   p.uhgs_id = p2_uhgsid ;
		nbPoints:=2;
	
		longueur_segment:=st_length(st_makeline(pointslist[0],pointslist[1])) ;

		update navigoviz.routepaths set direct_length = longueur_segment, broader_length = null 
		where  from_uhgs_id = p1_uhgsid and to_uhgs_id = p2_uhgsid;
	
		return longueur_segment/1000;
	else 
		RAISE notice ' Test p1 % p2 %', st_asewkt(test[1]), st_asewkt(test[2]);
		-- Mettre le premier point : le port de départ
		pointslist[0]:=  coalesce(port_on_line_1mile, point3857)  from  ports.port_points p where   p.uhgs_id = p1_uhgsid ;
		p1:=pointslist[0]; 
		-- ajout le 14/03/2022 pour test Marennes-Honfleur avec 100 km
		previousPoint:= pointslist[0];
		nbPoints:= nbPoints+1;
	end if;

	select  coalesce(port_on_line_1mile, point3857) into p2 from  ports.port_points  where   uhgs_id = p2_uhgsid ;

	RAISE notice  'DECOUPE de p1 % p2 %',  st_asewkt(p1), st_asewkt(p2) ;

	distance_ratio:= p_distance_ratio;
	
	--k:= 2;
	k:= array_length(test, 1);
	m_seg:= NULL;
	while m_seg IS NULL AND k > 1 loop
		
		RAISE notice 'Grande boucle  test[1] % test[2] %',  st_asewkt(test[1]) , st_asewkt(test[k]);
		RAISE notice 'Distance k % entre test[1] et test[2] %', k, ST_Distance(test[1], test[k]);
		shoreid:=  s.id from navigoviz.shoreline s  where s.shoreline is not null and s.area > 1 
			and st_intersects(s.shoreline, st_buffer(st_setsrid(st_makeline(test[1], test[k]), 3857), 10)) 
			order by st_distance(test[1],ST_ClosestPoint(shoreline, test[1])) asc 
			limit 1;
		RAISE notice 'Identifiant du shoreline à contourner %', shoreid ;
		-- assertion : les deux points test partagent la même shoreline car deux points successifs appartiennent à la même shoreline qu'ils coupent par l'intérieur
		f1 :=  ST_LineLocatePoint(s.shoreline, test[1]) 
			from navigoviz.shoreline s 
			where s.id = shoreid;
		f2 :=  ST_LineLocatePoint(s.shoreline , test[k]) 
			from navigoviz.shoreline s 
			where s.id = shoreid;
		RAISE notice 'cas comme AVANT , f1 % f2 % f1-f2 = %', f1, f2, case when f1> f2 then f1-f2 else f2-f1 end ;
		f2prime :=  ST_LineLocatePoint(st_addpoint(s.shoreline, test[1], 0) , test[k]) 
			from navigoviz.shoreline s 
			where s.id = shoreid;
		--RAISE notice 'f1 % f2 %', f1, f2 ;
		RAISE notice 'CAS où test1 devient le début, f1 % f2 % f1-f2 = %', 0, f2prime, f2prime ;
		if (f2prime < Abs(f2-f1)) then 			
			-- parfois ST_OffsetCurve renvoie des polylines, ce qui ne marche pas. Donc on prend la plus petite.
			select g into m_seg from (
				select ST_NumGeometries(geom),  (st_dump(geom)).geom as g
				from (
				select ST_OffsetCurve(ST_LineSubstring(st_addpoint(s.shoreline, test[1], 0), 0, f2prime), m_offset) as geom
							from navigoviz.shoreline s 
							where s.id = shoreid
				) as k
				) as q
			--where st_intersects(g, test[1]) and st_intersects(g, test[2]);
			order by  st_length(g) asc limit 1; 	
			f1:=0;	
			f2:=f2prime;
		else
			select g into m_seg from (
				select ST_NumGeometries(geom),  (st_dump(geom)).geom as g
				from (
				select ST_OffsetCurve(ST_LineSubstring(s.shoreline,case when f1<f2 then f1 else f2 end, case when f1<f2 then f2 else f1 end), m_offset) as geom
							from navigoviz.shoreline s 
							where s.id = shoreid
				) as k
				) as q
			--where st_intersects(g, test[1]) and st_intersects(g, test[2]);
			order by  st_length(g) desc limit 1; --comme avant
		end if;
		longueur_segment:= st_length(m_seg);
		

	
		RAISE notice 'Longueur du segment en offset %', longueur_segment ;
		k:=k-1;
	END loop;
	
	update navigoviz.routepaths set direct_length=st_length(st_makeline(p1,p2)),  broader_length = longueur_segment 
	where  from_uhgs_id = p1_uhgsid and to_uhgs_id = p2_uhgsid;

	return longueur_segment/1000;
END;
$BODY$
language plpgsql;