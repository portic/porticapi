This API has been developped since 2020 inside the [ANR PORTIC research programme](http://anr.portic.fr) 2019-2023, ANR-18-CE-38-0010.

With the contribution of those persons : 

- Christine Plumejeaud-Perreau  - auteur - développeur  - 
  - christine.perreau@univ-poitiers.fr, HumanID : cplumejeaud
  - UMR 7266 LIENSS (2019-2021) puis UMR 7301 MIGRINTER (2021-2023)
- Géraldine Geoffroy - auteur - développeur , 
  - Geraldine.GEOFFROY@univ-cotedazur.fr, HumanID : ggeoffroy 
  - Service Commun de la Documentation, Université de Nice
- Bernard Pradines - contributeur - 
  - bernard.pradines@univ-poitiers.fr, HumanID : bpradines
  - UMR 7301 MIGRINTER 
