# 1. Portic data API : get directly data from database to the Web

*This is a Web API (developped with Flask) for delivering data extracted from a postgres database. 
Source code is on humanum GIT : [https://gitlab.huma-num.fr/portic/porticapi](https://gitlab.huma-num.fr/portic/porticapi), released under the [AGPL v3](docs/agpl-3.0.md) licence.
This API delivers data in either CSV or JSON format in a way that is useable for any software or anybody for further analysis or vizualisation task. All the fields are described in a [dictionnary](porticapi\static\data\api_portic.csv), in a CSV format, readable online on [http://apidemo.portic.fr](http://apidemo.portic.fr)
In the SQL directory, the file `porticapi\sql\portic_routepaths_allfunctions.sql` provides a way to compute maritime paths that never intersect coast neither land, from one port to the other. A publication is under review for this subject. The set of PL/SQL procedures requires the postgis extension to be installed to work with.  A preliminary publication has been done in [Eurocarto conference in Vienne, september 2022](https://www.abstr-int-cartogr-assoc.net/5/38/2022/ica-abs-5-38-2022.pdf).
*

Data content is an extraction of the [navigocorpus database](http://navigocorpus.huma-num.fr/), that has been cleaned and enriched during the [ANR PORTIC programm](http://anr.portic.fr) by new variables (geographic nomenclatures, product nomenclatures, uncertainty quotation of various fields) and standardisation and translation of some fields (such as the name of port, of flags, of shipclass, etc.). 
  
See [user manual](docs/usermanual.md) first for details

If anyone wish to install the postgres database, juste write to the authors to get a database dump to restore. All data preprocessing and programming stuff for cleaning data is archived at [https://gitlab.huma-num.fr/portic/navigocorpus](https://gitlab.huma-num.fr/portic/navigocorpus). 
Data will be saved in a repository (zenodo or nakala) as a plain text backup before the 15th september 2023. 

- [1. Portic data API : get directly data from database to the Web](#1-portic-data-api--get-directly-data-from-database-to-the-web)
- [2. Installation](#2-installation)
- [3. User documentation website (at root level)](#3-user-documentation-website-at-root-level)
- [4. Running without Apache localy](#4-running-without-apache-localy)
- [5. Running with Apache on the server](#5-running-with-apache-on-the-server)
- [6. Database installation](#6-database-installation)


The porticapi server software is copyrighted : © the [ANR Portic Project](https://anr.portic.fr/) 2019-2023. All rights reserved.  
The source code is released under the [AGPL v3](docs/agpl-3.0.md) licence.

# 2. Installation

Need Python 3 to be installed

Clone or download this git repo, then :

`pip3 install -r requirements.txt`


# 3. User documentation website (at root level)

Statically generated HTML from markdown source files by MkDocs. Install MkDocs :

`pip install mkdocs mkdocs[i18n]`

The configuration of this mkdocs documentation project is at the root of this repository, in the file : [mkdocs.yml](mkdocs.yml)

The docs are in markdown format (.md), all source files are in the **[docs](/docs)** sub-directory.

To see the results during development (when you write or update the documentation), run this command in the console :

`mkdocs serve`

This runs a webserver. Click on the link in the console to preview the documentation website in your browser. Each time you save a .md file, HTML output documentation is automatically updated, and visible in your browser.

At deployment time, on the server, build the documentation :

`mkdocs build`

HTML output is generated in the **[manual](/manual)** sub-directory.  
Documentation is then available online, at the root ('/') of the porticapi website : [http://data.portic.fr/](http://data.portic.fr/), thanks to the additional routing code in the [apidata.py](apidata.py) file :

``` python
# API documentation as statically generated website
@app.route('/')
def serve_static_root():
    return send_from_directory('manual', 'index.html')
@app.route('/<path:filename>')
def serve_static_manual(filename):
    return send_from_directory('manual', filename)
```

This code works, because of this option in the [mkdocs.yml](mkdocs.yml) file :

``` yaml
use_directory_urls: false
```


# 4. Running without Apache localy

`python apidata.py`

or create a virtualenv, then activate it and :

`pip install -r requirements && python apidata.py`

Backend is running and serve the data on [http://localhost:80](http://localhost:80) (change the **port** variable in [apidata.py](apidata.py) at the beginning)

```py
port = '80'
```

You can test here : 
http://localhost:80/api/test/

The answer is : 

```JSON
{"pandas version":"1.3.5","python version ":"3.7.10"}
```

# 5. Running with Apache on the server

```bash

cd /home/plumegeo/navigo/Viz/porticapi
git fetch origin master
git reset --hard origin/master
cd ..
cp porticapi/porticapi/apidata.py porticapi/porticapi/__init__.py

sudo chown :www-data /home/plumegeo/navigo/Viz/porticapi/ -R
sudo chmod 755 /home/plumegeo/navigo/Viz/porticapi/ -R
for fic in $(find /home/plumegeo/navigo/Viz/porticapi/porticapi -type f -name "*.py"); do sudo dos2unix $fic; done

sudo  service apache2 reload
```

You can test here : 
http://data.portic.fr/api/test/

The answer is : 

```JSON
{"pandas version":"1.3.5","python version ":"3.7.10"}
```

# 6. Database installation

You need to install a server postgres (v11 or above - currently v14) with some extensions : 
- postgis  (required)
- pg_trgm  (required) 
- postgis_topology (optional)
- fuzzystrmatch (optional)
- plpython3u (optional)

Roles (navigo, api_user, porticapi) have to be created before the restore of the database. 

In the API, the role 'api_user' with read only access on the database is used. Its password is 'portic'.

```SQL
CREATE ROLE navigo WITH CREATEDB LOGIN PASSWORD 'xxxxxxxxxxxxx';
GRANT mydba TO navigo;
GRANT dba TO navigo;

create role porticapi;
create role api_user;
ALTER USER porticapi WITH PASSWORD 'portic';
ALTER USER api_user WITH PASSWORD 'portic';
```

Commands to create and restore the database content : 

```sh

createdb --encoding=UTF8  --owner=navigo portic_v9

psql -U postgres -d portic_v9 -c "create extension postgis; create extension postgis_topology; create extension fuzzystrmatch; create extension pg_trgm; create extension plpython3u;"

# example of command to restore from a CUSTOM backup
pg_restore -U postgres -d portic_v9 /path/to/portic_V9_23fevrier2023.sql
```

The name of the database (*portic_v9* in the example), and the port (5432) can be changed in [apidata.py](apidata.py) at the beginning. 


```py
postgresport = '5432'
database = 'portic_v9'
```

We use a *psycopg2* connexion for querying the database, through a pandas method
- pandas.read_sql_query() doc : https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_sql_query.html

